﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.KPIProformanceManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.KPIProformanceManage
{
    public class KPIPlanInfoYearBiz :BaseDataManageBiz
    {
        private const string SQL = @"SELECT [SEQ]
                                          ,[Company_SEQ]
                                          ,[CompanyName]
                                          ,[MonthNum1]
                                          ,[MonthNum2]
                                          ,[MonthNum3]
                                          ,[MonthNum4]
                                          ,[MonthNum5]
                                          ,[MonthNum6]
                                          ,[MonthNum7]
                                          ,[MonthNum8]
                                          ,[MonthNum9]
                                          ,[MonthNum10]
                                          ,[MonthNum11]
                                          ,[MonthNum12]
                                          ,[CreateTime]
                                      FROM [TicketManage].[dbo].[GX_PlanInfoYear] 
                                      where 1=1 ";

        private const string PersonSQL = @"SELECT [SEQ]
                                          ,[Company_SEQ]
                                          ,[CompanyName]
                                          ,[UserSEQ]
                                          ,[UserName]
                                          ,[MonthNum1]
                                          ,[MonthNum2]
                                          ,[MonthNum3]
                                          ,[MonthNum4]
                                          ,[MonthNum5]
                                          ,[MonthNum6]
                                          ,[MonthNum7]
                                          ,[MonthNum8]
                                          ,[MonthNum9]
                                          ,[MonthNum10]
                                          ,[MonthNum11]
                                          ,[MonthNum12]
                                          ,[CreateTime]
                                      FROM [TicketManage].[dbo].[GX_PlanPersonInfo]
                                      where 1=1 ";

        private const string ProfitSQL = @"SELECT [SEQ]
                                          ,[Company_SEQ]
                                          ,[CompanyName]
                                          ,[MonthNum1]
                                          ,[MonthNum2]
                                          ,[MonthNum3]
                                          ,[MonthNum4]
                                          ,[MonthNum5]
                                          ,[MonthNum6]
                                          ,[MonthNum7]
                                          ,[MonthNum8]
                                          ,[MonthNum9]
                                          ,[MonthNum10]
                                          ,[MonthNum11]
                                          ,[MonthNum12]
                                          ,[CreateTime]
                                      FROM [TicketManage].[dbo].[GX_PlanprofitInfoYear]
                                      where 1=1 ";
        #region  全年计划录入
        public List<GX_PlanInfoYear> GetPlanInfoYearList(GX_PlanInfoYearListPageModel model)
        {
            var finalSQL = new StringBuilder(SQL);
            if (model.PageModel != null)
            {
                finalSQL = finalSQL.AppendFormat("and CreateTime>='{0}'", ((DateTime)model.PageModel.CreateTime).ToString("yyyy-MM-dd hh:mm:ss"));
            }

            model.PageNum = DBContext.Database.SqlQuery<GX_PlanInfoYear>(finalSQL.ToString()).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var modelList = DBContext.Database.SqlQuery<GX_PlanInfoYear>(finalSQL.ToString())
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }
        /// <summary>
        /// 查询 全年计划
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public GX_PlanInfoYear GetPlanInfoYear(GX_PlanInfoYearListPageModel model)
        {
            try
            {
                var entry = DBContext.GX_PlanInfoYear.SingleOrDefault(x => x.SEQ == model.PageModel.SEQ);
                return entry;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 新增/修改 全年计划
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool AddPlanInfoYear(GX_PlanInfoYearListPageModel model) 
        {
            try
            {
                var entry = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0) 
                {
                   
                    model.PageModel.CreateTime = DateTime.Now;
                    entry.State = EntityState.Added;
                }
                else
                {
                    entry.State = EntityState.Modified;
                }
                int count =  DBContext.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }
        /// <summary>
        /// 删除 全年计划
        /// </summary>
        /// <param name="SEQ"></param>
        /// <returns></returns>
        public bool DeletePlanInfoYear(int SEQ)
        {
            try
            {
               var entry = DBContext.GX_PlanInfoYear.SingleOrDefault(X => X.SEQ == SEQ);
               DBContext.GX_PlanInfoYear.Remove(entry);
               DBContext.SaveChanges();
               return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region  个人计划

        /// <summary>
        /// 查询 利润计划
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_PlanPersonInfo> GetPlanPersonInfoList(GX_PlanPersonInfoListPageModel model,int useSEQ) 
        {
            var finalSQL = new StringBuilder(PersonSQL);
            finalSQL = finalSQL.AppendFormat("and UserSEQ ='{0}'", useSEQ);
            if (model.PageModel != null)
            {
                finalSQL = finalSQL.AppendFormat("and CreateTime>='{0}'", ((DateTime)model.PageModel.CreateTime).ToString("yyyy-MM-dd hh:mm:ss"));
            }
            model.PageNum = DBContext.Database.SqlQuery<GX_PlanPersonInfo>(finalSQL.ToString()).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var modelList = DBContext.Database.SqlQuery<GX_PlanPersonInfo>(finalSQL.ToString())
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 新增/修改 个人计划
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool AddPlanInfoPerson(GX_PlanPersonInfoListPageModel model) 
        {
            try
            {
                var entry = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0)
                {

                    model.PageModel.CreateTime = DateTime.Now;
                    entry.State = EntityState.Added;
                }
                else
                {
                    entry.State = EntityState.Modified;
                }
                int count = DBContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeletePlanInfoPserson(int SEQ)
        {
            try
            {
                var entry = DBContext.GX_PlanPersonInfo.SingleOrDefault(X => X.SEQ == SEQ);
                DBContext.GX_PlanPersonInfo.Remove(entry);
                DBContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public GX_PlanPersonInfo GetPlanInfoPerson(GX_PlanPersonInfoListPageModel model)
        {
            try
            {
                var entry = DBContext.GX_PlanPersonInfo.SingleOrDefault(x => x.SEQ == model.PageModel.SEQ);
                return entry;
            }
            catch
            {
                return null;
            }
        }


        #endregion

        #region 考核利润
        public List<GX_PlanprofitInfoYear> GetPlanprofitInfoList(GX_PlanprofitInfoYearListPageModel model)
        {
            var finalSQL = new StringBuilder(ProfitSQL);
            if (model.PageModel != null)
            {
                finalSQL = finalSQL.AppendFormat("and CreateTime>='{0}'", ((DateTime)model.PageModel.CreateTime).ToString("yyyy-MM-dd hh:mm:ss"));
            }

            model.PageNum = DBContext.Database.SqlQuery<GX_PlanprofitInfoYear>(finalSQL.ToString()).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var modelList = DBContext.Database.SqlQuery<GX_PlanprofitInfoYear>(finalSQL.ToString())
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }
        /// <summary>
        /// 查询 全年计划
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public GX_PlanprofitInfoYear GetPlanprofitInfo(GX_PlanprofitInfoYearListPageModel model)
        {
            try
            {
                var entry = DBContext.GX_PlanprofitInfoYear.SingleOrDefault(x => x.SEQ == model.PageModel.SEQ);
                return entry;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 新增/修改 全年计划
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool AddPlanprofitInfo(GX_PlanprofitInfoYearListPageModel model)
        {
            try
            {
                var entry = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0)
                {

                    model.PageModel.CreateTime = DateTime.Now;
                    entry.State = EntityState.Added;
                }
                else
                {
                    entry.State = EntityState.Modified;
                }
                int count = DBContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// 删除 全年计划
        /// </summary>
        /// <param name="SEQ"></param>
        /// <returns></returns>
        public bool DeletePlanprofitInfo(int SEQ)
        {
            try
            {
                var entry = DBContext.GX_PlanprofitInfoYear.SingleOrDefault(X => X.SEQ == SEQ);
                DBContext.GX_PlanprofitInfoYear.Remove(entry);
                DBContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion
    }
}
