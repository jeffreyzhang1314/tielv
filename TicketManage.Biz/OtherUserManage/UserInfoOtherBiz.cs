﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Entity.OtherUserManage;
using TicketManage.Repository.Models;
using TicketManage.Tools;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.OtherUserManage
{
    public class UserInfoOtherBiz : BaseDataManageBiz
    {

        private const string SQL = @"SELECT * FROM UserInfo_Other WHERE UserNo<>'admin'  ";

        /// <summary>
        /// 修改用户密码
        /// </summary>
        /// <param name="model">用户实体</param>
        /// <returns>True修改成功False修改失败</returns>
        public Boolean ChangePassWord(UserLoginPageModel model)
        {
            var flag = false;
            var tempModel = DBContext.UserInfo_Other.Where(a => a.UserNo == model.PageModel.UserNo).FirstOrDefault();
            if (tempModel != null)
            {
                var tools = new MD5Helper();
                if (tools.EncryptCode(model.OldPassword) == tempModel.UserPassword)
                {
                    tempModel.UserPassword = tools.EncryptCode(model.ConfirmPassword);
                    DBContext.SaveChanges();
                    model.Message = "修改密码成功";
                    model.MessageState = true;
                    flag = true;
                }
                else
                {
                    model.Message = "旧密码不正确！";
                    model.MessageState = false;
                    flag = false;
                }
            }
            return flag;
        }


        /// <summary>
        /// 获取指定序号的实体
        /// </summary>
        /// <param name="seq">实体序号</param>
        /// <returns></returns>
        public UserInfo_Other GetUserInfoOtherBySEQ(int seq)
        {
            var model = DBContext.UserInfo_Other.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }

        /// <summary>
        /// 根据条件查询用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<UserInfo_Other> GetUserInfoOtherListByCondition(UserInfoOtherPageModel model)
        {
            var sqlContent = new StringBuilder(SQL);
            if (model.PageModel != null)
            {
                if (!string.IsNullOrEmpty(model.PageModel.UserNo))
                {
                    sqlContent.AppendFormat(" and UserNo='{0}'", model.PageModel.UserNo);
                }
                if (!string.IsNullOrEmpty(model.PageModel.UserName))
                {
                    sqlContent.AppendFormat(" and  UserName='{0}'", model.PageModel.UserName);
                }
                if (!string.IsNullOrEmpty(model.PageModel.ContactNo))
                {
                    sqlContent.AppendFormat(" and ContactNo='{0}'", model.PageModel.ContactNo);
                }
            }
            var modelList = DBContext.Database.SqlQuery<UserInfo_Other>(sqlContent.ToString()).ToList();
            model.PageNum = modelList.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderBy(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 创建其他系统用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean CreateUserInfoOther(UserInfoOtherPageModel model)
        {
            var flag = false;
            try
            {
                DBContext.UserInfo_Other.Add(model.PageModel);
                model.MessageState = true;
                model.Message = "用户添加成功！";
            }
            catch (Exception ex)
            {
                model.MessageState = true;
                model.Message = ex.Message;
            }
            return flag;
        }

        /// <summary>
        /// 删除其他系统用户
        /// </summary>
        /// <param name="seq">用户序号</param>
        /// <returns></returns>
        public Boolean DeleteUserInfoOther(int seq)
        {
            var flag = false;
            try
            {
                var tempModel = DBContext.UserInfo_Other.Where(a => a.SEQ == seq).FirstOrDefault();
                if (tempModel != null)
                {
                    DBContext.UserInfo_Other.Remove(tempModel);
                    DBContext.SaveChanges();
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return flag;
        }

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditUserInfoOther(UserInfoOtherPageModel model)
        {
            try
            {
                //如果添加用户存在，需要将权限累加
                if (model.PageModel.SEQ == 0)
                {
                    var tempModel = DBContext.UserInfo_Other.Where(a => a.UserNo == model.PageModel.UserNo).FirstOrDefault();
                    if (tempModel != null)
                    {
                        model.MessageState = false;
                        model.Message = "用户名重复无法添加";
                        return false;
                    }
                    else
                    {
                        var tools = new MD5Helper();
                        model.PageModel.UserPassword = tools.EncryptCode("000000");
                        model.PageModel.UserState = true;
                        model.PageModel.CreateTime = DateTime.Now;
                        model.PageModel.CompanySEQ = model.PageModel.CompanySEQ;
                        model.PageModel.CompanyName = model.PageModel.CompanyName;
                        var entry = DBContext.Entry(model.PageModel);
                        entry.State = EntityState.Added;
                        DBContext.SaveChanges();
                        model.MessageState = true;
                        model.Message = "添加成功";
                        return true;
                    }

                }
                else
                {
                    var tempModel = DBContext.UserInfo_Other.Where(a => a.UserNo == model.PageModel.UserNo).FirstOrDefault();
                    if (tempModel != null && (tempModel.AuthNum & model.PageModel.AuthNum) == 0)
                    {
                        if ((tempModel.AuthNum & model.PageModel.AuthNum) != 0)
                        {
                            tempModel.AuthNum -= model.PageModel.AuthNum;
                        }
                        tempModel.AuthNum += model.PageModel.AuthNum;
                    }
                    if (tempModel != null)
                    {
                        tempModel.PassportNo = model.PageModel.PassportNo;
                        tempModel.Sex = model.PageModel.Sex;
                        tempModel.ContactNo = model.PageModel.ContactNo;
                        tempModel.Age = model.PageModel.Age;
                        tempModel.UserName = model.PageModel.UserName;
                        tempModel.CompanySEQ = model.PageModel.CompanySEQ;
                        tempModel.CompanyName = model.PageModel.CompanyName;
                        DBContext.SaveChanges();
                        model.MessageState = true;
                        model.Message = "修改成功";
                        return true;
                    }
                    else
                    {
                        model.MessageState = false;
                        model.Message = "未找到实体对象";
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 重置用户密码
        /// </summary>
        /// <param name="seq">用户序号</param>
        /// <returns>True重置成功False重置失败</returns>
        public Boolean ResetPasswordByUserInfoSEQ(int seq)
        {
            var tools = new MD5Helper();
            var model = DBContext.UserInfo_Other.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                model.UserPassword = tools.EncryptCode("000000");
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 获取国旅集团下全部分公司信息
        /// </summary>
        /// <returns></returns>
        public List<CompanyInfo> GetCompanyInfo()
        {
            var tempList = DBContext.CompanyInfoes.Where(a => a.GroupSEQ == 1).ToList();
            return tempList;
        }

    }
}
