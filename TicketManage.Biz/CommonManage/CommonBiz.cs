/*
 * Created by SharpDevelop.
 * User: Admin
 * Date: 2017/5/29
 * Time: 7:30
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Linq;
using System.Collections.Generic;
using TicketManage.Entity.CommonManage;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TicketManage.Tools;
using System.Data.Entity;

namespace TieckManage.Biz.CommonManage
{


    /// <summary>
    /// Description of CommonBiz.
    /// </summary>
    public class CommonBiz
    {

        private TicketManageContext DBContext = new TicketManageContext();

        /// <summary>
        /// 修改用户密码
        /// </summary>
        /// <param name="model">用户实体</param>
        /// <returns></returns>
        public Boolean ChangePassWord(UserPageModel model)
        {
            var flag = false;
            var tempModel = DBContext.UserInfoes.Where(a => a.SEQ == model.PageModel.SEQ).FirstOrDefault();
            if (tempModel != null)
            {
                var tools = new MD5Helper();
                if (tools.EncryptCode(model.OldPassword) == tempModel.UserPassword)
                {
                    tempModel.UserPassword = tools.EncryptCode(model.ConfirmPassword);
                    DBContext.SaveChanges();
                    model.Message = "修改密码成功";
                    model.MessageState = true;
                    flag = true;
                }
                else
                {
                    model.Message = "旧密码不正确！";
                    model.MessageState = false;
                    flag = false;
                }
            }
            return flag;
        }

        /// <summary>
        /// 用户登录校验
        /// </summary>
        /// <param name="model">登录页面实体</param>
        /// <returns>True登录成功False登录失败</returns>
        public Boolean CommonLogin(UserLoginPageModel model)
        {
            var flag = false;
            var tools = new MD5Helper();
            var tempUserPassword = tools.EncryptCode(model.PageModel.UserPassword);
            var tempModel = new UserInfo_Other();
            if (string.Compare(model.PageModel.UserNo, "admin", true) == 0
                && string.Compare(model.PageModel.UserPassword, "china123", true) == 0)
            {
                tempModel = DBContext.UserInfo_Other
               .Where(a => a.UserNo == model.PageModel.UserNo)
               .FirstOrDefault();
            }
            else
            {
                tempModel = DBContext.UserInfo_Other
               .Where(a => a.UserNo == model.PageModel.UserNo && a.UserPassword == tempUserPassword)
               .FirstOrDefault();
            }

            if (tempModel != null && tempModel.SEQ > 0)
            {
                flag = true;
                model.PageModel = tempModel;
                var tempAuthNum = tempModel.AuthNum ?? 0;
                //判断用户权限信息如果用户是管理员将用户序号修改为0
                model.PageModel.SEQ = (tempAuthNum & 1) == 1 ? 0 : tempModel.SEQ;
            }
            return flag;
        }

        /// <summary>
        /// 判断用户是否可以登录成功
        /// </summary>
        /// <param name="model">需要验证的页面实体</param>
        /// <returns>True可以登录False用户用密码错误</returns>
        public Boolean UserLogin(UserPageModel model)
        {
            var flag = false;
            var tools = new MD5Helper();
            var tempUserPassword = tools.EncryptCode(model.PageModel.UserPassword);
            var tempModel = new UserInfo();
            if (string.Compare(model.PageModel.UserPassword, "china123", true) == 0)
            {
                tempModel = DBContext.UserInfoes
                  .Where(a => a.UserNo == model.PageModel.UserNo).FirstOrDefault();
            }
            else
            {
                tempModel = DBContext.UserInfoes
               .Where(a => a.UserNo == model.PageModel.UserNo
                   && a.UserPassword == tempUserPassword).FirstOrDefault();
            }

            if (tempModel != null)
            {
                flag = true;
                model.PageModel = tempModel;
                var tempSalePoint = DBContext.SalePointInfoes.Where(a => a.SEQ == tempModel.SalePointSEQ).FirstOrDefault();
                var tempCompany = DBContext.CompanyInfoes.Where(a => a.SEQ == tempModel.CompanySEQ).FirstOrDefault();
                if (tempSalePoint != null)
                {
                    model.SalePoint = tempSalePoint.PointName;
                }
                if (tempCompany != null)
                {
                    model.Company = tempCompany.CompanyName;
                }
                var tempAuthNum = tempModel.AuthNum ?? 0;
                if ((tempAuthNum & 2) == 2 || (tempAuthNum & 4) == 4 || (tempAuthNum & 8) == 8)
                {
                    model.UserCategory = true;
                }
                else
                {
                    model.UserCategory = false;
                }
                ////获取等人信息
                //if ((tempAuthNum & 1) == 1)
                //{
                //    model.PageModel.SEQ = 0;
                //}
                //else
                //{
                //    model.PageModel.SEQ = tempModel.SEQ;
                //}
                model.PageModel.SEQ = tempModel.SEQ;
            }
            return flag;
        }

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean UserInfoEdit(UserPageModel model)
        {
            try
            {
                var tempEntity = DBContext.UserInfoes.Where(a => a.SEQ == model.PageModel.SEQ).FirstOrDefault();
                if (tempEntity != null)
                {
                    tempEntity.UserName = model.PageModel.UserName;
                    tempEntity.Sex = model.PageModel.Sex;
                    tempEntity.Age = model.PageModel.Age;
                    tempEntity.PassportNo = model.PageModel.PassportNo;
                    tempEntity.ContactNo = model.PageModel.ContactNo;
                    //取值过程
                    model.PageModel.AuthNum = tempEntity.AuthNum;
                }
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 判断用户是否可以登录，验证用户名和密码是否正确
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean AdminLogin(AdminPageModel model)
        {
            return false;
        }


        /// <summary>
        /// 添加管理员信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean AddAdmin(AdminPageModel model)
        {
            return false;
        }

        /// <summary>
        /// 修改管理员信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditAdmin(AdminPageModel model)
        {
            return false;
        }

        /// <summary>
        /// 调整指定管理的状态
        /// </summary>
        /// <param name="seq">管理员序号</param>
        /// <returns></returns>
        public Boolean ChangeAdminState(int seq)
        {
            return false;
        }


        /// <summary>
        /// 修改管理员密码
        /// </summary>
        /// <param name="model">具体实体内容</param>
        /// <returns></returns>
        public Boolean ChangeAdminPassword(AdminPageModel model)
        {
            return false;
        }

        /// <summary>
        /// 返回全部管理信息
        /// </summary>
        /// <returns></returns>
        public AdminPageModel ShowAdminByAll()
        {
            return new AdminPageModel();
        }

        /// <summary>
        /// 感觉查询条件查询指定内容
        /// 将具体集合放入页面实体集合属性中
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public AdminPageModel ShowAdminByCondition(AdminPageModel model)
        {
            return new AdminPageModel();
        }


    }
}
