﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.TravelLocalAgencyManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.TravelLocalAgencyManage
{
    /// <summary>
    /// 地接社评分业务类
    /// </summary>
    public class TravelAgencyPointBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 添加地接社评分详细信息
        /// </summary>
        /// <param name="model">评分详细页面实体</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean AddTravelAgencyPoint(TravelAgencyPointPageModel model)
        {
            try
            {
                DBContext.TravelAgencyPoints.Add(model.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除地接社评分详细
        /// </summary>
        /// <param name="seq">地接社评分序号</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean DelTravelAgencyPoint(int seq)
        {
            var model = DBContext.TravelAgencyPoints.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                UpdateTravelAgencyPoint(model.AgencySEQ, -(model.PointNum));
                DBContext.TravelAgencyPoints.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 编辑部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditTravelAgencyPoint(TravelAgencyPointPageModel model)
        {
            try
            {
                if (model.PageModel.SEQ != 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "修改成功";
                    return true;
                }
                else if (model.PageModel.SEQ == 0)
                {
                    model.PageModel.CreateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    UpdateTravelAgencyPoint(model.PageModel.AgencySEQ, model.PageModel.PointNum);
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Added;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "未找到实体对象";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 根据条件查询部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<TravelAgencyPoint> SearchTravelAgencyPointByCondition(TravelAgencyPointPageModel model)
        {
            var SQL = new StringBuilder("select * from TravelAgencyPoint where 1=1 ");
            if (model.PageModel.AgencySEQ != null)
            {
                SQL.AppendFormat("and AgencySEQ={0}", model.PageModel.AgencySEQ);
            }
            var modelList = DBContext.Database.SqlQuery<TravelAgencyPoint>(SQL.ToString()).ToList();
            model.PageNum = modelList.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }



        /// <summary>
        /// 获取制定序号的用户信息
        /// </summary>
        /// <param name="seq">用户序号信息</param>
        /// <returns></returns>
        public TravelAgencyPoint GetTravelAgencyPointBySEQ(int seq)
        {
            var model = DBContext.TravelAgencyPoints.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }

        /// <summary>
        /// 旅行社评分更新
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean UpdateTravelAgencyPoint(Nullable<int> seq, Nullable<decimal> pointNum)
        {
            var model = DBContext.TravelLocalAgencies.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                model.Point += pointNum;
                return true;
            }
            return false;
        }


    }
}
