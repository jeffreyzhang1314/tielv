using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using TicketManage.Entity.TravelLocalAgencyManage;
using TicketManage.Repository.Models;
using TicketManage.Repository.Models.ExtModels;
using TieckManage.Biz.TicketManage;



namespace TieckManage.Biz.TravelLocalAgencyManage
{
    public class TravelLocalAgencyBiz : BaseDataManageBiz
    {

        private string TRAVELAGENCYSQL = @"select a.TravelNo as AgencyNo,a.TravelName as AgencyName ,a.TravelAddress as [Address],Contacts as Contact
                                          ,ContactPhone as Telephone
                                          ,b.RoomPirce,b.RoomSummary,b.FoodPirce,b.FoodSummary,b.CarPirce,b.CarSummary
                                          from [TicketManage].[dbo].[TravelLocalAgency] as a
                                          left join (
	                                        select 
                                                TravelAgencySEQ
		                                        ,max(case plugInCategory when 1 then PlugInPirce else null end)  as 'RoomPirce'
		                                        ,max(case plugInCategory when 1 then Summary else null end)  as 'RoomSummary'
		                                        ,max(case plugInCategory when 2 then PlugInPirce else null end)  as 'FoodPirce'
		                                        ,max(case plugInCategory when 2 then Summary else null end)  as 'FoodSummary'
		                                        ,max(case plugInCategory when 3 then PlugInPirce else null end)  as 'CarPirce'
		                                        ,max(case plugInCategory when 3 then Summary else null end)  as 'CarSummary'
	                                          from [TicketManage].[dbo].[TravelAgencyPlugIn]
	                                          group by TravelAgencySEQ
                                          ) as b
                                          on a.SEQ=b.TravelAgencySEQ";

        private string TravelAgencySQL = @"select a.SEQ,a.TravelAreaCodeName,a.TravelAgencyName,a.RecordDate,a.AgencyBaseMoney,a.ContactInfo,a.TicketGate,a.BuyItem,a.StoreName
                                                        ,max(case b.PlugInCategory when '1' then b.PlugInPirce end ) as RoomPirce
                                                        ,max(case b.PlugInCategory when '1' then b.Summary end ) as RoomSummary
                                                        ,max(case b.PlugInCategory when '2' then b.PlugInPirce end ) as FoodPirce
                                                        ,max(case b.PlugInCategory when '2' then b.Summary end ) as FoodSummary
                                                        ,max(case b.PlugInCategory when '3' then b.PlugInPirce end ) as CarPirce
                                                        ,max(case b.PlugInCategory when '3' then b.Summary end ) as CarSummary
                                                        from TravelAgency as a
                                                        left join TravelAgencyPlugIn as b
                                                        on a.SEQ=b.TravelAgencySEQ
                                                        group by a.SEQ,a.TravelAreaCodeName,a.TravelAgencyName,a.RecordDate,a.AgencyBaseMoney,a.ContactInfo,a.TicketGate,a.BuyItem,a.StoreName Order by a.SEQ desc";

        /// <summary>
        /// 导出内容数据
        /// </summary>
        /// <returns></returns>
        public List<TravelAgencyExtPageModel> ShowExportTravelAgency()
        {
            var tempList = DBContext.Database.SqlQuery<TravelAgencyExtPageModel>(TravelAgencySQL).ToList();
            return tempList;
        }

        public List<TravelLocalAgencyExtPageModel> ShowAgencyInfo(string agencyName)
        {
            var tempList = new List<TravelLocalAgencyExtPageModel>();
            if (!string.IsNullOrEmpty(agencyName))
            {
                var tempSQL = string.Format("{0} where TravelName='{1}' ", TRAVELAGENCYSQL, agencyName);
                tempList.AddRange(DBContext.Database.SqlQuery<TravelLocalAgencyExtPageModel>(TRAVELAGENCYSQL));
            }
            else
            {
                tempList.AddRange(DBContext.Database.SqlQuery<TravelLocalAgencyExtPageModel>(TRAVELAGENCYSQL));
            }
            return tempList;
        }

        public Boolean MeetingRecordDel(int seq)
        {
            var pageModel = DBContext.MeetingInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
            if (pageModel != null)
            {
                DBContext.MeetingInfoes.Remove(pageModel);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public MeetingInfo GetMeetingRecordBySEQ(int seq)
        {
            var tempModel = DBContext.MeetingInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
            return tempModel;
        }

        public Boolean MeetingRecordEdit(MeetingInfoPageModel model)
        {
            var entry = DBContext.Entry(model.PageModel);
            if (model.PageModel.SEQ == 0)
            {
                entry.State = EntityState.Added;
            }
            else
            {
                entry.State = EntityState.Modified;
            }
            DBContext.SaveChanges();
            return true;
        }

        public Boolean TravelLocalAgencyEdit(TravelLocalAgencyPageModel model)
        {
            var entry = DBContext.Entry(model.PageModel);
            if (model.PageModel.SEQ == 0)
            {
                entry.State = EntityState.Added;
            }
            else
            {
                entry.State = EntityState.Modified;
            }
            DBContext.SaveChanges();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public TravelLocalAgency GetTravelLocalAgencyBySEQ(int seq)
        {
            var model = DBContext.TravelLocalAgencies.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }

        /// <summary>
        /// 删除指定序号数据
        /// </summary>
        /// <param name="seq">序号 </param>
        /// <returns></returns>
        public Boolean TravelLocalAgencyDel(int seq)
        {
            var pageModel = DBContext.TravelLocalAgencies.Where(a => a.SEQ == seq).FirstOrDefault();
            if (pageModel != null)
            {
                DBContext.TravelLocalAgencies.Remove(pageModel);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 查询全部地接社信息
        /// </summary>
        /// <returns></returns>
        public List<TravelLocalAgency> GetTravelLocalAgencyByAll(TravelLocalAgencyPageModel model)
        {
            var modelList = new List<TravelLocalAgency>();
            model.PageNum = DBContext.TravelLocalAgencies.Count();
            if (model.PageNum > 0)
            {
                model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
                modelList.AddRange(DBContext.TravelLocalAgencies
                    .OrderByDescending(a => a.CreateTime)
                    .Skip(model.PageSize * (model.CurrentPage - 1))
                   .Take(model.PageSize));
            }
            return modelList;
        }

        /// <summary>
        /// 根据条件查询地接社信息
        /// </summary>
        /// <param name="model">查询条件实体</param>
        /// <returns></returns>
        public List<TravelLocalAgency> GetTravelLocalAgencyByCondition(TravelLocalAgencyPageModel model)
        {
            var modelList = DBContext.TravelLocalAgencies.ToList();
            if (!string.IsNullOrEmpty(model.PageModel.TravelName))
            {
                modelList = modelList.Where(a => a.TravelName != null && a.TravelName.Contains(model.PageModel.TravelName)).ToList();
            }
            if (!string.IsNullOrEmpty(model.PageModel.CompanyName))
            {
                modelList = modelList.Where(a => a.CompanyName != null && a.CompanyName.Contains(model.PageModel.CompanyName)).ToList();
            }
            if (!string.IsNullOrEmpty(model.PageModel.Contacts))
            {
                modelList = modelList.Where(a => a.Contacts != null && a.Contacts.Contains(model.PageModel.Contacts)).ToList();
            }
            if (!string.IsNullOrEmpty(model.PageModel.ContactPhone))
            {
                modelList = modelList.Where(a => a.ContactPhone != null && a.ContactPhone.Contains(model.PageModel.ContactPhone)).ToList();
            }
            if (!string.IsNullOrEmpty(model.PageModel.CategoryBiz) && string.Compare(model.PageModel.CategoryBiz, "0", true) != 0)
            {
                modelList = modelList.Where(a => a.CategoryBiz != null && a.CategoryBiz == model.PageModel.CategoryBiz).ToList();
            }
            model.PageNum = modelList.Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }


        public List<TravelAgency> GetTravelAgencyByAll(TravelAgencyPageModel model)
        {
            var modelList = new List<TravelAgency>();
            model.PageNum = DBContext.TravelAgencies.Count();
            if (model.PageNum > 0)
            {
                model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
                modelList.AddRange(DBContext.TravelAgencies
                .OrderByDescending(a => a.SEQ)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize));
            }
            else
            {
                model.PageTotal = 0;
            }
            return modelList;
        }

        /// <summary>
        /// 查询地接社对比
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<TravelAgency> GetTravelAgencyByCondition(TravelAgencyPageModel model)
        {
            var modelList = new List<TravelAgency>();
            var SQL = new StringBuilder("select * From TravelAgency where 1=1 ");
            if (!string.IsNullOrEmpty(model.PageModel.TravelAreaCodeName))
            {
                SQL.AppendFormat(" and TravelAreaCodeName like '%{0}%' ", model.PageModel.TravelAreaCodeName);
            }
            if (!string.IsNullOrEmpty(model.RecordDateSearch))
            {
                SQL.AppendFormat(" and SUBSTRING( CONVERT(nvarchar, RecordDate,120),1,4)='{0}' ", model.RecordDateSearch);
            }
            if (!string.IsNullOrEmpty(model.PageModel.TravelAgencyName))
            {
                SQL.AppendFormat(" and TravelAgencyName like '%{0}%' ", model.PageModel.TravelAgencyName);
            }

            model.PageNum = DBContext.TravelAgencies.SqlQuery(SQL.ToString()).Count();
            if (model.PageNum > 0)
            {
                model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
                modelList.AddRange(DBContext.TravelAgencies.SqlQuery(SQL.ToString())
                .OrderByDescending(a => a.SEQ)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize));
            }
            else
            {
                model.PageTotal = 0;
            }
            return modelList;
        }

        public TravelAgency GetTravelAgencyBySEQ(int seq)
        {
            var tempModel = DBContext.TravelAgencies.Where(a => a.SEQ == seq).FirstOrDefault();
            return tempModel;
        }

        public Boolean TravelAgencyDel(int seq)
        {
            var tempModel = GetTravelAgencyBySEQ(seq);
            if (tempModel != null)
            {
                DBContext.TravelAgencies.Remove(tempModel);
                DBContext.SaveChanges();
                return true;
            }
            return false;
        }

        public Boolean TravelAgencyEdit(TravelAgencyPageModel model)
        {
            var tempModel = DBContext.Entry(model.PageModel);
            if (model.PageModel.SEQ != 0)
            {
                tempModel.State = EntityState.Modified;
            }
            else
            {
                tempModel.State = EntityState.Added;
            }
            DBContext.SaveChanges();
            SaveTravelAgencyPlugIn(model);
            return true;
        }

        public void SaveTravelAgencyPlugIn(TravelAgencyPageModel model)
        {
            foreach (var item in model.PluginList)
            {
                var tempModel = DBContext.Entry(item);
                item.TravelAgencySEQ = model.PageModel.SEQ;
                if (item.SEQ != 0)
                {
                    var tempObj = DBContext.TravelAgencyPlugIns.Where(a => a.SEQ == item.SEQ).FirstOrDefault();
                    if (!string.IsNullOrEmpty(item.FileContent))
                    {
                        tempObj.FileContent = item.FileContent;
                    }
                    tempObj.PlugInCategory = item.PlugInCategory;
                    tempObj.PlugInPirce = item.PlugInPirce;
                    tempObj.Summary = item.Summary;
                }
                else
                {
                    tempModel.State = EntityState.Added;
                }
            }
            DBContext.SaveChanges();
        }


        /// <summary>
        /// 查询会议内容
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<MeetingInfo> GetMeetingInfoByCondition(MeetingInfoPageModel model)
        {
            var modelList = DBContext.MeetingInfoes.ToList();

            if (model.PageModel != null)
            {
                if (!string.IsNullOrEmpty(model.PageModel.MeetingTitle))
                {
                    modelList = modelList.Where(a => a.MeetingTitle == model.PageModel.MeetingTitle).ToList();
                }
                if (!string.IsNullOrEmpty(model.PageModel.MeetingContent))
                {
                    modelList = modelList.Where(a => a.MeetingContent == model.PageModel.MeetingContent).ToList();
                }
            }
            model.PageNum = modelList.Count();
            if (model.PageNum > 0)
            {
                model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
                modelList = modelList.OrderByDescending(a => a.SEQ).Skip(model.PageSize * (model.CurrentPage - 1)).Take(model.PageSize).ToList();
            }
            else
            {
                model.PageTotal = 0;
            }
            return modelList;
        }


        /// <summary>
        /// 查询指定编号下的全部配套信息
        /// </summary>
        /// <param name="seqList">旅行社集合</param>
        /// <returns></returns>
        public List<TravelAgencyPlugIn> GetTravelAreaPlugInByTravelAreaSEQ(List<int> seqList)
        {
            var modelList = DBContext.TravelAgencyPlugIns.Where(a => seqList.Contains(a.TravelAgencySEQ ?? 0)).ToList();
            return modelList;
        }

        /// <summary>
        /// 删除地接社配套信息
        /// </summary>
        /// <param name="seq">配套信息序号</param>
        /// <returns></returns>
        public Boolean TravelAgencyPlugInsDel(int seq)
        {
            var model = DBContext.TravelAgencyPlugIns.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.TravelAgencyPlugIns.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 获取旅游地点数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<SystemCode> GetTravelAreaByCondition(TravelAreaListPageModel model)
        {
            var modelList = DBContext.SystemCodes.Where(a => a.CategoryNo == "TravelArea").ToList();
            if (model.PageModel != null)
            {
                if (!string.IsNullOrEmpty(model.PageModel.CodeName))
                {
                    modelList = modelList.Where(a => a.CodeName == model.PageModel.CodeName).ToList();
                }
            }
            model.PageNum = modelList.Count();
            if (model.PageNum > 0)
            {
                model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
                modelList = modelList.OrderByDescending(a => a.SEQ).Skip(model.PageSize * (model.CurrentPage - 1)).Take(model.PageSize).ToList();
            }
            else
            {
                model.PageTotal = 0;
            }
            return modelList;
        }

        /// <summary>
        /// 旅游景点查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<SystemCode> GetTravelAreaPointByCondition(TravelAreaListPageModel model)
        {
            var travelAreaSEQ = Convert.ToString(model.TraveAreaSEQ);
            var modelList = DBContext.SystemCodes.Where(a => a.CategoryNo == "TravelAreaPoint" && a.ParentCodeNo == travelAreaSEQ).ToList();
            if (model.PageModel != null)
            {
                if (!string.IsNullOrEmpty(model.PageModel.CodeName))
                {
                    modelList = modelList.Where(a => a.CodeName == model.PageModel.CodeName).ToList();
                }
            }
            model.PageNum = modelList.Count();
            if (model.PageNum > 0)
            {
                model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
                modelList = modelList.OrderByDescending(a => a.SEQ).Skip(model.PageSize * (model.CurrentPage - 1)).Take(model.PageSize).ToList();
            }
            else
            {
                model.PageTotal = 0;
            }
            return modelList;
        }

        public SystemCode GetTravelAreaBySEQ(int seq)
        {
            var tempModel = DBContext.SystemCodes.Where(a => a.SEQ == seq).FirstOrDefault();
            return tempModel;
        }

        public Boolean TravelAreaDel(int seq)
        {
            var tempModel = GetTravelAreaBySEQ(seq);
            if (tempModel != null)
            {
                DBContext.SystemCodes.Remove(tempModel);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean TravelAreaEdit(TravelAreaListPageModel model)
        {
            var tempModel = DBContext.Entry(model.PageModel);
            if (model.PageModel.SEQ != 0)
            {
                model.PageModel.CategoryName = "旅游地段";
                model.PageModel.CategoryNo = "TravelArea";
                model.PageModel.CodeNo = model.PageModel.CodeName;
                model.PageModel.IsParent = true;
                tempModel.State = EntityState.Modified;
            }
            else
            {
                model.PageModel.CategoryName = "旅游地段";
                model.PageModel.CategoryNo = "TravelArea";
                model.PageModel.CodeNo = model.PageModel.CodeName;
                model.PageModel.IsParent = true;
                tempModel.State = EntityState.Added;
            }
            DBContext.SaveChanges();
            return true;
        }

        public Boolean TravelAreaPointEdit(TravelAreaListPageModel model)
        {
            var tempModel = DBContext.Entry(model.PageModel);
            if (model.PageModel.SEQ != 0)
            {
                model.PageModel.CategoryName = "旅游景点";
                model.PageModel.CategoryNo = "TravelAreaPoint";
                model.PageModel.CodeNo = model.PageModel.CodeName;
                model.PageModel.IsParent = false;
                model.PageModel.ParentCodeNo = Convert.ToString(model.TraveAreaSEQ);
                tempModel.State = EntityState.Modified;
            }
            else
            {
                model.PageModel.CategoryName = "旅游景点";
                model.PageModel.CategoryNo = "TravelAreaPoint";
                model.PageModel.CodeNo = model.PageModel.CodeName;
                model.PageModel.IsParent = false;
                model.PageModel.ParentCodeNo = Convert.ToString(model.TraveAreaSEQ);
                tempModel.State = EntityState.Added;
            }
            DBContext.SaveChanges();
            return true;
        }

    }
}
