using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.TicketLaSaManage
{
    public class TicketLaSaBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 返回拉萨票列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        public List<TicketLaSa> GetTicketLaSaByCondition(TicketLaSaPageModel pageModel)
        {
            var SQLCONTEXT = new StringBuilder("select * from TicketLaSa where 1=1 ");
            if (pageModel != null)
            {
                if (!string.IsNullOrEmpty(pageModel.StartDate))
                {
                    SQLCONTEXT.Append(" and CreateTime>='" + pageModel.StartDate + "'");
                }
                if (!string.IsNullOrEmpty(pageModel.EndDate))
                {
                    SQLCONTEXT.Append(" and CreateTime<='" + pageModel.EndDate + "'");
                }

            }
            if (pageModel.OperatorUserNoList != null && pageModel.OperatorUserNoList.Count > 0)
            {
                SQLCONTEXT.Append(" and OperatorSEQ in (" + ConvertStringListToContext(pageModel.OperatorUserNoList) + ")");
            }
            var modelList = DBContext.Database
                    .SqlQuery<TicketLaSa>(SQLCONTEXT.ToString())
                    .OrderBy(a => a.CreateTime)
                    .ToList();
            return modelList;
        }
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SaveInfo(TicketLaSaPageModel model)
        {
            Boolean result = false;
            try
            {
                var entry = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ != 0)
                {
                    model.PageModel1.SEQ = model.PageModel.SEQ + 1;
                }

                var entry1 = DBContext.Entry(model.PageModel1);
                if (model.PageModel.SEQ == 0)
                {
                    entry.State = EntityState.Added;
                    entry1.State = EntityState.Added;
                }
                else
                {

                    entry.State = EntityState.Modified;
                    entry1.State = EntityState.Modified;
                }
                DBContext.SaveChanges();
                result = true;

            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean DeleteInfo(int seq)
        {
            Boolean result = false;
            try
            {
                TicketLaSa model = DBContext.TicketLaSas.Where(p => p.SEQ == seq).FirstOrDefault();
                TicketLaSa model1 = DBContext.TicketLaSas.Where(p => p.SEQ == seq + 1).FirstOrDefault();
                var entry = DBContext.Entry(model);
                entry.State = EntityState.Deleted;
                var entry1 = DBContext.Entry(model1);
                entry1.State = EntityState.Deleted;
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }
        /// <summary>
        /// 返回团队票信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public TicketLaSa GetTickLasaInfo(int seq)
        {
            TicketLaSa info = new TicketLaSa();
            info = DBContext.TicketLaSas.Where(s => s.SEQ == seq).FirstOrDefault();
            return info;
        }
    }
}
