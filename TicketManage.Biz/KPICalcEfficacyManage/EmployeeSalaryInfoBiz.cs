﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.KPICalcEfficacyManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.KPICalcEfficacyManage
{
    /// <summary>
    /// 外雇用人员工资计算业务操作类
    /// </summary>
    public class EmployeeSalaryInfoBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 外雇用人员工资信息
        /// </summary>
        /// <param name="model">外雇用人员页面实体</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean AddEmployeeSalaryInfo(EmployeeSalaryInfoPageModel model)
        {
            try
            {
                DBContext.GX_EmployeeSalaryInfo.Add(model.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除部门信息
        /// </summary>
        /// <param name="seq">部门序号</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean DelEmployeeSalaryInfo(int seq)
        {
            var model = DBContext.GX_PostionInfo.Where(a => a.seq == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.GX_PostionInfo.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 编辑部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditEmployeeSalaryInfo(EmployeeSalaryInfoPageModel model)
        {
            try
            {
                if (model.PageModel.SEQ != 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "修改成功";
                    return true;
                }
                else if (model.PageModel.SEQ == 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Added;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "未找到实体对象";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        /// <summary>
        /// 根据条件查询部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_EmployeeSalaryInfo> SearchEmployeeSalaryInfoByCondition(EmployeeSalaryInfoPageModel model)
        {
            var SQL = new StringBuilder("select * from GX_EmployeeSalaryInfo where 1=1 ");
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.ItemName))
            {
                SQL.AppendFormat("and ItemName='{0}'", model.PageModel.ItemName);
            }
            var modelList = DBContext.Database.SqlQuery<GX_EmployeeSalaryInfo>(SQL.ToString()).ToList();
            model.PageNum = modelList.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderBy(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 获取岗位信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public GX_EmployeeSalaryInfo GetEmployeeSalaryInfoBySEQ(int seq)
        {
            var model = DBContext.GX_EmployeeSalaryInfo.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }
    }
}
