﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.AdvertisementManage;
using TicketManage.Repository.Models;
using TicketManage.Repository.Models.ExtModels;
using TieckManage.Biz.OtherUserManage;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.AdvertisementManage
{
    public class AdvertisementBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 用户信息业务类
        /// </summary>
        private UserInfoOtherBiz biz = new UserInfoOtherBiz();

        #region 预算分劈业务

        /// <summary>
        /// 获取国旅集团分公司
        /// </summary>
        /// <returns></returns>
        public List<KeyValueModel> GetCompanySource()
        {
            return biz.GetCompanyInfoByGroupSEQ(1);
        }

        /// <summary>
        /// 添加分劈预算
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean BudgetSplitAdd(BudgetSplitModel model)
        {
            try
            {
                model.PageModel.CreateTime = DateTime.Now;
                var dbModel = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0)
                {
                    dbModel.State = EntityState.Added;
                }
                else
                {
                    dbModel.State = EntityState.Modified;
                }
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除指定序号分劈预算数据
        /// </summary>
        /// <param name="seq">序号</param>
        /// <returns></returns>
        public Boolean BudgetSplitDeleted(int seq)
        {
            var tempModel = DBContext.BudgetSplitInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
            if (tempModel != null)
            {
                DBContext.BudgetSplitInfoes.Remove(tempModel);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 根据序号查询实体
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public BudgetSplitInfo BudgetSplitSearchById(int seq)
        {
            var tempModel = DBContext.BudgetSplitInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
            return tempModel;
        }

        /// <summary>
        /// 返回所有预算信息
        /// </summary>
        /// <returns></returns>
        public List<BudgetSplitInfo> BudgetSplitSearchByAll()
        {
            var tempList = DBContext.BudgetSplitInfoes.ToList();
            return tempList;
        }

        /// <summary>
        /// 根据查询条件返回结果
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<BudgetSplitInfo> BudgetSplitSearchByCondition(BudgetSplitModel model)
        {
            var SQL = @"select * from BudgetSplitInfo where 1=1 ";
            var ExcelSQL = new StringBuilder(SQL);
            if (model.PageModel.BudgetYear != null)
            {
                ExcelSQL.AppendFormat("and BudgetYear={0} ", model.PageModel.BudgetYear);
            }
            if (model.PageModel.CompanySEQ != null && model.PageModel.CompanySEQ != 0)
            {
                ExcelSQL.AppendFormat("and CompanySEQ={0} ", model.PageModel.CompanySEQ);
            }
            model.PageNum = DBContext.Database.SqlQuery<BudgetSplitInfo>(ExcelSQL.ToString()).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var tempList = DBContext.Database.SqlQuery<BudgetSplitInfo>(ExcelSQL.ToString())
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
                .Take(model.PageSize)
                .ToList();
            return tempList;
        }

        #endregion

        #region 预算支出业务

        private string BudgetApplySQL = @"SELECT A.[SEQ],A.[CompanyName],A.[CategoryName],A.[BudgetYear],A.[BudgetMoney],A.[CreateTime],sum(ISNULL(b.ApplyMoney,0)) as BudgetApplyMoney,(select SUM(BudgetMoney) from [TicketManage].[dbo].[BudgetAppend] where BudgetSplitSEQ=A.[SEQ]) as BudgetAppendMoney
                                                           FROM [TicketManage].[dbo].[BudgetSplitInfo] as A
                                                           left join [TicketManage].[dbo].[ApplyBudget] as b
                                                           on A.seq=b.budgetseq
                                                           where 1=1 ";

        private string BudgetApplyDetailSQL = @"Select a.SEQ,b.BudgetYear,b.CompanyName,b.CategoryName,b.budgetMoney,a.budgetName,a.ApplyMoney,a.CurrentBudgetBalance,a.CreateTime
														   from [TicketManage].[dbo].[ApplyBudget] as a
														   left join [TicketManage].[dbo].[BudgetSplitInfo] as b
														   on a.BudgetSEQ=b.SEQ 
                                                           where 1=1 and b.SEQ  is not null";

        public Boolean BudgetApplySave(BudgetManageModel model)
        {
            return false;
        }

        public List<BudgetSplitInfoPageModel> BudgetApplySearchByAll()
        {
            var tempList = DBContext.Database.SqlQuery<BudgetSplitInfoPageModel>(BudgetApplySQL).ToList();
            return tempList;
        }

        public List<BudgetSplitInfoPageModel> BudgetApplySearchByCondition(BudgetManageModel model)
        {
            var ExcelSQL = new StringBuilder(BudgetApplySQL);
            if (model.PageModel.BudgetYear != null)
            {
                ExcelSQL.AppendFormat(" and BudgetYear= '{0}' ", model.PageModel.BudgetYear);
            }
            if (!string.IsNullOrEmpty(model.PageModel.CompanyName) && model.PageModel.CompanyName != "全部")
            {
                ExcelSQL.AppendFormat(" and CompanyName like '%{0}%' ", model.PageModel.CompanyName);
            }
            if (!string.IsNullOrEmpty(model.PageModel.CategoryName) && model.PageModel.CategoryName != "全部")
            {
                ExcelSQL.AppendFormat(" and CategoryName like '%{0}%' ", model.PageModel.CategoryName);
            }
            ExcelSQL.Append(" group by A.[SEQ],A.[CompanyName],A.[CategoryName],A.[BudgetYear],A.[BudgetMoney],A.[CreateTime]");
            model.PageNum = DBContext.Database.SqlQuery<BudgetSplitInfoPageModel>(ExcelSQL.ToString()).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var tempList = DBContext.Database.SqlQuery<BudgetSplitInfoPageModel>(ExcelSQL.ToString())
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
                .Take(model.PageSize)
                .ToList();
            return tempList;
        }

        /// <summary>
        /// 保存分劈预算的申请
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean BudgetApplySave(BudgetManageShowModel model)
        {
            try
            {
                var tempApplyMoneyNum = DBContext.ApplyBudgets.Where(a => a.BudgetSEQ == model.PageModel.BudgetSEQ).Sum(a => a.ApplyMoney);
                var tempBudgetMoneyNum = DBContext.BudgetSplitInfoes.Where(a => a.SEQ == model.PageModel.BudgetSEQ).Sum(a => a.BudgetMoney);
                var tempBudgetAppendMoneyNum = DBContext.BudgetAppends.Where(a => a.BudgetSplitSEQ == model.PageModel.BudgetSEQ).Sum(a => a.BudgetMoney);
                model.PageModel.CurrentBudgetBalance = (tempBudgetMoneyNum ?? 0) + (tempBudgetAppendMoneyNum ?? 0) - (tempApplyMoneyNum ?? 0) - model.PageModel.ApplyMoney;
                if (model.PageModel.CurrentBudgetBalance < 0)
                {
                    model.Message = "剩余预算不足~！";
                    return false;
                }
                else if (model.PageModel.ApplyMoney <= 0)
                {
                    model.Message = "领用预算不能为零~！";
                    return false;
                }
                else
                {
                    DBContext.ApplyBudgets.Add(model.PageModel);
                    DBContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 添加追加预算数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean BudgetAppendSave(BudgetAppendModel model)
        {
            try
            {
                model.PageModel.CreateTime = DateTime.Now;
                DBContext.BudgetAppends.Add(model.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 返回指定分劈预算的追加预算信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public BudgetAppend BudgetAppendSeachByBudgetSplitSEQ(int seq)
        {
            var tempModel = DBContext.BudgetAppends.Where(a => a.BudgetSplitSEQ == seq).FirstOrDefault();
            return tempModel;
        }

        /// <summary>
        /// 删除追加预算信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Boolean BudgetAppendDeleted(int id)
        {
            try
            {
                var tempModel = DBContext.BudgetAppends.Where(a => a.BudgetSplitSEQ == id).FirstOrDefault();
                if (tempModel != null)
                {
                    DBContext.BudgetAppends.Remove(tempModel);
                    DBContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ApplyBudgetPageModel> ApplyBudgetSearchByCondition(BudgetDetailModel model)
        {
            var ExcelSQL = new StringBuilder(BudgetApplyDetailSQL);
            if (model.PageModel != null && model.PageModel.BudgetSEQ != null && model.PageModel.BudgetSEQ > 0)
            {
                ExcelSQL.AppendFormat(" and BudgetSEQ={0}", model.PageModel.BudgetSEQ);
            }
            model.PageNum = DBContext.Database.SqlQuery<ApplyBudgetPageModel>(ExcelSQL.ToString()).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var tempList = DBContext.Database.SqlQuery<ApplyBudgetPageModel>(ExcelSQL.ToString())
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
                .Take(model.PageSize)
                .ToList();
            return tempList;
        }

        /// <summary>
        /// 获取上传图片详细
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public List<BudgetExpenditure> BudgetExpenditureListByBudgetSplitSEQ(int seq)
        {
            var tempList = DBContext.BudgetExpenditures.Where(a => a.BudgetSplitSEQ == seq).ToList();
            return tempList;
        }

        /// <summary>
        /// 删除预算内容
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean BudgetExpenditureDel(int seq)
        {
            var Flag = false;
            var tempModel = DBContext.ApplyBudgets.Where(a => a.SEQ == seq).FirstOrDefault();
            if (tempModel != null)
            {
                DBContext.ApplyBudgets.Remove(tempModel);
                DBContext.SaveChanges();
                Flag = true;
            }
            return Flag;
        }

        #endregion



    }
}
