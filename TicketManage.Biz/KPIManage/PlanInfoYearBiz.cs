﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TieckManage.Biz.TicketManage;
using TicketManage.Entity.KPIManage;
using System.Data;
using TicketManage.Repository.Models;

namespace TieckManage.Biz.KPIManage
{
    /// <summary>
    /// 全年计划业务类
    /// </summary>
    public class PlanInfoYearBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 添加全年计划信息
        /// </summary>
        /// <param name="model">全年计划页面实体</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean AddPlanInfoYear(PlanInfoYearListPageModel model)
        {
            try
            {
                DBContext.GX_PlanInfoYear.Add(model.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除部门信息
        /// </summary>
        /// <param name="seq">部门序号</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean DelPlanInfoYear(int seq)
        {
            var model = DBContext.GX_PlanInfoYear.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.GX_PlanInfoYear.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 编辑部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditPlanInfoYear(PlanInfoYearListPageModel model)
        {
            try
            {
                if (model.PageModel.SEQ != 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "修改成功";
                    return true;
                }
                else if (model.PageModel.SEQ == 0)
                {
                    model.PageModel.CreateTime = DateTime.Now;
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Added;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "未找到实体对象";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 添加部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SavePlanInfoYear(PlanInfoYearListPageModel model)
        {
            try
            {
                if (model.PageModel.SEQ == 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "实体对象已经纯在";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 根据条件查询全年计划信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_PlanInfoYear> SearchPlanInfoYearByCondition(PlanInfoYearListPageModel model)
        {
            var SQL = new StringBuilder("select * from GX_PlanInfoYear where 1=1 ");
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.CompanyName))
            {
                SQL.AppendFormat("and CompanyName='{0}'", model.PageModel.CompanyName);
            }
            var modelList = DBContext.Database.SqlQuery<GX_PlanInfoYear>(SQL.ToString()).ToList();
            model.PageNum = modelList.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderBy(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 获取岗位信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public GX_PlanInfoYear GetPlanInfoYearBySEQ(int seq)
        {
            var model = DBContext.GX_PlanInfoYear.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }
    }
}
