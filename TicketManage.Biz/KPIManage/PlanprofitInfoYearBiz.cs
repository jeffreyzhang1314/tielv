﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TieckManage.Biz.TicketManage;
using TicketManage.Entity.KPIManage;
using System.Data;
using TicketManage.Repository.Models;

namespace TieckManage.Biz.KPIManage
{
    /// <summary>
    /// 考核利润业务类
    /// </summary>
    public class PlanprofitInfoYearBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 添加考核利润信息
        /// </summary>
        /// <param name="model">考核利润页面实体</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean AddPlanprofitInfoYear(PlanprofitInfoYearPageModel model)
        {
            try
            {
                DBContext.GX_PlanprofitInfoYear.Add(model.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除考核利润信息
        /// </summary>
        /// <param name="seq">考核利润序号</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean DelPlanprofitInfoYear(int seq)
        {
            var model = DBContext.GX_PlanprofitInfoYear.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.GX_PlanprofitInfoYear.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 编辑考核利润信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditPlanprofitInfoYear(PlanprofitInfoYearPageModel model)
        {
            try
            {
                if (model.PageModel.SEQ != 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "修改成功";
                    return true;
                }
                else if (model.PageModel.SEQ == 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Added;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "未找到实体对象";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 添加考核利润
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SavePlanprofitInfoYear(PlanprofitInfoYearPageModel model)
        {
            try
            {
                if (model.PageModel.SEQ == 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "实体对象已经纯在";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 根据条件查询考核利润信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_PlanprofitInfoYear> SearchPlanprofitInfoYearByCondition(PlanprofitInfoYearPageModel model)
        {
            var SQL = new StringBuilder("select * from GX_PlanprofitInfoYear where 1=1 ");
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.CompanyName))
            {
                SQL.AppendFormat("and CompanyName='{0}'", model.PageModel.CompanyName);
            }
            var modelList = DBContext.Database.SqlQuery<GX_PlanprofitInfoYear>(SQL.ToString()).ToList();
            model.PageNum = modelList.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderBy(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 根据序号查询考核利润信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public GX_PlanprofitInfoYear GetPlanprofitInfoYearBySEQ(int seq)
        {
            var model = DBContext.GX_PlanprofitInfoYear.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }
    }
}
