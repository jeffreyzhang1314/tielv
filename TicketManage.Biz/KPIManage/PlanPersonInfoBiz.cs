﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TieckManage.Biz.TicketManage;
using TicketManage.Entity.KPIManage;
using System.Data;
using TicketManage.Repository.Models;
using TicketManage.Entity.CommonManage;

namespace TieckManage.Biz.KPIManage
{
    /// <summary>
    /// 个人计划业务类
    /// </summary>
    public class PlanPersonInfoBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 添加个人计划信息
        /// </summary>
        /// <param name="model">个人计划页面实体</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean AddPlanPersonInfo(PlanPersonInfoPageModel model)
        {
            try
            {
                DBContext.GX_PlanPersonInfo.Add(model.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除个人计划信息
        /// </summary>
        /// <param name="seq">个人计划序号</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean DelPlanPersonInfo(int seq)
        {
            var model = DBContext.GX_PlanPersonInfo.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.GX_PlanPersonInfo.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 对指定序号个人计划进行审核
        /// </summary>
        /// <param name="seq">个人计划序号</param>
        /// <param name="approveState">审批流程节点</param>
        /// <param name="approveResult">审批状态：0待审批1通过2驳回</param>
        /// <returns></returns>
        public Boolean ApprovePlanPerson(int seq, ApproveFlowNode approveState, int approveResult)
        {
            var flag = false;
            if (seq > 0 && approveResult > 0 && approveResult < 3)
            {
                var model = DBContext.GX_PlanPersonInfo.Where(a => a.SEQ == seq).FirstOrDefault();
                if (model != null)
                {
                    switch (approveState)
                    {
                        case ApproveFlowNode.个人计划一级审批:
                            model.Approve1 = approveResult;
                            break;
                        case ApproveFlowNode.个人计划二级审批:
                            model.Approve2 = approveResult;
                            if (approveResult == 2)
                            {
                                model.Approve1 = 0;
                            }
                            break;
                        case ApproveFlowNode.个人计划三级审批:
                            model.Approve3 = approveResult;
                            if (approveResult == 2)
                            {
                                model.Approve1 = 0;
                                model.Approve2 = 0;
                            }
                            break;
                        case ApproveFlowNode.个人计划四级审批:
                            model.Approve4 = approveResult;
                            if (approveResult == 2)
                            {
                                model.Approve1 = 0;
                                model.Approve2 = 0;
                                model.Approve3 = 0;
                            }
                            break;
                    }
                }
                DBContext.SaveChanges();
            }
            return flag;
        }

        /// <summary>
        /// 编辑个人计划信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditPlanPersonInfo(PlanPersonInfoPageModel model)
        {
            try
            {
                if (model.PageModel.SEQ != 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "修改成功";
                    return true;
                }
                else if (model.PageModel.SEQ == 0)
                {
                    model.PageModel.Approve1 = 0;
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Added;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "未找到实体对象";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 添加考核利润
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SavePlanPersonInfo(PlanPersonInfoPageModel model)
        {
            try
            {
                if (model.PageModel.SEQ == 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "实体对象已经存在！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 根据条件查询考核利润信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_PlanPersonInfo> SearchPlanPersonInfoByCondition(PlanPersonInfoPageModel model)
        {
            var SQL = new StringBuilder("select * from GX_PlanPersonInfo where 1=1 ");
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.CompanyName))
            {
                SQL.AppendFormat("and CompanyName='{0}'", model.PageModel.CompanyName);
            }
            if (model.OperatorAuthNum != null && (model.OperatorAuthNum & 128) == 128)
            {
                var companyUserList = GetUserNoByCompanySEQ(model.CompanySEQ);
                SQL.AppendFormat("and UserSEQ in ({0})", ConvertStringListToContext(companyUserList));
            }
            else if (model.OperatorAuthNum != null && ((model.OperatorAuthNum & 256) == 256
                && (model.OperatorAuthNum & 512) == 512
                && (model.OperatorAuthNum & 1024) == 1024))
            {
                var companyUserList = GetUserNoByGroupSEQ(model.GroupSEQ);
                SQL.AppendFormat("and UserSEQ in ({0})", ConvertStringListToContext(companyUserList));
            }
            var modelList = DBContext.Database.SqlQuery<GX_PlanPersonInfo>(SQL.ToString()).ToList();
            model.PageNum = modelList.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderBy(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 获取人员计划信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public GX_PlanPersonInfo GetPlanPersonInfoBySEQ(int seq)
        {
            var model = DBContext.GX_PlanPersonInfo.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }

        /// <summary>
        /// 获取全部职务信息
        /// </summary>
        /// <returns></returns>
        public List<GX_DutyInfo> GetDutyInfo()
        {
            var modelList = DBContext.GX_DutyInfo.ToList();
            return modelList;
        }
    }
}
