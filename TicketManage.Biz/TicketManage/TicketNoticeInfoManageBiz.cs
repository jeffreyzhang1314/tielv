using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;

namespace TieckManage.Biz.TicketManage
{
    public class TicketNoticeInfoManageBiz : BaseDataManageBiz
    {

        /// <summary>
        /// 获取通知公告表全部数据
        /// </summary>
        /// <returns></returns>
        public List<Ticket_NoticeInfo> GetTicket_NoticeInfoListByAll()
        {
            List<Ticket_NoticeInfo> companyList = new List<Ticket_NoticeInfo>();
            companyList = DBContext.Ticket_NoticeInfo.ToList();
            return companyList;
        }

        public List<Ticket_NoticeInfo> GetTicket_NoticeInfoListByCatgory(string Catgory)
        {
            List<Ticket_NoticeInfo> companyList = new List<Ticket_NoticeInfo>();
            if (Catgory == null)
            {
                companyList = DBContext.Ticket_NoticeInfo.ToList();
            }
            else
            {
                companyList = DBContext.Ticket_NoticeInfo.Where(p => p.Category == Catgory).ToList();
            }
            return companyList;
        }

        /// <summary>
        /// 保存通知/公告信息(添加/修改)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SaveTicket_NoticeInfo(Ticket_NoticoPageModel model)
        {
            Boolean result = false;
            try
            {

                var entry = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0)
                {
                    entry.State = EntityState.Added;
                }
                else
                {
                    entry.State = EntityState.Modified;
                }
                DBContext.SaveChanges();
                result = true;

            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 删除通知/公告信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean DeleteTicket_NoticeInfoByEntity(int seq)
        {
            Boolean result = false;
            try
            {
                Ticket_NoticeInfo sale = DBContext.Ticket_NoticeInfo.Where(p => p.SEQ == seq).FirstOrDefault();
                var entry = DBContext.Entry(sale);
                entry.State = EntityState.Deleted;
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 批量删除通知/公告信息
        /// </summary>
        /// <param name="Ticket_NoticeInfoList"></param>
        /// <returns></returns>
        public Boolean DeleteTicket_NoticeInfoByList(List<int> seqList)
        {
            Boolean result = false;
            try
            {
                foreach (var item in seqList)
                {
                    Ticket_NoticeInfo sale = DBContext.Ticket_NoticeInfo.Where(p => p.SEQ == item).FirstOrDefault();
                    var entry = DBContext.Entry(sale);
                    entry.State = EntityState.Deleted;
                    DBContext.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;

        }

        /// <summary>
        /// 通知公告点击量
        /// </summary>
        public bool SaveNoticeInfoClickCount(int Seq)
        {
            bool result = false;
            try
            {
                Ticket_NoticeInfo notice = DBContext.Ticket_NoticeInfo.Where(p => p.SEQ == Seq).FirstOrDefault();
                notice.PointNum += 1;
                var entry = DBContext.Entry(notice);
                entry.State = EntityState.Modified;
                DBContext.SaveChanges();
                result = true;
                return result;
            }
            catch (Exception ex)
            {
                return result;
                throw ex;
            }
        }

        /// <summary>
        /// 根据类型获取置顶的通知公告
        /// </summary>
        /// <returns></returns>
        public List<Ticket_NoticeInfo> GetNoticeTopList(string TopCount, string NockitType)
        {

            List<Ticket_NoticeInfo> noticeList = new List<Ticket_NoticeInfo>();
            try
            {
                noticeList = DBContext.Ticket_NoticeInfo.Where(p => p.IsTop == true && p.Category == NockitType).Take(Convert.ToInt32(TopCount)).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return noticeList;
        }

        public List<Ticket_NoticeInfo> GetTicketNoticeListByQueryable(Ticket_NoticoPageModel model, string startDate, string endDate)
        {
            List<Ticket_NoticeInfo> noticList = new List<Ticket_NoticeInfo>();
            string sqlStr = "Select * from Ticket_NoticeInfo Where 1 = 1";
            if (model != null && model.PageModel != null)
            {
                if (!string.IsNullOrEmpty(model.PageModel.Title))
                {
                    sqlStr += " and Title = '" + model.PageModel.Title + "'";
                }
                if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    sqlStr += " and CreateTime between '" + startDate + " 00:00:00' and '" + endDate + " 23:59:00'";
                }
                if (!string.IsNullOrEmpty(model.PageModel.Category) && string.Compare("0", model.PageModel.Category, true) != 0)
                {
                    sqlStr += " and  Category =  '" + model.PageModel.Category + "'";
                }
            }
            noticList = DBContext.Ticket_NoticeInfo.SqlQuery(sqlStr).ToList();
            return noticList;
        }

        public SystemCode GetCodeByCodeNo(string codeNo)
        {
            SystemCode code = new SystemCode();
            code = DBContext.SystemCodes.Where(p => p.CodeNo == codeNo && p.CategoryNo == "NoticeType").FirstOrDefault();
            return code;
        }

        public Ticket_NoticeInfo GetTicketNoticeBySEQ(int SEQ)
        {
            Ticket_NoticeInfo ticket = new Ticket_NoticeInfo();
            ticket = DBContext.Ticket_NoticeInfo.Where(p => p.SEQ == SEQ).FirstOrDefault();
            return ticket;
        }
    }
}
