using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TicketManage.Repository.Models.ExtModels;

namespace TieckManage.Biz.TicketManage
{
    public class CompanyInfoManageBiz : BaseDataManageBiz
    {

        /// <summary>
        /// 获取集团所属分公司内容
        /// </summary>
        /// <returns></returns>
        public List<GroupByCompany> GetGroupByCompanyByAll()
        {
            var SQL = "select a.SEQ as GroupSEQ,a.GroupName,b.companyName  from GroupInfo as a left join CompanyInfo as b  on a.seq=b.groupseq";
            var list = DBContext.Database.SqlQuery<GroupByCompany>(SQL);
            return list.ToList();
        }

        /// <summary>
        /// 获取集团所属分公司内容
        /// </summary>
        /// <returns></returns>
        public List<GroupByCompany> GetGroupByCompanyByAll(string groupName)
        {
            var SQL = "select a.SEQ as GroupSEQ,a.GroupName,b.companyName  from GroupInfo as a left join CompanyInfo as b  on a.seq=b.groupseq where a.GroupName='{0}'";
            var list = DBContext.Database.SqlQuery<GroupByCompany>(string.Format(SQL, groupName));
            return list.ToList();
        }

        /// <summary>
        /// 获取全部代售点信息
        /// </summary>
        /// <returns></returns>
        public List<CompanyBySalePoint> GetCompanyBySalePointByAll()
        {
            var SQL = "select a.SEQ as GroupSEQ,a.GroupName,b.companyName,c.PointName as SalePointName  from GroupInfo as a left join CompanyInfo as b  on a.seq=b.groupseq left join SalePointInfo as c on b.SEQ=c.CompanySEQ";
            var list = DBContext.Database.SqlQuery<CompanyBySalePoint>(SQL);
            return list.ToList();
        }

        /// <summary>
        /// 获取全部代售点信息
        /// </summary>
        /// <returns></returns>
        public List<CompanyBySalePoint> GetCompanyBySalePointByAll(string groupName, string companyName)
        {
            var SQL = "select a.SEQ as GroupSEQ,a.GroupName,b.companyName,c.PointName as SalePointName  from GroupInfo as a left join CompanyInfo as b  on a.seq=b.groupseq left join SalePointInfo as c on b.SEQ=c.CompanySEQ where 1=1 ";

            var SQLContent = new StringBuilder(SQL);
            if (!string.IsNullOrEmpty(groupName))
            {
                SQLContent.AppendFormat("and a.GroupName='{0}'", groupName);
            }

            if (!string.IsNullOrEmpty(companyName))
            {
                SQLContent.AppendFormat("and b.companyName='{0}'", companyName);
            }
            var list = DBContext.Database.SqlQuery<CompanyBySalePoint>(SQLContent.ToString());
            return list.ToList();
        }

        /// <summary>
        /// 统计集团下分公司和代售点数量
        /// </summary>
        /// <returns></returns>
        public List<GroupInfoCount> GetGroupCount()
        {
            var SQL = "  select a.GroupName,count(b.SEQ) as companyNum,COUNT(c.SEQ) as SalePointNum  from GroupInfo as a   left join CompanyInfo as b    on a.seq=b.groupseq   left join SalePointInfo as c   on b.SEQ=c.CompanySEQ   group by a.GroupName";
            var list = DBContext.Database.SqlQuery<GroupInfoCount>(SQL);
            return list.ToList();
        }



        /// <summary>
        /// 获取单位表全部数据
        /// </summary>
        /// <returns></returns>
        public List<CompanyInfo> GetCompanyInfoListByAll()
        {
            List<CompanyInfo> companyList = new List<CompanyInfo>();
            companyList = DBContext.CompanyInfoes.ToList();
            return companyList;
        }

        /// <summary>
        /// 保存单位信息(添加/修改)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SaveCompanyInfo(CompanyPageModel model)
        {
            Boolean result = false;
            try
            {
                var entry = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0)
                {
                    entry.State = EntityState.Added;
                }
                else
                {
                    entry.State = EntityState.Modified;
                }
                DBContext.SaveChanges();
                result = true;

            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 删除单位信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean DeleteCompanyInfoByEntity(int seq)
        {
            Boolean result = false;
            try
            {
                CompanyInfo model = DBContext.CompanyInfoes.Where(p => p.SEQ == seq).FirstOrDefault();
                var entry = DBContext.Entry(model);
                entry.State = EntityState.Deleted;
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 批量删除单位信息
        /// </summary>
        /// <param name="CompanyInfoList"></param>
        /// <returns></returns>
        public Boolean DeleteCompanyInfoByList(List<int> seqList)
        {
            Boolean result = false;
            try
            {
                if (seqList != null && seqList.Count > 0)
                {
                    foreach (var item in seqList)
                    {
                        CompanyInfo model = DBContext.CompanyInfoes.Where(p => p.SEQ == item).FirstOrDefault();
                        var entry = DBContext.Entry(model);
                        entry.State = EntityState.Deleted;
                        DBContext.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;

        }

        public CompanyInfo ShowCompanyInfoById(int SEQ)
        {
            CompanyInfo companyInfo = new CompanyInfo();
            companyInfo = DBContext.CompanyInfoes.Where(p => p.SEQ == SEQ).FirstOrDefault();
            return companyInfo;
        }

        public List<V_GroupCompanyView> GetCompanyByQueryable(CompanyPageModel model)
        {
            List<V_GroupCompanyView> companyList = new List<V_GroupCompanyView>();
            string sqlStr = "Select * from V_GroupCompanyView Where 1 = 1";
            if (model != null && model.PageModel != null)
            {
                if (model.PageModel != null && model.PageModel.GroupSEQ != null && model.PageModel.GroupSEQ != 0)
                {
                    sqlStr += " and GroupSEQ = " + model.PageModel.GroupSEQ.ToString();
                }
                if (!string.IsNullOrEmpty(model.PageModel.CompanyName))
                {
                    sqlStr += " and CompanyName = '" + model.PageModel.CompanyName + "'";
                }
            }
            model.PageNum = DBContext.V_GroupCompanyView.SqlQuery(sqlStr).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            companyList = DBContext.V_GroupCompanyView
                .SqlQuery(sqlStr)
                .Skip(model.PageSize * (model.CurrentPage - 1))
                .Take(model.PageSize)
                .ToList();
            return companyList;
        }
    }
}
