using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;

namespace TieckManage.Biz.TicketManage
{
    public class TicketPlanManageBiz : BaseDataManageBiz
    {

        public TicketPlan GetTicketPlan(int id, string yearStr)
        {
            TicketPlan ticketPlan = DBContext.TicketPlans.Where(p => p.CompanySEQ == id && p.YearStr == yearStr).FirstOrDefault();
            return ticketPlan;
        }


        public bool SaveTicketPlan(TicketPlanPageModel model)
        {
            Boolean result = false;
            try
            {
                var entry = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0)
                {
                    entry.State = EntityState.Added;
                }
                else
                {
                    entry.State = EntityState.Modified;
                }
                DBContext.SaveChanges();
                result = true;

            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }
    }
}
