using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;

namespace TieckManage.Biz.TicketManage
{
    public class TicketUnionBiz : BaseDataManageBiz
    {

        public TicketUnion GetTicketUnionById(int seq)
        {
            var model = DBContext.TicketUnions.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean TicketUnionDel(int seq)
        {
            var model = DBContext.TicketUnions.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.TicketUnions.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        /// 查询指定实体
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        public List<TicketUnion> GetTicketUnionByCondition(TicketUnionPageModel pageModel)
        {
            var SQLCONTEXT = new StringBuilder("select * from TicketUnion where 1=1 ");
            if (!string.IsNullOrEmpty(pageModel.StartDate) && !string.IsNullOrEmpty(pageModel.EndDate))
            {
                var tempStartDate = Convert.ToDateTime(pageModel.StartDate);
                var tempEndDate = Convert.ToDateTime(pageModel.EndDate);
                SQLCONTEXT.AppendFormat(" and CreateTime>='{0}' ", tempStartDate);
                SQLCONTEXT.AppendFormat(" and CreateTime<='{0}' ", tempEndDate);
            }
            if (pageModel.OperatorUserNoList != null && pageModel.OperatorUserNoList.Count > 0)
            {
                SQLCONTEXT.AppendFormat(" and OperationSEQ in ({0})", ConvertStringListToContext(pageModel.OperatorUserNoList));
            }
            pageModel.PageNum = DBContext.Database.SqlQuery<TicketUnion>(SQLCONTEXT.ToString()).Count();
            pageModel.PageTotal = (pageModel.PageNum + pageModel.PageSize - 1) / pageModel.PageSize;
            var modelList = DBContext.Database
                    .SqlQuery<TicketUnion>(SQLCONTEXT.ToString())
                    .OrderBy(a => a.CreateTime)
                    .Skip(pageModel.PageSize * (pageModel.CurrentPage - 1))
                    .Take(pageModel.PageSize)
                    .ToList();
            return modelList;
        }

       

        public Boolean TicketUnionAdd(TicketUnionPageModel pageModel)
        {
            try
            {
                DBContext.TicketUnions.Add(pageModel.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public Boolean TicketUnionEdit(TicketUnionPageModel pageModel)
        {
            try
            {
                var model = GetTicketUnionById(pageModel.PageModel.SEQ);
                model.CreateTime = pageModel.PageModel.CreateTime;
                model.PingRangNum = pageModel.PageModel.PingRangNum;
                model.XinYiZhouNum = pageModel.PageModel.XinYiZhouNum;
                model.KidNum = pageModel.PageModel.KidNum;
                model.BackNum = pageModel.PageModel.BackNum;
                model.StartNoTicket = pageModel.PageModel.StartNoTicket;
                model.CustomerNum = pageModel.PageModel.CustomerNum;
                model.SleepNum = pageModel.PageModel.SleepNum;
                model.MonthAcount = pageModel.PageModel.MonthAcount;
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
