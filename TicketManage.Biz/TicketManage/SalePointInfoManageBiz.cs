using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
namespace TieckManage.Biz.TicketManage
{
    public class SalePointInfoManageBiz : BaseDataManageBiz
    {


        /// <summary>
        /// 获取代售点表全部数据
        /// </summary>
        /// <returns></returns>
        public List<V_GroupCompanySaleView> GetSalePointViewInfoListByAll()
        {
            List<V_GroupCompanySaleView> companyList = new List<V_GroupCompanySaleView>();
            companyList = DBContext.V_GroupCompanySaleView.ToList();
            return companyList;
        }


        /// <summary>
        /// 获取代售点表全部数据
        /// </summary>
        /// <returns></returns>
        public List<SalePointInfo> GetSalePointInfoListByAll()
        {
            List<SalePointInfo> companyList = new List<SalePointInfo>();
            companyList = DBContext.SalePointInfoes.ToList();
            return companyList;
        }

        /// <summary>
        /// 保存代售点信息(添加/修改)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SaveSalePointInfo(SalePointPageModel model)
        {
            Boolean result = false;
            try
            {
                var entry = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0)
                {
                    entry.State = EntityState.Added;
                }
                else
                {
                    entry.State = EntityState.Modified;
                }
                DBContext.Configuration.ValidateOnSaveEnabled = false;
                DBContext.SaveChanges();
                DBContext.Configuration.ValidateOnSaveEnabled = false;
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 删除代售点信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean DeleteSalePointInfoByEntity(int seq)
        {
            Boolean result = false;
            try
            {
                SalePointInfo sale = DBContext.SalePointInfoes.Where(p => p.SEQ == seq).FirstOrDefault();
                var entry = DBContext.Entry(sale);
                entry.State = EntityState.Deleted;
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 批量删除代售点信息
        /// </summary>
        /// <param name="SalePointInfoList"></param>
        /// <returns></returns>
        public Boolean DeleteSalePointInfoByList(List<int> seqList)
        {
            Boolean result = false;
            try
            {
                if (seqList != null && seqList.Count > 0)
                {
                    foreach (var item in seqList)
                    {
                        SalePointInfo sale = DBContext.SalePointInfoes.Where(p => p.SEQ == item).FirstOrDefault();
                        var entry = DBContext.Entry(sale);
                        entry.State = EntityState.Deleted;
                        DBContext.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;

        }

        public List<V_GroupCompanySaleView> GetSalePointListByQueryable(SalePointPageModel model)
        {
            List<V_GroupCompanySaleView> companyList = new List<V_GroupCompanySaleView>();
            string sqlStr = "Select * from V_GroupCompanySaleView Where 1 = 1";
            if (model != null && model.PageModel != null)
            {
                if (model.PageModel.CompanySEQ != null && model.PageModel.CompanySEQ != 0)
                {
                    sqlStr += " and CSEQ =" + model.PageModel.CompanySEQ + " ";
                }

                if (model.PageModel.Category != null)
                {
                    var tempVal = model.PageModel.Category ?? true;
                    var tempCategoryValue = tempVal == true ? "1" : "0";
                    sqlStr += " and Category =" + tempCategoryValue + " ";
                }
            }

            model.PageNum = DBContext.Database.SqlQuery<V_GroupCompanySaleView>(sqlStr).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            companyList = DBContext.Database.SqlQuery<V_GroupCompanySaleView>(sqlStr)
                .Skip(model.PageSize * (model.CurrentPage - 1))
                .Take(model.PageSize)
                .ToList();
            return companyList;
        }

        public SalePointInfo ShowSalePointInfoById(int SEQ)
        {
            SalePointInfo salePointInfo = new SalePointInfo();
            salePointInfo = DBContext.SalePointInfoes.Where(p => p.SEQ == SEQ).FirstOrDefault();
            return salePointInfo;
        }
    }
}
