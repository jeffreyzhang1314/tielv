using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TicketManage.Tools;

namespace TieckManage.Biz.TicketManage
{
    public class UserInfoManageBiz : BaseDataManageBiz
    {

        private const string SQL = @"Select b.GroupName,c.CompanyName                                ,d.PointName,case d.Category when 0 then N'自营' when 1 then N'联营' end as CategoryName,a.*                                 from UserInfo as a                                 left join GroupInfo as b                                 on a.GroupSEQ=b.seq                                 left join CompanyInfo as c                                 on a.CompanySEQ=c.SEQ                                 left join SalePointInfo as d                                 on a.SalePointSEQ=d.SEQ";

        /// <summary>
        /// 重置指定用户的密码
        /// </summary>
        /// <param name="seq">用户序号</param>
        /// <returns>是否执行成功：True执行成功，False执行失败</returns>
        public Boolean ResetPasswordByUserInfoSEQ(int seq)
        {
            var tools = new MD5Helper();
            var model = DBContext.UserInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                model.UserPassword = tools.EncryptCode("000000");
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 获取人员表全部数据
        /// </summary>
        /// <returns></returns>
        public List<UserInfoPageModel> GetUserInfoListByAll()
        {
            var companyList = new List<UserInfoPageModel>();
            companyList = DBContext.Database.SqlQuery<UserInfoPageModel>(SQL).Where(a => a.UserNo != "Admin").ToList();
            return companyList;
        }

        /// <summary>
        /// 根据分页内容查询
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        public List<UserInfoPageModel> GetUserInfoListByAll(UserPageModel pageModel)
        {
            var skipNum = pageModel.PageSize * (pageModel.CurrentPage - 1);
            var companyList = new List<UserInfoPageModel>();
            companyList = DBContext.Database.SqlQuery<UserInfoPageModel>(SQL).Where(a => a.UserNo != "Admin")
                .OrderBy(a => a.CreateTime).Skip(skipNum).Take(pageModel.PageSize).ToList();
            pageModel.PageNum = DBContext.UserInfoes.Where(a => a.UserNo != "Admin").Count();
            pageModel.PageTotal = (pageModel.PageNum + pageModel.PageSize - 1) / pageModel.PageSize;
            return companyList;
        }


        /// <summary>
        /// 获取制定查询条件下的数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<UserInfoPageModel> GetUserInfoListByCondition(UserPageModel model)
        {
            var tempSQL = string.Format("{0} where a.UserNo not in ('Admin','{1}')", SQL, model.CurrentUserNo);
            var companyList = DBContext.Database.SqlQuery<UserInfoPageModel>(tempSQL).ToList();
            if (model.PageModel.GroupSEQ != null && model.PageModel.GroupSEQ > 0)
            {
                companyList = companyList.Where(a => a.GroupSEQ == model.PageModel.GroupSEQ).ToList();
            }
            if (model.PageModel.CompanySEQ != null && model.PageModel.CompanySEQ > 0)
            {
                companyList = companyList.Where(a => a.CompanySEQ == model.PageModel.CompanySEQ).ToList();
            }
            if (model.PageModel.SalePointSEQ != null && model.PageModel.SalePointSEQ > 0)
            {
                companyList = companyList.Where(a => a.SalePointSEQ == model.PageModel.SalePointSEQ).ToList();
            }
            if (!string.IsNullOrEmpty(model.PageModel.UserName))
            {
                companyList = companyList.Where(a => a.UserName.Contains(model.PageModel.UserName)).ToList();
            }
            model.PageNum = companyList.Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            companyList = companyList
                .OrderBy(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return companyList;
        }

        /// <summary>
        /// 保存人员信息(添加/修改)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SaveUserInfo(UserPageModel model)
        {
            Boolean result = false;
            try
            {
                if (model.PageModel.UserNo == "" || model.PageModel.UserNo == null)
                {
                    var tools = new MD5Helper();
                    model.PageModel.UserNo = model.PageModel.UserName + (model.PageModel.PassportNo).Substring(model.PageModel.PassportNo.Length - 4);
                    model.PageModel.UserPassword = tools.EncryptCode("000000");

                    model.PageModel.UserState = true;
                    model.PageModel.CreateTime = DateTime.Now;
                    if (model.PageModel.AuthNum != null && (model.PageModel.AuthNum & 4) == 4)
                    {
                        model.PageModel.CompanySEQ = null;
                        model.PageModel.SalePointSEQ = null;
                    }
                    else if (model.PageModel.AuthNum != null && (model.PageModel.AuthNum & 8) == 8)
                    {
                        model.PageModel.SalePointSEQ = null;
                    }

                    var entry = DBContext.Entry(model.PageModel);
                    if (model.PageModel.SEQ == 0)
                    {
                        entry.State = EntityState.Added;
                    }
                    else
                    {
                        entry.State = EntityState.Modified;
                    }
                    DBContext.SaveChanges();
                    result = true;
                }
                else
                {
                    if (!CheckUserNo(model.PageModel.UserNo))
                    {
                        var tools = new MD5Helper();
                        if (model.PageModel.AuthNum != null && (model.PageModel.AuthNum & 4) == 4)
                        {
                            model.PageModel.CompanySEQ = null;
                            model.PageModel.SalePointSEQ = null;
                        }
                        else if (model.PageModel.AuthNum != null && (model.PageModel.AuthNum & 8) == 8)
                        {
                            model.PageModel.SalePointSEQ = null;
                        }
                        model.PageModel.UserPassword = tools.EncryptCode("000000");
                        model.PageModel.UserState = true;
                        model.PageModel.CreateTime = DateTime.Now;
                        var entry = DBContext.Entry(model.PageModel);
                        if (model.PageModel.SEQ == 0)
                        {
                            entry.State = EntityState.Added;
                        }
                        else
                        {
                            entry.State = EntityState.Modified;
                        }
                        DBContext.SaveChanges();
                        result = true;
                    }
                    else
                    {
                        model.Message = "工号重复";
                        model.MessageState = false;
                        result = false;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 编辑用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditUserInfo(UserPageModel model)
        {
            var resultFlag = false;
            var dbModel = GetUserInfoById(model.PageModel.SEQ);
            if (dbModel != null)
            {
                dbModel.GroupSEQ = model.PageModel.GroupSEQ;
                dbModel.CompanySEQ = model.PageModel.CompanySEQ;
                dbModel.SalePointSEQ = model.PageModel.SalePointSEQ;
                dbModel.UserName = model.PageModel.UserName;
                dbModel.ContactNo = model.PageModel.ContactNo;
                dbModel.PassportNo = model.PageModel.PassportNo;
                dbModel.Age = model.PageModel.Age;
                dbModel.Sex = model.PageModel.Sex;
                dbModel.AuthNum = model.PageModel.AuthNum;
                model.Message = "添加成功！";
                model.MessageState = true;
                DBContext.SaveChanges();
                resultFlag = true;
            }
            else
            {
                model.Message = "未找到实体！";
                model.MessageState = false;
            }
            return resultFlag;
        }

        /// <summary>
        /// 删除人员信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean DeleteUserInfoByEntity(int seq)
        {
            Boolean result = false;
            try
            {
                UserInfo user = DBContext.UserInfoes.Where(p => p.SEQ == seq).FirstOrDefault();
                var entry = DBContext.Entry(user);
                entry.State = EntityState.Deleted;
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 批量删除人员信息
        /// </summary>
        /// <param name="UserInfoList"></param>
        /// <returns></returns>
        public Boolean DeleteUserInfoByList(List<int> seqList)
        {
            Boolean result = false;
            try
            {
                if (seqList != null && seqList.Count > 0)
                {
                    foreach (var item in seqList)
                    {
                        UserInfo user = DBContext.UserInfoes.Where(p => p.SEQ == item).FirstOrDefault();
                        var entry = DBContext.Entry(user);
                        entry.State = EntityState.Deleted;
                        DBContext.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;

        }

        /// <summary>
        /// 判断用户是否可以登录
        /// </summary>
        /// <param name="model">需要验证实体</param>
        /// <returns></returns>
        public Boolean LoginUserInfo(UserInfo model)
        {
            return false;
        }

        public UserInfo GetUserInfoById(int SEQ)
        {
            UserInfo userInfo = new UserInfo();
            userInfo = DBContext.UserInfoes.Where(p => p.SEQ == SEQ).FirstOrDefault();
            return userInfo;
        }
    }
}
