/*
 * Created by SharpDevelop.
 * User: Admin
 * Date: 2017/5/28
 * Time: 11:15
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketManage.Entity.CommonManage;
using TicketManage.Entity.TicketManage;
using TicketManage.Entity.TourGuideManage;
using TicketManage.Repository.Models;
using TicketManage.Tools;

namespace TieckManage.Biz.TicketManage
{
    /// <summary>
    /// Description of BaseDataManageBiz.
    /// </summary>
    public class BaseDataManageBiz
    {
        /// <summary>
        /// 获取全部其他用户需要的分公司信息
        /// </summary>
        /// <returns></returns>
        public List<KeyValueModel> GetOtherUserCompany()
        {
            var SQL = @"select SEQ as [Key],CodeName as [Value]
                                from TicketManage.dbo.SystemCode
                                where CategoryNo='CompanyCode'";
            var tempList = DBContext.Database.SqlQuery<KeyValueModel>(SQL).ToList();
            return tempList;
        }

        /// <summary>
        /// 获取指定分公司信息
        /// </summary>
        /// <returns></returns>
        public List<KeyValueModel> GetOtherUserCompanyByName(string companyName)
        {
            var tempList = new List<KeyValueModel>();
            var SQL = new StringBuilder(@"select SEQ as [Key],CodeName as [Value]
                                from TicketManage.dbo.SystemCode
                                where CategoryNo='CompanyCode'");
            if (!string.IsNullOrEmpty(companyName))
            {
                SQL.AppendFormat(" and CodeName='{0}' ", companyName);
                tempList = DBContext.Database.SqlQuery<KeyValueModel>(SQL.ToString()).ToList();
            }
            
            return tempList;
        }

        protected Boolean IsNumber(string str)
        {
            var flag = true;
            foreach (var item in str)
            {
                if (!"0123456789".Contains(item))
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }

        protected TicketManageContext DBContext = new TicketManageContext();

        /// <summary>
        /// 除票务外其他系统登录验证用户登录校验
        /// </summary>
        /// <param name="model">登录页面实体</param>
        /// <returns>True登录成功False登录失败</returns>
        public Boolean CommonLogin(UserLoginPageModel model)
        {
            var flag = false;
            var tools = new MD5Helper();
            var tempUserPassword = tools.EncryptCode(model.PageModel.UserPassword);
            var tempModel = DBContext.UserInfo_Other
                .Where(a => a.UserNo == model.PageModel.UserNo && a.UserPassword == tempUserPassword)
                .FirstOrDefault();
            if (tempModel != null)
            {
                flag = true;
                model.PageModel = tempModel;
                var tempAuthNum = tempModel.AuthNum ?? 0;
                //判断用户权限信息如果用户是管理员将用户序号修改为0
                model.PageModel.SEQ = (tempAuthNum & 1) == 1 ? 0 : tempModel.SEQ;
            }
            return flag;
        }

        /// <summary>
        /// 判断用户是否可以登录成功
        /// </summary>
        /// <param name="model">需要验证的页面实体</param>
        /// <returns>True可以登录False用户用密码错误</returns>
        public Boolean UserLogin(UserPageModel model)
        {
            var flag = false;
            var tools = new MD5Helper();
            var tempUserPassword = tools.EncryptCode(model.PageModel.UserPassword);
            var tempModel = DBContext.UserInfoes
                .Where(a => a.UserNo == model.PageModel.UserNo
                    && a.UserPassword == tempUserPassword).FirstOrDefault();
            if (tempModel != null)
            {
                flag = true;
                model.PageModel = tempModel;
                var tempSalePoint = DBContext.SalePointInfoes.Where(a => a.SEQ == tempModel.SalePointSEQ).FirstOrDefault();
                var tempCompany = DBContext.CompanyInfoes.Where(a => a.SEQ == tempModel.CompanySEQ).FirstOrDefault();
                if (tempSalePoint != null)
                {
                    model.SalePoint = tempSalePoint.PointName;
                }
                if (tempCompany != null)
                {
                    model.Company = tempCompany.CompanyName;
                }
                var tempAuthNum = tempModel.AuthNum ?? 0;
                if ((tempAuthNum & 2) == 2 || (tempAuthNum & 4) == 4 || (tempAuthNum & 8) == 8)
                {
                    model.UserCategory = true;
                }
                else
                {
                    model.UserCategory = false;
                }
                model.PageModel.SEQ = tempModel.SEQ;
            }
            return flag;
        }


        /// <summary>
        /// 转换数字集合为逗号分隔的字符串
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        protected string ConvertStringListToContext(List<int> list)
        {
            var tempStr = new StringBuilder();
            foreach (var item in list)
            {
                tempStr.AppendFormat("{0},", item);
            }
            return tempStr.Remove(tempStr.Length - 1, 1).ToString();
        }

        /// <summary>
        /// 查询集团下全部用户序号
        /// </summary>
        /// <param name="seq">集团序号</param>
        /// <returns>用户序号集合</returns>
        public List<int> GetUserNoByGroupSEQ(int? seq)
        {
            var tempModel = DBContext.UserInfoes.Where(a => a.GroupSEQ == seq).Select(a => a.SEQ).ToList();
            return tempModel;
        }

        /// <summary>
        /// 查询分公司下全部用户序号
        /// </summary>
        /// <param name="seq">分公司序号</param>
        /// <returns>用户序号集合</returns>
        public List<int> GetUserNoByCompanySEQ(int? seq)
        {
            var tempModel = DBContext.UserInfoes.Where(a => a.CompanySEQ == seq).Select(a => a.SEQ).ToList();
            return tempModel;
        }

        /// <summary>
        /// 判断用户名是否存在
        /// </summary>
        /// <param name="userNo">用户登陆账号</param>
        /// <returns>存在返回True不存在返回False</returns>
        public Boolean CheckUserNo(string userNo)
        {
            var tempModel = DBContext.UserInfoes.Where(a => a.UserNo == userNo).FirstOrDefault();
            return tempModel != null ? true : false;
        }

        /// <summary>
        /// 判断其它用户表中是否存在
        /// </summary>
        /// <param name="userNo">用户登陆账号</param>
        /// <returns>存在返回True不存在返回False</returns>
        public Boolean CheckOtherUserNo(string userNo)
        {
            var tempModel = DBContext.UserInfo_Other.Where(a => a.UserNo == userNo).FirstOrDefault();
            return tempModel != null ? true : false;
        }



        /// <summary>
        /// 检测用户密码是否正确 
        /// </summary>
        /// <param name="userNo"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Boolean CheckUserInfo(string userNo, string password, ref int userSEQ)
        {
            var tools = new MD5Helper();
            password = tools.EncryptCode(password);
            var tempModel = DBContext.UserInfoes.Where(a => a.UserNo == userNo && a.UserPassword == password).FirstOrDefault();
            if (tempModel != null)
            {
                userSEQ = tempModel.SEQ;
            }
            return tempModel != null ? true : false;
        }

        /// <summary>
        /// 创建公共用户信息
        /// </summary>
        /// <param name="userNo"></param>
        /// <param name="userName"></param>
        /// <param name="passWord"></param>
        /// <param name="authNum"></param>
        /// <returns></returns>
        public Boolean CreateUserNo(string userNo, string userName, string passWord, int authNum)
        {
            var tools = new MD5Helper();
            if (string.IsNullOrEmpty(passWord))
            {
                passWord = "0";
            }
            passWord = tools.EncryptCode(passWord);
            var model = new UserInfo() { UserNo = userNo, UserName = userName, UserPassword = passWord, AuthNum = authNum };
            DBContext.UserInfoes.Add(model);
            DBContext.SaveChanges();
            return true;
        }

        public List<SystemCode> GetSystemCodeListByType(string CodeType)
        {
            List<SystemCode> result = new List<SystemCode>();
            result = DBContext.SystemCodes.Where(p => p.CategoryNo == CodeType).ToList();

            return result;
        }

        /// <summary>
        /// 按列表和名字查询
        /// </summary>
        /// <param name="CodeType"></param>
        /// <param name="CodeName"></param>
        /// <returns></returns>
        public List<SystemCode> GetSystemCodeListByType(string CodeType, string CodeName)
        {
            List<SystemCode> result = new List<SystemCode>();
            if (!string.IsNullOrEmpty(CodeName))
            {
                result = DBContext.SystemCodes.Where(p => p.CategoryNo == CodeType && p.CodeName == CodeName).ToList();
            }
            else
            {
                result = DBContext.SystemCodes.Where(p => p.CategoryNo == CodeType).ToList();
            }
            return result;
        }


        /// <summary>
        /// 获取全部集团信息
        /// </summary>
        /// <returns></returns>
        public List<KeyValueModel> GetGroupInfoByAll()
        {
            var modelList = DBContext.GroupInfoes.Select(a => new KeyValueModel { Key = a.SEQ, Value = a.GroupName }).ToList();
            return modelList;
        }

        /// <summary>
        /// 获取指定序号的集团信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public List<KeyValueModel> GetGroupInfoById(int seq)
        {
            var modelList = DBContext.GroupInfoes
                .Where(a => a.SEQ == seq)
                .Select(a => new KeyValueModel { Key = a.SEQ, Value = a.GroupName }).ToList();
            return modelList;
        }

        /// <summary>
        /// 获取全部单位信息
        /// </summary>
        /// <returns></returns>
        public List<KeyValueModel> GetCompanyInfoByAll()
        {
            var modelList = DBContext.CompanyInfoes
                .Select(a => new KeyValueModel { Key = a.SEQ, Value = a.CompanyName })
                .ToList();
            return modelList;
        }

        /// <summary>
        /// 查询制定ID的单位信息
        /// </summary>
        /// <param name="seq">单位ID</param>
        /// <returns>返回集合形式数据</returns>
        public List<KeyValueModel> GetCompanyInfoById(int seq)
        {
            var modelList = DBContext.CompanyInfoes
                .Where(a => a.SEQ == seq)
               .Select(a => new KeyValueModel { Key = a.SEQ, Value = a.CompanyName })
               .ToList();
            return modelList;
        }

        /// <summary>
        /// 获取执行集团下全部单位信息
        /// </summary>
        /// <param name="seq">集团序号</param>
        /// <returns></returns>
        public List<KeyValueModel> GetCompanyInfoByGroupSEQ(int seq)
        {
            var modelList = DBContext.CompanyInfoes
                .Where(a => a.GroupSEQ == seq)
                .Select(a => new KeyValueModel { Key = a.SEQ, Value = a.CompanyName })
                .ToList();
            return modelList;
        }

        /// <summary>
        /// 获取全部代售点信息
        /// </summary>
        /// <returns></returns>
        public List<KeyValueModel> GetSalePointInfoByAll()
        {
            var modelList = DBContext.SalePointInfoes
                .Select(a => new KeyValueModel { Key = a.SEQ, Value = a.PointName })
                .ToList();
            return modelList;
        }

        /// <summary>
        /// 获取指定单位下代售点信息
        /// </summary>
        /// <returns></returns>
        public List<KeyValueModel> GetSalePointInfoByCompanySEQ(int seq)
        {
            var modelList = DBContext.SalePointInfoes
                .Where(a => a.CompanySEQ == seq)
                .Select(a => new KeyValueModel { Key = a.SEQ, Value = a.PointName })
                .ToList();
            return modelList;
        }

        /// <summary>
        /// 获取指定集团下全部代售点信息
        /// </summary>
        /// <param name="seq">集团序号</param>
        /// <returns>代售点信息集合</returns>
        public List<KeyValueModel> GetSalePointInfoByGroupSEQ(int seq)
        {
            var modelList = DBContext.SalePointInfoes
                .Where(a => a.GroupSEQ == seq)
                .Select(a => new KeyValueModel { Key = a.SEQ, Value = a.PointName })
                .ToList();
            return modelList;
        }

        /// <summary>
        /// 获取制定代售点信息
        /// </summary>
        /// <param name="seq">代售点序号</param>
        /// <returns>集合形式返回代售点数据</returns>
        public List<KeyValueModel> GetSalePointInfoById(int seq)
        {
            var modelList = DBContext.SalePointInfoes
                .Where(a => a.SEQ == seq)
                .Select(a => new KeyValueModel { Key = a.SEQ, Value = a.PointName })
                .ToList();
            return modelList;
        }

        /// <summary>
        /// 过去全部岗位信息
        /// </summary>
        /// <returns>岗位实体集合</returns>
        public List<GX_PostionInfo> GetPostionByAll()
        {
            return DBContext.GX_PostionInfo.ToList();
        }

        /// <summary>
        /// 获取全部职务信息
        /// </summary>
        /// <returns></returns>
        public List<GX_DutyInfo> GetDutyByAll()
        {
            return DBContext.GX_DutyInfo.ToList();
        }

        /// <summary>
        /// 获取全部级别信息
        /// </summary>
        /// <returns></returns>
        public List<GX_RankInfo> GetRankByAll()
        {
            return DBContext.GX_RankInfo.ToList();
        }

        /// <summary>
        /// 删除指定编号
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean DelSystemCodeBySEQ(int seq)
        {
            try
            {
                var flag = false;
                var model = DBContext.SystemCodes.Where(a => a.SEQ == seq).First();
                if (model != null)
                {
                    DBContext.SystemCodes.Remove(model);
                    DBContext.SaveChanges();
                    flag = true;
                }
                return flag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 修改编号名称
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditSystemCode(DirectoryCompanyPageModel model)
        {
            try
            {
                var flag = false;
                var tempModel = DBContext.SystemCodes.Where(a => a.SEQ == model.SearchModel.SEQ).First();
                if (tempModel != null)
                {
                    tempModel.CodeName = model.SearchModel.CodeName;
                    DBContext.SaveChanges();
                }
                return flag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }


}
