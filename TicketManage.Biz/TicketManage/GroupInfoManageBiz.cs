using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
namespace TieckManage.Biz.TicketManage
{

    public class GroupInfoManageBiz : BaseDataManageBiz
    {
        #region 集团信息表相关

        public GroupInfo ShowGroupInfoById(int SEQ)
        {
            GroupInfo groupInfo = new GroupInfo();
            groupInfo = DBContext.GroupInfoes.Where(p => p.SEQ == SEQ).FirstOrDefault();
            return groupInfo;
        }

        /// <summary>
        /// 添加集团信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SaveGroupInfo(GroupPageModel model)
        {
            Boolean result = false;
            try
            {
                var entry = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0)
                {
                    entry.State = EntityState.Added;
                }
                else
                {
                    entry.State = EntityState.Modified;
                }
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }
        /// <summary>
        /// 批量删除集团信息
        /// </summary>
        /// <param name="seqList"></param>
        /// <returns></returns>
        public Boolean DelGroupInfoByIdList(List<int> seqList)
        {
            Boolean result = false;
            try
            {
                if (seqList != null && seqList.Count > 0)
                {
                    foreach (var item in seqList)
                    {
                        GroupInfo model = DBContext.GroupInfoes.Where(p => p.SEQ == item).FirstOrDefault();
                        var entry = DBContext.Entry(model);
                        entry.State = EntityState.Deleted;
                        DBContext.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }
        /// <summary>
        /// 删除集团信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean DelGroupInfoById(int seq)
        {
            Boolean result = false;
            try
            {
                GroupInfo model = DBContext.GroupInfoes.Where(p => p.SEQ == seq).FirstOrDefault();
                var entry = DBContext.Entry(model);
                entry.State = EntityState.Deleted;
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }
        /// <summary>
        /// 修改集团信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditGroupInfo(List<GroupInfo> model)
        {
            Boolean result = false;
            try
            {
                foreach (var item in model)
                {
                    var entry = DBContext.Entry(item);

                    entry.State = EntityState.Modified;

                    DBContext.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 查询所有集团信息
        /// </summary>
        /// <returns></returns>
        public List<GroupInfo> ShowGroupInfoByAll()
        {
            List<GroupInfo> companyList = new List<GroupInfo>();
            companyList = DBContext.GroupInfoes.ToList();
            return companyList;
        }


        public List<GroupInfo> GetGroupListByQueryable(GroupPageModel model)
        {
            List<GroupInfo> companyList = new List<GroupInfo>();
            string sqlStr = "Select * from GroupInfo Where 1 = 1";
            if (model != null && model.PageModel != null)
            {
                if (model.PageModel.GroupName != null)
                {
                    sqlStr += " and GroupName = '" + model.PageModel.GroupName + "'";
                }
            }
            model.PageNum = DBContext.GroupInfoes.SqlQuery(sqlStr).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            companyList = DBContext.GroupInfoes
                .SqlQuery(sqlStr)
                .Skip(model.PageSize * (model.CurrentPage - 1))
                .Take(model.PageSize)
                .ToList();
            return companyList;
        }

        #endregion
    }
}
