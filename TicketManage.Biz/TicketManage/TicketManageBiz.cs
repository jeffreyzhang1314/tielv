using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using TicketManage.Entity.InterfaceTicketDataManage;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TicketManage.Repository.Models.ExtModels;
using TicketManage.Tools;

namespace TieckManage.Biz.TicketManage
{
    public class TicketManageBiz : BaseDataManageBiz
    {
        private const string SQL = @"select CompanySEQ,SalePointSEQ,CompanyName,SalePointName
												  ,CONVERT(nvarchar(12),CreateTime,111) as CreateTimeShow
												  ,MIN(StartTicketNo) as StartTicketNo,MAX(EndTicketNo) as EndTicketNo,sum(TicketNum) as TicketNum,sum(KidTicketNum) as KidTicketNum
												  ,sum(Brokerage) as Brokerage,sum(InsuranceMoney) as InsuranceMoney
                                                  ,sum(GroupTicketNum) as GroupTicketNum,sum(LaSaTicketNum) as LaSaTicketNum,max(FinalTable.DivideInto) as DivideInto
                                                  ,SUM(FinalTable.TicketMoney) as TicketMoney
												  from (SELECT b.CompanySEQ,b.SalePointSEQ,c.CompanyName,d.PointName as SalePointName,a.*
                                                  FROM TicketCustomer as a
                                                  left join UserInfo as b
                                                  on a.OperationSEQ=b.SEQ
                                                  left join CompanyInfo as c
                                                  on b.CompanySEQ=c.SEQ
                                                  left join SalePointInfo as d
                                                  on b.SalePointSEQ=d.SEQ) as FinalTable
												  where 1=1 {0}
												  group by CompanySEQ,SalePointSEQ,CompanyName,SalePointName,CONVERT(nvarchar(12),CreateTime,111)";

        private const string TICKETDDETAILSQL = @"SELECT b.CompanySEQ,b.SalePointSEQ,c.CompanyName,CONVERT(nvarchar(12),a.CreateTime,111) as CreateTimeShow,d.PointName as SalePointName,e.UserName,a.*
                                                                      FROM TicketCustomer as a
                                                                      left join UserInfo as b
                                                                      on a.OperationSEQ=b.SEQ
                                                                      left join CompanyInfo as c
                                                                      on b.CompanySEQ=c.SEQ
                                                                      left join SalePointInfo as d
                                                                      on b.SalePointSEQ=d.SEQ
												                      left join UserInfo as e
												                      on a.OperationSEQ=e.SEQ
                                                                      Where 1=1 {0}";
        /// <summary>
        /// 获取所有票务收入
        /// </summary>
        /// <returns></returns>
        public string GetTicketTotal()
        {
            var totalStr = string.Empty;
            var total = DBContext.TicketCustomers
                .Sum(a => a.Brokerage + a.GroupTicketNum * 5 + a.LaSaTicketNum * 5).Value;
            if (total > 0)
            {
                totalStr = Decimal.Divide(total, 10000M).ToString("n");
            }
            return totalStr;
        }


        public List<TicketTotalModel> GetTicketTotal(string searchYear)
        {
            var SQL = @"SELECT 
                              SUBSTRING(CONVERT(nvarchar(20),CreateTime,112),0,7) as [Month]
							  ,CONVERT(decimal(18,2),SUM(ISNULL(TicketMoney,0))/10000) as TicketAmount
							  ,CONVERT(decimal(18,2),(ISNULL(SUM(Brokerage),0)+ISNULL(SUM(LaSaTicketNum),0)+ISNULL(SUM(GroupTicketNum),0))/10000) as [Profit]
                              FROM [TicketManage].[dbo].[TicketCustomer]
                              WHERE SUBSTRING(CONVERT(nvarchar(20),CreateTime,120),0,5)='{0}'
                              GROUP BY SUBSTRING(CONVERT(nvarchar(20),CreateTime,112),0,7)
                              Order by SUBSTRING(CONVERT(nvarchar(20),CreateTime,112),0,7) asc";
            var tempList = DBContext.Database.SqlQuery<TicketTotalModel>(string.Format(SQL, searchYear)).ToList();
            return tempList;
        }

        /// <summary>
        /// 获取全部客票售票信息
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        public object GetTicketInfoDetial(string month)
        {
            var num = 0;
            var monthAmount = 0m;
            if (string.IsNullOrEmpty(month) || !IsNumber(month))
            {
                month = DateTime.Now.ToString("yyyyMM");
            }

            var startDate = Convert.ToDateTime(string.Format("{0}/{1}/01", month.Substring(0, 4), month.Substring(4, 2)));
            var endDate = startDate.AddMonths(1).AddDays(-1);
            var tempNumModel = DBContext.TicketCustomers.Where(a => a.CreateTime >= startDate && a.CreateTime <= endDate).Sum(a => a.TicketNum);
            num = tempNumModel ?? 0;
            var tempmonthAmount = DBContext.TicketCustomers.Where(a => a.CreateTime >= startDate && a.CreateTime <= endDate).Sum(a => a.Brokerage);
            monthAmount = tempmonthAmount ?? 0;
            var startNum = DBContext.SalePointInfoes.Where(a => a.CreateTime >= startDate && a.CreateTime <= endDate && a.SalePointState == true).Count();
            var endNum = DBContext.SalePointInfoes.Where(a => a.CreateTime >= startDate && a.CreateTime <= endDate && a.SalePointState != true).Count();
            var totalNumObj = DBContext.TicketCustomers.Where(a => startDate <= a.CreateTime && a.CreateTime <= endDate);
            var totalNum = 0;
            if (totalNumObj != null)
            {
                totalNum = totalNumObj.Sum(a => a.TicketNum) ?? 0;
            }
            var totalAmountObj = totalNumObj;
            var totalAmount = 0m;
            if (totalAmountObj != null)
            {
                totalAmount = totalAmountObj.Sum(a => a.Brokerage) ?? 0m;
            }
            return new
            {
                Month = month,
                StartNum = startNum,
                EndNum = endNum,
                TicketNum = totalNum,
                TicketAmount = totalAmount
            };
        }

        /// <summary>
        /// 票务累计收入对比
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="companyName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public object GetTicketTotalAmountCompare(string groupName, string companyName, string startDate, string endDate)
        {

            var SQLContent = @"select 
                                            SUBSTRING(CONVERT(nvarchar(20),a.CreateTime,112),0,7) as [Month]
                                            ,d.GroupName as GroupName
                                            ,c.CompanyName as CompanyName
                                            ,ISNULL(sum(a.Brokerage),0) as TotalAmount 
                                            ,ISNULL(SUM(a.TicketMoney),0) as profit
                                            from [TicketManage].[dbo].[TicketCustomer] as a
                                            left join [TicketManage].[dbo].[UserInfo] as b
                                            on a.OperationSEQ=b.SEQ
                                            left join [TicketManage].[dbo].[CompanyInfo] as c
                                            on b.CompanySEQ=c.SEQ
                                            left join [TicketManage].[dbo].[GroupInfo] as d
                                            on b.GroupSEQ=d.SEQ
                                            where c.CompanyName is not null";
            var ExecuteSQL = new StringBuilder(SQLContent);
            if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
            {
                var TempStartDate = Convert.ToDateTime(String.Format("{0:####-##-##}", Int32.Parse(startDate)));
                var TempEndDate = Convert.ToDateTime(String.Format("{0:####-##-##}", Int32.Parse(endDate)));
                ExecuteSQL.AppendFormat(" and a.CreateTime between '{0}' and '{1}'", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
            }
            if (!string.IsNullOrEmpty(companyName))
            {
                ExecuteSQL.AppendFormat(" and c.CompanyName='{0}'", companyName);
            }
            if (!string.IsNullOrEmpty(groupName))
            {
                ExecuteSQL.AppendFormat(" and d.GroupName='{0}'", groupName);
            }
            ExecuteSQL.Append(" group by SUBSTRING(CONVERT(nvarchar(20),a.CreateTime,112),0,7),c.CompanyName,d.GroupName");
            var tempModel = DBContext.Database.SqlQuery<TicketTotalAmountCompareModel>(ExecuteSQL.ToString()).ToList();
            return tempModel;
        }

        /// <summary>
        /// 票务统计 票务收入 接口14
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="companyName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public object GetTicketTotalAmount(string groupName, string companyName, string startDate, string endDate)
        {
            var SQLContent = @"select d.GroupName as GroupName ,c.CompanyName as CompanyName";
            var ExecuteSQL = new StringBuilder(SQLContent);
            if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
            {
                var TempStartDate = Convert.ToDateTime(String.Format("{0:####-##-##}", Int32.Parse(startDate)));
                var TempEndDate = Convert.ToDateTime(String.Format("{0:####-##-##}", Int32.Parse(endDate)));
                var TempCurrentYear = Convert.ToDateTime(String.Format("{0}-01-01", TempStartDate.Year));
                ExecuteSQL.AppendFormat(",sum(case when a.CreateTime>='{0}' and a.CreateTime<='{1}' then a.Brokerage else 0 end) as MonthlyTotalAmount", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(",sum(case when a.CreateTime>='{0}' and a.CreateTime<='{1}' then a.Brokerage else 0 end) as MonthlyPreTotalAmount", TempStartDate.AddMonths(-1).ToString("yyyy-MM-dd"), TempStartDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(",sum(case when a.CreateTime>='{0}' and a.CreateTime<='{1}' then a.Brokerage else 0 end) as YearTotalAmount", TempCurrentYear.ToString("yyyy-MM-dd"), TempCurrentYear.AddYears(1).AddDays(-1).ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(",sum(case when a.CreateTime>='{0}' and a.CreateTime<='{1}' then a.Brokerage else 0 end) as YearPreTotalAmount", TempCurrentYear.AddYears(-1).ToString("yyyy-MM-dd"), TempCurrentYear.AddDays(-1).ToString("yyyy-MM-dd"));
            }
            else
            {
                var TempStartDate = DateTime.Now.ToString("yyyy-MM-01");
                var TempEndDate = Convert.ToDateTime(TempStartDate).AddDays(-1).ToString("yyyy-MM-dd");
                var TempCurrentYear = DateTime.Now.ToString("yyyy-01-01");
                ExecuteSQL.AppendFormat(",sum(case when a.CreateTime>='{0}' and a.CreateTime<='{1}' then a.Brokerage else 0 end) as MonthlyTotalAmount", TempStartDate, TempEndDate);
                ExecuteSQL.AppendFormat(",sum(case when a.CreateTime>='{0}' and a.CreateTime<='{1}' then a.Brokerage else 0 end) as MonthlyPreTotalAmount", Convert.ToDateTime(TempStartDate).AddMonths(-1).ToString("yyyy-MM-dd"), TempStartDate);
                ExecuteSQL.AppendFormat(",sum(case when a.CreateTime>='{0}' and a.CreateTime<='{1}' then a.Brokerage else 0 end) as YearTotalAmount", TempCurrentYear, Convert.ToDateTime(TempCurrentYear).AddYears(1).AddDays(-1).ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(",sum(case when a.CreateTime>='{0}' and a.CreateTime<='{1}' then a.Brokerage else 0 end) as YearPreTotalAmount", Convert.ToDateTime(TempCurrentYear).AddYears(-1).ToString("yyyy-MM-dd"), Convert.ToDateTime(TempCurrentYear).AddDays(-1).ToString("yyyy-MM-dd"));
            }
            ExecuteSQL.Append(@" from [TicketManage].[dbo].[TicketCustomer] as a
                                        left join [TicketManage].[dbo].[UserInfo] as b
                                        on a.OperationSEQ=b.SEQ
                                        left join [TicketManage].[dbo].[CompanyInfo] as c
                                        on b.CompanySEQ=c.SEQ
                                        left join [TicketManage].[dbo].[GroupInfo] as d
                                        on b.GroupSEQ=d.SEQ
                                        where c.CompanyName is not null");
            if (!string.IsNullOrEmpty(companyName))
            {
                ExecuteSQL.AppendFormat(" and c.CompanyName='{0}'", companyName);
            }
            if (!string.IsNullOrEmpty(groupName))
            {
                ExecuteSQL.AppendFormat(" and d.GroupName='{0}'", groupName);
            }
            ExecuteSQL.Append(" group by c.CompanyName,d.GroupName");
            var tempModel = DBContext.Database.SqlQuery<TicketTotalAmountModel>(ExecuteSQL.ToString()).ToList();
            return tempModel;
        }

        public object GetTicketTotalNum(string groupName, string companyName, string startDate, string endDate)
        {

            var SQLContent = @"select SUBSTRING(CONVERT(nvarchar(20),a.CreateTime,112),0,7) as [Month],d.GroupName as GroupName,c.CompanyName as CompanyName";
            var ExecuteSQL = new StringBuilder(SQLContent);
            if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
            {
                var TempStartDate = Convert.ToDateTime(String.Format("{0:####-##-##}", Int32.Parse(startDate)));
                var TempEndDate = Convert.ToDateTime(String.Format("{0:####-##-##}", Int32.Parse(endDate)));
                var TempCurrentYear = Convert.ToDateTime(string.Format("{0}-01-01", TempStartDate.Year));
                ExecuteSQL.AppendFormat(",sum(case when a.CreateTime>='{0}' and a.CreateTime<='{1}' then a.TicketNum else 0 end) as MonthlyTicketNum", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                //ExecuteSQL.AppendFormat(",sum(case when a.CreateTime>='{0}' and a.CreateTime<='{1}' then a.TicketNum else 0 end) as MonthlyPreTicketNum", TempStartDate.AddMonths(-1).ToString("yyyy-MM-dd"), TempStartDate.ToString("yyyy-MM-dd"));
                //ExecuteSQL.AppendFormat(",sum(case when a.CreateTime>='{0}' and a.CreateTime<='{1}' then a.TicketNum else 0 end) as YearCurrentTicketNum", TempCurrentYear.ToString("yyyy-MM-dd"), TempCurrentYear.AddYears(1).AddDays(-1).ToString("yyyy-MM-dd"));
                //ExecuteSQL.AppendFormat(",sum(case when a.CreateTime>='{0}' and a.CreateTime<='{1}' then a.TicketNum else 0 end) as YearPreTicketNum", TempCurrentYear.AddYears(-1).ToString("yyyy-MM-dd"), TempCurrentYear.AddDays(-1).ToString("yyyy-MM-dd"));
            }
            ExecuteSQL.Append(@" from [TicketManage].[dbo].[TicketCustomer] as a
                                        left join [TicketManage].[dbo].[UserInfo] as b
                                        on a.OperationSEQ=b.SEQ
                                        left join [TicketManage].[dbo].[CompanyInfo] as c
                                        on b.CompanySEQ=c.SEQ
                                        left join [TicketManage].[dbo].[GroupInfo] as d
                                        on b.GroupSEQ=d.SEQ
                                        where c.CompanyName is not null");
            if (!string.IsNullOrEmpty(companyName))
            {
                ExecuteSQL.AppendFormat(" and c.CompanyName='{0}'", companyName);
            }
            if (!string.IsNullOrEmpty(groupName))
            {
                ExecuteSQL.AppendFormat(" and d.GroupName='{0}'", groupName);
            }
            ExecuteSQL.Append(" group by SUBSTRING(CONVERT(nvarchar(20),a.CreateTime,112),0,7),c.CompanyName,d.GroupName");
            var tempModel = DBContext.Database.SqlQuery<TicketTotalNumModel>(ExecuteSQL.ToString()).ToList();
            return tempModel;
        }

        /// <summary>
        /// 票务收入统计接口16
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="companyName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public Object GetTicketTotalAmountDetail(string groupName, string companyName, string startDate, string endDate)
        {
            var SQLContent = @"select SUBSTRING(CONVERT(nvarchar(20),a.CreateTime,112),0,7) as [Month],GroupName as GroupName,e.CompanyName as CompanyName";
            var ExecuteSQL = new StringBuilder(SQLContent);
            if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
            {
                var TempStartDate = Convert.ToDateTime(String.Format("{0:####-##-##}", Int32.Parse(startDate)));
                var TempEndDate = Convert.ToDateTime(String.Format("{0:####-##-##}", Int32.Parse(endDate)));
                var TempCurrentYear = Convert.ToDateTime(string.Format("{0}-01-01", TempStartDate.Year));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then 1 else 0 end) as RunNum ", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then 1 else 0 end) as PreDateRunNum ", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.TicketNum else 0 end) as TicketNum ", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.TicketNum else 0 end)/30 as AvgTicketNum ", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.TicketNum else 0 end) as PreDateTicketNum ", TempStartDate.AddMonths(-1).ToString("yyyy-MM-dd"), TempEndDate.AddMonths(-1).ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.TicketNum else 0 end)/30 as PreDateAvgTicketNum ", TempStartDate.AddMonths(-1).ToString("yyyy-MM-dd"), TempEndDate.AddMonths(-1).ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.TicketNum else 0 end) as TotalTicketNum ", TempCurrentYear.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.TicketNum else 0 end) as TotalPreDateTicketNum ", TempCurrentYear.AddYears(-1).ToString("yyyy-MM-dd"), TempCurrentYear.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.Brokerage else 0 end) as MonthAmount ", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.Brokerage else 0 end) as MonthPreAmount ", TempStartDate.AddMonths(-1).ToString("yyyy-MM-dd"), TempEndDate.AddMonths(-1).ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.Brokerage else 0 end) as TotalAmount ", TempCurrentYear.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.Brokerage else 0 end) as TotalPreAmount ", TempCurrentYear.AddYears(-1).ToString("yyyy-MM-dd"), TempCurrentYear.ToString("yyyy-MM-dd"));
            }
            else
            {

                var TempStartDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-01"));
                var TempEndDate = TempStartDate.AddMonths(1).AddDays(-1);
                var TempCurrentYear = Convert.ToDateTime(string.Format("{0}-01-01", TempStartDate.Year));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then 1 else 0 end) as RunNum ", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then 1 else 0 end) as PreDateRunNum ", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.TicketNum else 0 end) as TicketNum ", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.TicketNum else 0 end)/30 as AvgTicketNum ", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.TicketNum else 0 end) as PreDateTicketNum ", TempStartDate.AddMonths(-1).ToString("yyyy-MM-dd"), TempEndDate.AddMonths(-1).ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.TicketNum else 0 end)/30 as PreDateAvgTicketNum ", TempStartDate.AddMonths(-1).ToString("yyyy-MM-dd"), TempEndDate.AddMonths(-1).ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.TicketNum else 0 end) as TotalTicketNum ", TempCurrentYear.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.TicketNum else 0 end) as TotalPreDateTicketNum ", TempCurrentYear.AddYears(-1).ToString("yyyy-MM-dd"), TempCurrentYear.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.Brokerage else 0 end) as MonthAmount ", TempStartDate.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.Brokerage else 0 end) as MonthPreAmount ", TempStartDate.AddMonths(-1).ToString("yyyy-MM-dd"), TempEndDate.AddMonths(-1).ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.Brokerage else 0 end) as TotalAmount ", TempCurrentYear.ToString("yyyy-MM-dd"), TempEndDate.ToString("yyyy-MM-dd"));
                ExecuteSQL.AppendFormat(", SUM(case when b.CreateTime>='{0}' and b.CreateTime<='{1}' then d.Brokerage else 0 end) as TotalPreAmount ", TempCurrentYear.AddYears(-1).ToString("yyyy-MM-dd"), TempCurrentYear.ToString("yyyy-MM-dd"));

            }
            ExecuteSQL.Append(@" from [TicketManage].[dbo].[GroupInfo] as a
                                            left join [TicketManage].[dbo].[SalePointInfo] as b
                                            on a.SEQ=b.GroupSEQ
                                            left join [TicketManage].[dbo].[UserInfo] as c
                                            on c.GroupSEQ=a.SEQ
                                            left join [TicketManage].dbo.TicketCustomer as d
                                            on c.SEQ=d.OperationSEQ
                                            left join [TicketManage].[dbo].[CompanyInfo] as e
											on b.CompanySEQ=e.SEQ 
                                            where e.CompanyName is not null ");
            if (!string.IsNullOrEmpty(companyName))
            {
                ExecuteSQL.AppendFormat(" and c.CompanyName='{0}'", companyName);
            }
            if (!string.IsNullOrEmpty(groupName))
            {
                ExecuteSQL.AppendFormat(" and d.GroupName='{0}'", groupName);
            }
            ExecuteSQL.Append(" group by SUBSTRING(CONVERT(nvarchar(20),a.CreateTime,112),0,7),a.GroupName,e.CompanyName");
            var tempModel = DBContext.Database.SqlQuery<TicketTotalNumModel16>(ExecuteSQL.ToString()).ToList();
            return tempModel;
        }


        /// <summary>
        /// 查看用户当前日期是否已经保存过数据
        /// </summary>
        /// <param name="userNo">登陆名</param>
        /// <param name="createDate">数据创建时间</param>
        /// <returns></returns>
        public Boolean CheckTicketUser(string userNo, string createDate)
        {
            var flag = false;
            var tempModel = DBContext.UserInfoes.Where(a => a.UserNo == userNo).FirstOrDefault();
            var tempDate = Convert.ToDateTime(createDate);
            if (tempModel != null)
            {
                var model = DBContext.TicketCustomers.Where(a => a.OperationSEQ == tempModel.SEQ && a.CreateTime == tempDate).FirstOrDefault();
                if (model != null)
                {
                    flag = true;
                }
            }
            return flag;
        }

        /// <summary>
        /// 删除指定用户某一天的票务数据
        /// </summary>
        /// <param name="userNo">登陆名</param>
        /// <param name="createDate">票务数据创建时间</param>
        /// <returns></returns>
        public Boolean DelTicketUser(string userNo, string createDate)
        {
            var flag = false;
            var tempModel = DBContext.UserInfoes.Where(a => a.UserNo == userNo).FirstOrDefault();
            var tempDate = Convert.ToDateTime(createDate);
            if (tempModel != null)
            {
                var model = DBContext.TicketCustomers.Where(a => a.OperationSEQ == tempModel.SEQ && a.CreateTime == tempDate).FirstOrDefault();
                if (model != null)
                {
                    DBContext.TicketCustomers.Remove(model);
                    DBContext.SaveChanges();
                    flag = true;
                }
            }
            return flag;
        }

        /// <summary>
        /// 获取当前登录用户的所属份层信息
        /// </summary>
        /// <param name="seq">UserSEQ</param>
        /// <returns></returns>
        public decimal GetDivideIntoByUserSEQ(int seq)
        {
            var tempDivideIntoVal = 0m;
            var currentUserModel = DBContext.UserInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
            if (currentUserModel != null)
            {
                var salePointModel = DBContext.SalePointInfoes.Where(a => a.SEQ == currentUserModel.SalePointSEQ).FirstOrDefault();

                if (salePointModel != null
                    && !string.IsNullOrEmpty(salePointModel.DivideInto)
                    && (CommonHelper.IsNumeric(salePointModel.DivideInto) || CommonHelper.IsInt(salePointModel.DivideInto)))
                {
                    tempDivideIntoVal = Convert.ToDecimal(salePointModel.DivideInto);
                }
            }
            return tempDivideIntoVal;
        }


        /// <summary>
        /// 添加客票信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean AddTicket(TicketIndexPageModel model)
        {
            try
            {
                var entry = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0)
                {
                    model.PageModel.Brokerage = (((model.PageModel.TicketNum ?? 0) - (model.PageModel.KidTicketNum ?? 0)) * (model.PageModel.DivideInto ?? 0) + (model.PageModel.GroupTicketNum ?? 0) * 5 + (model.PageModel.LaSaTicketNum ?? 0) * 5);
                    entry.State = EntityState.Added;
                }
                else
                {
                    entry.State = EntityState.Modified;
                }
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="seq">实体序号</param>
        /// <returns></returns>
        public Boolean DelTicket(int seq)
        {
            try
            {
                var model = DBContext.TicketCustomers.Where(a => a.SEQ == seq).FirstOrDefault();
                if (model != null)
                {
                    DBContext.TicketCustomers.Remove(model);
                    DBContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 删除儿童票信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean DelTicketKid(int seq)
        {
            try
            {
                var model = DBContext.KidTicketInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
                if (model != null)
                {
                    DBContext.KidTicketInfoes.Remove(model);
                    DBContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 编辑票务信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditTicket(TicketIndexPageModel model)
        {
            var tempModel = DBContext.TicketCustomers.Where(a => a.SEQ == model.PageModel.SEQ).FirstOrDefault();
            if (tempModel != null)
            {
                tempModel.StartTicketNo = model.PageModel.StartTicketNo;
                tempModel.EndTicketNo = model.PageModel.EndTicketNo;
                tempModel.TicketNum = model.PageModel.TicketNum;
                tempModel.KidTicketNum = model.PageModel.KidTicketNum;
                model.PageModel.Brokerage = model.PageModel.TicketNum * 5;
                tempModel.InsuranceMoney = model.PageModel.InsuranceMoney;
                DBContext.SaveChanges();
            }
            return true;
        }

        /// <summary>
        /// 获取全部客票信息
        /// </summary>
        /// <returns></returns>
        public List<TicketCustomerPageModel> GetTicketByAll(TicketIndexPageModel model)
        {
            var modelList = new List<TicketCustomerPageModel>();
            var tempSQL = new StringBuilder(SQL);
            if (model.PageModel.OperationSEQ != 0)
            {
                tempSQL = tempSQL.AppendFormat(" and a.OperationSEQ={0}", model.PageModel.OperationSEQ);
                modelList.AddRange(DBContext.Database.SqlQuery<TicketCustomerPageModel>(tempSQL.ToString()).ToList());
            }
            else
            {
                modelList.AddRange(DBContext.Database.SqlQuery<TicketCustomerPageModel>(SQL).ToList());
            }
            model.PageNum = modelList.Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 根据条件查询
        /// </summary>
        /// <param name="model">页面获取实体</param>
        /// <returns></returns>
        public List<TicketCustomerPageModel> GetTicketByCondition(TicketIndexPageModel model)
        {
            var finalSQL = new StringBuilder();
            var conditionSQL = new StringBuilder();

            if (model.CompanySEQ != 0 && model.CompanySEQ != null)
            {
                conditionSQL.AppendFormat(" and CompanySEQ={0}", model.CompanySEQ);
            }
            if (model.SalePointSEQ != 0)
            {
                conditionSQL.AppendFormat(" and SalePointSEQ={0}", model.SalePointSEQ);
            }
            if (!string.IsNullOrEmpty(model.StartDate) && !string.IsNullOrEmpty(model.EndDate))
            {
                conditionSQL.AppendFormat(" and CreateTime between '{0}' and '{1}' ", model.StartDate, model.EndDate);
            }

            model.PageNum = DBContext.Database.SqlQuery<TicketCustomerPageModel>(finalSQL.AppendFormat(SQL, conditionSQL).ToString()).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var modelList = DBContext.Database.SqlQuery<TicketCustomerPageModel>(finalSQL.AppendFormat(SQL, conditionSQL).ToString())
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
                .Take(model.PageSize).ToList();
            model.TicketNumTotal = DBContext.Database.SqlQuery<TicketCustomerPageModel>(finalSQL.AppendFormat(SQL, conditionSQL).ToString()).Sum(a => a.TicketNum);
            model.TicketMoneyTotal = DBContext.Database.SqlQuery<TicketCustomerPageModel>(finalSQL.AppendFormat(SQL, conditionSQL).ToString()).Sum(a => a.TicketMoney);
            model.TicketInComeTotal = DBContext.Database.SqlQuery<TicketCustomerPageModel>(finalSQL.AppendFormat(SQL, conditionSQL).ToString()).Sum(a => (a.TicketNum ?? 0 - a.LaSaTicketNum ?? 0 - a.GroupTicketNum ?? 0 - a.KidTicketNum ?? 0) * a.DivideInto ?? 0 + (a.LaSaTicketNum ?? 0 * 5) + (a.GroupTicketNum ?? 0 * 5));
            return modelList;
        }

        /// <summary>
        /// 信息列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<TicketCustomerPageModel> GetTicketDetailByCondition(TicketIndexPageModel model)
        {
            var conditionSQL = new StringBuilder();

            if (model.SalePointSEQ != 0)
            {
                conditionSQL.AppendFormat(" and d.SEQ={0}", model.SalePointSEQ);
            }
            if (!string.IsNullOrEmpty(model.OperaotrDate))
            {
                conditionSQL.AppendFormat(" and CONVERT(nvarchar(12),a.CreateTime,111)='{0}'", model.OperaotrDate);
            }
            model.PageNum = DBContext.Database.SqlQuery<TicketCustomerPageModel>(string.Format(TICKETDDETAILSQL, conditionSQL)).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var modelList = DBContext.Database.SqlQuery<TicketCustomerPageModel>(string.Format(TICKETDDETAILSQL, conditionSQL))
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }


        public TicketCustomer GetTicketById(int seq)
        {
            var tempModel = DBContext.TicketCustomers.Where(a => a.SEQ == seq).FirstOrDefault();
            return tempModel;
        }

        /// <summary>
        /// 创建儿童票
        /// </summary>
        /// <param name="seq">附属客票编号</param>
        /// <param name="list">儿童票图片集合</param>
        /// <returns></returns>
        public Boolean AddKidTicket(int seq, Dictionary<string, string> list)
        {
            foreach (var item in list)
            {
                var tempModel = new KidTicketInfo();
                tempModel.CreateTIme = DateTime.Now;
                tempModel.TicketOutTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                tempModel.KidTicketContent = item.Key;
                tempModel.TicketCustomerSEQ = seq;
                DBContext.KidTicketInfoes.Add(tempModel);
            }
            DBContext.SaveChanges();
            return true;
        }

        /// <summary>
        /// 获取制定儿童票
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public List<KidTicketInfo> GetKidTicketByTicketSEQ(int seq, KidTicketListPageModel pageModel)
        {

            pageModel.PageNum = DBContext.KidTicketInfoes.Where(a => a.TicketCustomerSEQ == seq).Count();
            pageModel.PageTotal = (pageModel.PageNum + pageModel.PageSize - 1) / pageModel.PageSize;
            var resultList = DBContext.KidTicketInfoes
                .Where(a => a.TicketCustomerSEQ == seq)
                 .OrderByDescending(a => a.CreateTIme)
                 .Skip(pageModel.PageSize * (pageModel.CurrentPage - 1))
                .Take(pageModel.PageSize).ToList();
            return resultList;
        }

        /// <summary>
        /// 获取制定儿童票
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public List<KidTicketInfo> GetKidTicketBySalePointSEQ(int seq, KidTicketListPageModel pageModel)
        {
            var ListUserSEQ = DBContext.UserInfoes.Where(a => a.SalePointSEQ == seq).Select(a => a.SEQ).ToList();
            var ListTicketSEQ = DBContext.TicketCustomers.Where(a => ListUserSEQ.Contains(a.OperationSEQ ?? 0)).Select(a => a.SEQ).ToList();
            pageModel.PageNum = DBContext.KidTicketInfoes.Where(a => ListTicketSEQ.Contains(a.TicketCustomerSEQ ?? 0)).Count();
            pageModel.PageTotal = (pageModel.PageNum + pageModel.PageSize - 1) / pageModel.PageSize;
            var resultList = DBContext.KidTicketInfoes
                .Where(a => ListTicketSEQ.Contains(a.TicketCustomerSEQ ?? 0))
                 .OrderByDescending(a => a.CreateTIme)
                 .Skip(pageModel.PageSize * (pageModel.CurrentPage - 1))
                .Take(pageModel.PageSize).ToList();
            return resultList;
        }


        #region 统计票价

        public object TicketCalcTotal()
        {
            return null;
        }

        #endregion

    }
}
