﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;

namespace TieckManage.Biz.TicketManage
{
    public class TicketUnionBedBiz : BaseDataManageBiz
    {
        public TicketUnionBed GetTicketUnionBedById(int seq)
        {
            var model = DBContext.TicketUnionBeds.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean TicketUnionBedDel(int seq)
        {
            var model = DBContext.TicketUnionBeds.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.TicketUnionBeds.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        /// 查询指定实体
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        public List<TicketUnionBed> GetTicketUnionBedByCondition(TicketUnionBedPageModel pageModel)
        {
            var SQLCONTEXT = new StringBuilder("select * from TicketUnionBed where 1=1 ");
            if (!string.IsNullOrEmpty(pageModel.StartDate) && !string.IsNullOrEmpty(pageModel.EndDate))
            {
                var tempStartDate = Convert.ToDateTime(pageModel.StartDate);
                var tempEndDate = Convert.ToDateTime(pageModel.EndDate);
                SQLCONTEXT.AppendFormat(" and CreateTime>='{0}' ", tempStartDate);
                SQLCONTEXT.AppendFormat(" and CreateTime<='{0}' ", tempEndDate);
            }
            if (pageModel.OperatorUserNoList != null && pageModel.OperatorUserNoList.Count > 0)
            {
                SQLCONTEXT.AppendFormat(" and OperationSEQ in ({0})", ConvertStringListToContext(pageModel.OperatorUserNoList));
            }
            pageModel.PageNum = DBContext.Database.SqlQuery<TicketUnionBed>(SQLCONTEXT.ToString()).Count();
            pageModel.PageTotal = (pageModel.PageNum + pageModel.PageSize - 1) / pageModel.PageSize;
            var modelList = DBContext.Database
                     .SqlQuery<TicketUnionBed>(SQLCONTEXT.ToString())
                     .OrderBy(a => a.CreateTime)
                     .Skip(pageModel.PageSize * (pageModel.CurrentPage - 1))
                     .Take(pageModel.PageSize)
                     .ToList();
            return modelList;
        }

        public Boolean TicketUnionBedAdd(TicketUnionBedPageModel pageModel)
        {
            try
            {
                DBContext.TicketUnionBeds.Add(pageModel.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public Boolean TicketUnionBedEdit(TicketUnionBedPageModel pageModel)
        {
            try
            {
                var model = GetTicketUnionBedById(pageModel.PageModel.SEQ);
                model.TicketNumAll = pageModel.PageModel.TicketNumAll;
                model.TicketNumBad = pageModel.PageModel.TicketNumBad;
                model.StartNoTicket = pageModel.PageModel.StartNoTicket;
                model.MonthAcount = pageModel.PageModel.MonthAcount;

                DBContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
