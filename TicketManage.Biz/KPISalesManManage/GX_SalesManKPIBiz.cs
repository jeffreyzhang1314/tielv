﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.KPISalesManManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.KPISalesManManage
{
    /// <summary>
    /// 计调人员功效业务实体
    /// </summary>
    public class GX_SalesManKPIBiz : BaseDataManageBiz
    {

        /// <summary>
        /// 添加计调人员功效
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean AddGX_SalesManKPI(GX_SalesManKPIPageModel model)
        {
            try
            {
                //DBContext.GX_SalesManKPI.Add(model.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除计调人员功效
        /// </summary>
        /// <param name="seq">计调人员序号</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean DelGX_SalesManKPI(int seq)
        {
            //var model = DBContext.GX_SalesManKPI.Where(a => a.SEQ == seq).FirstOrDefault();
            //if (model != null)
            //{
            //    DBContext.GX_SalesManKPI.Remove(model);
            //    DBContext.SaveChanges();
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return false;
        }

        /// <summary>
        /// 编辑计调人员功效
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditGX_SalesManKPI(GX_SalesManKPIPageModel model)
        {
            try
            {
                if (model.PageModel.SEQ != 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "修改成功";
                    return true;
                }
                else if (model.PageModel.SEQ == 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Added;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "未找到实体对象";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        /// <summary>
        /// 根据条件查询计调人员功效
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_SalesManKPI> SearchGX_SalesManKPIByCondition(GX_SalesManKPIPageModel model)
        {
            var SQL = new StringBuilder("select * from GX_SalesManKPI where 1=1");
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.OperatorYear))
            {
                SQL.AppendFormat(" and OperatorYear='{0}'", model.PageModel.OperatorYear);
            }
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.OperatorMonth))
            {
                SQL.AppendFormat(" and OperatorMonth='{0}'", model.PageModel.OperatorMonth);
            }

            var modelList = DBContext.Database.SqlQuery<GX_SalesManKPI>(SQL.ToString()).ToList();
            model.PageNum = modelList.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderBy(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }



        /// <summary>
        /// 获取制定序号的计调人员功效
        /// </summary>
        /// <param name="seq">计调人员功效信息</param>
        /// <returns></returns>
        public GX_SalesManKPI GetGX_SalesManKPIBySEQ(int seq)
        {
            //var model = DBContext.GX_SalesManKPI.Where(a => a.SEQ == seq).FirstOrDefault();
            return null;//model;
        }
    }
}
