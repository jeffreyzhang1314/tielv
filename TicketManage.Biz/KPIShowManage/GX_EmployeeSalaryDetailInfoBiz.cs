﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.KPIShowManage
{
    public class GX_EmployeeSalaryDetailInfoBiz : BaseDataManageBiz
    {
        /// <summary>E:\Project\TicketManage\Source2\tielv\TicketManage.Biz\KPIShowManage\GX_SalesManKPIBiz.cs
        /// 添加实体
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean GX_EmployeeSalaryDetailInfoInsert(GX_EmployeeSalaryDetailInfo model)
        {
            try
            {
                model.CreateTime = DateTime.Now;
                DBContext.GX_EmployeeSalaryDetailInfo.Add(model);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 保存所有上传数据
        /// </summary>
        /// <param name="modelList"></param>
        /// <returns></returns>
        public Boolean SaveUploadExcel(List<GX_TicketSellerSalaryDetailInfo> modelList)
        {
            foreach (var item in modelList)
            {
                DBContext.GX_TicketSellerSalaryDetailInfo.Add(item);
            }
            DBContext.SaveChanges();
            return true;
        }

        /// <summary>
        /// 删除指定序号实体
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean GX_EmployeeSalaryDetailInfoDelete(int seq, ref string message)
        {
            var model = DBContext.GX_EmployeeSalaryDetailInfo.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                try
                {
                    var tempYear = Convert.ToString(model.OperatorYear);
                    var tempMonth = Convert.ToString(model.OperatorMonth);
                    var modelReport = DBContext.GX_SalesmanSalaryDetailInfoReport
                        .Where(a => a.OperatorYear == tempYear && a.OperatorMonth == tempMonth)
                        .FirstOrDefault();
                    if (modelReport != null && (modelReport.SubmitState ?? false))
                    {
                        message = "数据已经进入审批流程无法修改~！";
                        return false;
                    }
                    else
                    {
                        DBContext.GX_EmployeeSalaryDetailInfo.Remove(model);
                        DBContext.SaveChanges();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                message = "未找到数据~！";
                return false;
            }
        }

        /// <summary>
        /// 编辑指定序号的实体
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean GX_EmployeeSalaryDetailInfoEdit(GX_EmployeeSalaryDetailInfo model)
        {
            var entry = DBContext.Entry(model);
            if (model != null)
            {
                try
                {
                    if (model.SEQ != 0)
                    {
                        entry.State = EntityState.Modified;
                        DBContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 查询指定条件内容
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_EmployeeSalaryDetailInfo> GX_EmployeeSalaryDetailInfoSearch(GX_EmployeeSalaryDetailInfo model)
        {
            var SQLSTR = new StringBuilder("SELECT * FROM GX_EmployeeSalaryDetailInfo WHERE 1=1 ");
            if (model != null && model.OperatorYear != null)
            {
                SQLSTR.AppendFormat(" and OperatorYear='{0}' ", model.OperatorYear);
            }
            if (model != null && model.OperatorMonth != null)
            {
                SQLSTR.AppendFormat(" and OperatorMonth='{0}' ", model.OperatorMonth);
            }
            if (model != null && model.CompanySEQ != null)
            {
                SQLSTR.AppendFormat(" and CompanySEQ={0} ", model.CompanySEQ);
            }
            if (model != null && !string.IsNullOrEmpty(model.CompanyName))
            {
                SQLSTR.AppendFormat(" and CompanyName='{0}' ", model.CompanyName);
            }
            var tempList = DBContext.Database.SqlQuery<GX_EmployeeSalaryDetailInfo>(SQLSTR.ToString()).ToList();
            return tempList;
        }
    }
}
