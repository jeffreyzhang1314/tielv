﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.KPIShowManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.KPIShowManage
{
    /// <summary>
    /// 计调人员业务类
    /// </summary>
    public class GX_SalesManKPIBiz : BaseDataManageBiz
    {
        #region 售票员业务

        /// <summary>
        /// 生成售票员报表信息
        /// </summary>
        /// <param name="year">年份</param>
        /// <param name="month">月份</param>
        /// <param name="userName"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public Boolean GX_TicketSellerSalaryReportInsert(string year, string month, string userName, ref string message)
        {
            var flag = false;
            var pageModel = DBContext.GX_TicketSellerSalaryReport.Where(a => a.OperatorYear == year && a.OperatorMonth == month).FirstOrDefault();
            if (pageModel == null)
            {
                var tempModel = new GX_TicketSellerSalaryReport();
                tempModel.OperatorYear = year;
                tempModel.OperatorMonth = month;
                tempModel.SubmitUserName = userName;
                tempModel.SubmitState = false;
                tempModel.CreateTime = DateTime.Now;
                flag = true;
                DBContext.GX_TicketSellerSalaryReport.Add(tempModel);
                DBContext.SaveChanges();
                message = "报表生成成功~！";
            }
            else
            {
                if (pageModel.SubmitState == true)
                {
                    message = "审批流程无法重复生成报表~！";
                }
                else
                {
                    message = "流程状态错误~！";
                }
            }
            return flag;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public List<GX_TicketSellerSalaryReport> GX_TicketSellerSalaryReportByYear(string year, string month)
        {
            if (string.IsNullOrEmpty(year) || string.IsNullOrEmpty(month))
            {
                var list = DBContext.GX_TicketSellerSalaryReport.ToList();
                return list;
            }
            else
            {
                var list = DBContext.GX_TicketSellerSalaryReport.Where(a => a.OperatorYear == year && a.OperatorMonth == month).ToList();
                return list;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public GX_TicketSellerSalaryReport GX_TicketSellerSalaryReportBySEQ(int seq)
        {
            var tempObj = DBContext.GX_TicketSellerSalaryReport.Where(a => a.SEQ == seq).FirstOrDefault();
            return tempObj;
        }

        /// <summary>
        /// 删除指定序号报表
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean GX_TicketSellerSalaryDetailInfoReportDelete(int seq, ref string message)
        {
            var flag = false;
            var pageModel = DBContext.GX_TicketSellerSalaryReport.Where(a => a.SEQ == seq).FirstOrDefault();
            if (pageModel != null)
            {

                DBContext.GX_TicketSellerSalaryReport.Remove(pageModel);
                DBContext.SaveChanges();
                message = "删除成功~！";
                flag = true;
            }
            else
            {
                message = "未找到指定报表~！";
            }
            return flag;
        }

        /// <summary>
        /// 审批执行序号报表票务
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public Boolean GX_TicketSellerSalaryDetailInfoReportApprove(int seq, string type, string submitResult, string submitReason, string userName, ref string message)
        {
            var flag = false;
            var tempModel = DBContext.GX_TicketSellerSalaryReport.Where(a => a.SEQ == seq).FirstOrDefault();
            if (tempModel != null)
            {
                switch (type)
                {
                    case "Start":
                        if (tempModel.SubmitTimeUser == null)
                        {
                            tempModel.SubmitState = true;
                            tempModel.SubmitTimeUser = DateTime.Now;
                            tempModel.SubmitUserName = userName;
                        }
                        else
                        {
                            message = "不能重复提交审批流程~！";
                        }
                        break;
                    case "One":
                        if (tempModel.SubmitState != null && tempModel.SubmitState == true)
                        {
                            tempModel.NodeOneSubmitResult = submitResult;
                            tempModel.NodeOneSubmitReason = submitReason;
                            tempModel.NodeOneSubmitTime = DateTime.Now;
                            tempModel.NodeOneUserName = userName;
                        }
                        else
                        {
                            message = "没有提交审批流程暂时无法操作~！";
                        }
                        break;
                    case "Two":
                        if (tempModel.NodeOneSubmitTime != null)
                        {
                            tempModel.NodeTwoSubmitResult = submitResult;
                            tempModel.NodeTwoSubmitReason = submitReason;
                            tempModel.NodeTwoSubmitTime = DateTime.Now;
                            tempModel.NodeTwoUserName = userName;
                        }
                        else
                        {
                            message = "计调部长未审批暂时无法操作~！";
                        }
                        break;
                    case "Three":
                        if (tempModel.NodeTwoSubmitTime != null)
                        {
                            tempModel.NodeThreeSubmitResult = submitResult;
                            tempModel.NodeThreeSubmitReason = submitReason;
                            tempModel.NodeThreeSubmitTime = DateTime.Now;
                            tempModel.NodeThreeUserName = userName;
                        }
                        else
                        {
                            message = "主管计调副总未审批暂时无法操作~！";
                        }
                        break;
                    case "Four":
                        if (tempModel.NodeThreeSubmitTime != null)
                        {
                            tempModel.NodeFourSubmitResult = submitResult;
                            tempModel.NodeFourSubmitReason = submitReason;
                            tempModel.NodeFourSubmitTime = DateTime.Now;
                            tempModel.NodeFourUserName = userName;
                        }
                        else
                        {
                            message = "集团董事长未审批暂时无法操作~！";
                        }
                        break;
                }
                flag = true;
                DBContext.SaveChanges();
            }
            else
            {
                message = "未找到报表数据";
            }
            return flag;
        }

        /// <summary>
        /// 判断指定售票人员报表是否已经提交审批流程
        /// </summary>
        /// <param name="seq">报表序号</param>
        /// <param name="message">业务执行信息</param>
        /// <returns>是否进入审批流程：True已经进入审批流程False未进行审批</returns>
        public Boolean CheckGX_TicketSellerSalaryApprove(int seq, ref string message)
        {
            var flag = false;
            var tempModel = DBContext.GX_TicketSellerSalaryReport.Where(a => a.SEQ == seq).FirstOrDefault();
            if (tempModel != null)
            {
                if (tempModel.SubmitState ?? false)
                {
                    flag = true;
                    message = "已经进入审批流程无法删除~！";
                }
                else
                {
                    message = "未进入审批流程~！";
                }
            }
            else
            {
                message = "未找到指定报表~！";
            }
            return flag;
        }

        #endregion

        #region 计调业务

        /// <summary>
        /// 判断指定计调人员报表是否已经提交审批流程
        /// </summary>
        /// <param name="seq">报表序号</param>
        /// <param name="message">业务执行信息</param>
        /// <returns>是否进入审批流程：True已经进入审批流程False未进行审批</returns>
        public Boolean CheckGX_SalesManKPIApprove(int seq, ref string message)
        {
            var flag = false;
            var tempModel = DBContext.GX_SalesManKPIReport.Where(a => a.SEQ == seq).FirstOrDefault();
            if (tempModel != null)
            {
                if (tempModel.SubmitState ?? false)
                {
                    flag = true;
                    message = "已经进入审批流程无法删除~！";
                }
                else
                {
                    message = "未进入审批流程~！";
                }
            }
            else
            {
                message = "未找到指定报表~！";
            }
            return flag;
        }

        /// <summary>
        /// 删除计调人员绩效
        /// </summary>
        /// <param name="seq">绩效数据序号</param>
        /// <param name="message">操作信息</param>
        /// <returns>True删除成功False删除失败</returns>
        public Boolean GX_SalesManKPIReportDelete(int seq, ref string message)
        {
            var flag = false;
            var model = DBContext.GX_SalesManKPIReport.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.GX_SalesManKPIReport.Remove(model);
                DBContext.SaveChanges();
                flag = true;
            }
            else
            {
                message = "未找到数据序号~！";
            }
            return flag;
        }

        #endregion

        #region 前台销售人员业务

        /// <summary>
        /// 判断指定前台销售人员报表是否已经提交审批流程
        /// </summary>
        /// <param name="seq">报表序号</param>
        /// <param name="message">业务执行信息</param>
        /// <returns>是否进入审批流程：True已经进入审批流程False未进行审批</returns>
        public Boolean CheckGX_SalesmanSalaryApprove(int seq, ref string message)
        {
            var flag = false;
            var tempModel = DBContext.GX_SalesmanSalaryDetailInfoReport.Where(a => a.SEQ == seq).FirstOrDefault();
            if (tempModel != null)
            {
                if (tempModel.SubmitState ?? false)
                {
                    flag = true;
                    message = "已经进入审批流程无法删除~！";
                }
                else
                {
                    message = "未进入审批流程~！";
                }
            }
            else
            {
                message = "未找到指定报表~！";
            }
            return flag;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="userName"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public Boolean GX_TicketSellerSalaryDetailInfoReportInsert(string year, string month, string userName, ref string message)
        {
            var flag = false;
            //var pageModel = DBContext.GX_SalesmanSalaryDetailInfoReport.Where(a => a.OperatorYear == year && a.OperatorMonth == month).FirstOrDefault();
            //if (pageModel == null)
            //{
            //    var tempModel = new GX_SalesmanSalaryDetailInfoReport();
            //    tempModel.OperatorYear = year;
            //    tempModel.OperatorMonth = month;
            //    tempModel.SubmitUserName = userName;
            //    tempModel.SubmitState = false;
            //    tempModel.CreateTime = DateTime.Now;
            //    flag = true;
            //    DBContext.GX_SalesmanSalaryDetailInfoReport.Add(tempModel);
            //    DBContext.SaveChanges();
            //    message = "报表已生成~！";
            //}
            //else
            //{
            //    if (pageModel.SubmitState == true)
            //    {
            //        message = "审批流程无法重复生成报表~！";
            //    }
            //    else
            //    {
            //        message = "流程状态错误~！";
            //    }
            //}
            return flag;
        }

        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean GX_SalesManKPIInsert(GX_SalesManKPI model)
        {
            try
            {
                DBContext.GX_SalesManKPI.Add(model);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public List<GX_SalesManKPIReport> GX_SalesManKPIByYear(string year, string month)
        {
            if (string.IsNullOrEmpty(year) || string.IsNullOrEmpty(month))
            {
                var list = DBContext.GX_SalesManKPIReport.ToList();
                return list;
            }
            else
            {
                var list = DBContext.GX_SalesManKPIReport.Where(a => a.OperatorYear == year && a.OperatorMonth == month).ToList();
                return list;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public List<GX_SalesmanSalaryDetailInfoReport> GX_EmployeeSalaryDetailInfoByYear(GX_EmployeeSalaryDetailInfoReportModels model)
        {
            string year = model.SearchModel.OperatorYear;
            string month = model.SearchModel.OperatorMonth;
            var companySEQ = model.SearchModel.CompanySEQ ?? 0;
            string companyName = model.SearchModel.CompanyName;
            if (string.IsNullOrEmpty(year) || string.IsNullOrEmpty(month))
            {
                var list = DBContext.GX_SalesmanSalaryDetailInfoReport.ToList();
                return list;
            }
            else
            {
                if (companySEQ != 0 && !string.IsNullOrEmpty(companyName))
                {
                    var list = DBContext.GX_SalesmanSalaryDetailInfoReport
                        .Where(a => a.OperatorYear == year &&
                            a.OperatorMonth == month &&
                            a.CompanySEQ == companySEQ &&
                            a.CompanyName == companyName)
                        .ToList();
                    return list;
                }
                else
                {
                    var list = DBContext.GX_SalesmanSalaryDetailInfoReport.Where(a => a.OperatorYear == year && a.OperatorMonth == month).ToList();
                    return list;
                }

            }
        }

        /// <summary>
        /// 保存生成报表的数据
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="userName"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public Boolean GX_SalesManKPIReportInsert(string year, string month, string userName, ref string message)
        {
            var flag = false;
            var pageModel = DBContext.GX_SalesManKPIReport.Where(a => a.OperatorYear == year && a.OperatorMonth == month).FirstOrDefault();
            if (pageModel == null)
            {
                var tempModel = new GX_SalesManKPIReport();
                tempModel.OperatorYear = year;
                tempModel.OperatorMonth = month;
                tempModel.SubmitUserName = userName;
                tempModel.SubmitState = false;
                tempModel.CreateTime = DateTime.Now;
                flag = true;
                DBContext.GX_SalesManKPIReport.Add(tempModel);
                DBContext.SaveChanges();
                message = "报表生成成功~！";
            }
            else
            {
                if (pageModel.SubmitState == true)
                {
                    message = "审批流程无法重复生成报表~！";
                }
                else
                {
                    message = "流程状态错误~！";
                }
            }
            return flag;
        }

        /// <summary>
        /// 保存生成报表的数据
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="userName"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public Boolean GX_EmployeeSalaryDetailInfoReportDelete(int seq, ref string message)
        {
            var flag = false;
            var pageModel = DBContext.GX_SalesmanSalaryDetailInfoReport.Where(a => a.SEQ == seq).FirstOrDefault();
            if (pageModel != null)
            {

                DBContext.GX_SalesmanSalaryDetailInfoReport.Remove(pageModel);
                DBContext.SaveChanges();
                message = "删除成功~！";
                flag = true;
            }
            else
            {
                message = "未找到指定报表~！";
            }
            return flag;
        }

        /// <summary>
        /// 保存生成报表的数据
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="userName"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public Boolean GX_EmployeeSalaryDetailInfoReportInsert(string year, string month, string userName, string companySEQ, string companyName, ref string message)
        {
            var flag = false;
            var pageModel = DBContext.GX_SalesmanSalaryDetailInfoReport.Where(a => a.OperatorYear == year && a.OperatorMonth == month && a.CompanyName == companyName).FirstOrDefault();
            if (pageModel == null)
            {
                var tempModel = new GX_SalesmanSalaryDetailInfoReport();
                tempModel.OperatorYear = year;
                tempModel.OperatorMonth = month;
                tempModel.SubmitUserName = userName;
                tempModel.SubmitState = false;
                tempModel.CompanySEQ = Convert.ToInt32(companySEQ);
                tempModel.CompanyName = companyName;
                tempModel.CreateTime = DateTime.Now;
                flag = true;
                DBContext.GX_SalesmanSalaryDetailInfoReport.Add(tempModel);
                DBContext.SaveChanges();
                message = "报表生成成功~！";
            }
            else
            {
                if (pageModel.SubmitState == true)
                {
                    message = "审批流程无法重复生成报表~！";
                }
                else
                {
                    message = "报表已生成~！";
                }
            }
            return flag;
        }

        /// <summary>
        /// 更新信息
        /// </summary>
        /// <param name="editObj"></param>
        /// <returns></returns>
        public Boolean GX_SalesManKPIEdit(List<string> editObj)
        {
            var Flag = false;
            var tempSEQ = Convert.ToInt32(editObj[0]);
            var tempObj = DBContext.GX_SalesManKPI.Where(a => a.SEQ == tempSEQ).FirstOrDefault();
            if (tempObj != null)
            {
                tempObj.OperatorYear = editObj[1];
                tempObj.OperatorMonth = editObj[2];
                tempObj.DeptName = editObj[3];
                tempObj.UserName = editObj[4];
                tempObj.DutyName = editObj[5];
                tempObj.PostionSalary = editObj[6];
                tempObj.PerformanceNum = editObj[7];
                tempObj.GroupPlanNum = editObj[8];
                tempObj.PersonPlanNum = editObj[9];
                tempObj.ActualRatio = editObj[10];
                tempObj.KPINumYear = editObj[11];
                tempObj.KPINumMonthTotal = editObj[12];
                tempObj.KPINumMonth = editObj[13];
                tempObj.ShouldWages = editObj[14];
                tempObj.Memo = editObj[15];
                DBContext.SaveChanges();
                Flag = true;
            }
            return Flag;
        }

        public Boolean GX_EmployeeSalaryDetailInfoEdit(List<string> editObj)
        {
            var Flag = false;
            var tempSEQ = Convert.ToInt32(editObj[0]);
            var tempObj = DBContext.GX_EmployeeSalaryDetailInfo.Where(a => a.SEQ == tempSEQ).FirstOrDefault();
            if (tempObj != null)
            {
                tempObj.OperatorYear = !string.IsNullOrEmpty(editObj[1]) == true ? Convert.ToInt32(editObj[1]) : 0;
                tempObj.OperatorMonth = !string.IsNullOrEmpty(editObj[2]) == true ? Convert.ToInt32(editObj[2]) : 0;
                tempObj.DeptName = editObj[3];
                tempObj.UserName = editObj[4];
                tempObj.PostionName = editObj[5];
                tempObj.SalaryCardNo = editObj[6];
                tempObj.ContractSignNum = !string.IsNullOrEmpty(editObj[7]) == true ? Convert.ToInt32(editObj[7]) : 0;
                tempObj.ContractRatio = editObj[8];
                tempObj.GrossProfit = editObj[9];
                tempObj.ContractRatio2 = editObj[10];
                tempObj.ComprehensiveRatio = editObj[11];
                tempObj.PostionSalary = editObj[12];
                tempObj.SalaryStanderAverage = editObj[13];
                tempObj.RankingNum = editObj[14];
                tempObj.MoneyInCome = editObj[15];
                tempObj.PublicityNum = editObj[16];
                tempObj.SeasonSalary = editObj[17];
                tempObj.SalaryOther = editObj[18];
                tempObj.ShouldWages = editObj[19];
                DBContext.SaveChanges();
                Flag = true;
            }
            return Flag;
        }

        /// <summary>
        /// 售票员明细更新
        /// </summary>
        /// <param name="editObj"></param>
        /// <returns></returns>
        public Boolean GX_TicketSellerSalaryDetailInfoEdit(List<string> editObj)
        {
            var Flag = false;
            var tempSEQ = Convert.ToInt32(editObj[0]);
            var tempObj = DBContext.GX_TicketSellerSalaryDetailInfo.Where(a => a.SEQ == tempSEQ).FirstOrDefault();
            if (tempObj != null)
            {
                tempObj.DeptName = editObj[1];
                tempObj.UserName = editObj[2];
                tempObj.StartEndTime = editObj[3];
                tempObj.TicketNoStart = editObj[4];
                tempObj.TicketNoEnd = editObj[5];
                tempObj.SaleNum = editObj[6] != null ? Convert.ToInt32(editObj[6]) : 0;
                tempObj.TicketKidNum = editObj[7] != null ? Convert.ToInt32(editObj[7]) : 0;
                tempObj.SaleDay = editObj[8] != null ? Convert.ToInt32(editObj[8]) : 0;
                tempObj.PostionSalary = editObj[9] != null ? Convert.ToDecimal(editObj[9]) : 0;
                tempObj.PerformanceSalary = editObj[10] != null ? Convert.ToDecimal(editObj[10]) : 0;
                tempObj.SaleActualNum = editObj[11] != null ? Convert.ToInt32(editObj[11]) : 0;
                tempObj.GroupTicketNum = editObj[12] != null ? Convert.ToInt32(editObj[12]) : 0;
                tempObj.TotalTicketNum = editObj[13] != null ? Convert.ToInt32(editObj[13]) : 0;
                tempObj.PerformanceReward = editObj[14] != null ? Convert.ToDecimal(editObj[14]) : 0;
                tempObj.InsuranceNoStart = editObj[15];
                tempObj.InsuranceNoEnd = editObj[16];
                tempObj.GroupTicketNet = editObj[17] != null ? Convert.ToInt32(editObj[17]) : 0;
                tempObj.GroupTotalNum = editObj[18] != null ? Convert.ToInt32(editObj[18]) : 0;
                tempObj.PerformanceRewardGroup = editObj[19] != null ? Convert.ToDecimal(editObj[19]) : 0;
                tempObj.WagesNum = editObj[20] != null ? Convert.ToDecimal(editObj[20]) : 0;
                tempObj.CreateTime = DateTime.Now;
                DBContext.SaveChanges();
                Flag = true;
            }
            return Flag;
        }

        /// <summary>
        /// 根据用户用获取其它信息
        /// </summary>
        /// <param name="userName">获取用户名</param>
        /// <returns></returns>
        public GX_EmployeeInfo GetGX_EmployeeInfoByUserName(string userName)
        {
            var tempModel = DBContext.GX_EmployeeInfo.Where(a => a.EmployeeName == userName).FirstOrDefault();
            return tempModel;
        }

        /// <summary>
        /// 删除指定序号实体
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean GX_SalesManKPIDelete(int seq, ref string message)
        {
            var model = DBContext.GX_SalesManKPI.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                try
                {
                    var modelReport = DBContext.GX_SalesManKPIReport
                        .Where(a => a.OperatorYear == model.OperatorYear && a.OperatorMonth == model.OperatorMonth)
                        .FirstOrDefault();
                    if (modelReport != null)
                    {
                        var bizTwo = new GX_SalesManKPIBiz();
                        var checkResult = bizTwo.CheckGX_SalesManKPIApprove(model.SEQ, ref message);
                        if (!checkResult)
                        {
                            DBContext.GX_SalesManKPI.Remove(model);
                            DBContext.SaveChanges();
                            return true;
                        }
                        else
                        {
                            message = "数据已经进入审批流程无法删除~！";
                            return false;
                        }
                    }
                    else
                    {
                        DBContext.GX_SalesManKPI.Remove(model);
                        DBContext.SaveChanges();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                message = "未找到指定数据~！";
                return false;
            }
        }

        /// <summary>
        /// 编辑指定序号的实体
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean GX_SalesManKPIEdit(GX_SalesManKPI model)
        {
            var entry = DBContext.Entry(model);
            if (model != null)
            {
                try
                {
                    if (model.SEQ != 0)
                    {
                        entry.State = EntityState.Modified;
                        DBContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 查询用户报表信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public List<GX_SalesManKPI> GX_SalesManKPIReportSearch(string seq, int authNum)
        {
            if (!string.IsNullOrEmpty(seq))
            {
                var SEQ = Convert.ToInt32(seq);
                var tempModel = DBContext.GX_SalesManKPIReport.Where(a => a.SEQ == SEQ).FirstOrDefault();
                if (tempModel != null)
                {
                    var tempList = new List<GX_SalesManKPI>();
                    //判断当前用户权限和流程节点是否可以查看报表信息
                    if ( ((authNum & 1) == 1 || (authNum & 524288) == 524288) && tempModel != null && (tempModel.SubmitState ?? false))
                    {
                        tempList = GX_SalesManKPISearch(new GX_SalesManKPI { OperatorYear = tempModel.OperatorYear, OperatorMonth = tempModel.OperatorMonth });
                    }
                    else if ( ((authNum & 1) == 1 || (authNum & 256) == 256) && tempModel != null && !string.IsNullOrEmpty(tempModel.NodeOneUserName))
                    {
                        tempList = GX_SalesManKPISearch(new GX_SalesManKPI { OperatorYear = tempModel.OperatorYear, OperatorMonth = tempModel.OperatorMonth });
                    }
                    else if ( ((authNum & 1) == 1 || (authNum & 1024) == 1024) && tempModel != null && !string.IsNullOrEmpty(tempModel.NodeTwoUserName))
                    {
                        tempList = GX_SalesManKPISearch(new GX_SalesManKPI { OperatorYear = tempModel.OperatorYear, OperatorMonth = tempModel.OperatorMonth });
                    }
                    else if ( ((authNum & 1) == 1 || (authNum & 2048) == 2048) && tempModel != null && !string.IsNullOrEmpty(tempModel.NodeThreeUserName))
                    {
                        tempList = GX_SalesManKPISearch(new GX_SalesManKPI { OperatorYear = tempModel.OperatorYear, OperatorMonth = tempModel.OperatorMonth });
                    }
                    else if ( ((authNum & 1) == 1 || (authNum & 131072) == 131072))
                    {
                        tempList = GX_SalesManKPISearch(new GX_SalesManKPI { OperatorYear = tempModel.OperatorYear, OperatorMonth = tempModel.OperatorMonth });
                    }

                    return tempList;
                }
            }
            return null;
        }

        /// <summary>
        /// 查询用户报表信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public List<GX_EmployeeSalaryDetailInfo> GX_EmployeeSalaryDetailInfoReportSearch(string seq)
        {
            if (!string.IsNullOrEmpty(seq))
            {
                var SEQ = Convert.ToInt32(seq);
                var tempModel = DBContext.GX_SalesmanSalaryDetailInfoReport.Where(a => a.SEQ == SEQ).FirstOrDefault();
                if (tempModel != null)
                {
                    var searchModel = new GX_EmployeeSalaryDetailInfo()
                    {
                        OperatorYear = Convert.ToInt32(tempModel.OperatorYear),
                        OperatorMonth = Convert.ToInt32(tempModel.OperatorMonth),
                        CompanySEQ = tempModel.CompanySEQ,
                        CompanyName = tempModel.CompanyName
                    };
                    var tempList = GX_EmployeeSalaryDetailInfoISearch(searchModel);
                    return tempList;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                var tempList = GX_EmployeeSalaryDetailInfoISearch(new GX_EmployeeSalaryDetailInfo());
                return tempList;
            }
        }

        /// <summary>
        /// 查询用户报表信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public List<GX_EmployeeSalaryDetailInfo> GX_EmployeeSalaryDetailInfoReportSearch(string seq, int authNum)
        {
            if (!string.IsNullOrEmpty(seq))
            {
                var SEQ = Convert.ToInt32(seq);
                var tempModel = DBContext.GX_SalesmanSalaryDetailInfoReport.Where(a => a.SEQ == SEQ).FirstOrDefault();
                if (tempModel != null)
                {
                    var searchModel = new GX_EmployeeSalaryDetailInfo()
                    {
                        OperatorYear = Convert.ToInt32(tempModel.OperatorYear),
                        OperatorMonth = Convert.ToInt32(tempModel.OperatorMonth),
                        CompanySEQ = tempModel.CompanySEQ,
                        CompanyName = tempModel.CompanyName
                    };
                    var tempList = new List<GX_EmployeeSalaryDetailInfo>();
                    //判断当前用户权限和流程节点是否可以查看报表信息
                    if ( ((authNum & 1) == 1 || (authNum & 128) == 128) && tempModel != null && !(tempModel.SubmitState ?? true))
                    {
                        tempList = GX_EmployeeSalaryDetailInfoISearch(searchModel);
                    }
                    else if ( ((authNum & 1) == 1 || (authNum & 2048) == 2048 || (authNum & 1048576) == 1048576) && tempModel != null && !string.IsNullOrEmpty(tempModel.NodeOneUserName))
                    {
                        tempList = GX_EmployeeSalaryDetailInfoISearch(searchModel);
                    }
                    else if ( ((authNum & 1) == 1 || (authNum & 2097152) == 2097152) && tempModel != null && !string.IsNullOrEmpty(tempModel.NodeTwoUserName))
                    {
                        tempList = GX_EmployeeSalaryDetailInfoISearch(searchModel);
                    }
                    else if ( ((authNum & 1) == 1 || (authNum & 1024) == 1024) && tempModel != null && !string.IsNullOrEmpty(tempModel.NodeThreeUserName))
                    {
                        tempList = GX_EmployeeSalaryDetailInfoISearch(searchModel);
                    }
                    else if ( ((authNum & 1) == 1 || (authNum & 2048) == 2048))
                    {
                        tempList = GX_EmployeeSalaryDetailInfoISearch(searchModel);
                    }

                    return tempList;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                var tempList = GX_EmployeeSalaryDetailInfoISearch(new GX_EmployeeSalaryDetailInfo());
                return tempList;
            }
        }

        /// <summary>
        /// 审批执行序号报表
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public Boolean GX_EmployeeSalaryDetailInfoReportApprove(int seq, string type, string submitResult, string submitReason, string userName, ref string message)
        {
            var flag = false;
            var tempModel = DBContext.GX_SalesmanSalaryDetailInfoReport.Where(a => a.SEQ == seq).FirstOrDefault();
            if (tempModel != null)
            {
                switch (type)
                {
                    case "Start":
                        if (tempModel.SubmitTimeUser == null)
                        {
                            tempModel.SubmitState = true;
                            tempModel.SubmitTimeUser = DateTime.Now;
                            tempModel.SubmitUserName = userName;
                        }
                        else
                        {
                            message = "不能重复提交审批流程~！";
                        }
                        break;
                    case "One":
                        if (tempModel.SubmitState != null && tempModel.SubmitState == true)
                        {
                            tempModel.NodeOneSubmitResult = submitResult;
                            tempModel.NodeOneSubmitReason = submitReason;
                            tempModel.NodeOneSubmitTime = DateTime.Now;
                            tempModel.NodeOneUserName = userName;
                        }
                        else
                        {
                            message = "没有提交审批流程暂时无法操作~！";
                        }
                        break;
                    case "Two":
                        if (tempModel.NodeOneSubmitTime != null)
                        {
                            tempModel.NodeTwoSubmitResult = submitResult;
                            tempModel.NodeTwoSubmitReason = submitReason;
                            tempModel.NodeTwoSubmitTime = DateTime.Now;
                            tempModel.NodeTwoUserName = userName;
                        }
                        else
                        {
                            message = "计调部长未审批暂时无法操作~！";
                        }
                        break;
                    case "Three":
                        if (tempModel.NodeTwoSubmitTime != null)
                        {
                            tempModel.NodeThreeSubmitResult = submitResult;
                            tempModel.NodeThreeSubmitReason = submitReason;
                            tempModel.NodeThreeSubmitTime = DateTime.Now;
                            tempModel.NodeThreeUserName = userName;
                        }
                        else
                        {
                            message = "主管计调副总未审批暂时无法操作~！";
                        }
                        break;
                    case "Four":
                        if (tempModel.NodeThreeSubmitTime != null)
                        {
                            tempModel.NodeFourSubmitResult = submitResult;
                            tempModel.NodeFourSubmitReason = submitReason;
                            tempModel.NodeFourSubmitTime = DateTime.Now;
                            tempModel.NodeFourUserName = userName;
                        }
                        else
                        {
                            message = "集团董事长未审批暂时无法操作~！";
                        }
                        break;
                }
                flag = true;
                DBContext.SaveChanges();
            }
            else
            {
                message = "未找到报表数据";
            }
            return flag;
        }

        /// <summary>
        /// 审批执行序号报表
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public Boolean GX_SalesManKPIReportApprove(int seq, string type, string submitResult, string submitReason, string userName, ref string message)
        {
            var flag = false;
            var tempModel = DBContext.GX_SalesManKPIReport.Where(a => a.SEQ == seq).FirstOrDefault();
            if (tempModel != null)
            {
                switch (type)
                {
                    case "Start":
                        if (tempModel.SubmitTimeUser == null)
                        {
                            tempModel.SubmitState = true;
                            tempModel.SubmitTimeUser = DateTime.Now;
                            tempModel.SubmitUserName = userName;
                        }
                        else
                        {
                            message = "不能重复提交审批流程~！";
                        }
                        break;
                    case "One":
                        if (tempModel.SubmitState != null && tempModel.SubmitState == true)
                        {
                            tempModel.NodeOneSubmitResult = submitResult;
                            tempModel.NodeOneSubmitReason = submitReason;
                            tempModel.NodeOneSubmitTime = DateTime.Now;
                            tempModel.NodeOneUserName = userName;
                        }
                        else
                        {
                            message = "没有提交审批流程暂时无法操作~！";
                        }
                        break;
                    case "Two":
                        if (tempModel.NodeOneSubmitTime != null)
                        {
                            tempModel.NodeTwoSubmitResult = submitResult;
                            tempModel.NodeTwoSubmitReason = submitReason;
                            tempModel.NodeTwoSubmitTime = DateTime.Now;
                            tempModel.NodeTwoUserName = userName;
                        }
                        else
                        {
                            message = "计调部长未审批暂时无法操作~！";
                        }
                        break;
                    case "Three":
                        if (tempModel.NodeTwoSubmitTime != null)
                        {
                            tempModel.NodeThreeSubmitResult = submitResult;
                            tempModel.NodeThreeSubmitReason = submitReason;
                            tempModel.NodeThreeSubmitTime = DateTime.Now;
                            tempModel.NodeThreeUserName = userName;
                        }
                        else
                        {
                            message = "主管计调副总未审批暂时无法操作~！";
                        }
                        break;
                    case "Four":
                        if (tempModel.NodeThreeSubmitTime != null)
                        {
                            tempModel.NodeFourSubmitResult = submitResult;
                            tempModel.NodeFourSubmitReason = submitReason;
                            tempModel.NodeFourSubmitTime = DateTime.Now;
                            tempModel.NodeFourUserName = userName;
                        }
                        else
                        {
                            message = "集团董事长未审批暂时无法操作~！";
                        }
                        break;
                }
                flag = true;
                DBContext.SaveChanges();
            }
            else
            {
                message = "未找到报表数据";
            }
            return flag;
        }

        /// <summary>
        /// 查询指定条件内容
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_SalesManKPI> GX_SalesManKPISearch(GX_SalesManKPI model)
        {
            var SQLSTR = @"SELECT * FROM GX_SalesManKPI WHERE 1=1 ";
            if (model != null && !string.IsNullOrEmpty(model.OperatorYear))
            {
                SQLSTR = string.Format("{0} and OperatorYear='{1}' ", SQLSTR, model.OperatorYear);
            }
            if (model != null && !string.IsNullOrEmpty(model.OperatorMonth))
            {
                SQLSTR = string.Format("{0} and OperatorMonth='{1}' ", SQLSTR, model.OperatorMonth);
            }
            if (model != null && !string.IsNullOrEmpty(model.DeptName))
            {
                SQLSTR = string.Format("{0} and DeptName='{1}' ", SQLSTR, model.DeptName);
            }
            var tempList = DBContext.Database.SqlQuery<GX_SalesManKPI>(SQLSTR).ToList();
            return tempList;
        }

        /// <summary>
        /// 查询指定条件内容
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_EmployeeSalaryDetailInfo> GX_EmployeeSalaryDetailInfoISearch(GX_EmployeeSalaryDetailInfo model)
        {
            var SQLSTR = @"SELECT * FROM GX_EmployeeSalaryDetailInfo WHERE 1=1 ";
            if (model.OperatorYear != null)
            {
                SQLSTR = string.Format("{0} and OperatorYear='{1}' ", SQLSTR, model.OperatorYear);
            }
            if (model.OperatorMonth != null)
            {
                SQLSTR = string.Format("{0} and OperatorMonth='{1}' ", SQLSTR, model.OperatorMonth);
            }
            if (model.CompanySEQ != null)
            {
                SQLSTR = string.Format("{0} and CompanySEQ='{1}' ", SQLSTR, model.CompanySEQ);
            }
            var tempList = DBContext.Database.SqlQuery<GX_EmployeeSalaryDetailInfo>(SQLSTR).ToList();
            return tempList;
        }

        /// <summary>
        /// 保存所有上传数据
        /// </summary>
        /// <param name="modelList"></param>
        /// <returns></returns>
        public Boolean SaveUploadExcel(List<GX_SalesManKPI> modelList)
        {
            foreach (var item in modelList)
            {
                DBContext.GX_SalesManKPI.Add(item);
            }
            DBContext.SaveChanges();
            return true;
        }

        /// <summary>
        /// 保存所有上传数据
        /// </summary>
        /// <param name="modelList"></param>
        /// <returns></returns>
        public Boolean SaveUploadExcel(List<GX_EmployeeSalaryDetailInfo> modelList)
        {
            foreach (var item in modelList)
            {
                DBContext.GX_EmployeeSalaryDetailInfo.Add(item);
            }
            DBContext.SaveChanges();
            return true;
        }

    }
}

