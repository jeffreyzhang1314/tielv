﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.KPIShowManage
{
    public class GX_GrossProfitCompanyBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean GX_GrossProfitCompanyInsert(GX_GrossProfitCompany model)
        {
            try
            {
                DBContext.GX_GrossProfitCompany.Add(model);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除指定序号实体
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean GX_GrossProfitCompanyDelete(int seq)
        {
            var model = DBContext.GX_GrossProfitCompany.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                try
                {
                    DBContext.GX_GrossProfitCompany.Remove(model);
                    DBContext.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 编辑指定序号的实体
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean GX_GrossProfitCompanyEdit(GX_GrossProfitCompany model)
        {
            var entry = DBContext.Entry(model);
            if (model != null)
            {
                try
                {
                    if (model.SEQ != 0)
                    {
                        entry.State = EntityState.Modified;
                        DBContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 查询指定条件内容
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_GrossProfitCompany> GX_GrossProfitCompanySearch(GX_GrossProfitCompany model)
        {
            var SQLSTR = @"SELECT * FROM GX_GrossProfitCompany WHERE 1=1 ";
            if (model != null && !string.IsNullOrEmpty(model.OperatorYear))
            {
                SQLSTR = string.Format("{0} and OperatorYear='{1}' ", SQLSTR, model.OperatorYear);
            }
            if (model != null && !string.IsNullOrEmpty(model.OperatorMonth))
            {
                SQLSTR = string.Format("{0} and OperatorMonth='{1}' ", SQLSTR, model.OperatorMonth);
            }
            var tempList = DBContext.Database.SqlQuery<GX_GrossProfitCompany>(SQLSTR).ToList();
            return tempList;
        }
    }
}
