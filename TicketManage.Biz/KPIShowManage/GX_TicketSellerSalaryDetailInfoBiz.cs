﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.KPIShowManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;


namespace TieckManage.Biz.KPIShowManage
{
    public class GX_TicketSellerSalaryDetailInfoBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean GX_TicketSellerSalaryDetailInfoInsert(GX_TicketSellerSalaryDetailInfo model)
        {
            try
            {
                DBContext.GX_TicketSellerSalaryDetailInfo.Add(model);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除指定序号实体
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean GX_TicketSellerSalaryDetailInfoDelete(int seq, ref string message)
        {
            var model = DBContext.GX_TicketSellerSalaryDetailInfo.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                try
                {
                    var tempYear = Convert.ToString(model.OperatorYear);
                    var tempMonth = Convert.ToString(model.OperatorMonth);
                    var modelReport = DBContext.GX_TicketSellerSalaryReport
                        .Where(a => a.OperatorYear == tempYear && a.OperatorMonth == tempMonth)
                        .FirstOrDefault();
                    if (modelReport != null)
                    {
                        var bizTwo = new GX_SalesManKPIBiz();
                        var checkResult = bizTwo.CheckGX_TicketSellerSalaryApprove(modelReport.SEQ, ref message);
                        if (!checkResult)
                        {
                            DBContext.GX_TicketSellerSalaryDetailInfo.Remove(model);
                            DBContext.SaveChanges();
                            return true;
                        }
                        else
                        {
                            message = "数据已经进入审批流程无法删除~！";
                            return false;
                        }
                    }
                    else
                    {
                        DBContext.GX_TicketSellerSalaryDetailInfo.Remove(model);
                        DBContext.SaveChanges();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                message = "未找到指定数据~！";
                return false;
            }
        }

        /// <summary>
        /// 编辑指定序号的实体
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean GX_TicketSellerSalaryDetailInfoEdit(GX_TicketSellerSalaryDetailInfo model)
        {
            var entry = DBContext.Entry(model);
            if (model != null)
            {
                try
                {
                    if (model.SEQ != 0)
                    {
                        entry.State = EntityState.Modified;
                        DBContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 查询指定条件内容
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_TicketSellerSalaryDetailInfo> GX_TicketSellerSalaryDetailInfoSearch(GX_TicketSellerSalaryDetailInfo model)
        {
            var SQLSTR = @"SELECT * FROM GX_TicketSellerSalaryDetailInfo WHERE 1=1 ";
            if (model != null && model.OperatorYear != null)
            {
                SQLSTR = string.Format("{0} and OperatorYear='{1}' ", SQLSTR, model.OperatorYear);
            }
            if (model != null && model.OperatorMonth != null)
            {
                SQLSTR = string.Format("{0} and OperatorMonth='{1}' ", SQLSTR, model.OperatorMonth);
            }
            var tempList = DBContext.Database.SqlQuery<GX_TicketSellerSalaryDetailInfo>(SQLSTR).ToList();
            return tempList;
        }
    }
}
