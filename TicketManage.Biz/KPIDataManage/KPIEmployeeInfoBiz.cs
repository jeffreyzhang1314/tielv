﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.KPIDataManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.KPIDataManage
{
    /// <summary>
    /// 人员信息
    /// </summary>
    public class KPIEmployeeInfoBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 添加部门信息
        /// </summary>
        /// <param name="model">部门页面实体</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean AddEmployeeInfo(GX_EmployeeInfoPageModel model)
        {
            try
            {
                DBContext.GX_EmployeeInfo.Add(model.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除部门信息
        /// </summary>
        /// <param name="seq">部门序号</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean DelEmployeeInfo(int seq)
        {
            var model = DBContext.GX_EmployeeInfo.Where(a => a.seq == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.GX_EmployeeInfo.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 编辑部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditEmployeeInfo(GX_EmployeeInfoPageModel model)
        {
            try
            {
                if (model.PageModel.seq != 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "修改成功";
                    return true;
                }
                else if (model.PageModel.seq == 0)
                {
                    model.PageModel.IsJob = true;
                    CreateUserNo(model.PageModel.EmployeeNo, model.PageModel.EmployeeName, model.PageModel.LoginPassword, 32);
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Added;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "未找到实体对象";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        /// <summary>
        /// 根据条件查询部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_EmployeeInfo> SearchEmployeeInfoByCondition(GX_EmployeeInfoPageModel model)
        {
            var SQL = new StringBuilder("select a.* from GX_EmployeeInfo as a left join UserInfo as b on a.EmployeeNo=b.UserNo where 1=1 ");
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.EmployeeName))
            {
                SQL.AppendFormat("and EmployeeName='{0}'", model.PageModel.EmployeeName);
            }
            var modelList = DBContext.Database.SqlQuery<GX_EmployeeInfo>(SQL.ToString()).ToList();
            model.PageNum = modelList.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderBy(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }



        /// <summary>
        /// 获取制定序号的用户信息
        /// </summary>
        /// <param name="seq">用户序号信息</param>
        /// <returns></returns>
        public GX_EmployeeInfo GetEmployeeInfoBySEQ(int seq)
        {
            var model = DBContext.GX_EmployeeInfo.Where(a => a.seq == seq).FirstOrDefault();
            return model;
        }


        /// <summary>
        /// 获取全部部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_DeptInfo> GetDeptInfoByParent()
        {
            var SQL = new StringBuilder("select * from GX_DeptInfo where 1=1 and ParentSEQ is not null");
            var modelList = DBContext.Database
                .SqlQuery<GX_DeptInfo>(SQL.ToString())
                .OrderBy(a => a.seq)
                .ToList();
            return modelList;
        }

        public Boolean EmployeeIsJob(int seq)
        {
            var model = DBContext.GX_EmployeeInfo.Where(a => a.seq == seq).FirstOrDefault();
            if (model != null)
            {
                var flag = model.IsJob ?? false;
                model.IsJob = !flag;
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}
