﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.KPIDataManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.KPIDataManage
{
    public class KPIRankInfoBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 添加级别信息
        /// 默认添加创建时间
        /// </summary>
        /// <param name="model">部门页面实体</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean AddRankInfo(GX_RankInfoPageModel model)
        {
            try
            {
                DBContext.GX_RankInfo.Add(model.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除部门信息
        /// </summary>
        /// <param name="seq">部门序号</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean DelRankInfo(int seq)
        {
            var model = DBContext.GX_RankInfo.Where(a => a.seq == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.GX_RankInfo.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 编辑部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditRankInfo(GX_RankInfoPageModel model)
        {
            try
            {
                if (model.PageModel.seq != 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "修改成功";
                    return true;
                }
                else if (model.PageModel.seq == 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Added;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "未找到实体对象";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 添加部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SaveRankInfo(GX_RankInfoPageModel model)
        {
            try
            {
                if (model.PageModel.seq == 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "实体对象已经纯在";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 根据条件查询部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_RankInfo> SearchRankInfoByCondition(GX_RankInfoPageModel model)
        {
            var SQL = new StringBuilder("select * from GX_RankInfo where 1=1 ");
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.RankName))
            {
                SQL.AppendFormat("and RankName='{0}'", model.PageModel.RankName);
            }
            var modelList = DBContext.Database.SqlQuery<GX_RankInfo>(SQL.ToString()).ToList();
            model.PageNum = modelList.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderBy(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 获取级别信息根据序号
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public GX_RankInfo GetRankInfoBySEQ(int seq)
        {
            var model = DBContext.GX_RankInfo.Where(a => a.seq == seq).FirstOrDefault();
            return model;
        }
    }
}
