﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.KPIDataManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.KPIDataManage
{
    /// <summary>
    /// 职务信息方法
    /// </summary>
    public class KPIDutyInfoBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 添加职务信息
        /// </summary>
        /// <param name="model">职务页面实体</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean AddDutyInfo(GX_DutyInfoPageModel model)
        {
            try
            {
                DBContext.GX_DutyInfo.Add(model.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除职务信息
        /// </summary>
        /// <param name="seq">职务序号</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean DelDutyInfo(int seq)
        {
            var model = DBContext.GX_DutyInfo.Where(a => a.seq == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.GX_DutyInfo.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 编辑职务信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditDutyInfo(GX_DutyInfoPageModel model)
        {
            try
            {
                if (model.PageModel.seq != 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "修改成功";
                    return true;
                }
                else if (model.PageModel.seq == 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Added;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "未找到实体对象";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 根据条件查询职务信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_DutyInfo> SearchDeptInfoByCondition(GX_DutyInfoPageModel model)
        {
            var SQL = new StringBuilder("select * from GX_DutyInfo where 1=1 ");
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.DutyName))
            {
                SQL.AppendFormat("and DutyName='{0}'", model.PageModel.DutyName);
            }
            var modelList = DBContext.Database.SqlQuery<GX_DutyInfo>(SQL.ToString()).ToList();
            model.PageNum = modelList.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderBy(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 获取职务信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public GX_DutyInfo GetDutyInfoBySEQ(int seq)
        {
            var model = DBContext.GX_DutyInfo.Where(a => a.seq == seq).FirstOrDefault();
            return model;
        }
    }
}
