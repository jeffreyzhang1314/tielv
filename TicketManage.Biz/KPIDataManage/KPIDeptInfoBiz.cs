﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.KPIDataManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.KPIDataManage
{
    public class KPIDeptInfoBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 添加部门信息
        /// </summary>
        /// <param name="model">部门页面实体</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean AddDeptInfo(GX_DeptInfoPageModel model)
        {
            try
            {
                DBContext.GX_DeptInfo.Add(model.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除部门信息
        /// </summary>
        /// <param name="seq">部门序号</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean DelDeptInfo(int seq)
        {
            var model = DBContext.GX_DeptInfo.Where(a => a.seq == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.GX_DeptInfo.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 编辑部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditDeptInfo(GX_DeptInfoPageModel model)
        {
            try
            {
                if (model.PageModel.seq != 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "修改成功";
                    return true;
                }
                else if (model.PageModel.seq == 0)
                {
                    model.PageModel.CreateTime = DateTime.Now;
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Added;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "未找到实体对象";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 添加部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SaveDeptInfo(GX_DeptInfoPageModel model)
        {
            try
            {
                if (model.PageModel.seq == 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "实体对象已经纯在";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 根据条件查询部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_DeptInfo> SearchDeptInfoByCondition(GX_DeptInfoPageModel model)
        {
            var SQL = new StringBuilder("select * from GX_DeptInfo where 1=1 and seq>1");
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.DeptName))
            {
                SQL.AppendFormat("and DeptName='{0}'", model.PageModel.DeptName);
            }
            var modelList = DBContext.Database.SqlQuery<GX_DeptInfo>(SQL.ToString()).ToList();
            model.PageNum = modelList.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderBy(a => a.ParentSEQ)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 获取部门信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public GX_DeptInfo GetDeptInfoBySEQ(int seq)
        {
            var model = DBContext.GX_DeptInfo.Where(a => a.seq == seq).FirstOrDefault();
            return model;
        }

        /// <summary>
        /// 获取全部部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_DeptInfo> GetDeptInfoByParent()
        {
            var SQL = new StringBuilder("select * from GX_DeptInfo where 1=1");
            var modelList = DBContext.Database
                .SqlQuery<GX_DeptInfo>(SQL.ToString())
                .OrderBy(a => a.seq)
                .ToList();
            return modelList;
        }
    }
}
