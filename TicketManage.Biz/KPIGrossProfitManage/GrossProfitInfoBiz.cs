﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TicketManage.Entity.KPIGrossProfit;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.KPIGrossProfitManage
{
    public class GrossProfitInfoBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 添加毛利润
        /// </summary>
        /// <param name="model">毛利润实体</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean AddGrossProfit(GX_GrossProfitInfoPageModel model)
        {
            try
            {
                DBContext.GX_GrossProfitInfo.Add(model.PageModel);
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除毛利润实体
        /// </summary>
        /// <param name="seq">毛利润实体序号</param>
        /// <returns>True操作成功False操作失败</returns>
        public Boolean DelGrossProfit(int seq)
        {
            var model = DBContext.GX_GrossProfitInfo.Where(a => a.SEQ == seq).FirstOrDefault();
            if (model != null)
            {
                DBContext.GX_GrossProfitInfo.Remove(model);
                DBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 编辑毛利润
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean EditGrossProfit(GX_GrossProfitInfoPageModel model)
        {
            try
            {
                if (model.PageModel.SEQ != 0)
                {
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Modified;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "修改成功";
                    return true;
                }
                else if (model.PageModel.SEQ == 0)
                {
                    model.PageModel.CreateTime = DateTime.Now;
                    var entry = DBContext.Entry(model.PageModel);
                    entry.State = EntityState.Added;
                    DBContext.SaveChanges();
                    model.MessageState = true;
                    model.Message = "添加成功";
                    return true;
                }
                else
                {
                    model.MessageState = false;
                    model.Message = "未找到实体对象";
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 根据条件查询全年计划信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GX_GrossProfitInfo> SearchGrossProfitByCondition(GX_GrossProfitInfoPageModel model)
        {
            var SQL = new StringBuilder("select * from GX_GrossProfitInfo where 1=1 ");
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.UserName))
            {
                SQL.AppendFormat("and UserName='{0}'", model.PageModel.UserName);
            }
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.OperatorYear))
            {
                SQL.AppendFormat("and OperatorYear='{0}'", model.PageModel.OperatorYear);
            }
            if (model.PageModel != null && !string.IsNullOrEmpty(model.PageModel.OperatorMonth))
            {
                SQL.AppendFormat("and OperatorMonth='{0}'", model.PageModel.OperatorMonth);
            }

            var modelList = DBContext.Database.SqlQuery<GX_GrossProfitInfo>(SQL.ToString()).ToList();
            model.PageNum = modelList.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderBy(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 获取岗位信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public GX_GrossProfitInfo GetGrossProfitBySEQ(int seq)
        {
            var model = DBContext.GX_GrossProfitInfo.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }

        /// <summary>
        /// 获取全部职务信息
        /// </summary>
        /// <returns></returns>
        public List<GX_DutyInfo> GetDutyInfo()
        {
            var modelList = DBContext.GX_DutyInfo.ToList();
            return modelList;
        }
    }
}
