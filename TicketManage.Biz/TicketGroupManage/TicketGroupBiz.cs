using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using TicketManage.Entity.TicketGroupManage;
using TicketManage.Entity.TicketLaSaManage;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.TicketGroupManage
{
    public class TicketGroupBiz : BaseDataManageBiz
    {
        /// <summary>
        /// 获取团队票数据
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        public List<TicketGroup> GetTicketGroupByCondition(TicketGroupListPageModel pageModel)
        {
            List<TicketGroup> list = new List<TicketGroup>();
            StringBuilder sb = new StringBuilder();
            sb.Append("select [SEQ],[CompanySEQ],[CompanyName],[TicketWindow],[TicketGetCompany],[TicketGetContacts] ,[TicketGetPassportNo] ");
            sb.Append(",[TicketGetContactsNo],[OrderNo] , [StartDriveDate],[TrainNo],[StartStation],[EndSation],[TicketCategoryCode],[TicketCategoryCodeName],[TicketNum],[KidNum]");
            sb.Append(" ,[StartTicketNo],[TicketMoney] ,[Brokerage],[InsuranceMoney],[OperatorSEQ],[OperatorName] ,CreateTime");
            sb.Append(" ,[StartTicketNo1],[StartTicketNo2],[StartTicketNo3],[EndTicketNo1],[EndTicketNo2],[EndTicketNo3],[StartInsuranceNo1],[StartInsuranceNo2],[StartInsuranceNo3],[EndInsuranceNo1],[EndInsuranceNo2],[EndInsuranceNo3],[GrossProfitMemo],[ServiceMemo],[InsuranceMemo]");
            sb.Append(" from TicketGroup where 1 = 1 ");
            if (pageModel != null && pageModel.PageModel != null)
            {
                if (!string.IsNullOrEmpty(pageModel.PageModel.TicketGetCompany))
                {
                    sb.Append(" and TicketGetCompany like '%" + pageModel.PageModel.TicketGetCompany + "%'");
                }
                if (!string.IsNullOrEmpty(pageModel.PageModel.TicketGetContacts))
                {
                    sb.Append(" and TicketGetContacts like '%" + pageModel.PageModel.TicketGetContacts + "%'");
                }
            }
            if (pageModel.OperatorUserNoList != null && pageModel.OperatorUserNoList.Count > 0)
            {
                sb.Append(" and OperatorSEQ in (" + ConvertStringListToContext(pageModel.OperatorUserNoList) + ")");
            }
            list = DBContext.TicketGroups.SqlQuery(sb.ToString()).ToList();
            return list;
        }
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SaveInfo(TicketGroupListPageModel model)
        {
            Boolean result = false;
            try
            {
                var entry = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0)
                {
                    entry.State = EntityState.Added;
                }
                else
                {
                    entry.State = EntityState.Modified;
                }
                DBContext.SaveChanges();
                result = true;

            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean DeleteInfo(int seq)
        {
            Boolean result = false;
            try
            {
                TicketGroup model = DBContext.TicketGroups.Where(p => p.SEQ == seq).FirstOrDefault();
                var entry = DBContext.Entry(model);
                entry.State = EntityState.Deleted;
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }
        /// <summary>
        /// 返回团队票信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public TicketGroup GetTickGroupInfo(int seq)
        {
            TicketGroup info = new TicketGroup();
            info = DBContext.TicketGroups.Where(s => s.SEQ == seq).FirstOrDefault();
            return info;
        }

    }
}
