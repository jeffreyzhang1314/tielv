using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using TicketManage.Entity.AdvertisementManage;
using TicketManage.Entity.PublicityInfoManage;
using TicketManage.Repository.Models;
using TicketManage.Repository.Models.ExtModels;
using TieckManage.Biz.TicketManage;

namespace TieckManage.Biz.PublicityInfoManage
{
    public class PublicityInfoBiz : BaseDataManageBiz
    {



        /// <summary>
        /// 宣传预算申请
        /// </summary>
        private const string BudgetInfoSQL = @"SELECT a.[SEQ],[BudgetName],[BudgetMoney],[ApplyAmount],[OperatorSEQ],c.CompanyName as OperatorName
                                                                      ,a.[CreateTime]
                                                                  FROM [dbo].[BudgetInfo] as a
                                                                  left join UserInfo as b
                                                                  on a.OperatorSEQ=b.SEQ
                                                                  left join CompanyInfo as c
                                                                  on b.CompanySEQ=c.SEQ
                                                                  where 1=1 ";

        /// <summary>
        /// 宣传品详细
        /// </summary>
        private const string PublicitySQL = @"SELECT a.[SEQ],[PublicitySEQ],[publicityName],[AppNum],[TravelProductSEQ],[TravelProdcutName],[CustomerNum],a.[Memo],[OperatorSEQ],a.CompanyName as OperatorName,a.[CreateTime]
                                                             ,AppCategory                                                          
                                                            FROM (
                                                                select a.*,b.CompanyName 
                                                                from [dbo].[PublicityReceive] as a 
                                                                left join dbo.UserInfo_Other as b
                                                                on a.OperatorSEQ=b.SEQ
                                                            ) as a
                                                          where 1=1";


        public List<PublicityInfo> GetPublicityByAll(PublicityInfoPageModel model)
        {

            var modelList = DBContext.PublicityInfoes.ToList();
            model.PageNum = modelList.Count();
            if (model.PageNum > 0)
            {
                model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
                modelList = modelList
                    .OrderByDescending(a => a.CreateTime)
                    .Skip(model.PageSize * (model.CurrentPage - 1))
                   .Take(model.PageSize).ToList();
            }
            return modelList;
        }

        public List<PublicityInfo> GetPublicityByCondition(PublicityInfoPageModel model)
        {
            var modelList = DBContext.PublicityInfoes.ToList();
            if (!string.IsNullOrEmpty(model.PageModel.PublicityName))
            {
                modelList = modelList.Where(a => a.PublicityName == model.PageModel.PublicityName).ToList();
            }
            model.PageNum = modelList.Count();
            if (model.PageNum > 0)
            {
                model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
                modelList = modelList
                    .OrderByDescending(a => a.CreateTime)
                    .Skip(model.PageSize * (model.CurrentPage - 1))
                   .Take(model.PageSize).ToList();
            }
            return modelList;
        }

        /// <summary>
        /// 创建宣传品
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean PublicityAdd(PublicityInfoPageModel model)
        {
            model.PageModel.CreateTime = DateTime.Now;
            DBContext.PublicityInfoes.Add(model.PageModel);
            DBContext.SaveChanges();
            return true;
        }


        /// <summary>
        /// 查询宣传品审批
        /// </summary>
        /// <returns></returns>
        public List<ApplicationPageModel> ShowPublicityInfoByAll(ApplicationRecordPageModel model)
        {
            string SQL = @"select a.*,b.*,a.OperatorCompanyName as CompanyName
                                    from ApplicationRecord a 
                                    left join PublicityInfo b on a.PublicitySEQ=b.SEQ";
            model.PageNum = DBContext.Database.SqlQuery<ApplicationPageModel>(SQL).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var companyList = DBContext.Database.SqlQuery<ApplicationPageModel>(SQL)
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return companyList;
        }

        /// <summary>
        /// 查询宣传品审批按条件
        /// </summary>
        /// <returns></returns>
        public List<ApplicationPageModel> ShowPublicityInfoByCondition(ApplicationRecordPageModel model)
        {
            string SQL = @"select a.*,b.*,a.OperatorCompanyName as CompanyName
                                    from ApplicationRecord a 
                                    left join PublicityInfo b on a.PublicitySEQ=b.SEQ 
                                    where 1=1 ";
            var ExcelSQL = new StringBuilder(SQL);
            if (model.AppPageModel != null && model.AppPageModel.PublicityName != null)
            {
                ExcelSQL.AppendFormat(" and b.PublicityName like '%{0}%'", model.AppPageModel.PublicityName);
            }

            model.PageNum = DBContext.Database.SqlQuery<ApplicationPageModel>(ExcelSQL.ToString()).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var companyList = DBContext.Database.SqlQuery<ApplicationPageModel>(ExcelSQL.ToString())
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return companyList;
        }
        /// <summary>
        /// 查询宣传品领用
        /// </summary>
        /// <returns></returns>
        public List<PublicityReceiveInfo> ShowPublicityReceiveByAll(PublicityReceivePageModel model)
        {
            string sql = @"select c.*,sum(e.AppNum) as OverNum,(StockNum-sum(e.AppNum)) as LeftNum from (
								    select a.PublicitySEQ as SEQ,a.OperatorCompanyName as CompanyName,b.PublicityName,sum(a.Num) as StockNum
								    from TicketManage.dbo.ApplicationRecord as a
								    left join TicketManage.dbo.PublicityInfo as b
								    on a.PublicitySEQ=b.SEQ
								    where 1=1
								    and PublicityName is not null
								    and ApprovalTime is not null
								    and ApprovalFile is not null 
								    and ConfirmStockDate is not null ";
            var ExcuteSQL = new StringBuilder(sql);
            if ((model.OperatorAuthNum & 65536) == 65536)
            {
                ExcuteSQL.AppendFormat(" and a.OperatorCompanyName in (select CompanyName from UserInfo_Other where SEQ={0}) ", model.PageModel.OperatorSEQ);
            }
            ExcuteSQL.Append(@"  group by a.PublicitySEQ,PublicityName,a.OperatorCompanyName
							    )  as c
							    left join (
                                    select a.*,b.CompanyName
									from TicketManage.dbo.PublicityReceive as a
									left join TicketManage.dbo.UserInfo_Other as b
									on a.OperatorSEQ=b.SEQ
                                ) as e
							    on c.SEQ=e.PublicitySEQ and c.CompanyName=e.CompanyName
							    group by c.SEQ,c.PublicityName,c.CompanyName,c.StockNum
                                Order by c.SEQ desc");
            model.PageNum = DBContext.Database.SqlQuery<PublicityReceiveInfo>(ExcuteSQL.ToString()).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var companyList = DBContext.Database.SqlQuery<PublicityReceiveInfo>(ExcuteSQL.ToString())
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return companyList;
        }

        /// <summary>
        /// 查询宣传品领用按条件
        /// </summary>
        /// <returns></returns>
        public List<PublicityReceiveInfo> ShowPublicityReceiveByCondition(PublicityReceivePageModel model)
        {
            string sql = @"select c.*,sum(e.AppNum) as OverNum,(StockNum-sum(e.AppNum)) as LeftNum from (
								    select a.PublicitySEQ as SEQ,a.OperatorCompanyName as CompanyName,b.PublicityName,sum(a.Num) as StockNum
								    from TicketManage.dbo.ApplicationRecord as a
								    left join TicketManage.dbo.PublicityInfo as b
								    on a.PublicitySEQ=b.SEQ
								    where 1=1
								    and PublicityName is not null
								    and ApprovalTime is not null
								    and ApprovalFile is not null 
								    and ConfirmStockDate is not null ";
            var ExcuteSQL = new StringBuilder(sql);
            if (model.PageModel.publicityName != null)
            {
                ExcuteSQL.AppendFormat(" and b.PublicityName like '%{0}%'", model.PageModel.publicityName);
            }
            if (!string.IsNullOrEmpty(model.ComanyName) && string.Compare(model.ComanyName, "全部", true) != 0)
            {
                ExcuteSQL.AppendFormat(" and a.OperatorCompanyName = '{0}'", model.ComanyName);
            }
            if ((model.OperatorAuthNum & 65536) == 65536)
            {
                ExcuteSQL.AppendFormat(" and a.OperatorCompanyName in (select CompanyName from UserInfo_Other where SEQ={0}) ", model.PageModel.OperatorSEQ);
            }
            ExcuteSQL.Append(@"  group by a.PublicitySEQ,PublicityName,a.OperatorCompanyName
							    )  as c
							    left join TicketManage.dbo.PublicityReceive as e
							    on c.SEQ=e.PublicitySEQ
							    group by c.SEQ,c.PublicityName,CompanyName,c.StockNum
                                Order by c.SEQ desc");

            model.PageNum = DBContext.Database.SqlQuery<PublicityReceiveInfo>(ExcuteSQL.ToString()).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var companyList = DBContext.Database.SqlQuery<PublicityReceiveInfo>(ExcuteSQL.ToString())
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return companyList;
        }

        /// <summary>
        /// 审批宣传品
        /// </summary>
        /// <param name="seq">审批宣传品</param>
        /// <returns></returns>
        public Boolean PublicityStateApp(int seq)
        {
            Boolean result = false;
            try
            {
                int PubSEQ = 0;
                int appStockNum = 0;
                var model = DBContext.ApplicationRecords.Where(a => a.SEQ == seq).FirstOrDefault();
                model.OperationState = 1;
                model.ApprovalTime = DateTime.Now;
                PubSEQ = Convert.ToInt32(model.PublicitySEQ);
                appStockNum = Convert.ToInt32(model.Num);

                //更新宣传品数量
                var NumModel = DBContext.PublicityInfoes.Where(a => a.SEQ == PubSEQ).FirstOrDefault();
                NumModel.StockNum += appStockNum;
                DBContext.SaveChanges();

                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 审批宣传品
        /// </summary>
        /// <param name="seq">审批宣传品</param>
        /// <returns></returns>
        public Boolean PublicityStateApp(ApplicationRecordPageModel model)
        {
            Boolean result = false;
            try
            {
                int PubSEQ = 0;
                int appStockNum = 0;
                var tempModel = DBContext.ApplicationRecords.Where(a => a.SEQ == model.AppPageModel.SEQ).FirstOrDefault();
                tempModel.OperationState = 1;
                tempModel.ApprovalTime = DateTime.Now;
                tempModel.ApprovalSEQ = model.AppPageModel.ApprovalSEQ;
                tempModel.ApprovalName = model.AppPageModel.ApprovalName;
                PubSEQ = Convert.ToInt32(tempModel.PublicitySEQ);
                appStockNum = Convert.ToInt32(tempModel.Num);

                //更新宣传品数量
                var NumModel = DBContext.PublicityStoctInfoes
                    .Where(a => a.PublicitySEQ == PubSEQ && a.CompanySEQ == tempModel.OperatorCompanySEQ)
                    .FirstOrDefault();

                if (NumModel != null)
                {
                    NumModel.StockNum += appStockNum;
                }
                else
                {
                    DBContext.PublicityStoctInfoes.Add(
                        new PublicityStoctInfo()
                        {
                            StockNum = appStockNum,
                            PublicitySEQ = tempModel.PublicitySEQ,
                            CompanySEQ = tempModel.OperatorCompanySEQ,
                            CompanyName = tempModel.OperatorCompanyName
                        }
                    );
                }

                DBContext.SaveChanges();

                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 获取指定序号宣传品
        /// </summary>
        /// <param name="seq">宣传品</param>
        /// <returns></returns>
        public PublicityInfo ShowPublicityInfoBySeq(int seq)
        {
            var model = DBContext.PublicityInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }
        /// <summary>
        /// 增加或修改数据库
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SavePublicityInfo(PublicityInfoPageModel model)
        {
            try
            {
                model.PageModel.StockNum = 0;
                model.PageModel.CreateTime = DateTime.Now;

                var dbModel = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0)
                {
                    dbModel.State = EntityState.Added;
                }
                else
                {
                    dbModel.State = EntityState.Modified;
                }
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 删除指定编号
        /// </summary>
        /// <param name="seq">实体编号</param>
        /// <returns></returns>
        public Boolean DelPublicityInfo(int seq)
        {
            Boolean result = false;
            try
            {
                var model = DBContext.PublicityInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
                var entry = DBContext.Entry(model);
                entry.State = EntityState.Deleted;
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 查询宣传品申请
        /// </summary>
        /// <returns></returns>
        public List<ApplicationPageModel> ShowPublicityApplyByAll(ApplicationRecordPageModel model)
        {
            string sql1 = @"select a.*,b.*,a.OperatorCompanyName as CompanyName
                                    from ApplicationRecord a 
                                    left join PublicityInfo b 
                                    on a.PublicitySEQ=b.SEQ
                                    where 1=1";
            var ExcuteSQL = new StringBuilder(sql1);
            if ((model.OperatorAuthNum & 65536) == 65536)
            {
                ExcuteSQL.AppendFormat(" and OperatorCompanyName in (select CompanyName from UserInfo_Other where SEQ={0}) ", model.AppPageModel.OperatorSEQ);
            }
            model.PageNum = DBContext.Database.SqlQuery<ApplicationPageModel>(ExcuteSQL.ToString()).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var companyList = DBContext.Database.SqlQuery<ApplicationPageModel>(ExcuteSQL.ToString())
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return companyList;
        }

        /// <summary>
        /// 查询宣传品申请按条件
        /// </summary>
        /// <returns></returns>
        public List<ApplicationPageModel> ShowPublicityApplyByCondition(ApplicationRecordPageModel model)
        {
            string sql = @"select a.*,b.*,a.OperatorCompanyName as CompanyName
                                    from ApplicationRecord a 
                                    left join PublicityInfo b on a.PublicitySEQ=b.SEQ
                                    where 1=1";
            var ExcuteSQL = new StringBuilder(sql);
            if (model.AppPageModel.PublicityName != null)
            {
                ExcuteSQL.AppendFormat(" and b.PublicityName like '%{0}%' ", model.AppPageModel.PublicityName);
            }

            if ((model.OperatorAuthNum & 65536) == 65536)
            {
                ExcuteSQL.AppendFormat(" and OperatorCompanyName in (select CompanyName from UserInfo_Other where SEQ={0}) ", model.AppPageModel.OperatorSEQ);
            }
            model.PageNum = DBContext.Database.SqlQuery<ApplicationPageModel>(ExcuteSQL.ToString()).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var companyList = DBContext.Database.SqlQuery<ApplicationPageModel>(ExcuteSQL.ToString())
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return companyList;
        }
        /// <summary>
        /// 获取指定序号宣传品
        /// </summary>
        /// <param name="seq">宣传品</param>
        /// <returns></returns>
        public ApplicationRecord ShowPublicityApplyBySeq(int seq)
        {
            var model = DBContext.ApplicationRecords.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }
        /// <summary>
        /// 增加或修改数据库
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SavePublicityApply(ApplicationRecord model)
        {
            try
            {
                var dbModel = DBContext.Entry(model);
                if (model.SEQ == 0)
                {
                    dbModel.State = EntityState.Added;
                }
                else
                {
                    dbModel.State = EntityState.Modified;
                }
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 宣传品
        /// </summary>
        /// <param name="seq">宣传品</param>
        /// <returns></returns>
        public Boolean SavePublicityRecord(ApplicationRecord model)
        {
            Boolean result = false;
            try
            {
                var DBmodel = DBContext.ApplicationRecords.Where(a => a.SEQ == model.SEQ).FirstOrDefault();
                DBmodel.PublicitySEQ = model.PublicitySEQ;
                DBmodel.Num = model.Num;
                DBmodel.Memo = model.Memo;
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }
        /// <summary>
        /// 增加或修改数据库
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SavePublicityReceive(PublicityReceive model)
        {
            try
            {
                var tempUserModel = DBContext.UserInfo_Other.Where(a => a.SEQ == model.OperatorSEQ).FirstOrDefault();

                if (tempUserModel != null && !string.IsNullOrEmpty(tempUserModel.CompanyName))
                {
                    var tempUserModelByCompany = DBContext.UserInfo_Other.Where(a => a.CompanyName == tempUserModel.CompanyName).Select(a => a.SEQ).ToList();
                    if (tempUserModelByCompany != null && tempUserModelByCompany.Count > 0)
                    {
                        var tempNumModel = DBContext.ApplicationRecords.Where(a => a.PublicitySEQ == model.PublicitySEQ && a.OperatorCompanyName == tempUserModel.CompanyName && a.ApprovalTime != null).Sum(a => a.Num);
                        var tempAppNumModel = DBContext.PublicityReceives.Where(a => a.PublicitySEQ == model.PublicitySEQ && tempUserModelByCompany.Contains((a.OperatorSEQ ?? 0))).Sum(a => a.AppNum);

                        if (tempNumModel - (tempAppNumModel ?? 0) > model.AppNum)
                        {
                            var dbModel = DBContext.Entry(model);

                            dbModel.State = EntityState.Added;

                            DBContext.SaveChanges();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 宣传品确认入库时间
        /// </summary>
        /// <param name="seq">宣传品申请序号</param>
        /// <returns></returns>
        public Boolean SavePublicityApplyConfirmStock(int seq)
        {
            var flag = false;
            var tempModel = DBContext.ApplicationRecords.Where(a => a.SEQ == seq).FirstOrDefault();
            if (tempModel != null)
            {
                tempModel.ConfirmStockDate = DateTime.Now;
                flag = true;
                DBContext.SaveChanges();
            }
            return flag;
        }

        /// <summary>
        /// 删除申请内容
        /// </summary>
        /// <param name="seq">序号</param>
        /// <returns></returns>
        public Boolean DelPublicityApply(int seq)
        {
            var Flag = false;
            var tempModel = DBContext.ApplicationRecords.Where(a => a.SEQ == seq).FirstOrDefault();
            if (tempModel != null)
            {
                DBContext.ApplicationRecords.Remove(tempModel);
                DBContext.SaveChanges();
                Flag = true;
            }
            return Flag;
        }

        public List<PublicityInfo> GetPublicityNameByType()
        {
            List<PublicityInfo> result = new List<PublicityInfo>();
            result = DBContext.PublicityInfoes.ToList();

            return result;
        }
        public List<BudgetInfo> GetBudgetNameByType()
        {
            List<BudgetInfo> result = new List<BudgetInfo>();
            result = DBContext.BudgetInfoes.ToList();
            return result;
        }
        /// <summary>
        /// 获取指
        /// </summary>
        /// <param name="seq">宣传品</param>
        /// <returns></returns>
        public PublicityReceiveInfo ShowPublicityReceiveBySeq(int seq)
        {
            var PublicityReceive = new PublicityReceiveInfo();
            var model = DBContext.PublicityInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
            PublicityReceive.SEQ = seq;
            PublicityReceive.publicityName = model.PublicityName;
            return PublicityReceive;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        public List<PublicityReceive> ShowPublicityInfoReceiveDetail(PublicityReceivePageModel model)
        {
            var ExcuteSQL = new StringBuilder(PublicitySQL);
            if (model != null && model.PublicitySEQ > 0)
            {
                ExcuteSQL.AppendFormat(" and PublicitySEQ = {0} ", model.PublicitySEQ);
                ExcuteSQL.AppendFormat(" and CompanyName = '{0}' ", model.ComanyName);
                //if (model.CompanySEQ != null)
                //{
                //    ExcuteSQL.AppendFormat(" and OperatorSEQ in (select seq from TicketManage.dbo.UserInfo_Other where CompanySEQ={0})", model.CompanySEQ ?? 0);
                //}
            }
            model.PageNum = DBContext.Database.SqlQuery<PublicityReceive>(ExcuteSQL.ToString()).Where(a => a.PublicitySEQ == model.PublicitySEQ).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var companyList = DBContext.Database.SqlQuery<PublicityReceive>(ExcuteSQL.ToString()).Where(a => a.PublicitySEQ == model.PublicitySEQ)
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return companyList;
        }

        /// <summary>
        /// 删除指定序号领用信息
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public Boolean PublicityInfoReceiveDel(int seq)
        {
            var Flag = false;
            var tempModel = DBContext.PublicityReceives.Where(a => a.SEQ == seq).FirstOrDefault();
            if (tempModel != null)
            {
                DBContext.PublicityReceives.Remove(tempModel);
                DBContext.SaveChanges();
                Flag = true;
            }
            return Flag;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        public List<PublicityReceive> ShowPublicityInfoReceiveDetailByAll(PublicityReceivePageModel model)
        {
            var ExcuteSQL = new StringBuilder(PublicitySQL);
            if (model != null && model.PublicitySEQ > 0)
            {
                ExcuteSQL.AppendFormat(" and PublicitySEQ = {0} ", model.PublicitySEQ);
            }
            var tempList = DBContext.Database.SqlQuery<PublicityReceive>(ExcuteSQL.ToString()).Where(a => a.PublicitySEQ == model.PublicitySEQ).ToList();
            return tempList;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        public List<ApplyBudget> ShowPublicityBudgetDetail(PublicityReceivePageModel model)
        {
            string sql1 = @"select a.SEQ,a.BudgetSEQ,b.BudgetName,a.ApplyMoney,a.Voucher,a.OperatorName,a.OperatorSEQ,a.CreateTime 
                             from [ApplyBudget]  a left join [BudgetInfo] b on a.BudgetSEQ=b.SEQ
                             ";

            model.PageNum = DBContext.Database.SqlQuery<ApplyBudget>(string.Format(sql1, model.CompanySEQ))
                .Where(a => a.BudgetSEQ == model.PublicitySEQ)
                .Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var companyList = DBContext.Database.SqlQuery<ApplyBudget>(string.Format(sql1, model.CompanySEQ)).Where(a => a.BudgetSEQ == model.PublicitySEQ)
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return companyList;
        }

        public List<BudgetInfo> ShowPublicityInfoBudgetListByAll(PublicityReceivePageModel model)
        {
            model.PageNum = DBContext.Database.SqlQuery<BudgetInfo>(BudgetInfoSQL).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var companyList = DBContext.Database.SqlQuery<BudgetInfo>(BudgetInfoSQL)
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return companyList;
        }

        public List<BudgetInfo> GetBudgetInfoByCondition(PublicityReceivePageModel model)
        {
            var modelList = DBContext.Database.SqlQuery<BudgetInfo>(BudgetInfoSQL).ToList();
            if (!string.IsNullOrEmpty(model.BudgetPageModel.BudgetName))
            {
                modelList = modelList.Where(a => a.BudgetName == model.BudgetPageModel.BudgetName).ToList();
            }
            model.PageNum = modelList.Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 增加或修改数据库
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SaveBudgetAdd(BudgetInfo model)
        {
            try
            {
                var dbModel = DBContext.Entry(model);

                dbModel.State = EntityState.Added;

                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 增加或修改数据库
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SavePublicityBudgetOff(ApplyBudget model)
        {
            try
            {
                var GXmodel = DBContext.BudgetInfoes.Where(a => a.SEQ == model.BudgetSEQ).FirstOrDefault();
                GXmodel.ApplyAmount += model.ApplyMoney;
                DBContext.SaveChanges();
                var dbModel = DBContext.Entry(model);
                dbModel.State = EntityState.Added;
                var BudgetInfoModel = DBContext.BudgetInfoes.Where(a => a.SEQ == model.BudgetSEQ).FirstOrDefault();
                BudgetInfoModel.ApplyAmount = BudgetInfoModel.ApplyAmount ?? 0;
                if (BudgetInfoModel != null)
                {
                    BudgetInfoModel.ApplyAmount += model.ApplyMoney;
                }
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 上传申请物品需要的审批文件
        /// </summary>
        /// <param name="seq">申请序号</param>
        /// <param name="fileName">上传文件</param>
        /// <returns>True 上传成功 False 上传失败</returns>
        public Boolean SaveApplicationRecordFileUpload(int seq, string fileName)
        {
            try
            {
                var tempModel = DBContext.ApplicationRecords.Where(a => a.SEQ == seq).FirstOrDefault();
                if (tempModel != null)
                {
                    tempModel.ApprovalFile = fileName;
                    DBContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Boolean SaveBudgetExpenditureFileUpload(PartialBudgetExpenditureShowModel model)
        {
            try
            {
                var dbModel = DBContext.Entry(model.PageModel);
                dbModel.State = EntityState.Added;
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取指定用户的所属分公司
        /// </summary>
        /// <param name="userNo">用户登录名</param>
        /// <returns>返回分公司名称</returns>
        public string GetUserCompanyName(string userNo)
        {
            var SQL = @"select a.UserNo as UserNo_Other,a.UserName as UserName_Other,b.UserNo,b.UserName,c.CompanyName 
                            from TicketManage.dbo.UserInfo_Other as a
                            left join TicketManage.dbo.UserInfo as b
                            on a.UserNo=b.UserNo
                            left join TicketManage.dbo.CompanyInfo as c
                            on b.CompanySEQ=c.SEQ";
            var ExcelSQL = new StringBuilder(SQL);
            if (!string.IsNullOrEmpty(userNo))
            {
                ExcelSQL.AppendFormat(" where a.UserNo='{0}'", userNo);
            }
            var tempList = DBContext.Database.SqlQuery<UserCompanyModel>(ExcelSQL.ToString()).ToList();
            var tempName = string.Empty;
            if (tempList != null && tempList.Count > 0)
            {
                tempName = tempList.FirstOrDefault().CompanyName;
            }
            return tempName;
        }

        public KeyValueModel GetCompanyInfoByUserNo(string userNo)
        {
            var SQL = @"SELECT b.SEQ as [Key],b.CodeName as [Value]
                                  FROM [TicketManage].[dbo].[UserInfo_Other] as a
                                  left join [TicketManage].[dbo].[SystemCode] as b
                                  on a.CompanySEQ=b.SEQ";
            var ExcelSQL = new StringBuilder(SQL);
            if (!string.IsNullOrEmpty(userNo))
            {
                ExcelSQL.AppendFormat(" where a.UserNo='{0}'", userNo);
            }
            var tempList = DBContext.Database.SqlQuery<KeyValueModel>(ExcelSQL.ToString()).ToList();

            if (tempList != null && tempList.Count > 0)
            {
                return tempList.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

    }
}
