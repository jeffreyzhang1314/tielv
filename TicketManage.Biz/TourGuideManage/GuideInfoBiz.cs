using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using TicketManage.Entity.TourGuideInfoEvaluate;
using TicketManage.Entity.TourGuideManage;
using TicketManage.Repository.Models;
using TicketManage.Repository.Models.ExtModels;

namespace TieckManage.Biz.TourGuideManage
{
    /// <summary>
    /// 导游管理类相关服务方法
    /// </summary>
    public class GuideInfoBiz
    {
        private TicketManageContext DBContext = new TicketManageContext();

        public List<GuideInfoEvaluate> ShowEvaluateInfo()
        {
            var list = DBContext.GuideInfoEvaluates.ToList();
            return list;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<GuideInfoExtPageModel> ShowGuideInfo(string guideName)
        {
            string SQL = @"select GuideNo,GuideName,Sex,Age,PassportNo,GuideCardID,ContactNo,GuideState,[StartDate],[EndDate] from [GuideInfo] where 1=1 ";

            var SQLBUILDER = new StringBuilder(SQL);
            if (!string.IsNullOrEmpty(guideName))
            {
                SQLBUILDER.AppendFormat(" and GuideName='{0}' ", guideName);
            }
            return DBContext.Database.SqlQuery<GuideInfoExtPageModel>(SQLBUILDER.ToString()).ToList();
        }

        /// <summary>
        /// 调整导游出团时间和出团状态
        /// </summary>
        /// <param name="passportNo">身份证号</param>
        /// <param name="startDate">出团时间</param>
        /// <param name="endDate">回团时间</param>
        /// <returns></returns>
        public Boolean EditGuideLeadTime(string passportNo, DateTime? startDate, DateTime? endDate, ref string message)
        {
            var Flag = false;
            if (startDate != null && endDate != null)
            {
                var tempModel = DBContext.GuideInfoes.Where(a => a.PassportNo == passportNo).FirstOrDefault();
                if (tempModel != null)
                {
                    tempModel.StartDate = startDate;
                    tempModel.EndDate = endDate;
                    DBContext.SaveChanges();
                    Flag = true;
                    message = "操作成功";
                }
                else
                {
                    message = "身份证号为找到";
                }
            }
            else
            {
                message = "时期格式不正确";
            }
            return Flag;
        }

        /// <summary>
        /// 更新执行身份证号的导游出团回团时间
        /// </summary>
        /// <param name="passportNo">导游身份证号</param>
        /// <returns>是否执行成功</returns>
        public Boolean GuideLeadTimeClear(string passportNo, ref string message)
        {
            var flag = false;
            if (!string.IsNullOrEmpty(passportNo))
            {
                var tempModel = DBContext.GuideInfoes.Where(a => a.PassportNo == passportNo).FirstOrDefault();
                if (tempModel != null)
                {
                    tempModel.StartDate = null;
                    tempModel.EndDate = null;
                    tempModel.IsLeadState = false;
                    DBContext.SaveChanges();
                    flag = true;
                    message = "执行成功";
                }
                else
                {
                    message = "未找到身份号对应的导游信息~！";
                }
            }
            else
            {
                message = "身份证号不能为空~！";
            }
            return flag;
        }


        /// <summary>
        /// 增加或修改数据库导游实体
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SaveGuideInfo(GuideInfoPageModel model)
        {
            try
            {
                var dbModel = DBContext.Entry(model.PageModel);
                if (model.PageModel.SEQ == 0)
                {
                    model.PageModel.GuideState = true;
                    dbModel.State = EntityState.Added;
                }
                else
                {
                    dbModel.State = EntityState.Modified;
                }
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 删除指定编号导游信息
        /// </summary>
        /// <param name="seq">导游实体编号</param>
        /// <returns></returns>
        public Boolean DelGuideInfo(int seq)
        {
            Boolean result = false;
            try
            {
                var model = DBContext.GuideInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
                var entry = DBContext.Entry(model);
                entry.State = EntityState.Deleted;
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }
        /// <summary>
        /// 删除黑名单
        /// </summary>
        /// <param name="seq">导游实体编号</param>
        /// <returns></returns>
        public Boolean GuideStateDel(int seq)
        {
            Boolean result = false;
            try
            {
                var model = DBContext.GuideInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
                model.GuideState = true;
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }


        /// <summary>
        /// 获取指定序号的导游实体
        /// </summary>
        /// <param name="seq">导游序号</param>
        /// <returns></returns>
        public GuideInfo ShowGuideInfoBySeq(int seq)
        {
            var model = DBContext.GuideInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }

        /// <summary>
        /// 查询前部导游信息
        /// </summary>
        /// <returns></returns>
        public List<GuideInfo> ShowGuideblackByAll()
        {
            var modeList = DBContext.GuideInfoes.Where(a => a.GuideState == false).ToList();
            return modeList;
        }

        /// <summary>
        /// 查询前部导游信息
        /// </summary>
        /// <returns></returns>
        public List<GuideInfo> ShowGuideblackByAll(GuideInfoPageModel model)
        {
            //var modelList = new List<GuideInfo>();
            //model.PageNum = DBContext.GuideInfoes.Where(a => a.GuideState == false).Count();
            //if (model.PageNum > 0)
            //{
            //    model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            //    modelList = DBContext.GuideInfoes
            //        .Where(a => a.GuideState == false)
            //        .OrderByDescending(a => a.CreateTime)
            //        .Skip(model.PageSize * (model.CurrentPage - 1))
            //       .Take(model.PageSize).ToList();
            //}
            //return modelList;

            model.PageNum = DBContext.GuideInfoes.Where(a => a.GuideState == true).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var modeList = DBContext.GuideInfoes
                .Where(a => a.GuideState == false)
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modeList;
        }

        /// <summary>
        /// 根据查询条件返回符合条件的数据集合
        /// </summary>
        /// <param name="model">查询条件</param>
        /// <returns></returns>
        public List<GuideInfo> ShowGuideblackByCondition(GuideInfoSearchPageModel model)
        {
            var modelList = DBContext.GuideInfoes.Where(a => a.GuideState == false).ToList();
            //导游名字查询
            if (!string.IsNullOrEmpty(model.PageModel.GuideName))
            {
                modelList = modelList.Where(a => a.GuideName.Contains(model.PageModel.GuideName)).ToList();
            }
            //导游性别查询
            if (model.PageModel.Sex != null)
            {
                modelList = modelList.Where(a => a.Sex == model.PageModel.Sex).ToList();
            }
            //导游编号查询
            if (!string.IsNullOrEmpty(model.PageModel.GuideNo))
            {
                modelList = modelList.Where(a => a.GuideNo == model.PageModel.GuideNo).ToList();
            }
            //导游身份证号查询
            if (!string.IsNullOrEmpty(model.PageModel.PassportNo))
            {
                modelList = modelList.Where(a => a.PassportNo == model.PageModel.PassportNo).ToList();
            }
            //是否有导游证
            if (model.PageModel.IsGuideCard != null)
            {
                modelList = modelList.Where(a => a.IsGuideCard == model.PageModel.IsGuideCard).ToList();
            }
            //是否领队证查询
            if (model.PageModel.IsLeaderCard != null)
            {
                modelList = modelList.Where(a => a.IsLeaderCard == model.PageModel.IsLeaderCard).ToList();
            }
            //是否有护照
            if (model.PageModel.IsPassportCard != null)
            {
                modelList = modelList.Where(a => a.IsPassportCard == model.PageModel.IsPassportCard).ToList();
            }
            //是否有国集团经验
            if (model.PageModel.IsInterGroup != null)
            {
                modelList = modelList.Where(a => a.IsInterGroup == model.PageModel.IsInterGroup).ToList();
            }
            model.PageNum = modelList.Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }

        /// <summary>
        /// 查询黑名单导游信息
        /// </summary>
        /// <returns></returns>
        public List<GuideInfo> ShowGuideInfoByAll(GuideInfoPageModel model)
        {
            string SQL = @"select a.SEQ,GuideNo,GuideName,Sex,Age,PassportNo,GuideCardID,ContactNo,PlaceOrigin,
                                  b.CodeName Education,GuideState,Reason,ChangeStateTime,
                                  IsGuideCard,IsLeaderCard,IsPassportCard,IsInterGroup,a.CreateTime,GuideStarNum,GuidNo,
                                    PassportNoFile,GuideCardIDFile,GuidNoFile,DirectoryCompanyNo,StartDate,EndDate,IsLeadState
                             from [GuideInfo] a left join SystemCode b on a.Education =b.CodeNo and b.CategoryNo='EduType' where a.GuideState=1";

            model.PageNum = DBContext.Database.SqlQuery<GuideInfo>(SQL).Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var modeList = DBContext.Database.SqlQuery<GuideInfo>(SQL)
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modeList;

        }

        /// <summary>
        /// 查询黑名单导游信息
        /// </summary>
        /// <returns></returns>
        public List<GuideInfo> ShowGuideInfoByAll()
        {
            var modeList = DBContext.GuideInfoes.ToList();
            return modeList;
        }

        /// <summary>
        /// 根据查询条件返回符合条件的黑名单
        /// </summary>
        /// <param name="model">查询条件</param>
        /// <returns></returns>
        public List<GuideInfo> ShowGuideInfoByCondition(GuideInfoSearchPageModel model)
        {
            string SQL = @"select a.SEQ,GuideNo,GuideName,Sex,Age,PassportNo,GuideCardID,ContactNo,PlaceOrigin,
                                  b.CodeName Education,GuideState,Reason,ChangeStateTime,PassportNoFile,GuideCardIDFile,GuidNoFile,
                                  IsGuideCard,IsLeaderCard,IsPassportCard,IsInterGroup,a.CreateTime,GuideStarNum,GuidNo,DirectoryCompanyNo,StartDate,EndDate,IsLeadState
                             from [GuideInfo] a left join SystemCode b on a.Education =b.CodeNo and b.CategoryNo='EduType' where a.GuideState=1 ";

            //是否有导游证
            if (model.PageModel.IsGuideCard != null)
            {
                if (model.PageModel.IsGuideCard == true)
                {
                    SQL = SQL + " and isnull(a.GuideCardID,'')<>'' ";
                }
                else if (model.PageModel.IsGuideCard == false)
                {
                    SQL = SQL + " and isnull(a.GuideCardID,'')='' ";
                }
            }

            if (!string.IsNullOrEmpty(model.PageModel.GuideName))
            {
                SQL = SQL + " and GuideName='" + model.PageModel.GuideName + "'";
            }

            var modelList = DBContext.Database.SqlQuery<GuideInfo>(SQL).ToList();
            //导游性别查询
            if (model.PageModel.Sex != null)
            {
                modelList = modelList.Where(a => a.Sex == model.PageModel.Sex).ToList();
            }
            //导游编号查询
            if (!string.IsNullOrEmpty(model.PageModel.GuideNo))
            {
                modelList = modelList.Where(a => a.GuideNo == model.PageModel.GuideNo).ToList();
            }
            //导游身份证号查询
            if (!string.IsNullOrEmpty(model.PageModel.PassportNo))
            {
                modelList = modelList.Where(a => a.PassportNo == model.PageModel.PassportNo).ToList();
            }

            //是否领队证查询
            if (model.PageModel.IsLeaderCard != null)
            {
                modelList = modelList.Where(a => a.IsLeaderCard == model.PageModel.IsLeaderCard).ToList();
            }
            //是否有护照
            if (model.PageModel.IsPassportCard != null)
            {
                modelList = modelList.Where(a => a.IsPassportCard == model.PageModel.IsPassportCard).ToList();
            }
            //是否有国集团经验
            if (model.PageModel.IsInterGroup != null)
            {
                modelList = modelList.Where(a => a.IsInterGroup == model.PageModel.IsInterGroup).ToList();
            }

            //所属分公司
            if (!string.IsNullOrEmpty(model.PageModel.DirectoryCompanyNo))
            {
                modelList = modelList.Where(a => a.DirectoryCompanyNo == model.PageModel.DirectoryCompanyNo).ToList();
            }

            model.PageNum = modelList.Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;

        }


        public List<GuideInfo> ShowGuideInfoByExport(GuideInfoSearchPageModel model)
        {
            string SQL = @"select a.SEQ,GuideNo,GuideName,Sex,Age,PassportNo,GuideCardID,ContactNo,PlaceOrigin,
                                  b.CodeName Education,GuideState,Reason,ChangeStateTime,PassportNoFile,GuideCardIDFile,GuidNoFile,
                                  IsGuideCard,IsLeaderCard,IsPassportCard,IsInterGroup,a.CreateTime,GuideStarNum,GuidNo,DirectoryCompanyNo,StartDate,EndDate,IsLeadState
                             from [GuideInfo] a left join SystemCode b on a.Education =b.CodeNo and b.CategoryNo='EduType' where a.GuideState=1 ";

            //是否有导游证
            if (model.PageModel.IsGuideCard != null)
            {
                if (model.PageModel.IsGuideCard == true)
                {
                    SQL = SQL + " and isnull(a.GuideCardID,'')<>'' ";
                }
                else if (model.PageModel.IsGuideCard == false)
                {
                    SQL = SQL + " and isnull(a.GuideCardID,'')='' ";
                }
            }

            if (!string.IsNullOrEmpty(model.PageModel.GuideName))
            {
                SQL = SQL + " and GuideName='" + model.PageModel.GuideName + "'";
            }

            var modelList = DBContext.Database.SqlQuery<GuideInfo>(SQL).ToList();
            //导游性别查询
            if (model.PageModel.Sex != null)
            {
                modelList = modelList.Where(a => a.Sex == model.PageModel.Sex).ToList();
            }
            //导游编号查询
            if (!string.IsNullOrEmpty(model.PageModel.GuideNo))
            {
                modelList = modelList.Where(a => a.GuideNo == model.PageModel.GuideNo).ToList();
            }
            //导游身份证号查询
            if (!string.IsNullOrEmpty(model.PageModel.PassportNo))
            {
                modelList = modelList.Where(a => a.PassportNo == model.PageModel.PassportNo).ToList();
            }

            //是否领队证查询
            if (model.PageModel.IsLeaderCard != null)
            {
                modelList = modelList.Where(a => a.IsLeaderCard == model.PageModel.IsLeaderCard).ToList();
            }
            //是否有护照
            if (model.PageModel.IsPassportCard != null)
            {
                modelList = modelList.Where(a => a.IsPassportCard == model.PageModel.IsPassportCard).ToList();
            }
            //是否有国集团经验
            if (model.PageModel.IsInterGroup != null)
            {
                modelList = modelList.Where(a => a.IsInterGroup == model.PageModel.IsInterGroup).ToList();
            }
            modelList = modelList.OrderByDescending(a => a.CreateTime).ToList();
            return modelList;

        }

        public SystemCode GetCodeByCodeNo(string codeNo)
        {
            SystemCode code = new SystemCode();
            code = DBContext.SystemCodes.Where(p => p.CodeNo == codeNo && p.CategoryNo == "EduType").FirstOrDefault();
            return code;
        }
        public List<SystemCode> GetSystemCodeListByType(string CodeType)
        {
            List<SystemCode> result = new List<SystemCode>();
            result = DBContext.SystemCodes.Where(p => p.CategoryNo == CodeType).ToList();

            return result;
        }

        /// <summary>
        /// 查询黑名单导游信息
        /// </summary>
        /// <returns></returns>
        public List<GuideInfo> ShowGuideListEvaluateByAll()
        {
            var modeList = DBContext.GuideInfoes.ToList();
            return modeList;
        }

        /// <summary>
        /// 查询黑名单导游信息
        /// </summary>
        /// <returns></returns>
        public List<GuideInfo> ShowGuideListEvaluateByAll(GuideInfoPageModel model)
        {
            model.PageNum = DBContext.GuideInfoes.Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            var modeList = DBContext.GuideInfoes
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modeList;
        }

        /// <summary>
        /// 根据查询条件返回符合条件的黑名单
        /// </summary>
        /// <param name="model">查询条件</param>
        /// <returns></returns>
        public List<GuideInfo> ShowGuideListEvaluateByCondition(GuideInfoPageModel model)
        {
            var modelList = DBContext.GuideInfoes.ToList();
            //导游名字查询
            if (!string.IsNullOrEmpty(model.PageModel.GuideName))
            {
                modelList = modelList.Where(a => a.GuideName.Contains(model.PageModel.GuideName)).ToList();
            }
            //导游身份证号查询
            if (!string.IsNullOrEmpty(model.PageModel.PassportNo))
            {
                modelList = modelList.Where(a => a.PassportNo == model.PageModel.PassportNo).ToList();
            }
            if (!string.IsNullOrEmpty(model.IsBlackGuide) && string.Compare(model.PageModel.DirectoryCompanyNo, "全部", true) != 0)
            {
                modelList = modelList.Where(a => a.GuideState == model.PageModel.GuideState).ToList();
            }

            if (!string.IsNullOrEmpty(model.PageModel.DirectoryCompanyNo) && string.Compare(model.PageModel.DirectoryCompanyNo, "全部", true) != 0)
            {
                modelList = modelList.Where(a => a.DirectoryCompanyNo == model.PageModel.DirectoryCompanyNo).ToList();
            }

            model.PageNum = modelList.Count();
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            modelList = modelList
                .OrderByDescending(a => a.CreateTime)
                .Skip(model.PageSize * (model.CurrentPage - 1))
               .Take(model.PageSize).ToList();
            return modelList;
        }
        /// <summary>
        /// 获取指定序号的导游评价实体
        /// </summary>
        /// <param name="seq">导游序号</param>
        /// <returns></returns>
        public GuideInfo ShowGuideEvaluateBySeq(int seq)
        {
            var model = DBContext.GuideInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
            return model;
        }
        /// <summary>
        /// 增加或修改数据库导游实体
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean SaveGuideEvaluate(GuideInfoEvaluate model)
        {
            try
            {
                var dbModel = DBContext.Entry(model);

                dbModel.State = EntityState.Added;

                DBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 查询导游评价信息
        /// </summary>
        /// <returns></returns>
        public List<V_GuideInfoEvaluateView> ShowGuideDetailListEvaluateByAll(int obj)
        {
            var modeList = DBContext.V_GuideInfoEvaluateView.Where(a => a.GuideSEQ == obj).ToList();

            return modeList;
        }

        /// <summary>
        /// 查询导游评价信息
        /// </summary>
        /// <returns></returns>
        public List<V_GuideInfoEvaluateView> ShowGuideDetailListEvaluateByAll(GuideInfoEvaluatePageModel model)
        {
            var modeList = DBContext.V_GuideInfoEvaluateView.Where(a => a.GuideSEQ == model.GuideSEQ).ToList();
            model.PageNum = modeList.Count();
            if (model.PageNum > 0)
            {
                model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
                modeList = modeList
                    .OrderByDescending(a => a.SEQ)
                    .Skip(model.PageSize * (model.CurrentPage - 1))
                   .Take(model.PageSize).ToList();
            }
            return modeList;
        }

        /// <summary>
        /// 修改星级
        /// </summary>
        /// <param name="seq">修改星级</param>
        /// <returns></returns>
        public Boolean SaveGuideStarNum(int? seq, int? StarNum)
        {
            Boolean result = false;
            try
            {
                var model = DBContext.GuideInfoes.Where(a => a.SEQ == seq).FirstOrDefault();
                model.GuideStarNum = Convert.ToInt32(StarNum);
                DBContext.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

    }
}
