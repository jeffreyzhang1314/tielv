﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TicketManage.Tools
{
    public class CommonHelper
    {
        /// <summary>
        /// 判断字符是否为Numeric类型
        /// </summary>
        /// <param name="value">判断字符</param>
        /// <returns>True是Numeric类型False不是Numeric类型</returns>
        public static bool IsNumeric(string value)
        {
            return Regex.IsMatch(value, @"^[-]?\d+[.]?\d*$");
        }

        /// <summary>
        /// 判断字符是否为Int类型
        /// </summary>
        /// <param name="value">判断字符</param>
        /// <returns>True是Int类型False不是Int类型</returns>
        public static bool IsInt(string value)
        {
            return Regex.IsMatch(value, @"^\d+$");
        }


        public static bool IsUnsign(string value)
        {

            return Regex.IsMatch(value, @"^/d*[.]?/d*$");

        }
    }
}
