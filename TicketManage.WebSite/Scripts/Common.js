﻿function CheckAll(obj) {
    if (obj.checked) {
        $(obj).parent().parent().parent().parent().parent().find("tbody").find("input[type='checkbox']:checkbox").prop('checked', true);
    } else {
        $(obj).parent().parent().parent().parent().parent().find("tbody").find("input[type='checkbox']:checkbox").prop('checked', false);
    }
}


function initFileInput() {
    var url = "~/UploadFile/1.jpg";
    $("form").find("input[type='file']").each(function () {
        $(this).fileinput('refresh', {
            uploadUrl: "",
            overwriteInitial: false,
            showCaption: false,
            showUpload: false,
            showPreview: true,
            maxFileSize: 1000000,
            maxFileCount: 10,
            minFileCount: 0,
            language: 'zh',

        });
    });
}

function ListPager(totalCount, pageNumber, pagesize, controller, action) {
    var element = $('#ticketlist-pager');//获得数据装配的位置
    //初始化所需数据
    var options = {
        bootstrapMajorVersion: 3,
        currentPage: pageNumber, //当前页数
        numberOfPages: 6, //显示页码数标个数
        totalPages: totalCount, //总共的数据所需要的总页数
        itemTexts: function (type, page, current) {
            //图标的更改显示可以在这里修改。
            switch (type) {
                case "first":
                    return "首页";
                case "prev":
                    return "上一页";
                case "next":
                    return "下一页";
                case "last":
                    return "尾页";
                case "page":
                    return page;
            }
        },
        tooltipTitles: function (type, page, current) {
            //如果想要去掉页码数字上面的预览功能，则在此操作。例如：可以直接return。
            switch (type) {
                case "first":
                    return "Go to first page";
                case "prev":
                    return "Go to previous page";
                case "next":
                    return "Go to next page";
                case "last":
                    return "Go to last page";
                case "page":
                    return (page === current) ? "Current page is " + page : "Go to page " + page;
            }
        },
        onPageClicked: function (e, originalEvent, type, page) {
            //单击当前页码触发的事件。若需要与后台发生交互事件可在此通过ajax操作。page为目标页数。
            //console.log(e);
            //console.log(originalEvent);
            // console.log(type);

            //$("#tabList").load("/" + controller + "/" + action, inputData, function (data) {

            //});
            $.get("/" + controller + "/" + action, { PageNum: page, PageSize: pagesize }, function (data) {
                $("#tabList").html($(data).find("#tabList").html());
            });
        }
    };
    element.bootstrapPaginator(options);	//进行初始化
}

function ReturnPageBack() {
    history.go(-1);
}


function ChangePageShowCount(pageNumber, obj, controller, action) {
    var pagesize = $(obj).val();
    $.get("/" + controller + "/" + action, { PageSize: pagesize }, function (data) {
        $("#tabList").html($(data).find("#tabList").html());
    });
    var count = $("#totalCount").val();
    var totalCount = 0;
    if (parseInt(count) > parseFloat(pagesize)) {
        if (parseInt(count) % parseInt(pagesize) == 0) {
            totalCount = parseInt(count) / parseInt(pagesize);
        }
        else {
            totalCount = parseInt(count) / parseInt(pagesize) + 1;
        }
    }
    ListPager(totalCount, pageNumber, $(obj).val(), controller, action);
}

function LoadDateTimePicker() {
    $('.mydatapicker').datetimepicker({
        language: 'zh-CN',//显示中文
        format: 'yyyy-mm-dd',//显示格式
        minView: "month",
        initialDate: new Date(),//初始化当前日期
        autoclose: true,//选中自动关闭
        todayBtn: true//显示今日按钮
    })
}



function sendFile(files, editor, $editable) {
    var data = new FormData();
    data.append("ajaxTaskFile", files[0]);
    $.ajax({
        data: data,
        type: "POST",
        url: "/TicketNotice/SummernoteImage", //图片上传出来的url，返回的是图片上传后的路径，http格式  
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function (data) {//data是返回的hash,key之类的值，key是定义的文件名
            debugger;
            $('#summernote').summernote('insertImage', data.data);
        },
        error: function () {
            alert("上传失败");
        }
    });
}

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}