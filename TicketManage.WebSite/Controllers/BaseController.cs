using System;
using System.Web.Mvc;
using System.Configuration;
using TicketManage.Entity.TicketManage;
using System.Collections.Generic;
using TicketManage.Entity.CommonManage;
using System.Web;
using System.IO;
using TicketManage.Repository.Models;
using TicketManage.Entity.OtherUserManage;
using System.Text.RegularExpressions;

namespace TicketManage.WebSite.Controllers
{
    /// <summary>
    /// Description of BaseController.
    /// </summary>
    public class BaseController : Controller
    {
        protected List<KeyValueModel> BudgetSplitCategorySource = new List<KeyValueModel>() { new KeyValueModel { Key = 1, Value = "广告费" }, new KeyValueModel { Key = 2, Value = "业务宣传费" } };

        /// <summary>
        /// 转换字符串到系统枚举
        /// </summary>
        /// <param name="systemName">系统名称</param>
        /// <returns>系统枚举值</returns>
        protected SystemName ConvertSystemName(string systemName)
        {
            if (!string.IsNullOrEmpty(systemName)
              && (string.Compare(systemName, "地接管理系统", true) == 0
                  || string.Compare(systemName, "导游管理系统", true) == 0
                  || string.Compare(systemName, "宣传品管理系统", true) == 0
                  || string.Compare(systemName, "工效管理系统", true) == 0))
            {
                return (TicketManage.Entity.CommonManage.SystemName)Enum.Parse(typeof(TicketManage.Entity.CommonManage.SystemName), systemName);
            }
            else
            {
                return 0;
            }
        }

        protected List<Repository.Models.TravelAgencyPlugIn> FileUploadByTravelAgency(HttpRequestBase request)
        {
            var tempList = new List<TravelAgencyPlugIn>();
            string filepath = ConfigurationManager.AppSettings["FilePath"].ToString();
            int roomNum = 1, foodNum = 1, carNum = 1;
            string roomSummary = string.Empty, foodSummary = string.Empty, carSummary = string.Empty;
            string roomPrice = string.Empty, foodPrice = string.Empty, carPrice = string.Empty;
            int roomSEQ = 0, foodSEQ = 0, carSEQ = 0;
            if (request.Files != null)
            {
                for (int i = 0; i < request.Files.Count; i++)
                {
                    if (request.Files[i].ContentLength > 0)
                    {
                        HttpPostedFileBase file = request.Files[i];
                        var CFileName = string.Format("{0}.{1}", Guid.NewGuid().ToString(), file.FileName.Split('.')[1]);
                        var filename = Path.Combine(string.Format("{0}{1}", request.PhysicalApplicationPath, filepath), CFileName);
                        file.SaveAs(filename);
                        if (request.Files.Keys[i].IndexOf("PageModelPlugin.RoomFileName") > -1)
                        {
                            roomSummary = request.Form["PageModelPlugin.RoomSummary" + roomNum];
                            roomPrice = request.Form["PageModelPlugin.RoomPlugInPirce" + roomNum];
                            roomSEQ = Convert.ToInt32(request.Form["PageModelPlugin.RoomSEQ" + roomNum]);
                            tempList.Add(new TravelAgencyPlugIn { SEQ = roomSEQ, FileContent = CFileName, PlugInCategory = 1, Summary = roomSummary, PlugInPirce = roomPrice });
                        }
                        else if (request.Files.Keys[i].IndexOf("PageModelPlugin.FoodFileName") > -1)
                        {
                            foodSummary = request.Form["PageModelPlugin.FoodSummary" + foodNum];
                            foodPrice = request.Form["PageModelPlugin.FoodPlugInPirce" + foodNum];
                            foodSEQ = Convert.ToInt32(request.Form["PageModelPlugin.FoodSEQ" + roomNum]);
                            tempList.Add(new TravelAgencyPlugIn { SEQ = foodSEQ, FileContent = CFileName, PlugInCategory = 2, Summary = foodSummary, PlugInPirce = foodPrice });
                        }
                        else if (request.Files.Keys[i].IndexOf("PageModelPlugin.CarFileName") > -1)
                        {
                            carSummary = request.Form["PageModelPlugin.CarSummary" + carNum];
                            carPrice = request.Form["PageModelPlugin.CarPlugInPirce" + carNum];
                            carSEQ = Convert.ToInt32(request.Form["PageModelPlugin.CarSEQ" + roomNum]);
                            tempList.Add(new TravelAgencyPlugIn { SEQ = carSEQ, FileContent = CFileName, PlugInCategory = 3, Summary = carSummary, PlugInPirce = carPrice });
                        }
                        else if (request.Files.Keys[i].IndexOf("PageModel.TravelAgencyFile") > -1)
                        {
                            carSummary = request.Form["PageModelPlugin.TravelAgencyFile"];
                            tempList.Add(new TravelAgencyPlugIn { FileContent = CFileName, PlugInCategory = 4, Summary = carSummary });
                        }
                    }
                    else
                    {
                        var CFileName = string.Empty;
                        if (request.Files.Keys[i].IndexOf("PageModelPlugin.RoomFileName") > -1)
                        {
                            roomSummary = request.Form["PageModelPlugin.RoomSummary" + roomNum];
                            roomPrice = request.Form["PageModelPlugin.RoomPlugInPirce" + roomNum];
                            roomSEQ = Convert.ToInt32(request.Form["PageModelPlugin.RoomSEQ" + roomNum]);
                            if (string.IsNullOrEmpty(roomSummary) && string.IsNullOrEmpty(roomPrice) && roomSEQ == 0)
                            {

                            }
                            else
                            {
                                tempList.Add(new TravelAgencyPlugIn { SEQ = roomSEQ, PlugInCategory = 1, Summary = roomSummary, PlugInPirce = roomPrice });
                            }
                        }
                        else if (request.Files.Keys[i].IndexOf("PageModelPlugin.FoodFileName") > -1)
                        {
                            foodSummary = request.Form["PageModelPlugin.FoodSummary" + foodNum];
                            foodPrice = request.Form["PageModelPlugin.FoodPlugInPirce" + foodNum];
                            foodSEQ = Convert.ToInt32(request.Form["PageModelPlugin.FoodSEQ" + roomNum]);
                            if (string.IsNullOrEmpty(foodSummary) && string.IsNullOrEmpty(foodPrice) && foodSEQ == 0)
                            {

                            }
                            else
                            {
                                tempList.Add(new TravelAgencyPlugIn { SEQ = foodSEQ, PlugInCategory = 2, Summary = foodSummary, PlugInPirce = foodPrice });
                            }
                        }
                        else if (request.Files.Keys[i].IndexOf("PageModelPlugin.CarFileName") > -1)
                        {
                            carSummary = request.Form["PageModelPlugin.CarSummary" + carNum];
                            carPrice = request.Form["PageModelPlugin.CarPlugInPirce" + carNum];
                            carSEQ = Convert.ToInt32(request.Form["PageModelPlugin.CarSEQ" + roomNum]);
                            if (string.IsNullOrEmpty(carSummary) && string.IsNullOrEmpty(carPrice) && carSEQ == 0)
                            {

                            }
                            else
                            {
                                tempList.Add(new TravelAgencyPlugIn { SEQ = carSEQ, PlugInCategory = 3, Summary = carSummary, PlugInPirce = carPrice });
                            }

                        }
                    }
                }
            }
            return tempList;
        }

        protected Dictionary<string, string> FileUpload(HttpRequestBase request)
        {
            var tempList = new Dictionary<string, string>();
            string filepath = ConfigurationManager.AppSettings["FilePath"].ToString();
            if (request.Files != null)
            {
                for (int i = 0; i < request.Files.Count; i++)
                {
                    if (request.Files[i].ContentLength > 0)
                    {
                        HttpPostedFileBase file = request.Files[i];
                        string CFileName = Guid.NewGuid().ToString() + "." + file.FileName.Split('.')[1];
                        var filename = Path.Combine(string.Format("{0}{1}", request.PhysicalApplicationPath, filepath), CFileName);

                        file.SaveAs(filename);
                        tempList.Add(CFileName, request.Files.Keys[i]);
                    }
                }
            }
            return tempList;
        }

        /// <summary>
        /// 转换制定基础代码为Select数据源
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        protected List<SelectListItem> ConvertSystemCode(List<SystemCode> list)
        {
            var result = new List<SelectListItem>();
            result.Add(new SelectListItem() { Value = "0", Text = "-请选择-" });
            try
            {
                if (list != null && list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        SelectListItem selectItem = new SelectListItem()
                        {
                            Text = item.CodeName,
                            Value = item.CodeNo
                        };
                        result.Add(selectItem);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 查询性别数据源
        /// </summary>
        protected Dictionary<string, string> SchSexSource
        {
            get
            {
                var authNum = new Dictionary<string, string>();
                authNum.Add("", "全部");
                authNum.Add("True", "男");
                authNum.Add("False", "女");
                return authNum;
            }
        }

        /// <summary>
        /// 检测用户是否有权登录系统
        /// </summary>
        /// <param name="authNum">系统权限值</param>
        /// <param name="systemCategory">用户但钱访问系统标记</param>
        /// <returns>True 可以登录 False 不可以登录</returns>
        protected Boolean CheckUserAuth(int? authNum, SystemName systemCategory)
        {
            var flag = false;
            if (((authNum & 2) == 2
                || (authNum & 4) == 4
                || (authNum & 8) == 8
                || (authNum & 16) == 16
                || (authNum & 32) == 32)
                && systemCategory == SystemName.票务管理系统)
            {
                flag = true;
            }
            else if (((authNum & 4096) == 4096
                || (authNum & 8192) == 8192
                || (authNum & 16384) == 16384)
                && systemCategory == SystemName.地接管理系统)
            {
                flag = true;
            }
            else if (((authNum & 4096) == 4096
                || (authNum & 8192) == 8192
                || (authNum & 16384) == 16384
                || (authNum & 32768) == 32768)
                && systemCategory == SystemName.导游管理系统)
            {
                flag = true;
            }
            else if (((authNum & 65536) == 65536
                || (authNum & 131072) == 131072)
                && systemCategory == SystemName.宣传品管理系统)
            {
                flag = true;
            }
            else if (((authNum & 1) == 1
                || (authNum & 64) == 64
                || (authNum & 128) == 128
                || (authNum & 256) == 256
                || (authNum & 512) == 512
                || (authNum & 1024) == 1024
                || (authNum & 2048) == 2048
                || (authNum & 262144) == 262144
                || (authNum & 524288) == 524288
                || (authNum & 1048576) == 1048576
                || (authNum & 2097152) == 2097152)
                && systemCategory == SystemName.工效管理系统)
            {
                flag = true;
            }
            else if ((authNum & 1) == 1)
            {
                flag = true;
            }
            return flag;
        }


        /// <summary>
        /// 时间数据集
        /// </summary>
        protected Dictionary<string, string> TimeCategorySource
        {
            get
            {
                var timeCategorySource = new Dictionary<string, string>();
                timeCategorySource.Add("1", "今天");
                timeCategorySource.Add("2", "昨天");
                timeCategorySource.Add("3", "本月");
                timeCategorySource.Add("4", "上月");
                return timeCategorySource;
            }
        }

        /// <summary>
        /// 分页尺寸大小
        /// </summary>
        protected Dictionary<string, string> PageSizeSource
        {
            get
            {
                var pageSize = new Dictionary<string, string>();
                pageSize.Add("5", "5");
                pageSize.Add("10", "10");
                pageSize.Add("15", "15");
                pageSize.Add("20", "20");
                pageSize.Add("50", "50");
                pageSize.Add("100", "100");

                return pageSize;
            }
        }


        /// <summary>
        /// 性别数据源
        /// </summary>
        protected Dictionary<string, string> SexSource
        {
            get
            {
                var authNum = new Dictionary<string, string>();
                authNum.Add("True", "男");
                authNum.Add("False", "女");
                return authNum;
            }
        }

        /// <summary>
        /// 根据不同系统返回相应的权限集合
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected Dictionary<string, string> AuthNumSourceBySystem(SystemName name)
        {
            var authNum = new Dictionary<string, string>();
            var keyValue = ConfigurationManager.AppSettings["AuthNum"];
            if (!string.IsNullOrEmpty(keyValue))
            {
                var listValue = keyValue.Split(',');
                if (listValue.Length > 0)
                {
                    foreach (var item in listValue)
                    {
                        var tempArray = item.Split('/');
                        if (tempArray.Length == 2)
                        {
                            switch (name)
                            {
                                case SystemName.票务管理系统:
                                    if (string.Compare(tempArray[0], "1", true) == 0
                                        || string.Compare(tempArray[0], "2", true) == 0
                                        || string.Compare(tempArray[0], "4", true) == 0
                                        || string.Compare(tempArray[0], "8", true) == 0
                                        || string.Compare(tempArray[0], "16", true) == 0
                                        || string.Compare(tempArray[0], "32", true) == 0)
                                    {
                                        authNum.Add(tempArray[0], tempArray[1]);
                                    }
                                    break;
                                case SystemName.地接管理系统:
                                    if (string.Compare(tempArray[0], "1", true) == 0
                                        || string.Compare(tempArray[0], "4096", true) == 0
                                        || string.Compare(tempArray[0], "8192", true) == 0
                                        || string.Compare(tempArray[0], "147456", true) == 0)
                                    {
                                        authNum.Add(tempArray[0], tempArray[1]);
                                    }
                                    break;
                                case SystemName.导游管理系统:
                                    if (string.Compare(tempArray[0], "1", true) == 0
                                        || string.Compare(tempArray[0], "4096", true) == 0
                                        || string.Compare(tempArray[0], "8192", true) == 0
                                        || string.Compare(tempArray[0], "147456", true) == 0
                                        || string.Compare(tempArray[0], "32768", true) == 0)
                                    {
                                        authNum.Add(tempArray[0], tempArray[1]);
                                    }
                                    break;
                                case SystemName.宣传品管理系统:
                                    if (string.Compare(tempArray[0], "1", true) == 0
                                        || string.Compare(tempArray[0], "65536", true) == 0
                                        || string.Compare(tempArray[0], "147456", true) == 0)
                                    {
                                        authNum.Add(tempArray[0], tempArray[1]);
                                    }
                                    break;
                                case SystemName.工效管理系统:
                                    if (string.Compare(tempArray[0], "1", true) == 0//管理员
                                        || string.Compare(tempArray[0], "64", true) == 0//计调部管理员
                                        || string.Compare(tempArray[0], "128", true) == 0//子分公司负责人
                                        || string.Compare(tempArray[0], "256", true) == 0//主管票务副总
                                        || string.Compare(tempArray[0], "512", true) == 0//主管计调副总
                                        || string.Compare(tempArray[0], "1024", true) == 0//集团董事长
                                        || string.Compare(tempArray[0], "2048", true) == 0//人力资源部长
                                        || string.Compare(tempArray[0], "262144", true) == 0//计调部长
                                        || string.Compare(tempArray[0], "524288", true) == 0//集团票务部长
                                        || string.Compare(tempArray[0], "1048576", true) == 0//人力资源部
                                        || string.Compare(tempArray[0], "2097152", true) == 0)//主管人力副总

                                    {
                                        authNum.Add(tempArray[0], tempArray[1]);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            return authNum;
        }

        /// <summary>
        /// 获取所有权限信息
        /// </summary>
        protected Dictionary<string, string> AuthNumSource
        {
            get
            {
                var keyValue = ConfigurationManager.AppSettings["AuthNum"];
                var listValue = keyValue.Split(',');
                var key = string.Empty;
                var value = string.Empty;
                var authNum = new Dictionary<string, string>();
                foreach (var itemGroup in listValue)
                {
                    var tempArray = itemGroup.Split('/');
                    key = tempArray[0];
                    value = tempArray[1];
                    if ((UserInfoModel.PageModel.AuthNum & 8) == 8)
                    {
                        if (string.Compare(key, "2", true) == 0 || string.Compare(key, "8", true) == 0)
                        {
                            authNum.Add(key, value);
                        }
                    }
                    else if ((UserInfoModel.PageModel.AuthNum & 4) == 4)
                    {
                        if (string.Compare(key, "2", true) == 0
                           || string.Compare(key, "4", true) == 0
                           || string.Compare(key, "8", true) == 0)
                        {
                            authNum.Add(key, value);
                        }
                    }
                    else
                    {
                        authNum.Add(key, value);
                    }
                }
                if (string.Compare(UserInfoModel.PageModel.UserNo, "admin", true) != 0)
                {
                    authNum.Remove("1");
                }
                return authNum;
            }
        }

        /// <summary>
        /// 获取Session中用户信息的KeyValue值
        /// </summary>
        protected string UserInfo
        {
            get
            {
                var keyValue = ConfigurationManager.AppSettings["UserInfo"];
                return keyValue;
            }
        }

        /// <summary>
        /// 当前登录实体
        /// </summary>
        protected UserPageModel UserInfoModel
        {
            get
            {
                var tempModel = Session[UserInfo] as UserPageModel;
                return tempModel;
            }
        }

        /// <summary>
        /// KPI用户信息
        /// </summary>
        protected UserLoginPageModel KPIUserInfoModel
        {
            get
            {
                var tempModel = Session[UserInfo] as UserLoginPageModel;
                return tempModel;
            }
        }

        /// <summary>
        /// 地接，导游，宣存品需要实体
        /// </summary>
        protected UserLoginPageModel CommonUserInfoModel
        {
            get
            {
                var tempModel = Session[UserInfo] as UserLoginPageModel;
                return tempModel;
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        public static bool IsNumeric(string value)
        {
            return Regex.IsMatch(value, @"^[+-]?/d*[.]?/d*$");
        }
        public static bool IsInt(string value)
        {
            return Regex.IsMatch(value, @"^[0-9]*$");
        }
        public static bool IsUnsign(string value)
        {
            return Regex.IsMatch(value, @"^/d*[.]?/d*$");
        }

    }
}