﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TieckManage.Biz.CommonManage;

namespace TicketManage.WebSite.Controllers
{
    /// <summary>
    /// 功效系统登录信息
    /// </summary>
    public class KPICommonController : BaseController
    {

        private CommonBiz biz = new CommonBiz();


        private void LoginInit(UserLoginPageModel model)
        {
            ViewBag.Title = "工效管理系统登录";
            model.SysteCategory = SystemName.工效管理系统;
            model.SystemName = "工效管理系统";
        }

        [HttpGet]
        public ActionResult Login()
        {
            var pageModel = new UserLoginPageModel();
            LoginInit(pageModel);
            pageModel.MessageState = false;
            pageModel.PageModel = new Repository.Models.UserInfo_Other();
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult Login(UserLoginPageModel model)
        {
            LoginInit(model);
            var result = biz.CommonLogin(model);
            if (result)
            {
                if (CheckUserAuth(model.PageModel.AuthNum, model.SysteCategory))
                {
                    Session[UserInfo] = model;
                    return RedirectToAction("OrganizationList", "KPIData");
                }
                else
                {
                    model.Message = "您没有权限访问工效管理系统！";
                    model.MessageState = true;
                }
            }
            else
            {
                model.Message = "用户名密码错误！";
                model.MessageState = false;
            }
            return View(model);
        }

    }
}
