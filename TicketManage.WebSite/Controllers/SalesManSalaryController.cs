﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.KPICalcEfficacyManage;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.KPICalcEfficacyManage;

namespace TicketManage.WebSite.Controllers
{
    public class SalesManSalaryController : BaseController
    {
        private SalesmanSalaryInfoBiz SalesManBiz = new SalesmanSalaryInfoBiz();

        private void SalesManSalaryListInit(SalesmanSalaryInfoPageModel model)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.EmployeeSalaryList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
        }

        [HttpGet]
        public ActionResult SalesManSalaryList()
        {
            var model = new SalesmanSalaryInfoPageModel();
            SalesManSalaryListInit(model);
            model.ModelList = SalesManBiz.SearchSalesmanSalaryInfoByCondition(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult SalesManSalaryList(SalesmanSalaryInfoPageModel model)
        {
            SalesManSalaryListInit(model);
            model.ModelList = SalesManBiz.SearchSalesmanSalaryInfoByCondition(model);
            return View(model);
        }

        [HttpGet]
        public PartialViewResult ShowSalesManSalaryAdd()
        {
            var model = new SalesmanSalaryInfoPageModel();
            return PartialView("", model);
        }

        [HttpGet]
        public PartialViewResult ShowSalesManSalaryEdit(int id)
        {
            var model = SalesManBiz.GetSalesmanSalaryInfoBySEQ(id);
            return PartialView("", model);
        }

        [AjaxOnly]
        public JsonResult SalesManSalaryEdit(SalesmanSalaryInfoPageModel model)
        {
            var result = SalesManBiz.EditSalesmanSalaryInfo(model);
            return Json(new { result = result, message = "操作成功！" }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult SalesManSalaryDel(int id)
        {
            var result = SalesManBiz.DelSalesmanSalaryInfo(id);
            return Json(new { result = result, message = "操作成功！" }, JsonRequestBehavior.AllowGet);
        }

    }
}
