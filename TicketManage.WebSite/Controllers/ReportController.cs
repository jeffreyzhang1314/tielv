using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.TicketManage;
using TicketManage.WebSite.Filters;
using TicketManage.WebSite.Logic;
using TieckManage.Biz.TicketManage;
namespace TicketManage.WebSite.Controllers
{
    [LoginCheck]
    public class ReportController : BaseController
    {
        public ActionResult GroupReportByDay()
        {
            ViewBag.ReportNav = "active open";
            ViewBag.GroupReportByDay = "active";
            var pageModel = new UserPageModel();
            pageModel.PageModel = UserInfoModel.PageModel;
            return View(pageModel);
        }

        public ActionResult GroupReportByMonth()
        {
            ViewBag.ReportNav = "active open";
            ViewBag.GroupReportByMonth = "active";
            return View();
        }

        public ActionResult GroupReportByWeek()
        {
            ViewBag.ReportNav = "active open";
            ViewBag.GroupReportByWeek = "active";
            return View();
        }

        public PartialViewResult AddTicketPlan(string id, string YearStr)
        {
            TicketPlanManageBiz biz = new TicketPlanManageBiz();
            TicketPlanPageModel model = new TicketPlanPageModel();
            TicketManageLogic Logic = new TicketManageLogic();
            CompanyInfoManageBiz comBiz = new CompanyInfoManageBiz();
            if (string.IsNullOrEmpty(YearStr))
            {
                YearStr = DateTime.Now.Year.ToString();
            }
            model.PageModel = biz.GetTicketPlan(Convert.ToInt32(id), YearStr);
            model.YearList = new List<SelectListItem> { 
            new  SelectListItem{Text = DateTime.Now.Year.ToString(),Value = DateTime.Now.Year.ToString(),Selected=true},
            new  SelectListItem{Text = (DateTime.Now.Year-1).ToString(),Value = DateTime.Now.Year.ToString()},
            new  SelectListItem{Text = (DateTime.Now.Year-2).ToString(),Value = DateTime.Now.Year.ToString()},
            new  SelectListItem{Text = (DateTime.Now.Year-3).ToString(),Value = DateTime.Now.Year.ToString()},
            new  SelectListItem{Text = (DateTime.Now.Year-4).ToString(),Value = DateTime.Now.Year.ToString()},
            };
            model.CompanyList = Logic.ConvertUnitList(comBiz.GetCompanyInfoListByAll());
            return PartialView(model);
        }

        public JsonResult SaveTicketPlan(TicketPlanPageModel model)
        {
            JsonResult result = new JsonResult();
            TicketPlanManageBiz biz = new TicketPlanManageBiz();
            if (model.PageModel != null)
            {
                model.PageModel.CreateTime = DateTime.Now;
            }
            result.Data = biz.SaveTicketPlan(model);
            return result;

        }
        [HttpPost, AjaxOnly]
        public JsonResult LoadAddTicketPlan(string id, string YearStr)
        {
            TicketPlanManageBiz biz = new TicketPlanManageBiz();
            TicketPlanPageModel model = new TicketPlanPageModel();
            TicketManageLogic Logic = new TicketManageLogic();
            CompanyInfoManageBiz comBiz = new CompanyInfoManageBiz();
            if (string.IsNullOrEmpty(YearStr))
            {
                YearStr = DateTime.Now.Year.ToString();
            }
            JsonResult result = new JsonResult();
            result.Data = biz.GetTicketPlan(Convert.ToInt32(id), YearStr);
            return result;
        }

        public ActionResult TicketIncomManage()
        {
            ViewBag.ReportNav = "active open";
            ViewBag.TicketIncomManage = "active";
            return View();
        }
    }

}
