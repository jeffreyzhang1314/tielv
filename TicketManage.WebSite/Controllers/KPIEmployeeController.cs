﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.WebSite.Filters;

namespace TicketManage.WebSite.Controllers
{
    /// <summary>
    /// 外雇用工工资计算相关
    /// </summary>
    [LoginCheckKPI]
    public class KPIEmployeeController : BaseController
    {

        /// <summary>
        /// 外雇用工计算数据列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EmployeeSalary()
        {
            return View();
        }

    }
}
