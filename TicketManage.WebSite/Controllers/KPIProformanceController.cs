﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TieckManage.Biz.KPIProformanceManage;
using TicketManage.Entity.CommonManage;
using TicketManage.Entity.KPIProformanceManage;
using TicketManage.Repository.Models;
using System.Globalization;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.TicketManage;

namespace TicketManage.WebSite.Controllers
{
    public class KPIProformanceController : BaseController
    {
        private KPIPlanInfoYearBiz biz = new KPIPlanInfoYearBiz();
        private UserInfoManageBiz userInfoBiz = new UserInfoManageBiz();

        #region 全年计划录入
        //
        // GET: /KPIProformance/
        [HttpGet]
        public ActionResult KPIPlanInfoYearList()
        {
             var pageModel = new GX_PlanInfoYearListPageModel();
             pageModel.PageSize = 10;
             pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
             pageModel.ModelList = biz.GetPlanInfoYearList(pageModel);
             pageModel.CurrentPage = 1;
             return View(pageModel);
        
        }
        [HttpPost]
        public ActionResult KPIPlanInfoYearList(FormCollection form)
        {
            var pageModel = new GX_PlanInfoYearListPageModel();
            DateTime dt = DateTime.Parse(form["year"].ToString() + "-01-01");
            GX_PlanInfoYear PlanInfoYear = new GX_PlanInfoYear();
            PlanInfoYear.CreateTime = dt;
            pageModel.PageModel = PlanInfoYear;
            pageModel.PageSize = 10;
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.CurrentPage = int.Parse(form["CurrentPage"].ToString());
            pageModel.ModelList = biz.GetPlanInfoYearList(pageModel);
            ViewBag.year = form["year"].ToString();
            return View(pageModel);

          
        }
        [HttpGet]
        public PartialViewResult PlanInfoAdd(int? SEQ)
        {
            var pageModel = new GX_PlanInfoYearListPageModel();
            pageModel.PageModel = new GX_PlanInfoYear();
            if (SEQ.HasValue)
            {
                pageModel.PageModel.SEQ = (int)SEQ;
                pageModel.PageModel = biz.GetPlanInfoYear(pageModel);
            }

            return PartialView("_PartialPlanInfoAdd", pageModel);
        }

        [AjaxOnly]
        public JsonResult PlanInfoDelete(int SEQ) 
        {
            
            bool result = biz.DeletePlanInfoYear(SEQ);
            var message = result ? string.Empty : "传递参数有问题！";
            return Json(new { result = result, message = message });
        }

        [HttpPost]
        public ActionResult PlanInfoAdd(GX_PlanInfoYearListPageModel model)
        {
             biz.AddPlanInfoYear(model);
            return RedirectToAction("KPIPlanInfoYearList");
        }


        #endregion

        #region 个人计划

        [HttpGet]
        public ActionResult KPIPlanInfoPersonList()
        {
            var pageModel = new GX_PlanPersonInfoListPageModel();
            pageModel.PageSize = 10;
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.ModelList = biz.GetPlanPersonInfoList(pageModel, UserInfoModel.PageModel.SEQ);
            pageModel.CurrentPage = 1;
            return View(pageModel);
            
        
        }
        [HttpPost]
        public ActionResult KPIPlanInfoPersonList(FormCollection form)
        {
            var pageModel = new GX_PlanPersonInfoListPageModel();
            DateTime dt = DateTime.Parse(form["year"].ToString() + "-01-01");
            GX_PlanPersonInfo PlanInfoPerson = new GX_PlanPersonInfo();
            PlanInfoPerson.CreateTime = dt;
            pageModel.PageModel = PlanInfoPerson;
            pageModel.PageSize = 10;
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.CurrentPage = int.Parse(form["CurrentPage"].ToString());
            pageModel.ModelList = biz.GetPlanPersonInfoList(pageModel, UserInfoModel.PageModel.SEQ);
            ViewBag.year = form["year"].ToString();
            return View(pageModel);


        }
       [HttpGet]
        public PartialViewResult PlanInfoPersonAdd(int? SEQ)
        {
            var pageModel = new GX_PlanPersonInfoListPageModel();
            pageModel.PageModel = new GX_PlanPersonInfo();
            pageModel.PageModel.UserSEQ = UserInfoModel.PageModel.SEQ;
            pageModel.PageModel.UserName = UserInfoModel.PageModel.UserName;
            if (SEQ.HasValue)
            {
                pageModel.PageModel.SEQ = (int)SEQ;
                pageModel.PageModel = biz.GetPlanInfoPerson(pageModel);
            }

            return PartialView("_PartialPlanInfoPersonAdd", pageModel);
        }

        [HttpPost]
       public ActionResult PlanInfoPersonAdd(GX_PlanPersonInfoListPageModel model)
        {
            biz.AddPlanInfoPerson(model);
            return RedirectToAction("KPIPlanInfoPersonList");
        }

        [AjaxOnly]
        public JsonResult PlanInfoPersonDelete(int SEQ)
        {
            bool result = biz.DeletePlanInfoPserson(SEQ);
            var message = result ? string.Empty : "传递参数有问题！";
            return Json(new { result = result, message = message });
        }
        #endregion


        #region 考核利润
       
        [HttpGet]
        public ActionResult KPIPlanInfofitInfoYearList()
        {
            var pageModel = new GX_PlanprofitInfoYearListPageModel();
            pageModel.PageSize = 10;
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.ModelList = biz.GetPlanprofitInfoList(pageModel);
            pageModel.CurrentPage = 1;
            return View(pageModel);

        }
        [HttpPost]
        public ActionResult KPIPlanInfofitInfoYearList(FormCollection form)
        {
            var pageModel = new GX_PlanprofitInfoYearListPageModel();
            DateTime dt = DateTime.Parse(form["year"].ToString() + "-01-01");
            GX_PlanprofitInfoYear PlanprofitInfoYear = new GX_PlanprofitInfoYear();
            PlanprofitInfoYear.CreateTime = dt;
            pageModel.PageModel = PlanprofitInfoYear;
            pageModel.PageSize = 10;
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.CurrentPage = int.Parse(form["CurrentPage"].ToString());
            pageModel.ModelList = biz.GetPlanprofitInfoList(pageModel);
            ViewBag.year = form["year"].ToString();
            return View(pageModel);


        }
        [HttpGet]
        public PartialViewResult PlanInfofitInfoYearAdd(int? SEQ)
        {
            var pageModel = new GX_PlanprofitInfoYearListPageModel();
            pageModel.PageModel = new GX_PlanprofitInfoYear();
            if (SEQ.HasValue)
            {
                pageModel.PageModel.SEQ = (int)SEQ;
                pageModel.PageModel = biz.GetPlanprofitInfo(pageModel);
            }

            return PartialView("_PartialPlanInfofitInfoYearAdd", pageModel);
        }

        [AjaxOnly]
        public JsonResult PlanInfofitInfoYearDelete(int SEQ)
        {

            bool result = biz.DeletePlanprofitInfo(SEQ);
            var message = result ? string.Empty : "传递参数有问题！";
            return Json(new { result = result, message = message });
        }

        [HttpPost]
        public ActionResult PlanInfofitInfoYearAdd(GX_PlanprofitInfoYearListPageModel model)
        {
            biz.AddPlanprofitInfo(model);
            return RedirectToAction("KPIPlanInfofitInfoYearList");
        }


        #endregion
    }
}
