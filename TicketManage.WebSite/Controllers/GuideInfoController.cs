using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Entity.TicketManage;
using TicketManage.Entity.TourGuideInfoEvaluate;
using TicketManage.Entity.TourGuideManage;
using TicketManage.Repository.Models;
using TicketManage.WebSite.Filters;
using TicketManage.WebSite.Logic;
using TieckManage.Biz.CommonManage;
using TieckManage.Biz.TicketManage;
using TieckManage.Biz.TourGuideManage;

namespace TicketManage.WebSite.Controllers
{

    /// <summary>
    /// 导游信息管理
    /// </summary>
    public class GuideInfoController : BaseController
    {
        private CommonBiz biz = new CommonBiz();
        private GuideInfoBiz GuideInfoModelBiz = new GuideInfoBiz();
        private BaseDataManageBiz BaseBiz = new BaseDataManageBiz();

        [HttpGet]
        public ActionResult Login()
        {
            ViewBag.Title = "导游管理系统登录";
            var pageModel = new UserLoginPageModel();
            pageModel.MessageState = false;
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult Login(UserLoginPageModel model)
        {
            ViewBag.Title = "导游管理系统登录";
            model.SysteCategory = SystemName.导游管理系统;
            model.SystemName = "导游管理系统";
            var result = biz.CommonLogin(model);
            if (result)
            {
                if (CheckUserAuth(model.PageModel.AuthNum, model.SysteCategory))
                {
                    Session[UserInfo] = model;
                    return RedirectToAction("GuideInfoList");
                }
                else
                {
                    model.Message = "您没有权限访问导游管理系统！";
                    model.MessageState = true;
                }
            }
            else
            {
                model.Message = "用户名密码错误！";
                model.MessageState = true;
            }
            return View(model);
        }

        private void GuideInfoListInit(GuideInfoPageModel pageModel)
        {
            ViewBag.GuideInfoNav = "active open";
            ViewBag.GuideInfoList = "open";
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value");
            pageModel.SexSource = new SelectList(SchSexSource, "Key", "Value");
            var tempSource = BaseBiz.GetSystemCodeListByType("DirectoryCompany");
            tempSource.Insert(0, new SystemCode { CodeNo = "", CodeName = "全部" });
            pageModel.DirectoryCompanySource = new SelectList(tempSource, "CodeName", "CodeName");
        }

        [HttpGet, LoginCheck]
        public ActionResult GuideInfoList()
        {
            var pageModel = new GuideInfoPageModel() { PageSize = 10, CurrentPage = 1 };
            GuideInfoListInit(pageModel);
            pageModel.ListModel = GuideInfoModelBiz.ShowGuideInfoByAll(pageModel);
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult GuideInfoList(GuideInfoSearchPageModel model)
        {
            GuideInfoListInit(model);
            model.ListModel = GuideInfoModelBiz.ShowGuideInfoByCondition(model);
            model.EvaluateInfo = GuideInfoModelBiz.ShowEvaluateInfo();
            return View(model);
        }

        [AjaxOnly]
        public JsonResult ExcelExport(GuideInfoSearchPageModel model)
        {
            Session["ExcelSearchModel"] = model;
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 转换性别
        /// </summary>
        /// <param name="flag">True男Flase女</param>
        /// <returns></returns>
        private string ConvertSex(Boolean? flag)
        {
            if (flag != null)
            {
                var tempFlag = flag ?? true;
                return tempFlag ? "男" : "女";
            }
            else
            {
                return string.Empty;
            }
        }

        public FileContentResult ExcelDownLoad()
        {
            var searchCondition = Session["ExcelSearchModel"] as GuideInfoSearchPageModel;
            var dataResult = GuideInfoModelBiz.ShowGuideInfoByExport(searchCondition);
            var book = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = book.CreateSheet("ExcelExport");


            var rowTitle = sheet.CreateRow(0);
            rowTitle.CreateCell(0).SetCellValue("姓名");
            rowTitle.CreateCell(1).SetCellValue("性别");
            rowTitle.CreateCell(2).SetCellValue("身份证");
            rowTitle.CreateCell(3).SetCellValue("电话");
            rowTitle.CreateCell(4).SetCellValue("导游证号");

            var indexVal = 0;
            foreach (var item in dataResult)
            {
                var row = sheet.CreateRow(indexVal + 1);
                row.CreateCell(0).SetCellValue(item.GuideName);
                row.CreateCell(1).SetCellValue(ConvertSex(item.Sex));
                row.CreateCell(2).SetCellValue(item.PassportNo);
                row.CreateCell(3).SetCellValue(item.ContactNo);
                row.CreateCell(4).SetCellValue(item.GuideNo);
                indexVal += 1;
            }

            for (int i = 0; i < 4; i++)
            {
                sheet.AutoSizeColumn(i);
            }

            // 写入到客户端  
            var ms = new System.IO.MemoryStream();
            book.Write(ms);
            var fileName = string.Format("{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"));
            return File(ms.ToArray(), "application/ms-excel", fileName);
        }

        private void GuideblackListInit(GuideInfoPageModel pageModel)
        {
            ViewBag.GuideInfoNav = "active open";
            ViewBag.GuideblackList = "active";
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value");
            pageModel.SexSource = new SelectList(SchSexSource, "Key", "Value");
        }

        //黑名单列表
        [HttpGet, LoginCheck]
        public ActionResult GuideblackList()
        {

            var pageModel = new GuideInfoPageModel();
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            GuideblackListInit(pageModel);
            pageModel.ListModel = GuideInfoModelBiz.ShowGuideblackByAll(pageModel);
            return View(pageModel);
        }
        //黑名单列表
        [HttpPost, LoginCheck]
        public ActionResult GuideblackList(GuideInfoSearchPageModel model)
        {
            GuideblackListInit(model);
            model.ListModel = GuideInfoModelBiz.ShowGuideblackByCondition(model);
            return View(model);
        }


        [HttpGet]
        public ActionResult GuideInfoAdd()
        {
            TicketManageLogic logic = new TicketManageLogic();
            var pageModel = new GuideInfoPageModel();
            pageModel.PageModel = new GuideInfo();
            pageModel.PageModel.GuideNo = String.Format("DY_No{0}", DateTime.Now.ToString("yyyyMMddHHmmss"));
            pageModel.SexSource = new SelectList(SexSource, "Key", "Value");
            pageModel.EducationList = logic.CodeListToSelectList(GuideInfoModelBiz.GetSystemCodeListByType("EduType"));
            var tempSource = BaseBiz.GetSystemCodeListByType("DirectoryCompany");
            tempSource.Insert(0, new SystemCode { CodeNo = "", CodeName = "全部" });
            pageModel.DirectoryCompanySource = new SelectList(tempSource, "CodeName", "CodeName");
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult GuideInfoAdd(GuideInfoPageModel model)
        {
            var uploadFile = base.FileUpload(Request);
            foreach (var item in uploadFile)
            {
                if (string.Compare(item.Value, "PassportNoFile", true) == 0)
                {
                    model.PageModel.PassportNoFile = item.Key;
                }
                if (string.Compare(item.Value, "GuideCardIDFile", true) == 0)
                {
                    model.PageModel.GuideCardIDFile = item.Key;
                }
                if (string.Compare(item.Value, "GuidNoFile", true) == 0)
                {
                    model.PageModel.GuidNoFile = item.Key;
                }
            }
            var result = GuideInfoModelBiz.SaveGuideInfo(model);
            model.SexSource = new SelectList(SexSource, "Key", "Value");
            return Json(new { result = result, message = model.Message });
        }

        [HttpGet]
        public ActionResult GuideInfoEdit(int? id)
        {
            TicketManageLogic logic = new TicketManageLogic();
            var pageModel = new GuideInfoPageModel();
            pageModel.SexSource = new SelectList(SexSource, "Key", "Value");
            pageModel.EducationList = logic.CodeListToSelectList(GuideInfoModelBiz.GetSystemCodeListByType("EduType"));
            pageModel.PageModel = GuideInfoModelBiz.ShowGuideInfoBySeq(id ?? 0);
            var tempSource = BaseBiz.GetSystemCodeListByType("DirectoryCompany");
            tempSource.Insert(0, new SystemCode { CodeNo = "", CodeName = "全部" });
            pageModel.DirectoryCompanySource = new SelectList(tempSource, "CodeName", "CodeName");
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult GuideInfoEdit(GuideInfoPageModel model)
        {
            TicketManageLogic logic = new TicketManageLogic();
            var uploadFile = base.FileUpload(Request);
            foreach (var item in uploadFile)
            {
                if (string.Compare(item.Value, "PassportNoFile", true) == 0)
                {
                    model.PageModel.PassportNoFile = item.Key;
                }
                if (string.Compare(item.Value, "GuideCardIDFile", true) == 0)
                {
                    model.PageModel.GuideCardIDFile = item.Key;
                }
                if (string.Compare(item.Value, "GuidNoFile", true) == 0)
                {
                    model.PageModel.GuidNoFile = item.Key;
                }
            }
            var result = GuideInfoModelBiz.SaveGuideInfo(model);
            model.SexSource = new SelectList(SexSource, "Key", "Value");
            model.EducationList = logic.CodeListToSelectList(GuideInfoModelBiz.GetSystemCodeListByType("EduType"));
            return RedirectToAction("GuideInfoList");
        }

        [HttpGet]
        public ActionResult GuideStateEdit(int? id)
        {
            var pageModel = new GuideInfoPageModel();
            pageModel.SexSource = new SelectList(SexSource, "Key", "Value");
            pageModel.PageModel = GuideInfoModelBiz.ShowGuideInfoBySeq(id ?? 0);
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult GuideStateEdit(GuideInfoPageModel model)
        {
            model.PageModel.GuideState = false;
            var result = GuideInfoModelBiz.SaveGuideInfo(model);
            model.SexSource = new SelectList(SexSource, "Key", "Value");
            return Json(new { result = result, message = model.Message });
        }


        [HttpGet]
        public JsonResult GuideInfoDel(int id)
        {
            var result = GuideInfoModelBiz.DelGuideInfo(id);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GuideStateDel(int id)
        {
            var result = GuideInfoModelBiz.GuideStateDel(id);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }



        private void GuideListEvaluateInit(GuideInfoPageModel pageModel)
        {
            ViewBag.GuideInfoNav = "active open";
            ViewBag.GuideListEvaluate = "active";
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value");
            pageModel.SexSource = new SelectList(SchSexSource, "Key", "Value");
            var tempSource = BaseBiz.GetSystemCodeListByType("DirectoryCompany");
            tempSource.Insert(0, new SystemCode { CodeNo="",CodeName="全部"});
            pageModel.DirectoryCompanySource = new SelectList(tempSource, "CodeName", "CodeName");
        }

        [HttpGet, LoginCheck]
        public ActionResult GuideListEvaluate()
        {
            var pageModel = new GuideInfoPageModel();
            GuideListEvaluateInit(pageModel);
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            pageModel.ListModel = GuideInfoModelBiz.ShowGuideListEvaluateByAll(pageModel);
            pageModel.EvaluateInfo = GuideInfoModelBiz.ShowEvaluateInfo();
          
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult GuideListEvaluate(GuideInfoPageModel model)
        {
            GuideListEvaluateInit(model);
            model.ListModel = GuideInfoModelBiz.ShowGuideListEvaluateByCondition(model);
            model.EvaluateInfo = GuideInfoModelBiz.ShowEvaluateInfo();
            return View(model);
        }

        [HttpGet]
        public ActionResult GuideEvaluateEdit(int? id)
        {
            var model = new GuideInfoPageModel();
            model.SexSource = new SelectList(SexSource, "Key", "Value");
            model.PageModel = GuideInfoModelBiz.ShowGuideEvaluateBySeq(id ?? 0);
            var tempModel = Session[UserInfo] as UserLoginPageModel;

            TicketManageLogic logic = new TicketManageLogic();
            var pageModel = new GuideInfoEvaluatePageModel();
            pageModel.PageModel = new V_GuideInfoEvaluateView();
            pageModel.PageModel.GuideSEQ = model.PageModel.SEQ;
            pageModel.PageModel.GuideName = model.PageModel.GuideName;
            pageModel.PageModel.PassportNo = tempModel.PageModel.PassportNo;
            pageModel.PageModel.LeadDate = null;
            pageModel.PageModel.Summary = "";
            pageModel.PageModel.LevelCode = null;
            pageModel.PageModel.StarNum = model.PageModel.GuideStarNum;
            pageModel.ProductNameSource = logic.CodeListToSelectList(GuideInfoModelBiz.GetSystemCodeListByType("ProductName"));
            return View(pageModel);
        }
        [HttpPost]
        public ActionResult GuideEvaluateEdit(GuideInfoEvaluatePageModel model)
        {
            var EvaluateModel = new GuideInfoEvaluate();
            EvaluateModel.GuideSEQ = model.PageModel.GuideSEQ;
            EvaluateModel.LeadDate = model.PageModel.LeadDate;
            //EvaluateModel.ProductCode = model.PageModel.ProductCode;
            EvaluateModel.Summary = model.PageModel.Summary;
            EvaluateModel.ProductName = model.PageModel.ProductName;
            EvaluateModel.LevelName = model.PageModel.LevelName;
            bool result;

            if (model.StarNumNew != null)
            {
                if (model.StarNumNew > 5)
                {
                    model.StarNumNew = 5;
                }
                EvaluateModel.StarNum = model.StarNumNew;
                result = GuideInfoModelBiz.SaveGuideEvaluate(EvaluateModel);//保存信息
                GuideInfoModelBiz.SaveGuideStarNum(EvaluateModel.GuideSEQ, model.StarNumNew);//更新星级
            }
            else
            {
                if (model.PageModel.LevelName == "好评")//升星
                {
                    if (model.PageModel.StarNum < 5 || model.PageModel.StarNum == null)
                    {
                        EvaluateModel.StarNum = Convert.ToInt32(model.PageModel.StarNum) + 1;
                    }
                    else//不升星
                    {
                        EvaluateModel.StarNum = Convert.ToInt32(model.PageModel.StarNum);
                    }
                }
                else if (model.PageModel.LevelName == "差评")//降星
                {
                    if (model.PageModel.StarNum > 0 || model.PageModel.StarNum == null)
                    {
                        EvaluateModel.StarNum = Convert.ToInt32(model.PageModel.StarNum) - 1;
                    }
                    else//不升星
                    {
                        EvaluateModel.StarNum = Convert.ToInt32(model.PageModel.StarNum);
                    }
                }
                else//不升星
                {
                    EvaluateModel.StarNum = Convert.ToInt32(model.PageModel.StarNum);
                }
                result = GuideInfoModelBiz.SaveGuideEvaluate(EvaluateModel);
                GuideInfoModelBiz.SaveGuideStarNum(EvaluateModel.GuideSEQ, EvaluateModel.StarNum);
            }


            model.SexSource = new SelectList(SexSource, "Key", "Value");
            return Json(new { result = result, message = model.Message });
        }

        private void GuideDetailListEvaluateInit(GuideInfoEvaluatePageModel pageModel)
        {
            ViewBag.GuideInfoNav = "active open";
            ViewBag.GuideListEvaluate = "active";
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value");

        }

        [HttpGet, LoginCheck]
        public ActionResult GuideDetailListEvaluate(int id)
        {
            var pageModel = new GuideInfoEvaluatePageModel();
            GuideDetailListEvaluateInit(pageModel);
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            pageModel.GuideSEQ = id;
            pageModel.ListModel = GuideInfoModelBiz.ShowGuideDetailListEvaluateByAll(pageModel);
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult GuideDetailListEvaluate(GuideInfoEvaluatePageModel model)
        {
            GuideDetailListEvaluateInit(model);
            model.ListModel = GuideInfoModelBiz.ShowGuideDetailListEvaluateByAll(model);
            return View(model);
        }

        private void DirectoryCompanyInit(DirectoryCompanyPageModel model)
        {
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value");
        }

        [HttpGet]
        public ActionResult DirectoryCompany()
        {
            var pageModel = new DirectoryCompanyPageModel();
            DirectoryCompanyInit(pageModel);
            pageModel.ListModel = BaseBiz.GetSystemCodeListByType("DirectoryCompany");
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult DirectoryCompany(DirectoryCompanyPageModel model)
        {
            DirectoryCompanyInit(model);
            var list = BaseBiz.GetSystemCodeListByType("DirectoryCompany", model.SearchModel.CodeName);
            model.ListModel = list;
            return View(model);
        }

        [AjaxOnly]
        public JsonResult DirectoryCompanyDel(int seq)
        {
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult DirectoryCompanyEdit(int seq, string codeName)
        {
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 显示添加区域
        /// </summary>
        /// <returns></returns>
        public PartialViewResult DirectoryCompanyShowArea(int seq)
        {
            var pageModel = new SystemCode();
            return PartialView("DirectoryCompanyAdd", pageModel);
        }
    }
}
