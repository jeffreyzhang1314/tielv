﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Entity.OtherUserManage;
using TicketManage.Repository.Models;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.OtherUserManage;

namespace TicketManage.WebSite.Controllers
{
    [LoginCheck]
    public class UserInfoOtherController : BaseController
    {
        /// <summary>
        /// 用户操作业务实例
        /// </summary>
        private UserInfoOtherBiz biz = new UserInfoOtherBiz();

        /// <summary>
        /// 列表页面初始化
        /// </summary>
        /// <param name="model"></param>
        private void UserInfoListInit(UserInfoOtherPageModel model)
        {
            switch (model.SystemName)
            {
                case TicketManage.Entity.CommonManage.SystemName.票务管理系统:
                    break;
                case TicketManage.Entity.CommonManage.SystemName.地接管理系统:
                    ViewBag.TravelLocalAgencyNav = "active open";
                    break;
                case TicketManage.Entity.CommonManage.SystemName.导游管理系统:
                    ViewBag.GuideInfoNav = "active open";
                    break;
                case TicketManage.Entity.CommonManage.SystemName.宣传品管理系统:
                    ViewBag.PublicityNav = "active open";
                    break;
                case TicketManage.Entity.CommonManage.SystemName.工效管理系统:
                    ViewBag.KPIDataNav = "active open";
                    break;
            }
            ViewBag.UserInfoListNav = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
        }

        [HttpGet]
        public ActionResult UserInfoList()
        {
            var flagSystem = Request.QueryString["systemName"];
            var model = new UserInfoOtherPageModel() { PageSize = 10, CurrentPage = 1, OperatorAuthNum = CommonUserInfoModel.PageModel.AuthNum };
            var systemEnumVal = ConvertSystemName(flagSystem);
            if (systemEnumVal != 0)
            {
                model.SystemName = systemEnumVal;
                model.MessageState = false;
                UserInfoListInit(model);
                model.ModelList = biz.GetUserInfoOtherListByCondition(model);
            }
            else
            {
                model.MessageState = true;
                model.Message = "参数错误！";
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult UserInfoList(UserInfoOtherPageModel model)
        {
            UserInfoListInit(model);
            model.ModelList = biz.GetUserInfoOtherListByCondition(model);
            return View(model);
        }

        [AjaxOnly]
        public PartialViewResult ShowUserInfoOther(string systemName)
        {
            var model = new UserInfoOtherPageModel();
            var systemEnumVal = ConvertSystemName(systemName);
            if (systemEnumVal != 0)
            {
                model.AuthListSource = new SelectList(AuthNumSourceBySystem(systemEnumVal), "Key", "Value");
            }
            else
            {
                model.AuthListSource = new SelectList(new Dictionary<string, string> { { "", "" } }, "Key", "Value");
            }
            var tempCompanyList = new List<KeyValueModel>();
            var tempList = biz.GetOtherUserCompany();
            if (tempList != null && tempList.Count > 0)
            {
                tempCompanyList.AddRange(biz.GetOtherUserCompany());
                tempCompanyList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
            }
            else
            {
                tempCompanyList.Insert(0, new KeyValueModel { Key = 0, Value = "" });
            }
            model.CompanySource = new SelectList(tempCompanyList, "Key", "Value");
            model.SexSource = new SelectList(SexSource, "Key", "Value");
            model.SystemName = systemEnumVal;
            return PartialView(model);
        }

        [AjaxOnly]
        public PartialViewResult ShowUserInfoOtherByEdit(string systemName, int id)
        {
            var model = new UserInfoOtherPageModel();
            model.PageModel = biz.GetUserInfoOtherBySEQ(id);
            var systemEnumVal = ConvertSystemName(systemName);
            if (systemEnumVal != 0)
            {
                var tempListSource = AuthNumSourceBySystem(systemEnumVal);

                var indexFlag = 0;
                foreach (var item in tempListSource)
                {
                    if (((model.PageModel.AuthNum ?? 0) & Convert.ToInt32(item.Key)) != 0)
                    {
                        model.PageModel.AuthNum = Convert.ToInt32(item.Key);
                        indexFlag++;
                    }
                }
                if (indexFlag == 0)
                {
                    tempListSource.Add("0", string.Empty);
                }
                model.AuthListSource = new SelectList(tempListSource.OrderBy(a => a.Key), "Key", "Value");
            }
            else
            {
                model.AuthListSource = new SelectList(new Dictionary<string, string> { { "", "" } }, "Key", "Value");
            }
            var tempCompanyList = new List<KeyValueModel>();
            var templist = biz.GetOtherUserCompany();
            if (templist != null && templist.Count > 0)
            {
                tempCompanyList.AddRange(templist);
                tempCompanyList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
            }
            else
            {
                tempCompanyList.Insert(0, new KeyValueModel { Key = 0, Value = "" });
            }
            model.CompanySource = new SelectList(tempCompanyList, "Key", "Value");
            model.SexSource = new SelectList(SexSource, "Key", "Value");
            model.SystemName = systemEnumVal;
            return PartialView("ShowUserInfoOther", model);
        }



        [AjaxOnly]
        public JsonResult EditUserInfoOther(UserInfoOtherPageModel model)
        {
            model.AuthList = AuthNumSourceBySystem(model.SystemName);
            var result = biz.EditUserInfoOther(model);
            return Json(new { result = result, message = model.Message });
        }

        [AjaxOnly]
        public JsonResult UserInfoPasswordReset(int id)
        {
            var result = biz.ResetPasswordByUserInfoSEQ(id);
            return Json(result ? "操作完成！" : "操作失败！", JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult UserInfoDel(int id)
        {
            var result = biz.DeleteUserInfoOther(id);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            var model = CommonUserInfoModel;
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangePassword(UserLoginPageModel model)
        {
            var result = biz.ChangePassWord(model);
            return View(model);
        }
    }
}
