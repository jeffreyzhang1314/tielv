﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.TicketManage;

namespace TicketManage.WebSite.Controllers
{
    public class TicketUnionBedController : BaseController
    {
        private TicketUnionBedBiz biz = new TicketUnionBedBiz();

        private void TicketUnionBedListInit(TicketUnionBedPageModel pageModel)
        {
            ViewBag.TicketManageNav = "active open";
            ViewBag.TicketUnionBedList = "active";
            ViewBag.IsAdmin = UserInfoModel.IsAdmin ? "true" : "false";
            pageModel.TimeCategorySource = new SelectList(TimeCategorySource, "Key", "Value");
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.OperatorAuthNum = UserInfoModel.PageModel.AuthNum;
            if ((pageModel.OperatorAuthNum & 2) == 2)
            {
                pageModel.OperatorUserNoList = new List<int> { UserInfoModel.PageModel.SEQ };
            }
            else if ((pageModel.OperatorAuthNum & 4) == 4)
            {
                pageModel.OperatorUserNoList = biz.GetUserNoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ);
            }
            else if ((pageModel.OperatorAuthNum & 8) == 8)
            {
                pageModel.OperatorUserNoList = biz.GetUserNoByCompanySEQ(UserInfoModel.PageModel.CompanySEQ);
            }
        }

        [HttpGet, LoginCheck]
        public ActionResult TicketUnionBedList()
        {
            var pageModel = new TicketUnionBedPageModel() { PageSize = 10, CurrentPage = 1, PageModel = new TicketUnionBed() };
            TicketUnionBedListInit(pageModel);
            pageModel.StartDate = DateTime.Now.ToString("yyyy-MM-dd");
            pageModel.EndDate = DateTime.Now.ToString("yyyy-MM-dd");
            pageModel.ModelList = biz.GetTicketUnionBedByCondition(pageModel);
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult TicketUnionBedList(TicketUnionBedPageModel model)
        {
            TicketUnionBedListInit(model);
            model.CurrentPage = Convert.ToInt32(Request.Form["CurrentPage"]);
            model.PageSize = Convert.ToInt32(Request.Form["PageSize"]);
            model.ModelList = biz.GetTicketUnionBedByCondition(model);
            return View(model);
        }

        [HttpGet]
        public PartialViewResult TicketUnionBedAdd()
        {
            var pageModel = new TicketUnionBedPageModel();
            return PartialView("_PartialTicketUnionBedAdd", pageModel);
        }

        [HttpPost]
        public JsonResult TicketUnionBedAdd(TicketUnionBedPageModel model)
        {
            model.PageModel.OperationSEQ = UserInfoModel.PageModel.SEQ;
            model.PageModel.OperationName = UserInfoModel.PageModel.UserName;
            biz.TicketUnionBedAdd(model);
            return Json(new { result = true, message = "添加成功！" });
        }

        [HttpGet]
        public PartialViewResult TicketUnionBedEdit(int id)
        {
            var pageModel = new TicketUnionBedPageModel();
            pageModel.PageModel = biz.GetTicketUnionBedById(id);
            return PartialView("_PartialTicketUnionBedAdd", pageModel);
        }

        [HttpPost]
        public JsonResult TicketUnionBedEdit(TicketUnionBedPageModel pageModel)
        {

            pageModel.PageModel.OperationSEQ = UserInfoModel.PageModel.SEQ;
            biz.TicketUnionBedEdit(pageModel);
            return Json(new { result = true, message = "修改成功！" });
        }

        [AjaxOnly]
        public JsonResult TicketUnionBedDel(int id)
        {
            biz.TicketUnionBedDel(id);
            return Json("OK");
        }

    }
}
