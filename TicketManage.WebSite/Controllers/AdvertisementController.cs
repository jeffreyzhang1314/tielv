﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.AdvertisementManage;
using TicketManage.Entity.CommonManage;
using TicketManage.Entity.PublicityInfoManage;
using TicketManage.Repository.Models;
using TicketManage.WebSite.Filters;
using TicketManage.WebSite.Models.DataBaseManage;
using TieckManage.Biz.AdvertisementManage;
using TieckManage.Biz.PublicityInfoManage;
using TieckManage.Biz.TicketManage;

namespace TicketManage.WebSite.Controllers
{
    public class AdvertisementController : BaseController
    {
        private AdvertisementBiz biz = new AdvertisementBiz();
        private PublicityInfoBiz pBiz = new PublicityInfoBiz();
        private UserInfoManageBiz Userbiz = new UserInfoManageBiz();

        #region 预算分劈管理

        /// <summary>
        /// 预算分劈页面初始化
        /// </summary>
        /// <param name="model"></param>
        private void BudgetSplitInit(BudgetSplitModel model)
        {
            ViewBag.AdvertisementNav = "active open";
            ViewBag.BudgetSplit = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
            var userModel = CommonUserInfoModel.PageModel;
            var tempCompanyList = new List<TicketManage.Repository.Models.KeyValueModel>();
            var templist = Userbiz.GetOtherUserCompany();
            if (templist != null && templist.Count > 0)
            {
                tempCompanyList.AddRange(templist);
                if ((CommonUserInfoModel.PageModel.AuthNum & 65536) == 65536)
                {
                    tempCompanyList = tempCompanyList.Where(a => a.Value == userModel.CompanyName).ToList();
                }
                else
                {
                    tempCompanyList.Insert(0, new TicketManage.Repository.Models.KeyValueModel { Key = 0, Value = "全部" });
                }

            }
            else
            {
                tempCompanyList.Insert(0, new TicketManage.Repository.Models.KeyValueModel { Key = 0, Value = "" });
            }
            model.CompanySource = new SelectList(tempCompanyList, "Key", "Value");


        }

        /// <summary>
        /// 预算分劈
        /// 查询每年的分劈内容
        /// </summary>
        /// <returns></returns>
        [HttpGet, LoginCheck]
        public ActionResult BudgetSplit()
        {
            var model = new BudgetSplitModel();
            model.PageSize = 10;
            model.CurrentPage = 1;
            BudgetSplitInit(model);
            model.ListModel = biz.BudgetSplitSearchByAll();
            model.PageNum = model.ListModel.Count;
            model.PageTotal = (model.PageNum + model.PageSize - 1) / model.PageSize;
            model.PageModel = new BudgetSplitInfo();
            model.PageModel.BudgetYear = DateTime.Now.Year;
            return View(model);
        }

        /// <summary>
        /// 预算分劈查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, LoginCheck]
        public ActionResult BudgetSplit(BudgetSplitModel model)
        {
            BudgetSplitInit(model);
            model.ListModel = biz.BudgetSplitSearchByCondition(model);
            return View(model);
        }

        [HttpGet]
        public PartialViewResult BudgetSplitShowEdit(int id)
        {
            var model = new BudgetSplitModel();
            var userModel = CommonUserInfoModel.PageModel;
            var tempCompanyList = new List<TicketManage.Repository.Models.KeyValueModel>();
            var templist = Userbiz.GetOtherUserCompany();
            if (templist != null && templist.Count > 0)
            {
                tempCompanyList.AddRange(templist);
                if ((CommonUserInfoModel.PageModel.AuthNum & 65536) == 65536)
                {
                    tempCompanyList = tempCompanyList.Where(a => a.Value == userModel.CompanyName).ToList();
                }
                else
                {
                    tempCompanyList.Insert(0, new TicketManage.Repository.Models.KeyValueModel { Key = 0, Value = "全部" });
                }

            }
            else
            {
                tempCompanyList.Insert(0, new TicketManage.Repository.Models.KeyValueModel { Key = 0, Value = "" });
            }
            model.CompanySource = new SelectList(tempCompanyList, "Key", "Value");
            model.CategorySource = new SelectList(BudgetSplitCategorySource, "Key", "Value");
            if (id == 0)
            {
                model.PageModel = new Repository.Models.BudgetSplitInfo();
            }
            else
            {
                model.PageModel = biz.BudgetSplitSearchById(id);
            }
            model.PageModel.OperatorSEQ = CommonUserInfoModel.PageModel.SEQ;
            model.PageModel.OperatorName = CommonUserInfoModel.PageModel.UserName;
            return PartialView("_PartialBudgetSplitEdit", model);
        }

        [HttpPost, LoginCheck]
        public ActionResult BudgetSplitSaveEdit(BudgetSplitModel model)
        {
            var result = biz.BudgetSplitAdd(model);
            BudgetSplitInit(model);
            model.PageSize = 10;
            model.CurrentPage = 1;
            model.ListModel = biz.BudgetSplitSearchByCondition(model);
            return View("BudgetSplit", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AjaxOnly, LoginCheck]
        public JsonResult BudgetSplitDeleted(int id)
        {
            var result = biz.BudgetSplitDeleted(id);
            return Json(new { resultData = result });
        }

        #endregion

        #region

        /// <summary>
        /// 追加预算审批
        /// 查询所有带审批预算
        /// </summary>
        /// <returns></returns>
        [HttpGet, LoginCheck]
        public ActionResult BudgetApprove()
        {
            ViewBag.AdvertisementNav = "active open";
            ViewBag.BudgetApprove = "active";
            return View();
        }

        #endregion

        #region 预算支出管理

        private void BudgetManageInit(BudgetManageModel model)
        {
            ViewBag.AdvertisementNav = "active open";
            ViewBag.BudgetManage = "active";
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            if (tempModel.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value");

            var BudgetCategoryList = BudgetSplitCategorySource;
            BudgetCategoryList.Insert(0, new Repository.Models.KeyValueModel { Key = 0, Value = "全部" });
            model.CategorySource = new SelectList(BudgetCategoryList, "Value", "Value");
            var userModel = CommonUserInfoModel.PageModel;
            var tempCompanyList = new List<TicketManage.Repository.Models.KeyValueModel>();
            var templist = Userbiz.GetOtherUserCompany();
            if (templist != null && templist.Count > 0)
            {
                tempCompanyList.AddRange(templist);
                if ((CommonUserInfoModel.PageModel.AuthNum & 65536) == 65536)
                {
                    tempCompanyList = tempCompanyList.Where(a => a.Value == userModel.CompanyName).ToList();
                }
                else
                {
                    tempCompanyList.Insert(0, new TicketManage.Repository.Models.KeyValueModel { Key = 0, Value = "全部" });
                }

            }
            else
            {
                tempCompanyList.Insert(0, new TicketManage.Repository.Models.KeyValueModel { Key = 0, Value = "" });
            }
            model.CompanySource = new SelectList(tempCompanyList, "Value", "Value");
        }

        /// <summary>
        /// 分公司日常使用预算查询
        /// </summary>
        /// <returns></returns>
        [HttpGet, LoginCheck]
        public ActionResult BudgetManage()
        {
            var model = new BudgetManageModel() { PageModel = new Repository.Models.ExtModels.BudgetSplitInfoPageModel() };
            model.PageSize = 10;
            model.CurrentPage = 1;
            BudgetManageInit(model);
            model.PageModel.BudgetYear = DateTime.Now.Year;
            model.PageModel.CompanyName = CommonUserInfoModel.PageModel.CompanyName;
            model.ListModel = biz.BudgetApplySearchByCondition(model);

            return View(model);
        }

        [HttpPost, LoginCheck]
        public ActionResult BudgetManage(BudgetManageModel model)
        {
            BudgetManageInit(model);
            model.ListModel = biz.BudgetApplySearchByCondition(model);
            return View(model);
        }

        [HttpGet, LoginCheck]
        public PartialViewResult BudgetManageShowEdit(int id)
        {
            var model = new BudgetManageShowModel() { PageModel = new Repository.Models.ApplyBudget() };
            model.PageModel.BudgetSEQ = id;
            return PartialView("_PartialBudgetManageEdit", model);
        }

        [HttpPost, LoginCheck]
        public JsonResult BudgetManageSaveEdit(BudgetManageShowModel model)
        {
            model.PageModel.OperatorSEQ = CommonUserInfoModel.PageModel.SEQ;
            model.PageModel.OperatorName = CommonUserInfoModel.PageModel.UserName;
            var result = biz.BudgetApplySave(model);
            return Json(new { Result = result, Message = model.Message });
        }

        [HttpGet, LoginCheck]
        public PartialViewResult BudgetAppendShow(int id)
        {
            var model = new BudgetAppendModel() { PageModel = new BudgetAppend() };
            model.PageModel.BudgetSplitSEQ = id;
            model.PageModel.OperatorSEQ = CommonUserInfoModel.PageModel.SEQ;
            model.PageModel.OperatorName = CommonUserInfoModel.PageModel.UserName;
            var tempModel = biz.BudgetAppendSeachByBudgetSplitSEQ(id);
            if (tempModel != null)
            {
                model.PageModel.BudgetMoney = tempModel.BudgetMoney;
                model.PageModel.Voucher = tempModel.Voucher;
            }
            return PartialView("_PartialBudgetAppend", model);
        }

        [HttpPost, LoginCheck]
        public ActionResult BudgetAppendSave(BudgetAppendModel model)
        {
            var uploadFileList = FileUpload(this.Request);
            if (uploadFileList != null && uploadFileList.Count > 0)
            {
                var tempFileName = uploadFileList.FirstOrDefault().Key;
                model.PageModel.Voucher = tempFileName;
            }
            var result = biz.BudgetAppendSave(model);
            return RedirectToAction("BudgetManage");
        }

        [HttpGet, LoginCheck]
        public ActionResult BudgetAppendDeleted(int id)
        {
            var result = biz.BudgetAppendDeleted(id);
            return RedirectToAction("BudgetManage");
        }

        /// <summary>
        /// 显示支出详细
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, LoginCheck]
        public ActionResult BudgetDetail(int id)
        {
            ViewBag.AdvertisementNav = "active open";
            ViewBag.BudgetManage = "active";
            var model = new BudgetDetailModel();
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value");
            model.PageSize = 10;
            model.CurrentPage = 1;
            model.PageModel = new Repository.Models.ApplyBudget()
            {
                BudgetSEQ = id
            };

            model.ListModel = biz.ApplyBudgetSearchByCondition(model);
            return View(model);
        }

        [HttpGet]
        public PartialViewResult PartialBudgetExpenditureShow(int id, int seq)
        {
            var model = new PartialBudgetExpenditureShowModel() { SEQ = seq, PageModel = new Repository.Models.BudgetExpenditure() { VoucherDate = DateTime.Now, BudgetSplitSEQ = id } };
            return PartialView("_PartialBudgetExpenditureShow", model);
        }

        [HttpPost, LoginCheck]
        public ActionResult PartialBudgetExpenditureUpload()
        {
            var formSEQ = Convert.ToInt32(this.Request.Form["SEQ"]);
            var formBudgetSplitSEQ = Convert.ToInt32(this.Request.Form["PageModel.BudgetSplitSEQ"]);
            var formVoucherDate = this.Request.Form["PageModel.VoucherDate"];
            var formFileUpload = this.Request.Form["FileUpload"];
            var formSummary = this.Request.Form["PageModel.Summary"];
            var uploadFileList = FileUpload(this.Request);
            if (uploadFileList.Count == 1)
            {
                var tempFileName = uploadFileList.FirstOrDefault().Key;
                var model = new PartialBudgetExpenditureShowModel() { PageModel = new Repository.Models.BudgetExpenditure() };

                model.PageModel.BudgetSplitSEQ = formBudgetSplitSEQ;
                if (!string.IsNullOrEmpty(formVoucherDate))
                {
                    model.PageModel.VoucherDate = Convert.ToDateTime(formVoucherDate);
                }
                model.PageModel.Voucher = tempFileName;
                model.PageModel.OperatorSEQ = CommonUserInfoModel.PageModel.SEQ;
                model.PageModel.OperatorName = CommonUserInfoModel.PageModel.UserName;
                model.PageModel.CreateTime = DateTime.Now;
                model.PageModel.Summary = formSummary;
                pBiz.SaveBudgetExpenditureFileUpload(model);
            }
            return RedirectToAction("BudgetDetail", new { id = formSEQ });
        }

        /// <summary>
        /// 详情明细
        /// </summary>
        /// <returns></returns>
        [HttpGet, LoginCheck]
        public PartialViewResult BudgetExpenditureList(int id)
        {
            var model = biz.BudgetExpenditureListByBudgetSplitSEQ(id);
            return PartialView("_PartialBudgetExpenditureList", model);
        }

        [AjaxOnly, LoginCheck]
        public JsonResult BudgetExpenditureDel(int seq)
        {
            var result = biz.BudgetExpenditureDel(seq);
            return Json(new { result = result }, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}
