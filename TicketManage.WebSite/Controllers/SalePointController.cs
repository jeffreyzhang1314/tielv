using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TicketManage.WebSite.Filters;
using TicketManage.WebSite.Logic;
using TieckManage.Biz.TicketManage;

namespace TicketManage.WebSite.Controllers
{
    [LoginCheck]
    public class SalePointController : BaseController
    {
        private UserInfoManageBiz uiz = new UserInfoManageBiz();

        private SalePointInfoManageBiz biz = new SalePointInfoManageBiz();

        private void SalePointListManageInit(SalePointPageModel model)
        {
            ViewBag.BaseDataNav = "active open";
            ViewBag.SalePointListManage = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", 10);
            ViewBag.IsAdmin = UserInfoModel.IsAdmin ? "true" : "false";
            var tempCompanyList = new List<KeyValueModel>();
            if ((UserInfoModel.PageModel.AuthNum & 4) == 4)
            {
                tempCompanyList.AddRange(biz.GetCompanyInfoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ ?? 0));
            }
            else if ((UserInfoModel.PageModel.AuthNum & 8) == 8)
            {
                tempCompanyList.AddRange(biz.GetCompanyInfoById(UserInfoModel.PageModel.CompanySEQ ?? 0));
            }
            else if ((UserInfoModel.PageModel.AuthNum & 1) == 1 || (UserInfoModel.PageModel.AuthNum & 16) == 16)
            {
                tempCompanyList.Add(new KeyValueModel { Key = 0, Value = "全部" });
                tempCompanyList.AddRange(biz.GetCompanyInfoByAll());
            }
            model.CompanySource = new SelectList(tempCompanyList, "Key", "Value");
            var tempnatureSource = biz.GetSystemCodeListByType("PoolType");
            tempnatureSource.Insert(0, new SystemCode { CodeNo = "", CodeName = "全部" });
            model.NatureSource = new SelectList(tempnatureSource, "CodeNo", "CodeName");
            model.OperatorAuthNum = UserInfoModel.PageModel.AuthNum;
        }

        [HttpGet]
        public ActionResult SalePointListManage()
        {
            var model = new SalePointPageModel() { PageSize = 10, CurrentPage = 1, PageModel = new SalePointInfo() };
            SalePointListManageInit(model);
            if (model.CompanySource != null && model.CompanySource.Count() > 0)
            {
                model.PageModel.CompanySEQ = Convert.ToInt32(model.CompanySource.FirstOrDefault().Value);
            }
            model.ListModel = biz.GetSalePointListByQueryable(model);
            return View(model);
        }


        [HttpPost]
        public ActionResult SalePointListManage(SalePointPageModel model)
        {
            SalePointListManageInit(model);
            CompanyInfoManageBiz companyBiz = new CompanyInfoManageBiz();
            TicketManageLogic logic = new TicketManageLogic();
            model.ListModel = biz.GetSalePointListByQueryable(model);
            string pageCount = ConfigurationManager.AppSettings["PageCount"].ToString();
            model.ListCount = model.ListModel.Count;
            model.UnitList = logic.ConvertUnitList(companyBiz.GetCompanyInfoListByAll());

            return View(model);
        }

        [HttpGet]
        public PartialViewResult SalePointManage(string id)
        {
            SalePointInfoManageBiz biz = new SalePointInfoManageBiz();
            SalePointPageModel model = new SalePointPageModel();
            CompanyInfoManageBiz companyBiz = new CompanyInfoManageBiz();
            TicketManageLogic logic = new TicketManageLogic();
            var tempCompanyList = new List<KeyValueModel>();
            ViewBag.IsAdmin = UserInfoModel.IsAdmin ? "true" : "false";
            if ((UserInfoModel.PageModel.AuthNum & 4) == 4)
            {
                tempCompanyList.AddRange(biz.GetCompanyInfoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ ?? 0));
            }
            else if ((UserInfoModel.PageModel.AuthNum & 8) == 8)
            {
                tempCompanyList.AddRange(biz.GetCompanyInfoById(UserInfoModel.PageModel.CompanySEQ ?? 0));
            }
            else if ((UserInfoModel.PageModel.AuthNum & 1) == 1 || (UserInfoModel.PageModel.AuthNum & 16) == 16)
            {
                tempCompanyList.AddRange(biz.GetCompanyInfoByAll());
                tempCompanyList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
            }
            model.CompanySource = new SelectList(tempCompanyList, "Key", "Value");
            var groupList = new List<KeyValueModel>();
            if (UserInfoModel.IsAdmin)
            {
                groupList.AddRange(biz.GetGroupInfoByAll());
                groupList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
            }
            else
            {
                groupList.AddRange(biz.GetGroupInfoById(UserInfoModel.PageModel.GroupSEQ ?? 0));
            }
            model.GroupSource = new SelectList(groupList, "Key", "Value");
            model.PageModel = biz.ShowSalePointInfoById(Convert.ToInt32(id));
            model.NatureList = logic.CodeListToSelectList(biz.GetSystemCodeListByType("PoolType"));
            model.SalePointStateList = logic.CodeListToSelectList(biz.GetSystemCodeListByType("PointState"));
            model.LinkSiteList = logic.ConvertLinkSiteList(biz.GetSalePointInfoListByAll());
            model.WindowList = logic.CodeListToSelectList(new List<Repository.Models.SystemCode>());
            if (model.PageModel != null)
            {
                model.StartTime = model.PageModel.StartTime;
                model.EndTime = model.PageModel.EndTime;
                model.SalePointState = model.PageModel.SalePointState;
                model.DivideInto = model.PageModel.DivideInto;
                model.LinkPointBlock = model.PageModel.LinkPointBlock;
                model.LinkPointInfo = model.PageModel.LinkPointInfo;
            }
            return PartialView(model);
        }

        public ActionResult _PartialSalePointList(SalePointPageModel input)
        {
            return PartialView("~/Views/SalePoint/_PartialSalePointList.cshtml", input);
        }

        [HttpPost]
        public ActionResult SaveSalePointInfo(SalePointPageModel model)
        {
            JsonResult result = new JsonResult();
            bool res = false;
            TicketManageLogic logic = new TicketManageLogic();
            SalePointInfoManageBiz biz = new SalePointInfoManageBiz();
            var groupList = uiz.GetGroupInfoByAll();
            model.NatureList = logic.CodeListToSelectList(biz.GetSystemCodeListByType("PoolType"));
            model.GroupSource = new SelectList(groupList, "Key", "Value");
            logic.ConvertFileToEntity(Request, model);
            model.PageModel.CreateTime = DateTime.Now;
            

            //db.Configuration.ValidateOnSaveEnabled = false;

            res = biz.SaveSalePointInfo(model);

            //db.Configuration.ValidateOnSaveEnabled = true;
            

            if (res)
            {
                result.Data = "保存成功";
            }
            else
            {
                result.Data = "保存失败";
            }
            return RedirectToAction("SalePointListManage");
        }

        public JsonResult DeleteSalePointInfo(string seq)
        {
            JsonResult result = new JsonResult();
            SalePointInfoManageBiz biz = new SalePointInfoManageBiz();
            bool res = false;
            if (!string.IsNullOrEmpty(seq))
            {
                res = biz.DeleteSalePointInfoByEntity(Convert.ToInt32(seq));
            }
            if (res)
            {
                result.Data = "删除成功！";
            }
            else
            {
                result.Data = "删除失败！";
            }

            return result;
        }



    }
}
