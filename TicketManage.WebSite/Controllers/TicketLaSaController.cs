using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.TicketLaSaManage;
using TieckManage.Biz.TicketManage;

namespace TicketManage.WebSite.Controllers
{
    [LoginCheck]
    public class TicketLaSaController : BaseController
    {
        private TicketLaSaBiz biz = new TicketLaSaBiz();


        public ActionResult TicketLaSaList(TicketLaSaPageModel model)
        {
            ViewBag.TicketManageNav = "active open";
            ViewBag.TicketLaSaList = "active";
            model.OperatorAuthNum = UserInfoModel.PageModel.AuthNum;
            if ((model.OperatorAuthNum & 2) == 2)
            {
                model.OperatorUserNoList = new List<int> { UserInfoModel.PageModel.SEQ };
            }
            else if ((model.OperatorAuthNum & 4) == 4)
            {
                model.OperatorUserNoList = biz.GetUserNoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ);
            }
            else if ((model.OperatorAuthNum & 8) == 8)
            {
                model.OperatorUserNoList = biz.GetUserNoByCompanySEQ(UserInfoModel.PageModel.CompanySEQ);
            }
            model.ModelList = biz.GetTicketLaSaByCondition(model);
            string pageCount = ConfigurationManager.AppSettings["PageCount"].ToString();
            model.ListCount = model.ModelList.Count;
            if (model.PageNum == 0)
            {
                model.PageNum = 1;
            }
            else
            {
                model.PageNum = model.PageNum;
            }
            if (model.PageSize == 0)
            {
                model.PageSize = Convert.ToInt32(pageCount);
            }
            else
            {
                model.PageSize = Convert.ToInt32(model.PageSize);
            }
            if (model.ModelList.Count() > model.PageSize)
            {
                model.PageTotal = model.ModelList.Count() % model.PageSize == 0 ? model.ModelList.Count() / model.PageSize : (model.ModelList.Count() / model.PageSize) + 1;
            }
            model.ModelList = model.ModelList.Skip(model.PageSize * (model.PageNum - 1)).Take(model.PageSize).ToList();

            return View(model);
        }

        public PartialViewResult TicketLaSaInfoManage(string id)
        {
            TicketLaSaPageModel pageModel = new TicketLaSaPageModel();
            if (id != "0")
            {
                pageModel.PageModel = biz.GetTickLasaInfo(Convert.ToInt32(id));
                pageModel.PageModel1 = biz.GetTickLasaInfo(Convert.ToInt32(id) + 1);
            }
            pageModel.OperatorAuthNum = UserInfoModel.PageModel.AuthNum;
            return PartialView(pageModel);
        }

        [HttpPost]
        public JsonResult SaveTicketLaSaInfo(TicketLaSaPageModel model)
        {
            JsonResult result = new JsonResult();
            bool res = false;

            decimal brokeragemoney = 0;//服务费1
            decimal brokeragemoney1 = 0;//服务费2
            decimal totalmoney = 0;//总收入

            brokeragemoney = Convert.ToDecimal((model.PageModel.TicketNum + model.PageModel.KidNum) * 5);
            brokeragemoney1 = Convert.ToDecimal((model.PageModel1.TicketNum + model.PageModel1.KidNum) * 5);
            model.PageModel1.CostMoney = model.PageModel.CostMoney;//毛利收入

            model.PageModel.Brokerage = brokeragemoney;//服务费
            model.PageModel1.Brokerage = brokeragemoney1;//服务费

            totalmoney = Convert.ToDecimal(brokeragemoney + brokeragemoney1 + model.PageModel.InsuranceMoney + model.PageModel1.InsuranceMoney + model.PageModel.CostMoney);

            model.PageModel.TotalMoney = totalmoney;//总收入
            model.PageModel1.TotalMoney = totalmoney;//总收入

            string groupno = Guid.NewGuid().ToString();
            model.PageModel.GroupNo = groupno;//组号
            model.PageModel1.GroupNo = groupno;//组号

            model.PageModel1.CreateTime = model.PageModel.CreateTime;
            model.PageModel1.StartDriveDate = model.PageModel.StartDriveDate;
            model.PageModel1.TicketWindow = model.PageModel.TicketWindow;
            model.PageModel1.OperatorName = model.PageModel.OperatorName;
            model.PageModel1.OperatorSEQ = UserInfoModel.PageModel.SEQ;
            res = biz.SaveInfo(model);
            if (res)
            {
                result.Data = "保存成功";
            }
            else
            {
                result.Data = "保存失败";
            }
            return result;
        }
        public JsonResult DeleteTicketLaSaInfo(string seq)
        {
            JsonResult result = new JsonResult();
            bool res = false;
            if (!string.IsNullOrEmpty(seq))
            {
                res = biz.DeleteInfo(Convert.ToInt32(seq));

            }
            if (res)
            {
                result.Data = "删除成功！";
            }
            else
            {
                result.Data = "删除失败！";
            }

            return result;
        }

    }
}
