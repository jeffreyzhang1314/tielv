﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.WebSite.Filters;
using TicketManage.WebSite.Models.DataBaseManage;

namespace TicketManage.WebSite.Controllers
{
    public class DataBaseManageController : Controller
    {

        #region 登录页

        [HttpGet]
        public ActionResult Login()
        {
            var routeDataValue = this.Request.QueryString["Message"];
            var model = new LoginModel();
            model.Message = routeDataValue;
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            model.Message = "用户名密码发错误~！";
            if (string.Compare(model.UserNo, "admin", true) == 0 && string.Compare(model.PassWord, "china123!", true) == 0)
            {
                Session["DataBaseManageInfo"] = model;
                return RedirectToAction("Index");
            }
            return View(model);
        }

        #endregion

        #region 显示内容页

        //
        // GET: /DataBaseManage/
        [HttpGet, DataBaseManage]
        public ActionResult Index()
        {
            var model = new IndexModel();
            model.TableNameSource = new SelectList(GetDataBaseTableName(), "Key", "Value");
            return View(model);
        }

        [HttpPost, DataBaseManage]
        public ActionResult Index(IndexModel model)
        {
            model.TableNameSource = new SelectList(GetDataBaseTableName(), "Key", "Value");
            model.TableContent = GetTableDataByTableName(model.TableName);
            return View(model);
        }

        [AjaxOnly, DataBaseManage]
        public JsonResult DelTableData(int seq, string tableName)
        {
            var excuteFlag = false;
            var strMessage = string.Empty;
            using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TicketManageConnectionString"].ConnectionString))
            {
                var tempStr = string.Format("delete {0} where seq={1}", tableName, seq);
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(tempStr, con))
                {
                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        excuteFlag = true;
                        strMessage = "执行成功~！";
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        con.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return Json(new { result = excuteFlag, message = strMessage }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 业务处理

        /// <summary>
        /// 查询数据库中所有表信息
        /// </summary>
        /// <returns></returns>
        public List<KeyValueModel> GetDataBaseTableName()
        {
            var conn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TicketManageConnectionString"].ConnectionString);
            var dap = new System.Data.SqlClient.SqlDataAdapter("select * from sys.tables order by name", conn);
            var ds = new DataSet();
            dap.Fill(ds);
            var tempList = new List<KeyValueModel>();
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                tempList.Add(new KeyValueModel { Key = Convert.ToString(item["name"]), Value = Convert.ToString(item["name"]) });
            }
            return tempList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName">表格名称</param>
        /// <returns></returns>
        public System.Data.DataTable GetTableDataByTableName(string tableName)
        {
            var conn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TicketManageConnectionString"].ConnectionString);
            var dap = new System.Data.SqlClient.SqlDataAdapter(string.Format("select * from {0}", tableName), conn);
            var dt = new System.Data.DataTable();
            dap.Fill(dt);
            return dt;
        }

        #endregion

    }
}
