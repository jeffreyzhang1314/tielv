using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TicketManage.WebSite.Filters;
using TicketManage.WebSite.Logic;
using TieckManage.Biz.TicketManage;

namespace TicketManage.WebSite.Controllers
{
    [LoginCheck]
    public class CompanyController : BaseController
    {

        private CompanyInfoManageBiz biz = new CompanyInfoManageBiz();
        private GroupInfoManageBiz groupBiz = new GroupInfoManageBiz();
        //
        // GET: /Company/
        public PartialViewResult CompanyInfoManage(string id)
        {
            CompanyPageModel model = new CompanyPageModel();
            model.PageModel = biz.ShowCompanyInfoById(Convert.ToInt32(id));
            TicketManageLogic logic = new TicketManageLogic();
            var groupList = new List<KeyValueModel>();
            if ((UserInfoModel.PageModel.AuthNum & 1) == 1
                || (UserInfoModel.PageModel.AuthNum & 16) == 16)
            {
                groupList.AddRange(biz.GetGroupInfoByAll());
                groupList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
            }
            else
            {
                groupList.AddRange(biz.GetGroupInfoById(UserInfoModel.PageModel.GroupSEQ ?? 0));
            }
            model.GroupSource = new SelectList(groupList, "Key", "Value");
            return PartialView(model);
        }

        private void CompanyListManageInit(CompanyPageModel model)
        {
            ViewBag.BaseDataNav = "active open";
            ViewBag.CompanyNav = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", 10);
            TicketManageLogic logic = new TicketManageLogic();
            model.GroupList = logic.ConvertGroupList(groupBiz.ShowGroupInfoByAll());
            var groupList = new List<KeyValueModel>();
            if ((UserInfoModel.PageModel.AuthNum & 1) == 1
                || (UserInfoModel.PageModel.AuthNum & 16) == 16)
            {
                groupList.AddRange(biz.GetGroupInfoByAll());
                groupList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
            }
            else
            {
                groupList.AddRange(biz.GetGroupInfoById(UserInfoModel.PageModel.GroupSEQ ?? 0));
            }
            model.GroupSource = new SelectList(groupList, "Key", "Value");

            model.OperatorAuthNum = UserInfoModel.PageModel.AuthNum;
        }


        [HttpGet]
        public ActionResult CompanyListManage()
        {
            var model = new CompanyPageModel() { CurrentPage = 1, PageSize = 10, PageModel = new CompanyInfo() };
            CompanyListManageInit(model);
            model.PageModel.GroupSEQ = UserInfoModel.PageModel.GroupSEQ;
            model.ListModel = biz.GetCompanyByQueryable(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult CompanyListManage(CompanyPageModel model)
        {
            CompanyListManageInit(model);
            model.ListModel = biz.GetCompanyByQueryable(model);
            return View(model);
        }

        public ActionResult _PartialCompanyList(CompanyPageModel input)
        {
            return PartialView("~/Views/Company/_PartialCompanyList.cshtml", input);
        }

        [HttpPost]
        public JsonResult SaveCompanyInfo(CompanyPageModel model)
        {
            JsonResult result = new JsonResult();
            bool res = false;
            TicketManageLogic logic = new TicketManageLogic();
            CompanyInfoManageBiz biz = new CompanyInfoManageBiz();
            model.PageModel.CreateTime = DateTime.Now;
            res = biz.SaveCompanyInfo(model);
            if (res)
            {
                result.Data = "保存成功";
            }
            else
            {
                result.Data = "保存失败";
            }
            return result;
        }

        public JsonResult DeleteCompanyInfo(string seq)
        {
            JsonResult result = new JsonResult();
            CompanyInfoManageBiz biz = new CompanyInfoManageBiz();
            bool res = false;
            if (!string.IsNullOrEmpty(seq))
            {
                res = biz.DeleteCompanyInfoByEntity(Convert.ToInt32(seq));
            }
            if (res)
            {
                result.Data = "删除成功！";
            }
            else
            {
                result.Data = "删除失败！";
            }

            return result;
        }

    }
}
