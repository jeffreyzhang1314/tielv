﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TieckManage.Biz.TravelLocalAgencyManage;

namespace TicketManage.WebSite.Controllers
{
    /// <summary>
    /// 地接社信息接口信息
    /// </summary>
    public class ITravelLocalAgencyController : BaseController
    {
        /// <summary>
        /// 返回所有导游信息
        /// </summary>
        /// <returns></returns>
        public JsonResult ShowTravelLocalAgency(string agencyName)
        {
            var biz = new TravelLocalAgencyBiz();
            return Json(biz.ShowAgencyInfo(agencyName), JsonRequestBehavior.AllowGet);
        }

    }
}
