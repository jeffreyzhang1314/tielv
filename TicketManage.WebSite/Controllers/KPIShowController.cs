﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.KPIShowManage;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.KPIShowManage;
using TieckManage.Biz.OtherUserManage;

namespace TicketManage.WebSite.Controllers
{
    [LoginCheck]
    public class KPIShowController : BaseController
    {

        #region 操作变量

        private GX_SalesManKPIBiz bizTwo = new GX_SalesManKPIBiz();
        private GX_TicketSellerSalaryDetailInfoBiz bizOne = new GX_TicketSellerSalaryDetailInfoBiz();
        private GX_EmployeeSalaryDetailInfoBiz bizThree = new GX_EmployeeSalaryDetailInfoBiz();
        private GX_GrossProfitCompanyBiz bizFour = new GX_GrossProfitCompanyBiz();
        /// <summary>
        /// 用户操作业务实例
        /// </summary>
        private UserInfoOtherBiz bizUserInfoOther = new UserInfoOtherBiz();
        private List<string> ExcelModelHead = new List<string> { "单位", "姓名", "起止时间", "起号码", "止号码", "售票总张数", "学生票作废票", "售票天数", "岗位工资", "绩效基数", "实际售票张数", "自用团体票张数", "代售点总张数", "代售点绩效奖励", "乘意险起号", "乘意险止号", "网团张数", "乘意险总张数", "绩效奖励", "应付工资" };

        #endregion

        #region 售票员工资明细

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GX_TicketSellerSalaryDetailInfo()
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_TicketSellerSalaryDetailInfo = "active";
            ViewBag.SystemName = "工效管理系统";
            var model = new GX_TicketSellerSalaryDetailInfoModels() { SearchModel = new Repository.Models.GX_TicketSellerSalaryDetailInfo() };
            model.SearchModel.OperatorYear = DateTime.Now.Year;
            model.SearchModel.OperatorMonth = DateTime.Now.Month;
            model.ModelList = bizOne.GX_TicketSellerSalaryDetailInfoSearch(model.SearchModel);
            model.CurrentOperatorName = this.KPIUserInfoModel.PageModel.UserName;
            ViewBag.Message = TempData["message"];
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GX_TicketSellerSalaryDetailInfo(GX_TicketSellerSalaryDetailInfoModels model)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_TicketSellerSalaryDetailInfo = "active";
            ViewBag.SystemName = "工效管理系统";
            if (RouteData.Values["id"] != null)
            {
                model.MessageState = true;
                model.Message = Convert.ToString(RouteData.Values["id"]);
            }
            model.ModelList = bizOne.GX_TicketSellerSalaryDetailInfoSearch(model.SearchModel);
            return View(model);
        }

        [HttpPost]
        public ActionResult GX_TicketSellerSalaryDetailInfoSave(GX_TicketSellerSalaryDetailInfoModels model)
        {
            var result = bizOne.GX_TicketSellerSalaryDetailInfoInsert(model.SearchModel);
            return RedirectToAction("GX_TicketSellerSalaryDetailInfo", new { id = result ? "添加成功~！" : "添加失败~！" });
        }

        [AjaxOnly]
        public ActionResult GX_TicketSellerSalaryDetailInfoDelete(int seq)
        {
            var message = string.Empty;
            var result = bizOne.GX_TicketSellerSalaryDetailInfoDelete(seq, ref message);
            return Json(new { result = result, message = message });
        }

        /// <summary>
        /// 导出Excel文件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult ExcelExport(GX_TicketSellerSalaryDetailInfoModels model)
        {
            Session["ExcelSearchModel"] = model;
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 生成下载文件
        /// </summary>
        /// <returns></returns>
        public FileContentResult ExcelDownLoad()
        {

            var ExcelTitleList = new List<string>() { "单位", "姓名", "起止时间", "起号码", "售票总张数", "学生票作废票", "售票天数" };
            var searchCondition = Session["ExcelSearchModel"] as GX_TicketSellerSalaryDetailInfoModels;
            var dataResult = bizOne.GX_TicketSellerSalaryDetailInfoSearch(searchCondition.SearchModel);
            var book = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = book.CreateSheet("ExcelExport");

            var tableBorderStyle = book.CreateCellStyle();
            tableBorderStyle.BorderBottom = BorderStyle.Thin;
            tableBorderStyle.BorderLeft = BorderStyle.Thin;
            tableBorderStyle.BorderRight = BorderStyle.Thin;
            tableBorderStyle.BorderTop = BorderStyle.Thin;
            var tableContentFont = book.CreateFont();
            tableContentFont.FontName = "微软雅黑";
            tableContentFont.FontHeightInPoints = 12;
            tableBorderStyle.SetFont(tableContentFont);

            var tableTitleStyle = book.CreateCellStyle();
            tableTitleStyle.BorderBottom = BorderStyle.Thin;
            tableTitleStyle.BorderLeft = BorderStyle.Thin;
            tableTitleStyle.BorderRight = BorderStyle.Thin;
            tableTitleStyle.BorderTop = BorderStyle.Thin;
            var tableTitleFont = book.CreateFont();
            tableTitleFont.FontName = "微软雅黑";
            tableTitleFont.FontHeightInPoints = 12;
            tableTitleFont.Boldweight = short.MaxValue;
            tableTitleStyle.SetFont(tableTitleFont);

            ICellStyle style = book.CreateCellStyle();

            //设置单元格的样式：水平对齐居中
            style.Alignment = HorizontalAlignment.Center;
            //新建一个字体样式对象
            IFont font = book.CreateFont();
            //设置字体加粗样式
            font.Boldweight = short.MaxValue;
            font.FontName = "微软雅黑";
            font.FontHeightInPoints = 16;
            //使用SetFont方法将字体样式添加到单元格样式中 
            style.SetFont(font);
            //将新的样式赋给单元格
            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 19));

            var row0 = sheet.CreateRow(0);
            row0.CreateCell(0).SetCellValue(string.Format("{0} 年 {1} 月 售票员工资明细", searchCondition.SearchModel.OperatorYear, searchCondition.SearchModel.OperatorMonth));
            row0.GetCell(0).CellStyle = style;


            var indexVal = 0;

            var rowTitle = sheet.CreateRow(indexVal + 3);
            rowTitle.CreateCell(0).SetCellValue("序号");
            rowTitle.CreateCell(1).SetCellValue("单位");
            rowTitle.CreateCell(2).SetCellValue("姓名");
            rowTitle.CreateCell(3).SetCellValue("起止时间");
            rowTitle.CreateCell(4).SetCellValue("起号码");
            rowTitle.CreateCell(5).SetCellValue("止号码");
            rowTitle.CreateCell(6).SetCellValue("售票总张数");
            rowTitle.CreateCell(7).SetCellValue("学生票作废票");
            rowTitle.CreateCell(8).SetCellValue("售票天数");
            rowTitle.CreateCell(9).SetCellValue("岗位工资");
            rowTitle.CreateCell(10).SetCellValue("绩效基数");
            rowTitle.CreateCell(11).SetCellValue("实际售票张数");
            rowTitle.CreateCell(12).SetCellValue("自用团体票张数");
            rowTitle.CreateCell(13).SetCellValue("代售点总张数");
            rowTitle.CreateCell(14).SetCellValue("代售点绩效奖励");
            rowTitle.CreateCell(15).SetCellValue("乘意险起号");
            rowTitle.CreateCell(16).SetCellValue("乘意险止号");
            rowTitle.CreateCell(17).SetCellValue("网团张数");
            rowTitle.CreateCell(18).SetCellValue("乘意险总张数");
            rowTitle.CreateCell(19).SetCellValue("绩效奖励");
            rowTitle.CreateCell(20).SetCellValue("应付工资");

            for (int i = 0; i < 21; i++)
            {
                rowTitle.GetCell(i).CellStyle = tableTitleStyle;
            }


            indexVal = 4;
            int totalSaleActualNum = 0, totalGroupTicketNum = 0, totalTotalTicketNum = 0, totalGroupTicketNet = 0, totalGroupTotalNum = 0;
            decimal totalPerformanceReward = 0m, totalPerformanceRewardGroup = 0m, totalWagesNum = 0m;
            foreach (var item in dataResult)
            {
                var row = sheet.CreateRow(indexVal);
                row.CreateCell(0).SetCellValue(indexVal - 3);
                row.CreateCell(1).SetCellValue(item.DeptName);
                row.CreateCell(2).SetCellValue(item.UserName);
                row.CreateCell(3).SetCellValue(item.StartEndTime);
                row.CreateCell(4).SetCellValue(item.TicketNoStart);
                row.CreateCell(5).SetCellValue(item.TicketNoEnd);
                row.CreateCell(6).SetCellValue(Convert.ToString(item.SaleNum ?? 0));
                row.CreateCell(7).SetCellValue(Convert.ToString(item.TicketKidNum ?? 0));
                row.CreateCell(8).SetCellValue(Convert.ToString(item.SaleDay ?? 0));
                row.CreateCell(9).SetCellValue(Convert.ToString(item.PostionSalary ?? 0));
                row.CreateCell(10).SetCellValue(Convert.ToString(item.PerformanceSalary ?? 0));
                row.CreateCell(11).SetCellValue(Convert.ToString(item.SaleActualNum ?? 0));
                row.CreateCell(12).SetCellValue(Convert.ToString(item.GroupTicketNum ?? 0));
                row.CreateCell(13).SetCellValue(Convert.ToString(item.TotalTicketNum ?? 0));
                row.CreateCell(14).SetCellValue(Convert.ToString(item.PerformanceReward ?? 0));
                row.CreateCell(15).SetCellValue(item.InsuranceNoStart);
                row.CreateCell(16).SetCellValue(item.InsuranceNoEnd);
                row.CreateCell(17).SetCellValue(Convert.ToString(item.GroupTicketNet ?? 0));
                row.CreateCell(18).SetCellValue(Convert.ToString(item.GroupTotalNum ?? 0));
                row.CreateCell(19).SetCellValue(Convert.ToString(item.PerformanceRewardGroup ?? 0));
                row.CreateCell(20).SetCellValue(Convert.ToString(item.WagesNum ?? 0));

                for (int i = 0; i < 21; i++)
                {
                    row.GetCell(i).CellStyle = tableBorderStyle;
                }

                indexVal += 1;
                totalSaleActualNum += item.SaleActualNum ?? 0;
                totalGroupTicketNum += item.GroupTicketNum ?? 0;
                totalTotalTicketNum += item.TotalTicketNum ?? 0;
                totalPerformanceReward += item.PerformanceReward ?? 0;
                totalGroupTicketNet += item.GroupTicketNet ?? 0;
                totalGroupTotalNum += item.GroupTicketNum ?? 0;
                totalPerformanceRewardGroup += item.PerformanceRewardGroup ?? 0;
                totalWagesNum += item.WagesNum ?? 0;
            }
            var rowFoot = sheet.CreateRow(++indexVal);
            rowFoot.CreateCell(0).SetCellValue("合计");
            rowFoot.CreateCell(11).SetCellValue(Convert.ToString(totalSaleActualNum));
            rowFoot.CreateCell(12).SetCellValue(Convert.ToString(totalGroupTicketNum));
            rowFoot.CreateCell(13).SetCellValue(Convert.ToString(totalTotalTicketNum));
            rowFoot.CreateCell(14).SetCellValue(Convert.ToString(totalPerformanceReward));
            rowFoot.CreateCell(17).SetCellValue(Convert.ToString(totalGroupTicketNet));
            rowFoot.CreateCell(18).SetCellValue(Convert.ToString(totalGroupTotalNum));
            rowFoot.CreateCell(19).SetCellValue(Convert.ToString(totalPerformanceRewardGroup));
            rowFoot.CreateCell(20).SetCellValue(Convert.ToString(totalWagesNum));
            rowFoot.GetCell(0).CellStyle = tableTitleStyle;
            rowFoot.GetCell(11).CellStyle = tableTitleStyle;
            rowFoot.GetCell(12).CellStyle = tableTitleStyle;
            rowFoot.GetCell(13).CellStyle = tableTitleStyle;
            rowFoot.GetCell(14).CellStyle = tableTitleStyle;
            rowFoot.GetCell(17).CellStyle = tableTitleStyle;
            rowFoot.GetCell(18).CellStyle = tableTitleStyle;
            rowFoot.GetCell(19).CellStyle = tableTitleStyle;
            rowFoot.GetCell(20).CellStyle = tableTitleStyle;

            var row2 = sheet.CreateRow(indexVal + 3);
            row2.CreateCell(0).SetCellValue("董事长签字：");
            row2.GetCell(0).CellStyle = style;
            row2.CreateCell(8).SetCellValue("部长签字：");
            row2.GetCell(8).CellStyle = style;
            row2.CreateCell(16).SetCellValue("制表人：");
            row2.GetCell(16).CellStyle = style;
            for (int i = 0; i < 21; i++)
            {
                sheet.AutoSizeColumn(i);
            }

            // 写入到客户端  
            var ms = new System.IO.MemoryStream();
            book.Write(ms);
            var fileName = string.Format("{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"));
            return File(ms.ToArray(), "application/ms-excel", fileName);
        }

        /// <summary>
        /// 接受上传文件
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GX_FileImport()
        {
            try
            {
                var pageYear = Request.Form["ShearchYeartxt"];
                var pageMonth = Request.Form["ShearchMonthtxt"];
                //保存文件
                var fileList = FileUpload(this.Request);
                if (fileList != null && fileList.Count > 0)
                {
                    IWorkbook workbook = null;
                    FileStream fs = null;
                    ISheet sheet = null;

                    var model = new GX_TicketSellerSalaryDetailInfoModels();
                    model.ModelList = new List<Repository.Models.GX_TicketSellerSalaryDetailInfo>();
                    DataTable data = new DataTable();
                    int startRow = 0;


                    string filepath = ConfigurationManager.AppSettings["FilePath"].ToString();
                    var filename = Path.Combine(string.Format("{0}{1}", Request.PhysicalApplicationPath, filepath), fileList.FirstOrDefault().Key);
                    //读取文件
                    fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
                    if (filename.IndexOf(".xlsx") > 0)
                    {
                        workbook = new XSSFWorkbook(fs);
                    }
                    else if (filename.IndexOf(".xls") > 0)
                    {
                        workbook = new HSSFWorkbook(fs);
                    }
                    sheet = workbook.GetSheetAt(0);
                    if (sheet != null)
                    {
                        IRow firstRow = sheet.GetRow(3);
                        int cellCount = firstRow.LastCellNum;

                        startRow = sheet.FirstRowNum + 4;
                        //最后一列的标号
                        int rowCount = sheet.LastRowNum;

                        for (int i = startRow; i <= rowCount; ++i)
                        {
                            IRow row = sheet.GetRow(i);
                            if (row == null) continue; //没有数据的行默认是null　　
                            if (!IsInt(Convert.ToString(row.GetCell(0))) && i > 4)
                            {
                                break;
                            }
                            var tempModel = new Repository.Models.GX_TicketSellerSalaryDetailInfo();
                            tempModel.OperatorYear = Convert.ToInt32(pageYear);
                            tempModel.OperatorMonth = Convert.ToInt32(pageMonth);
                            tempModel.DeptName = Convert.ToString(row.GetCell(1));
                            tempModel.UserName = Convert.ToString(row.GetCell(2));
                            tempModel.StartEndTime = Convert.ToString(row.GetCell(3));
                            tempModel.TicketNoStart = Convert.ToString(row.GetCell(4));
                            tempModel.TicketNoEnd = Convert.ToString(row.GetCell(5));
                            tempModel.SaleNum = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(row.GetCell(6))) ? "0" : Convert.ToString(row.GetCell(6)));
                            tempModel.TicketKidNum = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(row.GetCell(7))) ? "0" : Convert.ToString(row.GetCell(7)));
                            tempModel.SaleDay = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(row.GetCell(8))) ? "0" : Convert.ToString(row.GetCell(8)));
                            tempModel.PostionSalary = Convert.ToDecimal(string.IsNullOrEmpty(Convert.ToString(row.GetCell(9))) ? "0" : Convert.ToString(row.GetCell(9)));
                            tempModel.PerformanceSalary = Convert.ToDecimal(string.IsNullOrEmpty(Convert.ToString(row.GetCell(10))) ? "0" : Convert.ToString(row.GetCell(10)));
                            tempModel.SaleActualNum = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(row.GetCell(11))) ? "0" : Convert.ToString(row.GetCell(11)));
                            tempModel.GroupTicketNum = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(row.GetCell(12))) ? "0" : Convert.ToString(row.GetCell(12)));
                            tempModel.TotalTicketNum = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(row.GetCell(13))) ? "0" : Convert.ToString(row.GetCell(13)));
                            tempModel.PerformanceReward = Convert.ToDecimal(string.IsNullOrEmpty(Convert.ToString(row.GetCell(14))) ? "0" : Convert.ToString(row.GetCell(14)));
                            tempModel.InsuranceNoStart = Convert.ToString(row.GetCell(15));
                            tempModel.InsuranceNoEnd = Convert.ToString(row.GetCell(16));
                            tempModel.GroupTicketNet = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(row.GetCell(17))) ? "0" : Convert.ToString(row.GetCell(17)));
                            tempModel.GroupTotalNum = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(row.GetCell(18))) ? "0" : Convert.ToString(row.GetCell(18)));
                            tempModel.PerformanceRewardGroup = Convert.ToDecimal(string.IsNullOrEmpty(Convert.ToString(row.GetCell(19))) ? "0" : Convert.ToString(row.GetCell(19)));
                            tempModel.WagesNum = Convert.ToDecimal(string.IsNullOrEmpty(Convert.ToString(row.GetCell(20))) ? "0" : Convert.ToString(row.GetCell(20)));
                            model.ModelList.Add(tempModel);
                        }
                        bizThree.SaveUploadExcel(model.ModelList);
                    }
                }

                TempData["message"] = "上传文件成功~！";
                return RedirectToAction("GX_TicketSellerSalaryDetailInfo", "KPIShow");
            }
            catch (Exception ex)
            {

                throw ex;
            }



        }

        /// <summary>
        /// 售票员明细编辑
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GX_TicketSellerSalaryDetailInfoEdit()
        {
            var tempArray = Request.Form[0];
            var editObjArray = new List<string>();
            foreach (var item in tempArray.Split(','))
            {
                editObjArray.Add(item);
            }
            var reuslt = bizTwo.GX_TicketSellerSalaryDetailInfoEdit(editObjArray);
            return Json(reuslt, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult GX_TicketSellerSalaryDetailInfoReportSave()
        {
            var year = Request.Form["ShearchYeartxt"];
            var month = Request.Form["ShearchMonthtxt"];
            var operatorName = Request.Form["ReportOperatorName"];
            var message = string.Empty;
            var result = false;
            if (!string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month))
            {
                result = bizTwo.GX_TicketSellerSalaryReportInsert(year, month, operatorName, ref message);
            }
            else
            {
                message = "年份月份不能为空";
            }

            return Json(new { Result = result, Message = message }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult GX_TicketSellerSalaryDetailInfoReportDel(int seq)
        {
            var message = string.Empty;
            var checkResult = bizTwo.CheckGX_TicketSellerSalaryApprove(seq, ref message);
            if (!checkResult)
            {
                var result = bizTwo.GX_TicketSellerSalaryDetailInfoReportDelete(seq, ref message);
                return Json(new { result = result, message = message });
            }
            else
            {
                return Json(new { result = !checkResult, message = message });
            }
        }

        /// <summary>
        /// 显示售票员报表信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GX_TicketSellerSalaryDetailInfoReport()
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_TicketSellerSalaryDetailInfo = "active";
            ViewBag.SystemName = "工效管理系统";
            var pageModel = new GX_TicketSellerSalaryReportModels();
            var year = DateTime.Now.Year.ToString();
            var month = DateTime.Now.Month.ToString();
            pageModel.ModelList = bizTwo.GX_TicketSellerSalaryReportByYear(year, month);
            pageModel.SearchModel = new Repository.Models.GX_TicketSellerSalaryReport();
            pageModel.SearchModel.OperatorYear = year;
            pageModel.SearchModel.OperatorMonth = month;
            return View(pageModel);
        }

        /// <summary>
        /// 查询售票员报表信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GX_TicketSellerSalaryDetailInfoReport(GX_TicketSellerSalaryReportModels model)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_TicketSellerSalaryDetailInfo = "active";
            ViewBag.SystemName = "工效管理系统";
            model.ModelList = bizTwo.GX_TicketSellerSalaryReportByYear(model.SearchModel.OperatorYear, model.SearchModel.OperatorMonth);
            return View(model);
        }

        /// <summary>
        /// 显示详细报表内容
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GX_TicketSellerSalaryDetailInfoReportDetail(int id)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_TicketSellerSalaryDetailInfo = "active";
            ViewBag.SystemName = "工效管理系统";
            var model = new GX_TicketSellerSalaryDetailInfoModels() { SearchModel = new Repository.Models.GX_TicketSellerSalaryDetailInfo() };
            var tempModel = bizTwo.GX_TicketSellerSalaryReportBySEQ(id);
            if (tempModel != null)
            {
                model.SearchModel.OperatorYear = string.IsNullOrEmpty(tempModel.OperatorYear) ? 0 : Convert.ToInt32(tempModel.OperatorYear);
                model.SearchModel.OperatorMonth = string.IsNullOrEmpty(tempModel.OperatorMonth) ? 0 : Convert.ToInt32(tempModel.OperatorMonth);
            }
            model.CurrentOperatorName = this.KPIUserInfoModel.PageModel.UserName;
            model.AuthNum = this.KPIUserInfoModel.PageModel.AuthNum;
            //判断当前用户权限和流程节点是否可以查看报表信息
            if (model.AuthNum != null && ((model.AuthNum & 1) == 1 || (model.AuthNum & 524288) == 524288) && tempModel != null && (tempModel.SubmitState ?? false))
            {
                model.ModelList = bizOne.GX_TicketSellerSalaryDetailInfoSearch(model.SearchModel);
            }
            else if (model.AuthNum != null && ((model.AuthNum & 1) == 1 || (model.AuthNum & 256) == 256) && tempModel != null && !string.IsNullOrEmpty(tempModel.NodeOneUserName))
            {
                model.ModelList = bizOne.GX_TicketSellerSalaryDetailInfoSearch(model.SearchModel);
            }
            else if (model.AuthNum != null && ((model.AuthNum & 1) == 1 || (model.AuthNum & 1024) == 1024) && tempModel != null && !string.IsNullOrEmpty(tempModel.NodeTwoUserName))
            {
                model.ModelList = bizOne.GX_TicketSellerSalaryDetailInfoSearch(model.SearchModel);
            }
            else if (model.AuthNum != null && ((model.AuthNum & 1) == 1 || (model.AuthNum & 2048) == 2048) && tempModel != null && !string.IsNullOrEmpty(tempModel.NodeThreeUserName))
            {
                model.ModelList = bizOne.GX_TicketSellerSalaryDetailInfoSearch(model.SearchModel);
            }
            else if (model.AuthNum != null && ((model.AuthNum & 1) == 1 || (model.AuthNum & 131072) == 131072))
            {
                model.ModelList = bizOne.GX_TicketSellerSalaryDetailInfoSearch(model.SearchModel);
            }
            ViewBag.Message = id;
            model.ReportDetailSEQ = Convert.ToInt32(id);
            return View(model);
        }

        [AjaxOnly]
        public JsonResult GX_TicketSellerSalaryDetailInfoReportApprove(int seq, string type)
        {
            var userName = this.KPIUserInfoModel.PageModel.UserName;
            var message = "审批完成";
            var result = bizTwo.GX_TicketSellerSalaryDetailInfoReportApprove(seq, type, "通过", "完成", userName, ref message);
            return Json(new { result = result, message = message }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 计调人员绩效联挂

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GX_SalesManKPI()
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_SalesManKPI = "active";
            ViewBag.SystemName = "工效管理系统";
            var pagemodel = new GX_SalesManKPIModels() { SearchModel = new Repository.Models.GX_SalesManKPI() };
            pagemodel.SearchModel.OperatorYear = DateTime.Now.Year.ToString();
            pagemodel.SearchModel.OperatorMonth = DateTime.Now.Month.ToString();
            pagemodel.ModelList = bizTwo.GX_SalesManKPISearch(pagemodel.SearchModel);
            pagemodel.CurrentOperatorName = this.KPIUserInfoModel.PageModel.UserName;
            return View(pagemodel);
        }

        [HttpPost]
        public ActionResult GX_SalesManKPI(GX_SalesManKPIModels model)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_SalesManKPI = "active";
            ViewBag.SystemName = "工效管理系统";
            if (RouteData.Values["id"] != null)
            {
                model.MessageState = true;
                model.Message = Convert.ToString(RouteData.Values["id"]);
            }
            model.ModelList = bizTwo.GX_SalesManKPISearch(model.SearchModel);
            return View(model);
        }

        [HttpGet]
        public ActionResult GX_SalesManKPIReport()
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_SalesManKPI = "active";
            var pageModel = new GX_SalesManKPIReportModels();
            var year = DateTime.Now.Year.ToString();
            var month = DateTime.Now.Month.ToString();
            pageModel.ModelList = bizTwo.GX_SalesManKPIByYear(year, month);
            pageModel.SearchModel = new Repository.Models.GX_SalesManKPIReport();
            pageModel.SearchModel.OperatorYear = year;
            pageModel.SearchModel.OperatorMonth = month;
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult GX_SalesManKPIReport(GX_SalesManKPIReportModels model)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_SalesManKPI = "active";
            model.ModelList = bizTwo.GX_SalesManKPIByYear(model.SearchModel.OperatorYear, model.SearchModel.OperatorMonth);
            return View(model);
        }

        [HttpGet]
        public ActionResult GX_SalesManKPIReportDetail(string id)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_SalesManKPI = "active";
            var pageModel = new GX_SalesManKPIModels();
            pageModel.AuthNum = this.KPIUserInfoModel.PageModel.AuthNum;

            pageModel.ModelList = bizTwo.GX_SalesManKPIReportSearch(id, pageModel.AuthNum ?? 0);
            pageModel.ReportDetailSEQ = Convert.ToInt32(id);
            return View(pageModel);
        }

        /// <summary>
        /// 查看报表中删除按钮功能
        /// </summary>
        /// <param name="id">删除指定序号报表</param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GX_SalesManKPIReportDelete(int id)
        {
            var message = string.Empty;
            var checkResult = bizTwo.CheckGX_SalesManKPIApprove(id, ref message);
            if (!checkResult)
            {
                var result = bizTwo.GX_SalesManKPIReportDelete(id, ref message);
                return Json(new { result = result, message = message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { result = !checkResult, message = message }, JsonRequestBehavior.AllowGet);
            }
        }
        [AjaxOnly]
        public JsonResult GX_SalesManKPIReportApprove(int seq, string type)
        {
            var userName = this.KPIUserInfoModel.PageModel.UserName;
            var message = "审批完成";
            var result = bizTwo.GX_SalesManKPIReportApprove(seq, type, "通过", "完成", userName, ref message);
            return Json(new { result = result, message = message }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult GX_SalesManKPIReportSave()
        {
            var year = Request.Form["ShearchYeartxt"];
            var month = Request.Form["ShearchMonthtxt"];
            var operatorName = Request.Form["ReportOperatorName"];
            var message = string.Empty;
            var result = false;
            if (!string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month))
            {
                result = bizTwo.GX_SalesManKPIReportInsert(year, month, operatorName, ref message);
            }
            else
            {
                message = "年份月份不能为空";
            }

            return Json(new { Result = result, Message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GX_SalesManKPISave(GX_SalesManKPIModels model)
        {
            var result = bizTwo.GX_SalesManKPIInsert(model.SearchModel);

            return RedirectToAction("GX_SalesManKPI", new { id = result ? "添加成功~！" : "添加失败~！" });
        }

        [AjaxOnly]
        public JsonResult GX_SalesManKPIDelete(int seq)
        {
            var message = string.Empty;
            var result = bizTwo.GX_SalesManKPIDelete(seq, ref message);
            return Json(new { result = result, message = message });
        }

        [AjaxOnly]
        public JsonResult GX_SalesManKPIEdit()
        {
            var tempArray = Request.Form[0];
            var editObjArray = new List<string>();
            foreach (var item in tempArray.Split(','))
            {
                editObjArray.Add(item);
            }
            var reuslt = bizTwo.GX_SalesManKPIEdit(editObjArray);
            return Json(reuslt, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 导出Excel文件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult ExcelExportSalesManKPI(GX_SalesManKPIModels model)
        {
            Session["ExcelSearchModelSalesManKPI"] = model;
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 生成下载文件
        /// </summary>
        /// <returns></returns>
        public FileContentResult ExcelDownLoadSalesManKPI()
        {
            var searchCondition = Session["ExcelSearchModelSalesManKPI"] as GX_SalesManKPIModels;
            var dataResult = bizTwo.GX_SalesManKPISearch(searchCondition.SearchModel);
            var book = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = book.CreateSheet("ExcelExport");

            var tableBorderStyle = book.CreateCellStyle();
            tableBorderStyle.BorderBottom = BorderStyle.Thin;
            tableBorderStyle.BorderLeft = BorderStyle.Thin;
            tableBorderStyle.BorderRight = BorderStyle.Thin;
            tableBorderStyle.BorderTop = BorderStyle.Thin;
            var tableContentFont = book.CreateFont();
            tableContentFont.FontName = "微软雅黑";
            tableContentFont.FontHeightInPoints = 12;
            tableBorderStyle.SetFont(tableContentFont);

            var tableTitleStyle = book.CreateCellStyle();
            tableTitleStyle.BorderBottom = BorderStyle.Thin;
            tableTitleStyle.BorderLeft = BorderStyle.Thin;
            tableTitleStyle.BorderRight = BorderStyle.Thin;
            tableTitleStyle.BorderTop = BorderStyle.Thin;
            var tableTitleFont = book.CreateFont();
            tableTitleFont.FontName = "微软雅黑";
            tableTitleFont.FontHeightInPoints = 12;
            tableTitleFont.Boldweight = short.MaxValue;
            tableTitleStyle.SetFont(tableTitleFont);

            ICellStyle style = book.CreateCellStyle();

            //设置单元格的样式：水平对齐居中
            style.Alignment = HorizontalAlignment.Center;
            //新建一个字体样式对象
            IFont font = book.CreateFont();
            //设置字体加粗样式
            font.Boldweight = short.MaxValue;
            font.FontName = "微软雅黑";
            font.FontHeightInPoints = 16;
            //使用SetFont方法将字体样式添加到单元格样式中 
            style.SetFont(font);
            //将新的样式赋给单元格
            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 13));

            var row0 = sheet.CreateRow(0);
            row0.CreateCell(0).SetCellValue(string.Format("{0}年沈铁国旅集团计调人员绩效联挂（{1}月份工资用）", searchCondition.SearchModel.OperatorYear, searchCondition.SearchModel.OperatorMonth));
            row0.GetCell(0).CellStyle = style;


            var indexVal = 0;

            var rowTitle = sheet.CreateRow(indexVal + 2);
            rowTitle.CreateCell(0).SetCellValue("序号");
            rowTitle.CreateCell(1).SetCellValue("单位");
            rowTitle.CreateCell(2).SetCellValue("姓名");
            rowTitle.CreateCell(3).SetCellValue("职名");
            rowTitle.CreateCell(4).SetCellValue("职名工资");
            rowTitle.CreateCell(5).SetCellValue("绩效工资");
            rowTitle.CreateCell(6).SetCellValue("集团下达指标（万元）");
            rowTitle.CreateCell(7).SetCellValue("实际完成毛利润（万元）");
            rowTitle.CreateCell(8).SetCellValue("完成比率");
            rowTitle.CreateCell(9).SetCellValue("年度累计绩效");
            rowTitle.CreateCell(10).SetCellValue("实付累计绩效");
            rowTitle.CreateCell(11).SetCellValue("本月实付绩效");
            rowTitle.CreateCell(12).SetCellValue("应付工资");
            rowTitle.CreateCell(13).SetCellValue("备注");
            rowTitle.GetCell(0).CellStyle = tableTitleStyle;
            rowTitle.GetCell(1).CellStyle = tableTitleStyle;
            rowTitle.GetCell(2).CellStyle = tableTitleStyle;
            rowTitle.GetCell(3).CellStyle = tableTitleStyle;
            rowTitle.GetCell(4).CellStyle = tableTitleStyle;
            rowTitle.GetCell(5).CellStyle = tableTitleStyle;
            rowTitle.GetCell(6).CellStyle = tableTitleStyle;
            rowTitle.GetCell(7).CellStyle = tableTitleStyle;
            rowTitle.GetCell(8).CellStyle = tableTitleStyle;
            rowTitle.GetCell(9).CellStyle = tableTitleStyle;
            rowTitle.GetCell(10).CellStyle = tableTitleStyle;
            rowTitle.GetCell(11).CellStyle = tableTitleStyle;
            rowTitle.GetCell(12).CellStyle = tableTitleStyle;
            rowTitle.GetCell(13).CellStyle = tableTitleStyle;

            indexVal = 3;

            var rowCount = 1;
            foreach (var item in dataResult)
            {
                var row = sheet.CreateRow(indexVal);
                row.CreateCell(0).SetCellValue(rowCount);
                row.CreateCell(1).SetCellValue(item.DeptName);
                row.CreateCell(2).SetCellValue(item.UserName);
                row.CreateCell(3).SetCellValue(item.DutyName);
                row.CreateCell(4).SetCellValue(item.PostionSalary);
                row.CreateCell(5).SetCellValue(item.PerformanceNum);
                row.CreateCell(6).SetCellValue(item.GroupPlanNum);
                row.CreateCell(7).SetCellValue(item.PersonPlanNum);
                row.CreateCell(8).SetCellValue(item.ActualRatio);
                row.CreateCell(9).SetCellValue(item.KPINumYear);
                row.CreateCell(10).SetCellValue(item.KPINumMonthTotal);
                row.CreateCell(11).SetCellValue(item.KPINumMonth);
                row.CreateCell(12).SetCellValue(item.ShouldWages);
                row.CreateCell(13).SetCellValue(item.Memo);

                row.GetCell(0).CellStyle = tableBorderStyle;
                row.GetCell(1).CellStyle = tableBorderStyle;
                row.GetCell(2).CellStyle = tableBorderStyle;
                row.GetCell(3).CellStyle = tableBorderStyle;
                row.GetCell(4).CellStyle = tableBorderStyle;
                row.GetCell(5).CellStyle = tableBorderStyle;
                row.GetCell(6).CellStyle = tableBorderStyle;
                row.GetCell(7).CellStyle = tableBorderStyle;
                row.GetCell(8).CellStyle = tableBorderStyle;
                row.GetCell(9).CellStyle = tableBorderStyle;
                row.GetCell(10).CellStyle = tableBorderStyle;
                row.GetCell(11).CellStyle = tableBorderStyle;
                row.GetCell(12).CellStyle = tableBorderStyle;
                row.GetCell(13).CellStyle = tableBorderStyle;
                indexVal += 1;
                rowCount += 1;
            }



            for (int i = 0; i < 13; i++)
            {
                sheet.AutoSizeColumn(i);
            }

            // 写入到客户端  
            var ms = new System.IO.MemoryStream();
            book.Write(ms);
            var fileName = string.Format("{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"));
            return File(ms.ToArray(), "application/ms-excel", fileName);
        }

        [AjaxOnly]
        public JsonResult GetUserInfoByUserName(string userName)
        {
            var pageModel = bizTwo.GetGX_EmployeeInfoByUserName(userName);
            return Json(new { result = true, message = "添加成功", userInfo = pageModel }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GX_FileImportSalesManKPI()
        {
            try
            {
                var pageYear = Request.Form["ShearchYeartxt"];
                var pageMonth = Request.Form["ShearchMonthtxt"];
                //保存文件
                var fileList = FileUpload(this.Request);
                if (fileList != null && fileList.Count > 0)
                {
                    IWorkbook workbook = null;
                    FileStream fs = null;
                    ISheet sheet = null;

                    var model = new GX_SalesManKPIModels();
                    model.ModelList = new List<Repository.Models.GX_SalesManKPI>();
                    DataTable data = new DataTable();
                    int startRow = 0;


                    string filepath = ConfigurationManager.AppSettings["FilePath"].ToString();
                    var filename = Path.Combine(string.Format("{0}{1}", Request.PhysicalApplicationPath, filepath), fileList.FirstOrDefault().Key);
                    //读取文件
                    fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
                    if (filename.IndexOf(".xlsx") > 0)
                    {
                        workbook = new XSSFWorkbook(fs);
                    }
                    else if (filename.IndexOf(".xls") > 0)
                    {
                        workbook = new HSSFWorkbook(fs);
                    }
                    sheet = workbook.GetSheetAt(0);
                    if (sheet != null)
                    {
                        IRow firstRow = sheet.GetRow(3);
                        int cellCount = firstRow.LastCellNum;

                        startRow = sheet.FirstRowNum + 3;
                        //最后一列的标号
                        int rowCount = sheet.LastRowNum;

                        for (int i = startRow; i <= rowCount; ++i)
                        {
                            IRow row = sheet.GetRow(i);
                            if (row == null) continue; //没有数据的行默认是null　　
                            if (!IsInt(Convert.ToString(row.GetCell(0))) && i > 4)
                            {
                                break;
                            }
                            var tempModel = new Repository.Models.GX_SalesManKPI();
                            tempModel.OperatorYear = pageYear;
                            tempModel.OperatorMonth = pageMonth;
                            tempModel.DeptName = Convert.ToString(row.GetCell(1));
                            tempModel.UserName = Convert.ToString(row.GetCell(2));
                            tempModel.DutyName = Convert.ToString(row.GetCell(3));
                            tempModel.PostionSalary = Convert.ToString(row.GetCell(4));
                            tempModel.PerformanceNum = Convert.ToString(row.GetCell(5));
                            tempModel.GroupPlanNum = Convert.ToString(row.GetCell(6));
                            tempModel.PersonPlanNum = Convert.ToString(row.GetCell(7));
                            tempModel.ActualRatio = Convert.ToString(row.GetCell(8));
                            tempModel.KPINumYear = Convert.ToString(row.GetCell(9));
                            tempModel.KPINumMonthTotal = Convert.ToString(row.GetCell(10));
                            tempModel.KPINumMonth = Convert.ToString(row.GetCell(11));
                            tempModel.ShouldWages = Convert.ToString(row.GetCell(12));
                            tempModel.Memo = Convert.ToString(row.GetCell(13));
                            model.ModelList.Add(tempModel);
                        }
                        bizTwo.SaveUploadExcel(model.ModelList);
                    }
                }

                TempData["message"] = "上传文件成功~！";
                return RedirectToAction("GX_SalesManKPI", "KPIShow");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

        #region 前台销售人员工资明细

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GX_EmployeeSalaryDetailInfo()
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_EmployeeSalaryDetailInfo = "active";
            ViewBag.SystemName = "工效管理系统";
            var model = new GX_EmployeeSalaryDetailInfoModels() { SearchModel = new Repository.Models.GX_EmployeeSalaryDetailInfo() };
            model.SearchModel.OperatorYear = DateTime.Now.Year;
            model.SearchModel.OperatorMonth = DateTime.Now.Month;
            model.CurrentOperatorName = this.KPIUserInfoModel.PageModel.UserName;
            model.SearchModel.CompanySEQ = this.KPIUserInfoModel.PageModel.CompanySEQ;
            model.SearchModel.CompanyName = this.KPIUserInfoModel.PageModel.CompanyName;
            model.ModelList = bizThree.GX_EmployeeSalaryDetailInfoSearch(model.SearchModel);
            if ((KPIUserInfoModel.PageModel.AuthNum & 1) == 1 ||
                (KPIUserInfoModel.PageModel.AuthNum & 12097152) == 12097152 ||
                (KPIUserInfoModel.PageModel.AuthNum & 1048576) == 1048576 ||
                (KPIUserInfoModel.PageModel.AuthNum & 2048) == 2048)
            {
                model.CompanySource = new SelectList(bizUserInfoOther.GetOtherUserCompany(), "Key", "Value");
            }
            else
            {
                model.CompanySource = new SelectList(bizUserInfoOther.GetOtherUserCompanyByName(model.SearchModel.CompanyName), "Key", "Value");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult GX_EmployeeSalaryDetailInfo(GX_EmployeeSalaryDetailInfoModels model)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_EmployeeSalaryDetailInfo = "active";
            ViewBag.SystemName = "工效管理系统";
            model.ModelList = bizThree.GX_EmployeeSalaryDetailInfoSearch(model.SearchModel);
            if ((KPIUserInfoModel.PageModel.AuthNum & 1) == 1 ||
               (KPIUserInfoModel.PageModel.AuthNum & 12097152) == 12097152 ||
               (KPIUserInfoModel.PageModel.AuthNum & 1048576) == 1048576 ||
               (KPIUserInfoModel.PageModel.AuthNum & 2048) == 2048)
            {
                model.CompanySource = new SelectList(bizUserInfoOther.GetOtherUserCompany(), "Key", "Value");
            }
            else
            {
                model.CompanySource = new SelectList(bizUserInfoOther.GetOtherUserCompanyByName(model.SearchModel.CompanyName), "Key", "Value");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult GX_EmployeeSalaryDetailInfoSave(GX_EmployeeSalaryDetailInfoModels model)
        {
            var result = bizThree.GX_EmployeeSalaryDetailInfoInsert(model.SearchModel);
            return RedirectToAction("GX_EmployeeSalaryDetailInfo", new { id = result ? "添加成功~！" : "添加失败~！" });
        }

        [AjaxOnly]
        public JsonResult GX_EmployeeSalaryDetailInfoDelete(int seq)
        {
            var message = string.Empty;
            var result = bizThree.GX_EmployeeSalaryDetailInfoDelete(seq, ref message);
            return Json(new { result = result, message = message });
        }

        [AjaxOnly]
        public JsonResult GX_EmployeeSalaryDetailInfoEdit()
        {
            var tempArray = Request.Form[0];
            var editObjArray = new List<string>();
            foreach (var item in tempArray.Split(','))
            {
                editObjArray.Add(item);
            }
            var reuslt = bizTwo.GX_EmployeeSalaryDetailInfoEdit(editObjArray);
            return Json(reuslt, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GX_FileImportGX_EmployeeSalaryDetailInfo()
        {
            try
            {
                var pageYear = Request.Form["ShearchYeartxt"];
                var pageMonth = Request.Form["ShearchMonthtxt"];
                var pageCompanySEQ = Request.Form["ShearchCompanySEQ"];
                var pageCompanyName = Request.Form["ShearchCompanyName"];
                //保存文件
                var fileList = FileUpload(this.Request);
                if (fileList != null && fileList.Count > 0)
                {
                    IWorkbook workbook = null;
                    FileStream fs = null;
                    ISheet sheet = null;

                    var model = new GX_EmployeeSalaryDetailInfoModels();
                    model.ModelList = new List<Repository.Models.GX_EmployeeSalaryDetailInfo>();
                    DataTable data = new DataTable();
                    int startRow = 0;


                    string filepath = ConfigurationManager.AppSettings["FilePath"].ToString();
                    var filename = Path.Combine(string.Format("{0}{1}", Request.PhysicalApplicationPath, filepath), fileList.FirstOrDefault().Key);
                    //读取文件
                    fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
                    if (filename.IndexOf(".xlsx") > 0)
                    {
                        workbook = new XSSFWorkbook(fs);
                    }
                    else if (filename.IndexOf(".xls") > 0)
                    {
                        workbook = new HSSFWorkbook(fs);
                    }
                    sheet = workbook.GetSheetAt(0);
                    if (sheet != null)
                    {
                        IRow firstRow = sheet.GetRow(3);
                        int cellCount = firstRow.LastCellNum;

                        startRow = sheet.FirstRowNum + 3;
                        //最后一列的标号
                        int rowCount = sheet.LastRowNum;

                        for (int i = startRow; i <= rowCount; ++i)
                        {
                            IRow row = sheet.GetRow(i);
                            if (row == null) continue; //没有数据的行默认是null　　
                            if (!IsInt(Convert.ToString(row.GetCell(0))) && i > 4)
                            {
                                break;
                            }
                            var tempModel = new Repository.Models.GX_EmployeeSalaryDetailInfo();
                            tempModel.OperatorYear = !string.IsNullOrEmpty(pageYear) == true ? Convert.ToInt32(pageYear) : 0;
                            tempModel.OperatorMonth = !string.IsNullOrEmpty(pageMonth) == true ? Convert.ToInt32(pageMonth) : 0;
                            tempModel.DeptName = Convert.ToString(row.GetCell(1));
                            tempModel.UserName = Convert.ToString(row.GetCell(2));
                            tempModel.PostionName = Convert.ToString(row.GetCell(3));
                            tempModel.SalaryCardNo = Convert.ToString(row.GetCell(4));
                            if (!string.IsNullOrEmpty(row.GetCell(5).ToString()))
                            {
                                tempModel.ContractSignNum = Convert.ToInt32(row.GetCell(5).ToString());
                            }
                            tempModel.ContractRatio = Convert.ToString(row.GetCell(6));
                            tempModel.GrossProfit = Convert.ToString(row.GetCell(7));
                            tempModel.ContractRatio2 = Convert.ToString(row.GetCell(8));
                            tempModel.ComprehensiveRatio = Convert.ToString(row.GetCell(9));
                            tempModel.PostionSalary = Convert.ToString(row.GetCell(10));
                            tempModel.SalaryStanderAverage = Convert.ToString(row.GetCell(11));
                            tempModel.RankingNum = Convert.ToString(row.GetCell(12));
                            tempModel.MoneyInCome = Convert.ToString(row.GetCell(13));
                            tempModel.PublicityNum = Convert.ToString(row.GetCell(14));
                            tempModel.SeasonSalary = Convert.ToString(row.GetCell(15));
                            tempModel.SalaryOther = Convert.ToString(row.GetCell(16));
                            tempModel.ShouldWages = Convert.ToString(row.GetCell(17));
                            if (!string.IsNullOrEmpty(pageCompanySEQ))
                            {
                                tempModel.CompanySEQ = Convert.ToInt32(pageCompanySEQ);
                            }
                            tempModel.CompanyName = pageCompanyName;
                            model.ModelList.Add(tempModel);
                        }
                        bizTwo.SaveUploadExcel(model.ModelList);
                    }
                }

                TempData["message"] = "上传文件成功~！";
                return RedirectToAction("GX_EmployeeSalaryDetailInfo", "KPIShow");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 导出Excel文件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult ExcelExportGX_EmployeeSalaryDetailInfo(GX_EmployeeSalaryDetailInfoModels model)
        {
            Session["ExcelSearchModelGX_EmployeeSalaryDetailInfoModels"] = model;
            return Json("OK", JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 生成下载文件
        /// </summary>
        /// <returns></returns>
        public FileContentResult ExcelDownLoadGX_EmployeeSalaryDetailInfo()
        {
            var searchCondition = Session["ExcelSearchModelGX_EmployeeSalaryDetailInfoModels"] as GX_EmployeeSalaryDetailInfoModels;
            var dataResult = bizTwo.GX_EmployeeSalaryDetailInfoISearch(searchCondition.SearchModel);
            var book = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = book.CreateSheet("ExcelExport");

            var tableBorderStyle = book.CreateCellStyle();
            tableBorderStyle.BorderBottom = BorderStyle.Thin;
            tableBorderStyle.BorderLeft = BorderStyle.Thin;
            tableBorderStyle.BorderRight = BorderStyle.Thin;
            tableBorderStyle.BorderTop = BorderStyle.Thin;
            var tableContentFont = book.CreateFont();
            tableContentFont.FontName = "微软雅黑";
            tableContentFont.FontHeightInPoints = 12;
            tableBorderStyle.SetFont(tableContentFont);

            var tableTitleStyle = book.CreateCellStyle();
            tableTitleStyle.BorderBottom = BorderStyle.Thin;
            tableTitleStyle.BorderLeft = BorderStyle.Thin;
            tableTitleStyle.BorderRight = BorderStyle.Thin;
            tableTitleStyle.BorderTop = BorderStyle.Thin;
            var tableTitleFont = book.CreateFont();
            tableTitleFont.FontName = "微软雅黑";
            tableTitleFont.FontHeightInPoints = 12;
            tableTitleFont.Boldweight = short.MaxValue;
            tableTitleStyle.SetFont(tableTitleFont);

            ICellStyle style = book.CreateCellStyle();

            //设置单元格的样式：水平对齐居中
            style.Alignment = HorizontalAlignment.Center;
            //新建一个字体样式对象
            IFont font = book.CreateFont();
            //设置字体加粗样式
            font.Boldweight = short.MaxValue;
            font.FontName = "微软雅黑";
            font.FontHeightInPoints = 16;
            //使用SetFont方法将字体样式添加到单元格样式中 
            style.SetFont(font);
            //将新的样式赋给单元格
            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 13));

            var row0 = sheet.CreateRow(0);
            row0.CreateCell(0).SetCellValue(string.Format("{0}年沈铁国旅集团前台销售人员绩效（{1}月份工资用）", searchCondition.SearchModel.OperatorYear, searchCondition.SearchModel.OperatorMonth));
            row0.GetCell(0).CellStyle = style;


            var indexVal = 0;

            var rowTitle = sheet.CreateRow(indexVal + 2);
            rowTitle.CreateCell(0).SetCellValue("序号");
            rowTitle.CreateCell(1).SetCellValue("单位");
            rowTitle.CreateCell(2).SetCellValue("姓名");
            rowTitle.CreateCell(3).SetCellValue("岗位");
            rowTitle.CreateCell(4).SetCellValue("工资卡号");
            rowTitle.CreateCell(5).SetCellValue("合同签约");
            rowTitle.CreateCell(6).SetCellValue("占比1");
            rowTitle.CreateCell(7).SetCellValue("毛利润");
            rowTitle.CreateCell(8).SetCellValue("占比2");
            rowTitle.CreateCell(9).SetCellValue("综合占比");
            rowTitle.CreateCell(10).SetCellValue("岗位工资");
            rowTitle.CreateCell(11).SetCellValue("人均绩效标准");
            rowTitle.CreateCell(12).SetCellValue("排名联挂");
            rowTitle.CreateCell(13).SetCellValue("绩效联挂");
            rowTitle.CreateCell(14).SetCellValue("绩效工资");
            rowTitle.CreateCell(15).SetCellValue("稿费");
            rowTitle.CreateCell(16).SetCellValue("其他奖");
            rowTitle.CreateCell(17).SetCellValue("应付工资");
            rowTitle.GetCell(0).CellStyle = tableTitleStyle;
            rowTitle.GetCell(1).CellStyle = tableTitleStyle;
            rowTitle.GetCell(2).CellStyle = tableTitleStyle;
            rowTitle.GetCell(3).CellStyle = tableTitleStyle;
            rowTitle.GetCell(4).CellStyle = tableTitleStyle;
            rowTitle.GetCell(5).CellStyle = tableTitleStyle;
            rowTitle.GetCell(6).CellStyle = tableTitleStyle;
            rowTitle.GetCell(7).CellStyle = tableTitleStyle;
            rowTitle.GetCell(8).CellStyle = tableTitleStyle;
            rowTitle.GetCell(9).CellStyle = tableTitleStyle;
            rowTitle.GetCell(10).CellStyle = tableTitleStyle;
            rowTitle.GetCell(11).CellStyle = tableTitleStyle;
            rowTitle.GetCell(12).CellStyle = tableTitleStyle;
            rowTitle.GetCell(13).CellStyle = tableTitleStyle;
            rowTitle.GetCell(14).CellStyle = tableTitleStyle;
            rowTitle.GetCell(15).CellStyle = tableTitleStyle;
            rowTitle.GetCell(16).CellStyle = tableTitleStyle;
            rowTitle.GetCell(17).CellStyle = tableTitleStyle;

            indexVal = 3;
            var ContractSignNumTotal = 0;
            var GrossProfitTotal = 0m;
            var SalaryStanderAverageTotal = 0m;
            var RankingNumTotal = 0m;
            var MoneyInComeTotal = 0m;
            var PublicityNumTotal = 0m;
            var SeasonSalaryTotal = 0m;
            var SalaryOtherTotal = 0m;
            var ShouldWagesTotal = 0m;
            var rowCount = 1;
            foreach (var item in dataResult)
            {
                var row = sheet.CreateRow(indexVal);
                row.CreateCell(0).SetCellValue(rowCount);
                row.CreateCell(1).SetCellValue(item.DeptName);
                row.CreateCell(2).SetCellValue(item.UserName);
                row.CreateCell(3).SetCellValue(item.PostionName);
                row.CreateCell(4).SetCellValue(item.SalaryCardNo);
                ContractSignNumTotal += item.ContractSignNum ?? 0;
                row.CreateCell(5).SetCellValue(item.ContractSignNum != null ? Convert.ToString(item.ContractSignNum) : string.Empty);
                row.CreateCell(6).SetCellValue(item.ContractRatio);
                if (!string.IsNullOrEmpty(item.GrossProfit))
                {
                    GrossProfitTotal += Convert.ToDecimal(item.GrossProfit);
                }
                row.CreateCell(7).SetCellValue(item.GrossProfit);
                row.CreateCell(8).SetCellValue(item.ContractRatio2);
                row.CreateCell(9).SetCellValue(item.ComprehensiveRatio);
                row.CreateCell(10).SetCellValue(item.PostionSalary);
                if (!string.IsNullOrEmpty(item.SalaryStanderAverage))
                {
                    SalaryStanderAverageTotal += Convert.ToDecimal(item.SalaryStanderAverage);
                }
                row.CreateCell(11).SetCellValue(item.SalaryStanderAverage);
                if (!string.IsNullOrEmpty(item.RankingNum))
                {
                    RankingNumTotal += Convert.ToDecimal(item.RankingNum);
                }
                row.CreateCell(12).SetCellValue(item.RankingNum);
                if (!string.IsNullOrEmpty(item.MoneyInCome))
                {
                    MoneyInComeTotal += Convert.ToDecimal(item.MoneyInCome);
                }
                row.CreateCell(13).SetCellValue(item.MoneyInCome);
                if (!string.IsNullOrEmpty(item.PublicityNum))
                {
                    PublicityNumTotal += Convert.ToDecimal(item.PublicityNum);
                }
                row.CreateCell(14).SetCellValue(item.PublicityNum);
                if (!string.IsNullOrEmpty(item.SeasonSalary))
                {
                    SeasonSalaryTotal += Convert.ToDecimal(item.SeasonSalary);
                }
                row.CreateCell(15).SetCellValue(item.SeasonSalary);
                if (!string.IsNullOrEmpty(item.SalaryOther))
                {
                    SalaryOtherTotal += Convert.ToDecimal(item.SalaryOther);
                }
                row.CreateCell(16).SetCellValue(item.SalaryOther);
                if (!string.IsNullOrEmpty(item.ShouldWages))
                {
                    ShouldWagesTotal += Convert.ToDecimal(item.ShouldWages);
                }
                row.CreateCell(17).SetCellValue(item.ShouldWages);

                row.GetCell(0).CellStyle = tableBorderStyle;
                row.GetCell(1).CellStyle = tableBorderStyle;
                row.GetCell(2).CellStyle = tableBorderStyle;
                row.GetCell(3).CellStyle = tableBorderStyle;
                row.GetCell(4).CellStyle = tableBorderStyle;
                row.GetCell(5).CellStyle = tableBorderStyle;
                row.GetCell(6).CellStyle = tableBorderStyle;
                row.GetCell(7).CellStyle = tableBorderStyle;
                row.GetCell(8).CellStyle = tableBorderStyle;
                row.GetCell(9).CellStyle = tableBorderStyle;
                row.GetCell(10).CellStyle = tableBorderStyle;
                row.GetCell(11).CellStyle = tableBorderStyle;
                row.GetCell(12).CellStyle = tableBorderStyle;
                row.GetCell(13).CellStyle = tableBorderStyle;
                row.GetCell(14).CellStyle = tableBorderStyle;
                row.GetCell(15).CellStyle = tableBorderStyle;
                row.GetCell(16).CellStyle = tableBorderStyle;
                row.GetCell(17).CellStyle = tableBorderStyle;
                indexVal += 1;
                rowCount += 1;
            }

            var tempRow = sheet.CreateRow(indexVal);
            tempRow.CreateCell(0).SetCellValue("合计");
            tempRow.CreateCell(5).SetCellValue(ContractSignNumTotal);
            tempRow.CreateCell(7).SetCellValue(GrossProfitTotal.ToString("N2"));
            tempRow.CreateCell(11).SetCellValue(SalaryStanderAverageTotal.ToString("N2"));
            tempRow.CreateCell(12).SetCellValue(RankingNumTotal.ToString("N2"));
            tempRow.CreateCell(13).SetCellValue(MoneyInComeTotal.ToString("N2"));
            tempRow.CreateCell(14).SetCellValue(PublicityNumTotal.ToString("N2"));
            tempRow.CreateCell(15).SetCellValue(SeasonSalaryTotal.ToString("N2"));
            tempRow.CreateCell(16).SetCellValue(SalaryOtherTotal.ToString("N2"));
            tempRow.CreateCell(17).SetCellValue(ShouldWagesTotal.ToString("N2"));
            tempRow.GetCell(0).CellStyle = tableBorderStyle;
            tempRow.GetCell(5).CellStyle = tableBorderStyle;
            tempRow.GetCell(7).CellStyle = tableBorderStyle;
            tempRow.GetCell(11).CellStyle = tableBorderStyle;
            tempRow.GetCell(12).CellStyle = tableBorderStyle;
            tempRow.GetCell(13).CellStyle = tableBorderStyle;
            tempRow.GetCell(14).CellStyle = tableBorderStyle;
            tempRow.GetCell(15).CellStyle = tableBorderStyle;
            tempRow.GetCell(16).CellStyle = tableBorderStyle;
            tempRow.GetCell(17).CellStyle = tableBorderStyle;

            for (int i = 0; i < 18; i++)
            {
                sheet.AutoSizeColumn(i);
            }

            // 写入到客户端  
            var ms = new System.IO.MemoryStream();
            book.Write(ms);
            var fileName = string.Format("{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"));
            return File(ms.ToArray(), "application/ms-excel", fileName);
        }

        [AjaxOnly]
        public JsonResult GX_EmployeeSalaryDetailInfoReportDel(int seq)
        {
            var message = string.Empty;
            var checkResult = bizTwo.CheckGX_SalesmanSalaryApprove(seq, ref message);
            if (!checkResult)
            {
                var result = bizTwo.GX_EmployeeSalaryDetailInfoReportDelete(seq, ref message);
                return Json(new { result = result, message = message });
            }
            else
            {
                return Json(new { result = !checkResult, message = message });
            }
        }

        [AjaxOnly]
        public JsonResult GX_EmployeeSalaryDetailInfoReportSave()
        {
            var year = Request.Form["ShearchYeartxt"];
            var month = Request.Form["ShearchMonthtxt"];
            var operatorName = Request.Form["ReportOperatorName"];
            var pageCompanySEQ = Request.Form["ReportCompanySEQ"];
            var pageCompanyName = Request.Form["ReportCompanyName"];
            var message = string.Empty;
            var result = false;
            if (!string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month))
            {
                result = bizTwo.GX_EmployeeSalaryDetailInfoReportInsert(year, month, operatorName, pageCompanySEQ, pageCompanyName, ref message);
            }
            else
            {
                message = "年份月份不能为空";
            }

            return Json(new { Result = result, Message = message }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult GX_EmployeeSalaryDetailInfoReport()
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_EmployeeSalaryDetailInfo = "active";
            ViewBag.SystemName = "工效管理系统";
            var model = new GX_EmployeeSalaryDetailInfoReportModels();
            var year = DateTime.Now.Year.ToString();
            var month = DateTime.Now.Month.ToString();
            model.SearchModel = new Repository.Models.GX_SalesmanSalaryDetailInfoReport();
            model.SearchModel.OperatorYear = year;
            model.SearchModel.OperatorMonth = month;
            model.SearchModel.CompanySEQ = this.KPIUserInfoModel.PageModel.CompanySEQ;
            model.SearchModel.CompanyName = this.KPIUserInfoModel.PageModel.CompanyName;
            model.ModelList = bizTwo.GX_EmployeeSalaryDetailInfoByYear(model);
            if ((KPIUserInfoModel.PageModel.AuthNum & 1) == 1 ||
                (KPIUserInfoModel.PageModel.AuthNum & 12097152) == 12097152 ||
                (KPIUserInfoModel.PageModel.AuthNum & 1048576) == 1048576 ||
                (KPIUserInfoModel.PageModel.AuthNum & 2048) == 2048)
            {
                model.CompanySource = new SelectList(bizUserInfoOther.GetOtherUserCompany(), "Key", "Value");
            }
            else
            {
                model.CompanySource = new SelectList(bizUserInfoOther.GetOtherUserCompanyByName(model.SearchModel.CompanyName), "Key", "Value");
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult GX_EmployeeSalaryDetailInfoReport(GX_EmployeeSalaryDetailInfoReportModels model)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_EmployeeSalaryDetailInfo = "active";
            ViewBag.SystemName = "工效管理系统";

            if ((KPIUserInfoModel.PageModel.AuthNum & 1) == 1 ||
                (KPIUserInfoModel.PageModel.AuthNum & 12097152) == 12097152 ||
                (KPIUserInfoModel.PageModel.AuthNum & 1048576) == 1048576 ||
                (KPIUserInfoModel.PageModel.AuthNum & 2048) == 2048)
            {
                model.CompanySource = new SelectList(bizUserInfoOther.GetOtherUserCompany(), "Key", "Value");
            }
            else
            {
                model.CompanySource = new SelectList(bizUserInfoOther.GetOtherUserCompanyByName(model.SearchModel.CompanyName), "Key", "Value");
            }
            model.ModelList = bizTwo.GX_EmployeeSalaryDetailInfoByYear(model);
            return View(model);
        }

        [HttpGet]
        public ActionResult GX_EmployeeSalaryDetailInfoReportDetail(string id)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_EmployeeSalaryDetailInfo = "active";
            ViewBag.SystemName = "工效管理系统";
            var pageModel = new GX_EmployeeSalaryDetailInfoModels();
            pageModel.AuthNum = this.KPIUserInfoModel.PageModel.AuthNum;
            pageModel.ModelList = bizTwo.GX_EmployeeSalaryDetailInfoReportSearch(id, pageModel.AuthNum ?? 0);
            pageModel.ReportDetailSEQ = Convert.ToInt32(id);
            return View(pageModel);
        }

        [AjaxOnly]
        public JsonResult GX_EmployeeSalaryDetailInfoReportApprove(int seq, string type)
        {
            var userName = this.KPIUserInfoModel.PageModel.UserName;
            var message = "审批完成";
            var result = bizTwo.GX_EmployeeSalaryDetailInfoReportApprove(seq, type, "通过", "完成", userName, ref message);
            return Json(new { result = result, message = message }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 公司利润完成情况统计

        [HttpGet]
        public ActionResult GX_GrossProfitCompany()
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_GrossProfitCompany = "active";
            ViewBag.SystemName = "工效管理系统";
            var model = new GX_GrossProfitCompanyModels() { SearchModel = new Repository.Models.GX_GrossProfitCompany() };
            model.SearchModel.OperatorYear = DateTime.Now.Year.ToString();
            model.SearchModel.OperatorMonth = DateTime.Now.Month.ToString();
            model.ModelList = bizFour.GX_GrossProfitCompanySearch(model.SearchModel);
            return View(model);
        }

        [HttpPost]
        public ActionResult GX_GrossProfitCompany(GX_GrossProfitCompanyModels model)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.GX_GrossProfitCompany = "active";
            ViewBag.SystemName = "工效管理系统";
            model.ModelList = bizFour.GX_GrossProfitCompanySearch(model.SearchModel);
            return View();
        }

        [HttpPost]
        public ActionResult GX_GrossProfitCompanySave(GX_GrossProfitCompanyModels model)
        {
            var result = bizFour.GX_GrossProfitCompanyInsert(model.SearchModel);
            return RedirectToAction("GX_GrossProfitCompany", new { id = result ? "添加成功~！" : "添加失败~！" });
        }

        [AjaxOnly]
        public JsonResult GX_GrossProfitCompanyDelete(int seq)
        {
            var result = bizFour.GX_GrossProfitCompanyDelete(seq);
            return Json(new { result = result, message = result ? "删除成功" : "删除失败" });
        }

        #endregion

    }
}
