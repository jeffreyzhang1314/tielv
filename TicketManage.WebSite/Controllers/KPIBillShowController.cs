﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.KPIManage;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.KPIManage;
using TicketManage.Entity.CommonManage;

namespace TicketManage.WebSite.Controllers
{
    /// <summary>
    /// 指标台帐功能
    /// </summary>
    [LoginCheckKPI]
    public class KPIBillShowController : BaseController
    {
        #region 全年计划录入

        private PlanInfoYearBiz PlanInfoBiz = new PlanInfoYearBiz();

        private void PlanInfoYearListInit(PlanInfoYearListPageModel model)
        {
            ViewBag.KPIBillShowNav = "active open";
            ViewBag.PlanInfoYearList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
        }

        [HttpGet]
        public ActionResult PlanInfoYearList()
        {
            var model = new PlanInfoYearListPageModel() { PageSize = 10, CurrentPage = 1 };
            PlanInfoYearListInit(model);
            model.ModelList = PlanInfoBiz.SearchPlanInfoYearByCondition(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult PlanInfoYearList(PlanInfoYearListPageModel model)
        {
            PlanInfoYearListInit(model);
            model.ModelList = PlanInfoBiz.SearchPlanInfoYearByCondition(model);
            return View(model);
        }

        [HttpGet]
        public PartialViewResult ShowPlanInfoYearAdd()
        {
            var model = new PlanInfoYearListPageModel();
            return PartialView("PartialPlanInfoYearEdit", model);
        }

        [HttpGet]
        public PartialViewResult ShowPlanInfoYearEdit(int id)
        {
            var model = new PlanInfoYearListPageModel();
            model.PageModel = PlanInfoBiz.GetPlanInfoYearBySEQ(id);
            return PartialView("PartialPlanInfoYearEdit", model);
        }

        [AjaxOnly]
        public JsonResult PlanInfoYearEdit(PlanInfoYearListPageModel model)
        {
            var result = PlanInfoBiz.EditPlanInfoYear(model);
            return Json(new { result = result, message = model.Message });
        }

        public JsonResult PlanInfoYearDel(int id)
        {
            var result = PlanInfoBiz.DelPlanInfoYear(id);
            return Json(new { Result = result, message = "删除成功" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 考核利润录入

        private PlanprofitInfoYearBiz PlanProfitInfoBiz = new PlanprofitInfoYearBiz();

        private void PlanprofitInfoYearListInit(PlanprofitInfoYearPageModel model)
        {
            ViewBag.KPIBillShowNav = "active open";
            ViewBag.PlanprofitInfoYearList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
        }

        /// <summary>
        /// 考核利润列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PlanprofitInfoYearList()
        {
            var model = new PlanprofitInfoYearPageModel() { PageSize = 10, CurrentPage = 1 };
            PlanprofitInfoYearListInit(model);
            model.ModelList = PlanProfitInfoBiz.SearchPlanprofitInfoYearByCondition(model);
            return View(model);
        }

        /// <summary>
        /// 考核利润列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PlanprofitInfoYearList(PlanprofitInfoYearPageModel model)
        {
            PlanprofitInfoYearListInit(model);
            model.ModelList = PlanProfitInfoBiz.SearchPlanprofitInfoYearByCondition(model);
            return View(model);
        }

        [HttpGet]
        public PartialViewResult ShowPlanprofitInfoYearAdd()
        {
            var model = new PlanprofitInfoYearPageModel();
            return PartialView("PartialPlanprofitInfoYearEdit", model);
        }

        [HttpGet]
        public PartialViewResult ShowPlanprofitInfoYearEdit(int id)
        {
            var model = new PlanprofitInfoYearPageModel();
            model.PageModel = PlanProfitInfoBiz.GetPlanprofitInfoYearBySEQ(id);
            return PartialView("PartialPlanprofitInfoYearEdit", model);
        }

        [AjaxOnly]
        public JsonResult PlanProfitInfoEdit(PlanprofitInfoYearPageModel model)
        {
            var result = PlanProfitInfoBiz.EditPlanprofitInfoYear(model);
            return Json(new { result = result, message = model.Message });
        }

        public JsonResult PlanProfitInfoDel(int id)
        {
            var result = PlanProfitInfoBiz.DelPlanprofitInfoYear(id);
            return Json(new { Result = result, message = "删除成功" }, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region 个人计划录入

        private PlanPersonInfoBiz PlanPersonBiz = new PlanPersonInfoBiz();

        private void PlanPersonInfoListInit(PlanPersonInfoPageModel model)
        {
            ViewBag.KPIBillShowNav = "active open";
            ViewBag.PlanPersonInfoList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
            model.YearSource = new SelectList(PlanPersonBiz.GetSystemCodeListByType("CommonYear"), "CodeNo", "CodeName");
            model.DutySource = new SelectList(PlanPersonBiz.GetDutyInfo(), "seq", "DutyName");
        }

        /// <summary>
        /// 个人计划列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PlanPersonInfoList()
        {
            var model = new PlanPersonInfoPageModel() { PageSize = 10, CurrentPage = 1 };
            PlanPersonInfoListInit(model);
            model.ModelList = PlanPersonBiz.SearchPlanPersonInfoByCondition(model);
            model.PageModel = new Repository.Models.GX_PlanPersonInfo()
            {
                OperatorYear = Convert.ToString(DateTime.Now.Year)
            };
            return View(model);
        }

        /// <summary>
        /// 个人计划列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PlanPersonInfoList(PlanPersonInfoPageModel model)
        {
            PlanPersonInfoListInit(model);
            model.ModelList = PlanPersonBiz.SearchPlanPersonInfoByCondition(model);
            return View(model);
        }

        [HttpGet]
        public PartialViewResult ShowPlanPersonInfoAdd()
        {
            var model = new PlanPersonInfoPageModel();
            PlanPersonInfoListInit(model);
            model.PageModel = new Repository.Models.GX_PlanPersonInfo()
            {
                UserSEQ = CommonUserInfoModel.PageModel.SEQ,
                UserName = CommonUserInfoModel.PageModel.UserName
            };
            return PartialView("PartialPlanPersonInfoEdit", model);
        }

        [HttpGet]
        public PartialViewResult ShowPlanPersonInfoEdit(int id)
        {
            var model = new PlanPersonInfoPageModel();
            PlanPersonInfoListInit(model);
            model.PageModel = PlanPersonBiz.GetPlanPersonInfoBySEQ(id);
            return PartialView("PartialPlanPersonInfoEdit", model);
        }

        [AjaxOnly]
        public JsonResult PlanPersonInfoEdit(PlanPersonInfoPageModel model)
        {
            var result = PlanPersonBiz.EditPlanPersonInfo(model);
            return Json(new { result = result, message = model.Message });
        }


        public JsonResult PlanPersonInfoDel(int id)
        {
            var result = PlanPersonBiz.DelPlanPersonInfo(id);
            return Json(new { Result = result, message = "删除成功" }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult PlanPersonInfoApprove(int id, int approveState, int approveResult)
        {
            var result = false;
            var message = "操作失败";
            if (id > 0 && approveState > 0 && approveState < 5 && approveResult > 0 && approveResult < 3)
            {
                var nodeVal = ApproveFlowNode.个人计划一级审批;
                if (approveState == 1)
                {
                    nodeVal = ApproveFlowNode.个人计划一级审批;
                }
                else if (approveState == 2)
                {
                    nodeVal = ApproveFlowNode.个人计划二级审批;
                }
                else if (approveState == 3)
                {
                    nodeVal = ApproveFlowNode.个人计划三级审批;
                }
                else if (approveState == 4)
                {
                    nodeVal = ApproveFlowNode.个人计划四级审批;
                }
                result = PlanPersonBiz.ApprovePlanPerson(id, nodeVal, approveResult);
            }
            return Json(new { Result = result, message = message }, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}
