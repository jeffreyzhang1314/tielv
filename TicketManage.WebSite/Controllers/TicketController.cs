using System;
using System.Web.Mvc;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TicketManage.WebSite.Filters;
using System.Collections.Generic;
using TieckManage.Biz.TicketManage;
using TicketManage.Entity.CommonManage;

namespace TicketManage.WebSite.Controllers
{
    /// <summary>
    /// Description of TicketController.
    /// </summary>
    [LoginCheck]
    public class TicketController : BaseController
    {
        private TicketManageBiz biz = new TicketManageBiz();
        private TicketUnionBiz uBiz = new TicketUnionBiz();
        private UserInfoManageBiz userInfoBiz = new UserInfoManageBiz();

        #region 票务业务相关

        /// <summary>
        /// 初始化查询区域需要数据源
        /// </summary>
        /// <param name="pageModel"></param>
        private void InitIndex(TicketIndexPageModel pageModel)
        {
            ViewBag.TicketManageNav = "active open";
            ViewBag.TicketNav = "active";
            var tempCompanyList = new List<KeyValueModel>();
            var tempSalepointList = new List<KeyValueModel>();
            ViewBag.IsAdmin = UserInfoModel.IsAdmin ? "true" : "false";
            if ((UserInfoModel.PageModel.AuthNum & 4) == 4)
            {
                tempCompanyList.AddRange(biz.GetCompanyInfoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ ?? 0));
                if (tempCompanyList != null && tempCompanyList.Count > 0)
                {
                    tempSalepointList.AddRange(biz.GetSalePointInfoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ ?? 0));
                }
                tempSalepointList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
            }
            else if ((UserInfoModel.PageModel.AuthNum & 8) == 8)
            {
                tempCompanyList.AddRange(biz.GetCompanyInfoById(UserInfoModel.PageModel.CompanySEQ ?? 0));
                if (tempCompanyList.Count > 0 && pageModel.CompanySEQ == null)
                {
                    pageModel.CompanySEQ = tempCompanyList[0].Key;
                }
                if (tempCompanyList != null && tempCompanyList.Count > 0)
                {
                    tempSalepointList.AddRange(biz.GetSalePointInfoByCompanySEQ(UserInfoModel.PageModel.CompanySEQ ?? 0));
                }
                tempSalepointList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
            }
            else if ((UserInfoModel.PageModel.AuthNum & 2) == 2)
            {
                tempCompanyList.AddRange(biz.GetCompanyInfoById(UserInfoModel.PageModel.CompanySEQ ?? 0));
                if (tempCompanyList.Count > 0 && pageModel.CompanySEQ == null)
                {
                    pageModel.CompanySEQ = tempCompanyList[0].Key;
                }
                if (tempCompanyList != null && tempCompanyList.Count > 0)
                {
                    tempSalepointList.AddRange(biz.GetSalePointInfoById(UserInfoModel.PageModel.SalePointSEQ ?? 0));
                    if (tempSalepointList.Count > 0 && pageModel.SalePointSEQ == 0)
                    {
                        pageModel.SalePointSEQ = tempSalepointList[0].Key;
                    }
                }
            }
            else if ((UserInfoModel.PageModel.AuthNum & 1) == 1
                || (UserInfoModel.PageModel.AuthNum & 16) == 16
                || (UserInfoModel.PageModel.AuthNum & 32) == 32)
            {
                tempCompanyList.Add(new KeyValueModel { Key = 0, Value = "全部" });
                tempCompanyList.AddRange(biz.GetCompanyInfoByAll());
                tempSalepointList.AddRange(biz.GetSalePointInfoByAll());
                tempSalepointList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
            }

            pageModel.CompanySource = new SelectList(tempCompanyList, "Key", "Value");
            pageModel.SalePointSource = new SelectList(tempSalepointList, "Key", "Value");
            pageModel.TimeCategorySource = new SelectList(TimeCategorySource, "Key", "Value");
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.OperatorAuthNum = UserInfoModel.PageModel.AuthNum;
            pageModel.DivideInto = biz.GetDivideIntoByUserSEQ(UserInfoModel.PageModel.SEQ);
        }

        [HttpGet]
        public ActionResult Index()
        {
            var pageModel = new TicketIndexPageModel() { PageSize = 10, CurrentPage = 1, PageModel = new TicketCustomer() { OperationSEQ = UserInfoModel.PageModel.SEQ } };
            InitIndex(pageModel);
            pageModel.StartDate = DateTime.Now.ToString("yyyy-MM-dd");
            pageModel.EndDate = DateTime.Now.ToString("yyyy-MM-dd");
            pageModel.ModelList = biz.GetTicketByCondition(pageModel);
            Session["TicketIndexModel"] = pageModel;
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult Index(TicketIndexPageModel model)
        {
            InitIndex(model);
            model.ModelList = biz.GetTicketByCondition(model);
            model.CurrentPage = model.CurrentPage;
            Session["TicketIndexModel"] = model;
            return View(model);
        }

        [HttpGet, LoginCheck]
        public ActionResult TicketDetail(int id, string dateTime)
        {
            ViewBag.TicketManageNav = "active open";
            ViewBag.TicketNav = "active";
            if (UserInfoModel.IsAdmin)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            var pageModel = new TicketIndexPageModel();
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            pageModel.SalePointSEQ = id;
            pageModel.OperaotrDate = dateTime;
            pageModel.ModelList = biz.GetTicketDetailByCondition(pageModel);
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult TicketDetail(TicketIndexPageModel pageModel)
        {
            ViewBag.TicketManageNav = "active open";
            ViewBag.TicketNav = "active";
            if (UserInfoModel.IsAdmin)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            pageModel = new TicketIndexPageModel();
            pageModel.PageSize = Convert.ToInt32(Request.Form["PageSize"]);
            pageModel.CurrentPage = Convert.ToInt32(Request.Form["CurrentPage"]);
            pageModel.SalePointSEQ = Convert.ToInt32(Request.Form["SalePointSEQ"]);
            pageModel.OperaotrDate = Request.Form["OperaotrDate"];
            pageModel.ModelList = biz.GetTicketDetailByCondition(pageModel);
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            return View(pageModel);
        }

        [HttpGet]
        public PartialViewResult TicketAdd()
        {
            var pageModel = new TicketIndexPageModel();
            pageModel.PageModel = new TicketCustomer();
            pageModel.PageModel.OperationSEQ = UserInfoModel.PageModel.SEQ;
            var tempUserModel = userInfoBiz.GetUserInfoById(pageModel.PageModel.OperationSEQ ?? 0);
            pageModel.OperatorUserNo = tempUserModel.UserNo;
            pageModel.OperatorUserName = tempUserModel.UserName;
            pageModel.OperaotrDate = DateTime.Now.ToString("yyyy-MM-dd");
            pageModel.DivideInto = biz.GetDivideIntoByUserSEQ(UserInfoModel.PageModel.SEQ);
            pageModel.PageModel.DivideInto = pageModel.DivideInto;
            return PartialView("_PartialTicketAdd", pageModel);
        }

        [HttpPost]
        public ActionResult TicketAdd(TicketIndexPageModel model)
        {
            ViewBag.TicketManageNav = "active open";
            ViewBag.TicketNav = "active";
            biz.DelTicketUser(model.OperatorUserNo, model.OperaotrDate);
            model.PageModel.CreateTime = Convert.ToDateTime(model.OperaotrDate);
            if (
                model.TicketNum1 == 0 && string.IsNullOrEmpty(model.PageModel.StartTicketNo) && string.IsNullOrEmpty(model.PageModel.EndTicketNo)
                && model.TicketNum2 == 0 && string.IsNullOrEmpty(model.PageModel.StartTicketNo2) && string.IsNullOrEmpty(model.PageModel.EndTicketNo2))
            {
                model.PageModel.TicketNum = 0;
            }
            else
            {
                model.PageModel.TicketNum = model.TicketNum1 + model.TicketNum2 + 1;
            }
            var result = biz.AddTicket(model);
            var resultList = FileUpload(Request);
            biz.AddKidTicket(model.PageModel.SEQ, resultList);
            model = Session["TicketIndexModel"] as TicketIndexPageModel;
            model.ModelList = biz.GetTicketByCondition(model);
            return View("Index", model);
        }

        [HttpPost]
        public ActionResult TicketEdit(TicketIndexPageModel model)
        {
            var result = biz.EditTicket(model);
            var resultList = FileUpload(Request);
            model.PageModel.TicketNum = model.TicketNum1 + model.TicketNum2 + 1;
            biz.AddKidTicket(model.PageModel.SEQ, resultList);
            return RedirectToAction("TicketDetail", new { id = model.SalePointSEQ, dateTime = model.OperaotrDate });
        }

        [HttpGet]
        public PartialViewResult TicketEdit(int id, int SalePointSEQ, string OperatorDate)
        {
            var pageModel = new TicketIndexPageModel();
            pageModel.OperaotrDate = OperatorDate;
            pageModel.SalePointSEQ = SalePointSEQ;
            pageModel.PageModel = biz.GetTicketById(id);
            pageModel.PageModel.OperationSEQ = UserInfoModel.PageModel.SEQ;
            var tempUserModel = userInfoBiz.GetUserInfoById(pageModel.PageModel.OperationSEQ ?? 0);
            pageModel.OperatorUserNo = tempUserModel.UserNo;
            pageModel.OperatorUserName = tempUserModel.UserName;
            if (pageModel.PageModel.CreateTime != null)
            {
                pageModel.OperaotrDate = pageModel.PageModel.CreateTime.Value.ToString("yyyy-MM-dd");
            }
            return PartialView("_PartialTicketEdit", pageModel);
        }

        [HttpGet, LoginCheck]
        public ActionResult KidTicketList(int id)
        {
            ViewBag.TicketManageNav = "active open";
            ViewBag.TicketNav = "active";
            if (UserInfoModel.IsAdmin)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            var pageModel = new KidTicketListPageModel();
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            pageModel.TicketCustomerSEQ = id;
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.ModelList = biz.GetKidTicketByTicketSEQ(id, pageModel);
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult KidTicketList(KidTicketListPageModel pageModel)
        {
            ViewBag.TicketManageNav = "active open";
            ViewBag.TicketNav = "active";
            if (UserInfoModel.IsAdmin)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.ModelList = biz.GetKidTicketByTicketSEQ(pageModel.TicketCustomerSEQ, pageModel);
            return View(pageModel);
        }

        [AjaxOnly]
        public JsonResult TicketDel(int id)
        {
            var result = biz.DelTicket(id);
            var message = result ? string.Empty : "传递参数有问题！";
            return Json(new { result = result, message = message });
        }

        [AjaxOnly]
        public JsonResult TicketKidDel(int id)
        {
            var result = biz.DelTicketKid(id);
            var message = result ? string.Empty : "传递参数有问题！";
            return Json(new { result = result, message = message });
        }

        [HttpGet, LoginCheck]
        public ActionResult KidTicketBySalepointList(int id)
        {
            var pageModel = new KidTicketListPageModel();
            ViewBag.TicketManageNav = "active open";
            ViewBag.TicketNav = "active";
            ViewBag.IsAdmin = UserInfoModel.IsAdmin ? "true" : "false";
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.ModelList = biz.GetKidTicketBySalePointSEQ(id, pageModel);
            return View("KidTicketList", pageModel);
        }

        [AjaxOnly]
        public JsonResult TicketCalcTotal(int id)
        {
            var result = biz.DelTicket(id);
            var message = result ? string.Empty : "传递参数有问题！";
            return Json(new { result = result, message = message });
        }

        #endregion

        #region 联运票

        private void TicketUnionListInit(TicketUnionPageModel pageModel)
        {
            ViewBag.TicketManageNav = "active open";
            ViewBag.TicketUnionList = "active";
            if (UserInfoModel.IsAdmin)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            pageModel.TimeCategorySource = new SelectList(TimeCategorySource, "Key", "Value");
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.OperatorAuthNum = UserInfoModel.PageModel.AuthNum;
            if ((pageModel.OperatorAuthNum & 2) == 2)
            {
                pageModel.OperatorUserNoList = new List<int> { UserInfoModel.PageModel.SEQ };
            }
            else if ((pageModel.OperatorAuthNum & 4) == 4)
            {
                pageModel.OperatorUserNoList = biz.GetUserNoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ);
            }
            else if ((pageModel.OperatorAuthNum & 8) == 8)
            {
                pageModel.OperatorUserNoList = biz.GetUserNoByCompanySEQ(UserInfoModel.PageModel.CompanySEQ);
            }
        }

        [HttpGet, LoginCheck]
        public ActionResult TicketUnionList()
        {
            var pageModel = new TicketUnionPageModel() { PageSize = 10, CurrentPage = 1, PageModel = new TicketUnion() };
            TicketUnionListInit(pageModel);
            pageModel.StartDate = DateTime.Now.ToString("yyyy-MM-dd");
            pageModel.EndDate = DateTime.Now.ToString("yyyy-MM-dd");
            pageModel.ModelList = uBiz.GetTicketUnionByCondition(pageModel);
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult TicketUnionList(TicketUnionPageModel pageModel)
        {
            TicketUnionListInit(pageModel);
            pageModel.PageModel = new TicketUnion();
            pageModel.ModelList = uBiz.GetTicketUnionByCondition(pageModel);
            return View(pageModel);
        }

        [HttpGet]
        public PartialViewResult TicketUnionAdd()
        {
            var pageModel = new TicketUnionPageModel();
            return PartialView("_PartialTicketUnionAdd", pageModel);
        }

        [HttpGet]
        public PartialViewResult TicketUnionEdit(int id)
        {
            var pageModel = new TicketUnionPageModel();
            pageModel.PageModel = uBiz.GetTicketUnionById(id);
            return PartialView("_PartialTicketUnionAdd", pageModel);
        }

        [AjaxOnly]
        public JsonResult TicketUnionDel(int id)
        {
            uBiz.TicketUnionDel(id);
            return Json("OK");
        }

        [HttpPost]
        public JsonResult TicketUnionAdd(TicketUnionPageModel model)
        {
            model.PageModel.OperationSEQ = UserInfoModel.PageModel.SEQ;
            uBiz.TicketUnionAdd(model);
            return Json(new { result = true, message = "添加成功！" });
        }

        [HttpPost]
        public JsonResult TicketUnionEdit(TicketUnionPageModel model)
        {
            model.PageModel.OperationSEQ = UserInfoModel.PageModel.SEQ;
            uBiz.TicketUnionEdit(model);
            return Json(new { result = true, message = "修改成功！" });
        }

        #endregion

        #region 手机访问页面

        [HttpGet]
        public ActionResult ShowMobileTicketAdd()
        {
            var pageModel = new TicketIndexPageModel();
            pageModel.PageModel = new TicketCustomer();
            pageModel.OperaotrDate = DateTime.Now.ToString("yyyy-MM-dd");

            return View(pageModel);
        }

        [HttpPost]
        public ActionResult ShowMobileTicketAdd(TicketIndexPageModel model)
        {
            model.PageModel.DivideInto = biz.GetDivideIntoByUserSEQ(model.PageModel.OperationSEQ ?? 0);
            biz.DelTicketUser(model.OperatorUserNo, model.OperaotrDate);
            model.PageModel.CreateTime = Convert.ToDateTime(model.OperaotrDate);
            var result = biz.AddTicket(model);
            var resultList = FileUpload(Request);
            biz.AddKidTicket(model.PageModel.SEQ, resultList);
            return RedirectToAction("ShowMobileTicketAdd");
        }

        [AjaxOnly]
        public JsonResult CheckUserInfo(TicketIndexPageModel model)
        {
            var tempSEQ = 0;
            var result = biz.CheckUserInfo(model.OperatorUserNo, model.UserPassword, ref tempSEQ);
            return Json(new { data = tempSEQ, message = result ? "可以提交！" : "用户名密码错误！" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 辅助方法

        /// <summary>
        /// 获取日期
        /// </summary>
        /// <param name="type">获取类型1今天2昨天3本月4上月</param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetDate(int type)
        {
            var tempStartDate = DateTime.Now;
            var tempEndDate = DateTime.Now;
            var pageStartDate = string.Empty;
            var pageEndDate = string.Empty;
            if (type == 1)
            {
                pageStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                pageEndDate = tempEndDate.AddDays(1).ToString("yyyy-MM-dd");
            }
            else if (type == 2)
            {
                pageStartDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                pageEndDate = tempEndDate.ToString("yyyy-MM-dd");
            }
            else if (type == 3)
            {
                pageStartDate = string.Format("{0}-{1:D2}-{2:D2}", tempStartDate.Year, tempStartDate.Month, 1);
                var tempDate = tempStartDate.Month + 1;
                var tempYear = tempStartDate.Year;
                if (tempDate > 12)
                {
                    tempYear += 1;
                    tempDate = 1;
                }
                tempEndDate = new DateTime(tempYear, tempDate, 1).AddDays(-1);
                pageEndDate = string.Format("{0}-{1:D2}-{2:D2}", tempEndDate.Year, tempEndDate.Month, tempEndDate.Day);
            }
            else if (type == 4)
            {
                tempStartDate = tempStartDate.AddMonths(-1);
                pageStartDate = string.Format("{0}-{1:D2}-{2:D2}", tempStartDate.Year, tempStartDate.Month, 1);
                var tempDate = tempStartDate.Month;
                var tempYear = tempStartDate.Year;
                if (tempDate > 12)
                {
                    tempYear += 1;
                    tempDate = 1;
                }
                tempEndDate = new DateTime(tempYear, tempDate, 1).AddMonths(1).AddDays(-1);
                pageEndDate = string.Format("{0}-{1:D2}-{2:D2}", tempEndDate.Year, tempEndDate.Month, tempEndDate.Day);
            }
            return Json(new { StartDate = pageStartDate, EndDate = pageEndDate });
        }

        /// <summary>
        /// 获取指定单位下的代售点信息
        /// </summary>
        /// <param name="id">单位序号</param>
        /// <returns></returns>
        [AjaxOnly]
        public PartialViewResult ShowSalePoint(int id)
        {
            var modelList = new List<KeyValueModel>();
            if (id != 0)
            {
                modelList.AddRange(biz.GetSalePointInfoByCompanySEQ(id));
                if ((UserInfoModel.PageModel.AuthNum & 1) == 1
                    || (UserInfoModel.PageModel.AuthNum & 4) == 4
                    || (UserInfoModel.PageModel.AuthNum & 8) == 8)
                {
                    modelList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                }

            }
            else
            {
                modelList.Add(new KeyValueModel { Key = 0, Value = "全部" });
            }

            return PartialView("_PartialSelect", modelList);
        }

        #endregion

    }
}