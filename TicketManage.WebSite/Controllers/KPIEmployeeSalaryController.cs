﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.KPICalcEfficacyManage;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.KPICalcEfficacyManage;

namespace TicketManage.WebSite.Controllers
{
    [LoginCheck]
    public class KPIEmployeeSalaryController : BaseController
    {
        private EmployeeSalaryInfoBiz EmployeeBiz = new EmployeeSalaryInfoBiz();

        private void EmployeeSalaryListInit(EmployeeSalaryInfoPageModel model)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.EmployeeSalaryList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
        }

        [HttpGet]
        public ActionResult EmployeeSalaryList()
        {
            var model = new EmployeeSalaryInfoPageModel() { PageSize = 10, CurrentPage = 1 };
            EmployeeSalaryListInit(model);
            model.ModelList = EmployeeBiz.SearchEmployeeSalaryInfoByCondition(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult EmployeeSalaryList(EmployeeSalaryInfoPageModel model)
        {
            EmployeeSalaryListInit(model);
            model.ModelList = EmployeeBiz.SearchEmployeeSalaryInfoByCondition(model);
            return View(model);
        }

        [HttpGet]
        public PartialViewResult ShowEmployeeSalaryAdd()
        {
            var model = new EmployeeSalaryInfoPageModel();
            return PartialView("PartialEmployeeSalaryEdit", model);
        }

        [HttpGet]
        public PartialViewResult ShowEmployeeSalaryEdit(int id)
        {
            var model = new EmployeeSalaryInfoPageModel();
            model.PageModel = EmployeeBiz.GetEmployeeSalaryInfoBySEQ(id);
            return PartialView("PartialEmployeeSalaryEdit", model);
        }

        [AjaxOnly]
        public JsonResult EmployeeSalaryEdit(EmployeeSalaryInfoPageModel model)
        {
            var result = EmployeeBiz.EditEmployeeSalaryInfo(model);
            return Json(new { result = result, message = "操作成功！" }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult EmployeeSalaryDel(int id)
        {
            var result = EmployeeBiz.DelEmployeeSalaryInfo(id);
            return Json(new { result = result, message = "操作成功！" }, JsonRequestBehavior.AllowGet);
        }

    }
}
