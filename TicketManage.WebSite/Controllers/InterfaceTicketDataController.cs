﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Repository.Models.ExtModels;
using TieckManage.Biz.TicketManage;

namespace TicketManage.WebSite.Controllers
{
    /// <summary>
    /// 对外接口提供票务信息
    /// </summary>
    public class InterfaceTicketDataController : BaseController
    {
        /// <summary>
        /// 获取集团信息业务类
        /// </summary>
        private GroupInfoManageBiz groupBiz = new GroupInfoManageBiz();

        /// <summary>
        /// 获取分公司信息业务类
        /// </summary>
        private CompanyInfoManageBiz companyBiz = new CompanyInfoManageBiz();

        /// <summary>
        /// 票务业务类
        /// </summary>
        private TicketManageBiz ticketBiz = new TicketManageBiz();

        /// <summary>
        /// 获取所有集团信息
        /// </summary>
        /// <returns>集团信息列表</returns>
        [HttpGet]
        public JsonResult GroupList()
        {
            var list = groupBiz.GetGroupInfoByAll();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取所有分公司信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult CompanyList()
        {
            var groupName = Request.QueryString["groupName"];
            var list = new List<GroupByCompany>();
            if (string.IsNullOrEmpty(groupName))
            {
                list.AddRange(companyBiz.GetGroupByCompanyByAll());
            }
            else
            {
                list.AddRange(companyBiz.GetGroupByCompanyByAll(groupName));
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取代售点信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult SalePointList()
        {
            var groupName = Request.QueryString["groupName"];
            var companyName = Request.QueryString["companyName"];
            var list = new List<CompanyBySalePoint>();
            if (string.IsNullOrEmpty(groupName) && string.IsNullOrEmpty(companyName))
            {
                list.AddRange(companyBiz.GetCompanyBySalePointByAll());
            }
            else
            {
                list.AddRange(companyBiz.GetCompanyBySalePointByAll(groupName, companyName));
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取统计数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GroupReport()
        {
            var list = companyBiz.GetGroupCount();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult TicketCustomer()
        {

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取所有客票收入，单位（万元）接口12
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult TicketTotal(string searchYear)
        {
            if (string.IsNullOrEmpty(searchYear))
            {
                searchYear = Convert.ToString(DateTime.Now.Year);
            }
            return Json(ticketBiz.GetTicketTotal(searchYear), JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        /// 客票售票信息 接口13
        /// </summary>
        /// <param name="month">查询月份yyyymm</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult TicketInfoDetial(string month)
        {
            var objList = new List<object>();
            for (int i = 1; i <= 12; i++)
            {
                objList.Add(ticketBiz.GetTicketInfoDetial(string.Format("{0}{1:D2}", DateTime.Now.Year, i)));
            }
            return Json(objList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 票务累计收入对比 接口14-1
        /// </summary>
        /// <param name="groupName">集团名称</param>
        /// <param name="branchName">分公司名称</param>
        /// <param name="startDate">开始时间</param>
        /// <param name="endDate">结束时间</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult TicketTotalAmountCompare(string groupName, string companyName, string startDate, string endDate)
        {
            var tempDate = string.Format("{0}-01-01", DateTime.Now.Year);
            startDate = string.Format("{0}0101", DateTime.Now.Year);
            endDate = Convert.ToDateTime(tempDate).AddYears(1).AddDays(-1).ToString("yyyyMMdd");
            return Json(ticketBiz.GetTicketTotalAmountCompare(groupName, companyName, startDate, endDate), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 票务统计 票务收入 接口14-2
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="companyName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult TicketTotalAmount(string groupName, string companyName, string startDate, string endDate)
        {
            return Json(ticketBiz.GetTicketTotalAmount(groupName, companyName, startDate, endDate), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 票务统计 售票张数 接口15
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="companyName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult TicketTotalNum(string groupName, string companyName, string startDate, string endDate)
        {
            var tempDate = string.Format("{0}-01-01", DateTime.Now.Year);
            startDate = string.Format("{0}0101", DateTime.Now.Year);
            endDate = Convert.ToDateTime(tempDate).AddYears(1).AddDays(-1).ToString("yyyyMMdd");
            return Json(ticketBiz.GetTicketTotalNum(groupName, companyName, startDate, endDate), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 票务收入统计 接口16
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="companyName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult TicketTotalAmountDetail(string groupName, string companyName, string startDate, string endDate)
        {
            var tempDate = string.Format("{0}-01-01", DateTime.Now.Year);
            startDate = string.Format("{0}0101", DateTime.Now.Year);
            endDate = Convert.ToDateTime(tempDate).AddYears(1).AddDays(-1).ToString("yyyyMMdd");
            return Json(ticketBiz.GetTicketTotalAmountDetail(groupName, companyName, startDate, endDate), JsonRequestBehavior.AllowGet);
        }




    }
}
