using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Entity.PublicityInfoManage;
using TicketManage.Repository.Models;
using TicketManage.WebSite.Filters;
using TicketManage.WebSite.Logic;
using TieckManage.Biz.CommonManage;
using TieckManage.Biz.PublicityInfoManage;
using TieckManage.Biz.TicketManage;

namespace TicketManage.WebSite.Controllers
{
    public class PublicityController : BaseController
    {
        private CommonBiz biz = new CommonBiz();
        private PublicityInfoBiz pBiz = new PublicityInfoBiz();
        private UserInfoManageBiz Userbiz = new UserInfoManageBiz();

        [HttpGet]
        public ActionResult Login()
        {
            ViewBag.Title = "宣传品管理系统登录";
            var pageModel = new UserLoginPageModel();
            pageModel.MessageState = false;
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult Login(UserLoginPageModel model)
        {
            ViewBag.Title = "宣传品管理系统登录";
            model.SysteCategory = SystemName.宣传品管理系统;
            model.SystemName = "宣传品管理系统";
            var result = biz.CommonLogin(model);
            if (result)
            {
                if (CheckUserAuth(model.PageModel.AuthNum, model.SysteCategory))
                {
                    Session[UserInfo] = model;
                    if ((model.PageModel.AuthNum & 65536) == 65536)
                    {
                        return RedirectToAction("PublicityInfoApplyList");
                    }
                    else if ((model.PageModel.AuthNum & 1) == 1 || (model.PageModel.AuthNum & 147456) == 147456)
                    {
                        return RedirectToAction("PublicityInfoList");
                    }
                }
                else
                {
                    model.Message = "您没有权限访问宣传品管理系统！";
                    model.MessageState = true;
                }
            }
            else
            {
                model.Message = "用户名密码错误！";
                model.MessageState = false;
            }
            return View(model);
        }

        #region 宣传品信息

        private void PublicityInfoListInit(PublicityInfoPageModel pageModel)
        {
            ViewBag.PublicityNav = "active open";
            ViewBag.PublicityInfoList = "active";
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            if (tempModel.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.OperatorAuthNum = CommonUserInfoModel.PageModel.AuthNum;
        }

        [HttpGet, LoginCheck]
        public ActionResult PublicityInfoList()
        {
            var pageModel = new PublicityInfoPageModel();
            PublicityInfoListInit(pageModel);
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            pageModel.ModelList = pBiz.GetPublicityByAll(pageModel);
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult PublicityInfoList(PublicityInfoPageModel model)
        {
            PublicityInfoListInit(model);
            model.ModelList = pBiz.GetPublicityByCondition(model);
            return View(model);
        }

        [HttpGet, LoginCheck]
        public PartialViewResult PublicityAdd()
        {
            var pageModel = new PublicityInfoPageModel();
            return PartialView("_PartialPublicityEdit", pageModel);
        }

        [HttpGet]
        public ActionResult PublicityInfoAdd()
        {
            var pageModel = new PublicityInfoPageModel();
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult PublicityInfoAdd(PublicityInfoPageModel model)
        {
            var result = pBiz.SavePublicityInfo(model);
            return Json(new { result = result, message = model.Message });
        }
        [HttpGet, LoginCheck]
        public ActionResult PublicityInfoEdit(int? id)
        {

            var pageModel = new PublicityInfoPageModel();
            pageModel.PageModel = pBiz.ShowPublicityInfoBySeq(id ?? 0);
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult PublicityInfoEdit(PublicityInfoPageModel model)
        {
            var result = pBiz.SavePublicityInfo(model);
            return Json(new { result = result, message = model.Message });
        }

        [HttpGet]
        public JsonResult PublicityInfoDel(int id)
        {
            var result = pBiz.DelPublicityInfo(id);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region 宣传品申请

        private void PublicityInfoApplyListInit(ApplicationRecordPageModel model)
        {
            ViewBag.PublicityNav = "active open";
            ViewBag.PublicityInfoApplyList = "active";
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            if (tempModel.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
            model.OperatorAuthNum = CommonUserInfoModel.PageModel.AuthNum;
            model.AppPageModel.OperatorSEQ = CommonUserInfoModel.PageModel.SEQ;
        }

        [HttpGet, LoginCheck]
        public ActionResult PublicityInfoApplyList()
        {
            var pageModel = new ApplicationRecordPageModel() { AppPageModel = new ApplicationPageModel() };
            PublicityInfoApplyListInit(pageModel);
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            pageModel.AppModelList = pBiz.ShowPublicityApplyByAll(pageModel);
            return View(pageModel);
        }
        [HttpPost, LoginCheck]
        public ActionResult PublicityInfoApplyList(ApplicationRecordPageModel model)
        {
            model.AppPageModel.OperatorSEQ = CommonUserInfoModel.PageModel.SEQ;
            PublicityInfoApplyListInit(model);
            model.AppModelList = pBiz.ShowPublicityApplyByCondition(model);
            return View(model);
        }

        [HttpGet, LoginCheck]
        public ActionResult PublicityApplyAdd()
        {
            TicketManageLogic logic = new TicketManageLogic();
            var pageModel = new ApplicationRecordPageModel();
            pageModel.PublicityNameSource = logic.ToPublicitySelectList(pBiz.GetPublicityNameByType());
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public JsonResult PublicityApplyAdd(ApplicationRecordPageModel model)
        {
            var tempCompany = pBiz.GetCompanyInfoByUserNo(CommonUserInfoModel.PageModel.UserNo);
            TicketManageLogic logic = new TicketManageLogic();
            var pageModel = new ApplicationRecord();
            pageModel.PublicitySEQ = model.AppPageModel.PublicitySEQ;
            pageModel.Num = model.AppPageModel.Num;
            pageModel.Memo = model.AppPageModel.Memo;
            pageModel.OperationState = 0;//未审批
            pageModel.OperatorSEQ = CommonUserInfoModel.PageModel.SEQ;
            pageModel.OperatorName = CommonUserInfoModel.PageModel.UserName;
            pageModel.CreateTime = DateTime.Now;
            if (tempCompany != null)
            {
                pageModel.OperatorCompanySEQ = tempCompany.Key;
                pageModel.OperatorCompanyName = tempCompany.Value;
            }
            var result = pBiz.SavePublicityApply(pageModel);
            return Json(new { result = result, message = model.Message });
        }
        [HttpGet, LoginCheck]
        public ActionResult PublicityApplyEdit(int? id)
        {
            TicketManageLogic logic = new TicketManageLogic();
            var pageModel = new ApplicationRecordPageModel();
            pageModel.PageModel = pBiz.ShowPublicityApplyBySeq(id ?? 0);
            pageModel.PublicityNameSource = logic.ToPublicitySelectList(pBiz.GetPublicityNameByType());
            return View(pageModel);
        }

        [AjaxOnly, LoginCheck]
        public JsonResult PublicityApplyEdit(ApplicationRecordPageModel model)
        {
            TicketManageLogic logic = new TicketManageLogic();
            var result = pBiz.SavePublicityRecord(model.PageModel);
            return Json(new { result = result, message = model.Message });
        }

        [AjaxOnly, LoginCheck]
        public JsonResult PublicityApplyConfirm(int id)
        {
            var result = pBiz.SavePublicityApplyConfirmStock(id);
            return Json(new { resultData = result ? "OK" : "Error" });
        }

        [AjaxOnly, LoginCheck]
        public JsonResult PublicityApplyDel(int id)
        {
            var result = pBiz.DelPublicityApply(id);
            return Json(new { resultData = result ? "OK" : "Error" });
        }

        #endregion

        #region 宣传品审批

        private void PublicityInfoApproveListInit(ApplicationRecordPageModel model)
        {
            ViewBag.PublicityNav = "active open";
            ViewBag.PublicityInfoApproveList = "active";
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            if (tempModel.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value");
            //model.CompanyName = pBiz.GetUserCompanyName(tempModel.PageModel.UserNo);
        }

        [HttpGet, LoginCheck]
        public ActionResult PublicityInfoApproveList()
        {
            var pageModel = new ApplicationRecordPageModel();
            PublicityInfoApproveListInit(pageModel);
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            pageModel.AppModelList = pBiz.ShowPublicityInfoByCondition(pageModel);
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult PublicityInfoApproveList(ApplicationRecordPageModel model)
        {
            PublicityInfoApproveListInit(model);
            model.AppModelList = pBiz.ShowPublicityInfoByCondition(model);
            return View(model);
        }

        [HttpPost, LoginCheck]
        public ActionResult PublicityInfoApproveUpload()
        {
            var formApplicationRecordSEQ = Convert.ToInt32(this.Request.Form["SEQ"]);
            var formPublicityName = this.Request.Form["PublicityName"];
            var formFileUpload = this.Request.Form["FileUpload"];
            var formPageSize = this.Request.Form["PageSize"];
            var formCurrentPage = this.Request.Form["CurrentPage"];
            var uploadFileList = FileUpload(this.Request);
            if (uploadFileList.Count == 1)
            {
                var tempFileName = uploadFileList.FirstOrDefault().Key;
                pBiz.SaveApplicationRecordFileUpload(formApplicationRecordSEQ, tempFileName);
            }
            var pageModel = new ApplicationRecordPageModel() { AppPageModel = new ApplicationPageModel() };
            PublicityInfoApproveListInit(pageModel);
            pageModel.PageSize = string.IsNullOrEmpty(formPageSize) == true ? 10 : Convert.ToInt32(formPageSize);
            pageModel.CurrentPage = string.IsNullOrEmpty(formCurrentPage) == true ? 1 : Convert.ToInt32(formCurrentPage);
            pageModel.AppPageModel.PublicityName = formPublicityName;
            pageModel.AppModelList = pBiz.ShowPublicityInfoByCondition(pageModel);
            return View("PublicityInfoApproveList", pageModel);
        }

        [HttpGet, LoginCheck]
        public JsonResult PublicityStateApp(int id)
        {
            var model = new ApplicationRecordPageModel() { AppPageModel = new ApplicationPageModel() };
            model.AppPageModel.SEQ = id;
            model.AppPageModel.ApprovalSEQ = CommonUserInfoModel.PageModel.SEQ;
            model.AppPageModel.ApprovalName = CommonUserInfoModel.PageModel.UserName;
            var result = pBiz.PublicityStateApp(model);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 宣传品领用台账

        private void PublicityInfoReceiveListInit(PublicityReceivePageModel model)
        {
            ViewBag.PublicityNav = "active open";
            ViewBag.PublicityInfoReceiveList = "active";
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            if (tempModel.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value");
            model.OperatorAuthNum = CommonUserInfoModel.PageModel.AuthNum;
            model.PageModel.OperatorSEQ = CommonUserInfoModel.PageModel.SEQ;
            var tempCompanyList = new List<KeyValueModel>();
            var templist = Userbiz.GetOtherUserCompany();
            if (templist != null && templist.Count > 0)
            {
                tempCompanyList.AddRange(templist);
                tempCompanyList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
            }
            else
            {
                tempCompanyList.Insert(0, new KeyValueModel { Key = 0, Value = "" });
            }
            model.CompanySource = new SelectList(tempCompanyList, "Value", "Value");
        }

        [HttpGet, LoginCheck]
        public ActionResult PublicityInfoReceiveList()
        {
            var pageModel = new PublicityReceivePageModel() { PageModel = new PublicityReceive() };
            PublicityInfoReceiveListInit(pageModel);
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            pageModel.RecModelList = pBiz.ShowPublicityReceiveByAll(pageModel);
            return View(pageModel);
        }
        [HttpPost, LoginCheck]
        public ActionResult PublicityInfoReceiveList(PublicityReceivePageModel model)
        {
            PublicityInfoReceiveListInit(model);
            model.RecModelList = pBiz.ShowPublicityReceiveByCondition(model);
            return View(model);
        }

        [HttpGet, LoginCheck]
        public ActionResult PublicityInfoReceiveEdit(int? id)
        {
            ViewBag.PublicityNav = "active open";
            ViewBag.PublicityInfoReceiveList = "active";
            var tempModel1 = Session[UserInfo] as UserLoginPageModel;
            if (tempModel1.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            TicketManageLogic logic = new TicketManageLogic();
            var pageModel = new PublicityReceivePageModel();
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            pageModel.PassportNo = tempModel.PageModel.PassportNo;
            pageModel.TravelProdcutNameSource = logic.CodeListToSelectList(pBiz.GetSystemCodeListByType("TravelProdcutNameSourceNothing"));
            pageModel.RecPageModel = pBiz.ShowPublicityReceiveBySeq(id ?? 0);

            //pageModel.PublicityNameSource = logic.ToPublicitySelectList(pBiz.GetPublicityNameByType());

            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult PublicityInfoReceiveEdit(PublicityReceivePageModel model)
        {
            ViewBag.PublicityNav = "active open";
            ViewBag.PublicityInfoReceiveList = "active";
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            if (tempModel.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            TicketManageLogic logic = new TicketManageLogic();
            var publicRec = new PublicityReceive();
            publicRec.PublicitySEQ = model.RecPageModel.SEQ;
            publicRec.publicityName = model.RecPageModel.publicityName;
            publicRec.AppNum = model.RecPageModel.AppNum;
            publicRec.TravelProdcutName = model.RecPageModel.TravelProdcutName;
            publicRec.CustomerNum = model.RecPageModel.CustomerNum;
            publicRec.Memo = model.RecPageModel.Memo;
            publicRec.CreateTime = model.PageModel.CreateTime ?? DateTime.Now;
            publicRec.OperatorSEQ = CommonUserInfoModel.PageModel.SEQ;
            publicRec.OperatorName = CommonUserInfoModel.PageModel.UserName;
            publicRec.AppCategory = model.PageModel.AppCategory;

            var result = pBiz.SavePublicityReceive(publicRec);
            //model.PublicityNameSource = logic.ToPublicitySelectList(pBiz.GetPublicityNameByType());
            if (!result)
            {
                model.Message = "剩余库存量不足！";
            }
            return Json(new { result = result, message = model.Message });
        }

        private void PublicityInfoReceiveDetailInit(PublicityReceivePageModel model)
        {
            ViewBag.PublicityNav = "active open";
            ViewBag.PublicityInfoReceiveList = "active";
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            if (tempModel.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value");
        }

        [HttpGet, LoginCheck]
        public ActionResult PublicityInfoReceiveDetail(int obj, string name)
        {
            var pageModel = new PublicityReceivePageModel();
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            pageModel.PublicitySEQ = obj;
            pageModel.CompanySEQ = CommonUserInfoModel.PageModel.CompanySEQ;
            pageModel.ComanyName = name;
            PublicityInfoReceiveDetailInit(pageModel);
            pageModel.ModelList = pBiz.ShowPublicityInfoReceiveDetail(pageModel);
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult PublicityInfoReceiveDetail(PublicityReceivePageModel model)
        {
            PublicityInfoReceiveDetailInit(model);
            model.ModelList = pBiz.ShowPublicityInfoReceiveDetail(model);
            return View(model);
        }

        [AjaxOnly, LoginCheck]
        public JsonResult PublicityInfoReceiveDel(int seq)
        {
            var result = pBiz.PublicityInfoReceiveDel(seq);
            return Json(new { result = result }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 导出Excel文件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult ExcelExport(int objId)
        {
            Session["ExcelPublicity"] = objId;
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 生成下载文件
        /// </summary>
        /// <returns></returns>
        public FileContentResult ExcelDownLoad()
        {
            var searchCondition = string.Empty;
            if (Session["ExcelPublicity"] != null)
            {
                searchCondition = Convert.ToString(Session["ExcelPublicity"]);
            }
            var pageModel = new PublicityReceivePageModel() { PublicitySEQ = Convert.ToInt32(searchCondition) };
            var dataResult = pBiz.ShowPublicityInfoReceiveDetailByAll(pageModel);
            var book = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = book.CreateSheet("ExcelExport");

            var tableBorderStyle = book.CreateCellStyle();
            tableBorderStyle.BorderBottom = BorderStyle.Thin;
            tableBorderStyle.BorderLeft = BorderStyle.Thin;
            tableBorderStyle.BorderRight = BorderStyle.Thin;
            tableBorderStyle.BorderTop = BorderStyle.Thin;
            var tableContentFont = book.CreateFont();
            tableContentFont.FontName = "微软雅黑";
            tableContentFont.FontHeightInPoints = 12;
            tableBorderStyle.SetFont(tableContentFont);

            var tableTitleStyle = book.CreateCellStyle();
            tableTitleStyle.BorderBottom = BorderStyle.Thin;
            tableTitleStyle.BorderLeft = BorderStyle.Thin;
            tableTitleStyle.BorderRight = BorderStyle.Thin;
            tableTitleStyle.BorderTop = BorderStyle.Thin;
            var tableTitleFont = book.CreateFont();
            tableTitleFont.FontName = "微软雅黑";
            tableTitleFont.FontHeightInPoints = 12;
            tableTitleFont.Boldweight = short.MaxValue;
            tableTitleStyle.SetFont(tableTitleFont);

            ICellStyle style = book.CreateCellStyle();

            //设置单元格的样式：水平对齐居中
            style.Alignment = HorizontalAlignment.Center;
            //新建一个字体样式对象
            IFont font = book.CreateFont();
            //设置字体加粗样式
            font.Boldweight = short.MaxValue;
            font.FontName = "微软雅黑";
            font.FontHeightInPoints = 16;
            //使用SetFont方法将字体样式添加到单元格样式中 
            style.SetFont(font);
            //将新的样式赋给单元格
            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 9));

            var row0 = sheet.CreateRow(0);
            row0.CreateCell(0).SetCellValue(string.Format("宣传品领用记录"));
            row0.GetCell(0).CellStyle = style;

            var indexVal = 0;

            var rowTitle = sheet.CreateRow(indexVal + 3);
            rowTitle.CreateCell(0).SetCellValue("分公司");
            rowTitle.CreateCell(1).SetCellValue("名称");
            rowTitle.CreateCell(2).SetCellValue("领用数量");
            rowTitle.CreateCell(3).SetCellValue("申请类型");
            rowTitle.CreateCell(4).SetCellValue("产品名");
            rowTitle.CreateCell(5).SetCellValue("营业部名称");
            rowTitle.CreateCell(6).SetCellValue("收客人数");
            rowTitle.CreateCell(7).SetCellValue("备注");
            rowTitle.CreateCell(8).SetCellValue("领用时间");
            rowTitle.GetCell(0).CellStyle = tableTitleStyle;
            rowTitle.GetCell(1).CellStyle = tableTitleStyle;
            rowTitle.GetCell(2).CellStyle = tableTitleStyle;
            rowTitle.GetCell(3).CellStyle = tableTitleStyle;
            rowTitle.GetCell(4).CellStyle = tableTitleStyle;
            rowTitle.GetCell(5).CellStyle = tableTitleStyle;
            rowTitle.GetCell(6).CellStyle = tableTitleStyle;
            rowTitle.GetCell(7).CellStyle = tableTitleStyle;
            rowTitle.GetCell(8).CellStyle = tableTitleStyle;


            indexVal = 4;

            foreach (var item in dataResult)
            {
                var row = sheet.CreateRow(indexVal);
                row.CreateCell(0).SetCellValue(item.OperatorName);
                row.CreateCell(1).SetCellValue(item.publicityName);
                row.CreateCell(2).SetCellValue(Convert.ToString(item.AppNum ?? 0));
                row.CreateCell(3).SetCellValue(item.AppCategory);
                row.CreateCell(4).SetCellValue(item.TravelProdcutName);
                row.CreateCell(5).SetCellValue(string.Empty);
                row.CreateCell(6).SetCellValue(Convert.ToString(item.CustomerNum ?? 0));
                row.CreateCell(7).SetCellValue(item.Memo);
                var tempDateTime = string.Empty;
                if (item.CreateTime != null)
                {
                    tempDateTime = Convert.ToDateTime(item.CreateTime).ToString("yyyy-MM-dd");
                }
                row.CreateCell(8).SetCellValue(tempDateTime);
                row.GetCell(0).CellStyle = tableBorderStyle;
                row.GetCell(1).CellStyle = tableBorderStyle;
                row.GetCell(2).CellStyle = tableBorderStyle;
                row.GetCell(3).CellStyle = tableBorderStyle;
                row.GetCell(4).CellStyle = tableBorderStyle;
                row.GetCell(5).CellStyle = tableBorderStyle;
                row.GetCell(6).CellStyle = tableBorderStyle;
                row.GetCell(7).CellStyle = tableBorderStyle;
                row.GetCell(8).CellStyle = tableBorderStyle;
                indexVal += 1;
            }

            for (int i = 0; i < 9; i++)
            {
                sheet.AutoSizeColumn(i);
            }

            // 写入到客户端  
            var ms = new System.IO.MemoryStream();
            book.Write(ms);
            var fileName = string.Format("{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"));
            return File(ms.ToArray(), "application/ms-excel", fileName);
        }

        #endregion

        #region 宣传品预算管理

        private void PublicityBudgetInfoListInit(PublicityReceivePageModel model)
        {
            ViewBag.PublicityNav = "active open";
            ViewBag.PublicityBudgetInfoList = "active";
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            if (tempModel.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value");
        }

        [HttpGet, LoginCheck]
        public ActionResult PublicityBudgetInfoList()
        {
            var pageModel = new PublicityReceivePageModel();
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            PublicityBudgetInfoListInit(pageModel);
            pageModel.BudgetModelList = pBiz.ShowPublicityInfoBudgetListByAll(pageModel);
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult PublicityBudgetInfoList(PublicityReceivePageModel pageModel)
        {
            PublicityBudgetInfoListInit(pageModel);
            pageModel.BudgetModelList = pBiz.GetBudgetInfoByCondition(pageModel);
            return View(pageModel);
        }

        [HttpGet, LoginCheck]
        public ActionResult PublicityBudgetAdd()
        {
            ViewBag.PublicityNav = "active open";
            ViewBag.PublicityBudgetInfoList = "active";
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            if (tempModel.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            var pageModel = new PublicityReceivePageModel();
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult PublicityBudgetAdd(PublicityReceivePageModel model)
        {
            ViewBag.PublicityNav = "active open";
            ViewBag.PublicityBudgetInfoList = "active";
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            if (tempModel.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            var pageMod = new BudgetInfo();
            pageMod.BudgetName = model.BudgetPageModel.BudgetName;
            pageMod.BudgetMoney = model.BudgetPageModel.BudgetMoney;
            pageMod.OperatorSEQ = CommonUserInfoModel.PageModel.SEQ;
            pageMod.OperatorName = CommonUserInfoModel.PageModel.UserName;
            pageMod.CreateTime = DateTime.Now;
            var result = pBiz.SaveBudgetAdd(pageMod);
            return Json(new { result = result, message = model.Message });
        }



        [HttpGet, LoginCheck]
        public ActionResult PublicityBudgetOff()
        {
            TicketManageLogic logic = new TicketManageLogic();
            var pageModel = new PublicityReceivePageModel();
            pageModel.PublicityBudgetSource = logic.ToBudgetSelectList(pBiz.GetBudgetNameByType());
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult PublicityBudgetOff(PublicityReceivePageModel model)
        {
            var pageMod = new ApplyBudget();
            pageMod.BudgetSEQ = model.ApplyBudgetPageModel.BudgetSEQ;
            pageMod.BudgetName = model.ApplyBudgetPageModel.BudgetName;
            pageMod.ApplyMoney = model.ApplyBudgetPageModel.ApplyMoney;
            pageMod.OperatorSEQ = CommonUserInfoModel.PageModel.SEQ;
            pageMod.OperatorName = CommonUserInfoModel.PageModel.UserName;
            pageMod.CreateTime = DateTime.Now;
            var list = FileUpload(Request);
            if (list != null && list.Count == 1)
            {
                pageMod.Voucher = list.FirstOrDefault().Key;
            }
            var result = pBiz.SavePublicityBudgetOff(pageMod);
            return RedirectToAction("PublicityBudgetInfoList");
        }

        private void PublicityBudgetDetailInit(PublicityReceivePageModel model)
        {
            ViewBag.PublicityNav = "active open";
            ViewBag.PublicityBudgetInfoList = "active";
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            if (tempModel.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value");
        }

        private void ConvertFileName(List<ApplyBudget> modelList)
        {
            string filepath = ConfigurationManager.AppSettings["FilePath"].ToString();
            foreach (var item in modelList)
            {
                item.Voucher = string.Format("{0}{1}", filepath, item.Voucher);
            }
        }

        [HttpGet, LoginCheck]
        public ActionResult PublicityBudgetDetail(int obj)
        {
            var pageModel = new PublicityReceivePageModel();
            pageModel.PublicitySEQ = obj;
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            pageModel.ApplyBudgetModelList = pBiz.ShowPublicityBudgetDetail(pageModel);
            ConvertFileName(pageModel.ApplyBudgetModelList);
            PublicityBudgetDetailInit(pageModel);
            return View(pageModel);
        }



        public ActionResult PublicityBudgetDetail(PublicityReceivePageModel model)
        {
            ViewBag.PublicityNav = "active open";
            ViewBag.PublicityBudgetInfoList = "active";
            var tempModel = Session[UserInfo] as UserLoginPageModel;
            if (tempModel.PageModel.AuthNum == 1)
            {
                ViewBag.IsAdmin = "true";
            }
            else
            {
                ViewBag.IsAdmin = "false";
            }
            model.ApplyBudgetModelList = pBiz.ShowPublicityBudgetDetail(model);
            ConvertFileName(model.ApplyBudgetModelList);
            PublicityBudgetDetailInit(model);
            return View(model);
        }

        #endregion


    }
}
