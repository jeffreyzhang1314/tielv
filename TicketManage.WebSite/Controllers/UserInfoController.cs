using System;
using System.Web.Mvc;
using TicketManage.Entity.TicketManage;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.TicketManage;
using System.Collections.Generic;
using TicketManage.WebSite.Models;
using TicketManage.Repository.Models;

namespace TicketManage.WebSite.Controllers
{
    [LoginCheck]
    public class UserInfoController : BaseController
    {
        private UserInfoManageBiz biz = new UserInfoManageBiz();

        /// <summary>
        /// 页面初始化
        /// </summary>
        /// <param name="model"></param>
        private void UserListManageInit(UserPageModel model)
        {
            ViewBag.BaseDataNav = "active open";
            ViewBag.PageNav = "active";
            model.OperatorAuthNum = UserInfoModel.PageModel.AuthNum;
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", 10);
            var groupList = new List<KeyValueModel>();
            var tempCompanyList = new List<KeyValueModel>();
            var tempSalepointList = new List<KeyValueModel>();
            ViewBag.IsAdmin = UserInfoModel.IsAdmin ? "true" : "false";
            if ((UserInfoModel.PageModel.AuthNum & 4) == 4)
            {
                groupList.AddRange(biz.GetGroupInfoById(UserInfoModel.PageModel.GroupSEQ ?? 0));
                var tempCompanyListSource = biz.GetCompanyInfoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ ?? 0);
                tempCompanyListSource.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                tempCompanyList.AddRange(tempCompanyListSource);
                var tempSalepointListSource = biz.GetSalePointInfoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ ?? 0);
                tempSalepointListSource.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                tempSalepointList.AddRange(tempSalepointListSource);
            }
            else if ((UserInfoModel.PageModel.AuthNum & 8) == 8)
            {
                groupList.AddRange(biz.GetGroupInfoById(UserInfoModel.PageModel.GroupSEQ ?? 0));
                tempCompanyList.AddRange(biz.GetCompanyInfoById(UserInfoModel.PageModel.CompanySEQ ?? 0));
                var tempSalepointListSource = biz.GetSalePointInfoByCompanySEQ(UserInfoModel.PageModel.CompanySEQ ?? 0);
                tempSalepointListSource.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                tempSalepointList.AddRange(tempSalepointListSource);
            }
            else if ((UserInfoModel.PageModel.AuthNum & 1) == 1 || (UserInfoModel.PageModel.AuthNum & 16) == 16)
            {
                groupList.AddRange(biz.GetGroupInfoByAll());
                groupList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                tempCompanyList.AddRange(biz.GetCompanyInfoByAll());
                tempCompanyList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                tempSalepointList.AddRange(biz.GetSalePointInfoByAll());
                tempSalepointList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
            }


            model.PageModel.CompanySEQ = UserInfoModel.PageModel.CompanySEQ ?? 0;
            model.PageModel.SalePointSEQ = UserInfoModel.PageModel.SalePointSEQ ?? 0;

            model.GroupSource = new SelectList(groupList, "Key", "Value");
            model.CompanySource = new SelectList(tempCompanyList, "Key", "Value");
            model.SalePointSource = new SelectList(tempSalepointList, "Key", "Value");
        }

        [HttpGet]
        public ActionResult UserListManage()
        {
            var pageModel = new UserPageModel() { PageSize = 10, CurrentPage = 1, PageModel = new UserInfo() };
            UserListManageInit(pageModel);
            if ((UserInfoModel.PageModel.AuthNum & 4) == 4)
            {
                pageModel.PageModel.GroupSEQ = UserInfoModel.PageModel.GroupSEQ;
            }
            else if ((UserInfoModel.PageModel.AuthNum & 8) == 8)
            {
                pageModel.PageModel.CompanySEQ = UserInfoModel.PageModel.CompanySEQ;
            }
            pageModel.CurrentUserNo = UserInfoModel.PageModel.UserNo;
            pageModel.ListModel = biz.GetUserInfoListByCondition(pageModel);
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult UserListManage(UserPageModel model)
        {
            model.ListModel = biz.GetUserInfoListByCondition(model);
            UserListManageInit(model);
            return View(model);
        }

        [HttpGet]
        public ActionResult UserInfoManage(int id)
        {
            ViewBag.BaseDataNav = "active open";
            ViewBag.PageNav = "active";
            var pageModel = new UserPageModel();
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult UserInfoManage(UserPageModel model)
        {
            ViewBag.BaseDataNav = "active open";
            ViewBag.PageNav = "active";
            return View(model);
        }


        [AjaxOnly]
        public JsonResult UserInfoPasswordReset(int id)
        {
            var result = biz.ResetPasswordByUserInfoSEQ(id);
            return Json(result ? "操作完成！" : "操作失败！", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult UserInfoDel(int id)
        {
            var result = biz.DeleteUserInfoByEntity(id);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取指定集团下单位信息
        /// </summary>
        /// <param name="id">集团序号</param>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult ShowCompanyByGroupSEQ(int id)
        {
            var pageModel = new SelectPageModel();
            pageModel.ControlId = "PageModel_CompanySEQ";
            pageModel.ControlName = "PageModel.CompanySEQ";
            if (id != 0)
            {
                var pageModelList = biz.GetCompanyInfoByGroupSEQ(id);
                if (pageModelList.Count == 0
                    || (UserInfoModel.PageModel.AuthNum & 1) == 1
                    || (UserInfoModel.PageModel.AuthNum & 4) == 4
                    || (UserInfoModel.PageModel.AuthNum & 8) == 8
                    || (UserInfoModel.PageModel.AuthNum & 16) == 16
                    || (UserInfoModel.PageModel.AuthNum & 32) == 32)
                {
                    pageModelList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                }
                var selectSource = new SelectList(pageModelList, "Key", "Value");
                pageModel.SelectSource = selectSource;
            }
            else
            {
                var pageModelList = new List<KeyValueModel>();
                pageModelList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                var selectSource = new SelectList(pageModelList, "Key", "Value");
                pageModel.SelectSource = selectSource;
            }
            return PartialView("_PartialSelectList", pageModel);
        }

        /// <summary>
        /// 获取指定单位下代售点信息
        /// </summary>
        /// <param name="id">单位序号</param>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult ShowSalePointByCompanySEQ(int id)
        {
            var pageModel = new SelectPageModel();
            pageModel.ControlId = "PageModel_SalePointSEQ";
            pageModel.ControlName = "PageModel.SalePointSEQ";
            if (id != 0)
            {
                var pageModelList = biz.GetSalePointInfoByCompanySEQ(id);
                pageModelList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                var selectSource = new SelectList(pageModelList, "Key", "Value");
                pageModel.SelectSource = selectSource;
            }
            else
            {
                var pageModelList = new List<KeyValueModel>();
                pageModelList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                var selectSource = new SelectList(pageModelList, "Key", "Value");
                pageModel.SelectSource = selectSource;
            }

            return PartialView("_PartialSelectList", pageModel);
        }

        [HttpGet]
        public ActionResult UserInfoAdd()
        {
            var pageModel = new UserPageModel();
            pageModel.AuthListSource = new SelectList(AuthNumSource, "Key", "Value");
            pageModel.SexSource = new SelectList(SexSource, "Key", "Value");

            var groupList = new List<KeyValueModel>();
            var tempCompanyList = new List<KeyValueModel>();
            var tempSalepointList = new List<KeyValueModel>();
            if (UserInfoModel.IsAdmin)
            {
                groupList.AddRange(biz.GetGroupInfoByAll());
                tempCompanyList.AddRange(biz.GetCompanyInfoByAll());
                tempCompanyList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                tempSalepointList.AddRange(biz.GetSalePointInfoByAll());
                tempSalepointList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                ViewBag.IsAdmin = "true";
            }
            else
            {
                groupList.AddRange(biz.GetGroupInfoById(UserInfoModel.PageModel.GroupSEQ ?? 0));
                if ((UserInfoModel.PageModel.AuthNum & 4) == 4)
                {
                    tempCompanyList.AddRange(biz.GetCompanyInfoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ ?? 0));
                    tempCompanyList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                    tempSalepointList.AddRange(biz.GetSalePointInfoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ ?? 0));
                    tempSalepointList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                }
                else if ((UserInfoModel.PageModel.AuthNum & 8) == 8)
                {
                    tempCompanyList.AddRange(biz.GetCompanyInfoById(UserInfoModel.PageModel.CompanySEQ ?? 0));
                    tempSalepointList.AddRange(biz.GetSalePointInfoByCompanySEQ(UserInfoModel.PageModel.CompanySEQ ?? 0));
                    tempSalepointList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                }
                else
                {
                    tempSalepointList.AddRange(biz.GetSalePointInfoById(UserInfoModel.PageModel.SalePointSEQ ?? 0));
                }
                ViewBag.IsAdmin = "false";
            }
            pageModel.GroupSource = new SelectList(groupList, "Key", "Value");
            pageModel.CompanySource = new SelectList(tempCompanyList, "Key", "Value");
            pageModel.SalePointSource = new SelectList(tempSalepointList, "Key", "Value");
            return View(pageModel);
        }

        [HttpPost]
        public JsonResult UserInfoAdd(UserPageModel model)
        {
            var result = biz.SaveUserInfo(model);
            return Json(new { result = result, message = model.Message });
        }

        [HttpGet]
        public ActionResult UserInfoEdit(int id)
        {
            var pageModel = new UserPageModel();
            pageModel.PageModel = biz.GetUserInfoById(id);
            pageModel.AuthListSource = new SelectList(AuthNumSource, "Key", "Value");
            pageModel.SexSource = new SelectList(SexSource, "Key", "Value");
            var groupList = new List<KeyValueModel>();
            var tempCompanyList = new List<KeyValueModel>();
            var tempSalepointList = new List<KeyValueModel>();
            if (UserInfoModel.IsAdmin)
            {
                groupList.AddRange(biz.GetGroupInfoByAll());
                tempCompanyList.AddRange(biz.GetCompanyInfoByAll());
                tempCompanyList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                tempSalepointList.AddRange(biz.GetSalePointInfoByAll());
                tempSalepointList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                ViewBag.IsAdmin = "true";
            }
            else
            {
                groupList.AddRange(biz.GetGroupInfoById(UserInfoModel.PageModel.GroupSEQ ?? 0));
                if ((UserInfoModel.PageModel.AuthNum & 4) == 4)
                {
                    tempCompanyList.AddRange(biz.GetCompanyInfoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ ?? 0));
                    tempCompanyList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                    tempSalepointList.AddRange(biz.GetSalePointInfoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ ?? 0));
                    tempSalepointList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                }
                else if ((UserInfoModel.PageModel.AuthNum & 8) == 8)
                {
                    tempCompanyList.AddRange(biz.GetCompanyInfoById(UserInfoModel.PageModel.CompanySEQ ?? 0));
                    tempSalepointList.AddRange(biz.GetSalePointInfoByCompanySEQ(UserInfoModel.PageModel.CompanySEQ ?? 0));
                    tempSalepointList.Insert(0, new KeyValueModel { Key = 0, Value = "全部" });
                }
                else
                {
                    tempSalepointList.AddRange(biz.GetSalePointInfoById(UserInfoModel.PageModel.SalePointSEQ ?? 0));
                }
                ViewBag.IsAdmin = "false";
            }
            pageModel.GroupSource = new SelectList(groupList, "Key", "Value");
            pageModel.CompanySource = new SelectList(tempCompanyList, "Key", "Value");
            pageModel.SalePointSource = new SelectList(tempSalepointList, "Key", "Value");
            return View(pageModel);
        }

        [HttpPost]
        public JsonResult UserInfoEdit(UserPageModel model)
        {
            var result = biz.EditUserInfo(model);
            return Json(new { result = result, message = model.Message });
        }

    }
}