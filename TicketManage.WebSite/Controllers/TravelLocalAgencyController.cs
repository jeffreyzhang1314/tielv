using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Entity.TravelLocalAgencyManage;
using TicketManage.WebSite.Filters;
using TicketManage.WebSite.Logic;
using TicketManage.WebSite.Models;
using TieckManage.Biz.CommonManage;
using TieckManage.Biz.TravelLocalAgencyManage;

namespace TicketManage.WebSite.Controllers
{
    public class TravelLocalAgencyController : BaseController
    {

        private CommonBiz biz = new CommonBiz();
        /// <summary>
        /// 地接社业务实体
        /// </summary>
        private TravelLocalAgencyBiz tBiz = new TravelLocalAgencyBiz();

        [HttpGet]
        public ActionResult Login()
        {
            ViewBag.Title = "地接管理系统登录";
            var pageModel = new UserLoginPageModel();
            pageModel.MessageState = false;
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult Login(UserLoginPageModel model)
        {
            ViewBag.Title = "地接管理系统登录";
            model.SysteCategory = SystemName.地接管理系统;
            model.SystemName = "地接管理系统";
            var result = biz.CommonLogin(model);
            if (result)
            {
                if (CheckUserAuth(model.PageModel.AuthNum, model.SysteCategory))
                {
                    Session[UserInfo] = model;
                    return RedirectToAction("TravelLocalAgencyList");
                }
                else
                {
                    model.Message = "您没有权限访问地接管理系统！";
                    model.MessageState = true;
                }
            }
            else
            {
                model.Message = "用户名密码错误！";
                model.MessageState = true;
            }
            return View(model);
        }

        #region 地接社信息管理

        /// <summary>
        /// 初始化查询区域需要数据源
        /// </summary>
        /// <param name="pageModel"></param>
        private void InitIndex(TravelLocalAgencyPageModel pageModel)
        {
            ViewBag.TravelLocalAgencyNav = "active open";
            ViewBag.TravelLocalAgencyListNav = "active";
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
            pageModel.CategoryAreaSource = ConvertSystemCode(tBiz.GetSystemCodeListByType("CategoryArea"));
            pageModel.CategoryBizSource = ConvertSystemCode(tBiz.GetSystemCodeListByType("TravelArea"));
        }

        [HttpGet, LoginCheck]
        public ActionResult TravelLocalAgencyList()
        {
            var pageModel = new TravelLocalAgencyPageModel();
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            InitIndex(pageModel);
            pageModel.ModelList = tBiz.GetTravelLocalAgencyByAll(pageModel);
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult TravelLocalAgencyList(TravelLocalAgencyPageModel model)
        {
            InitIndex(model);
            model.ModelList = tBiz.GetTravelLocalAgencyByCondition(model);
            return View(model);
        }

        [AjaxOnly, LoginCheck]
        public JsonResult TravelLocalAgencyDel(int id)
        {
            var result = tBiz.TravelLocalAgencyDel(id);
            return Json(new { result = result, message = result ? "删除成功！" : "未找到要删除数据！" });
        }

        [HttpGet, LoginCheck]
        public PartialViewResult TravelLocalAgencyAdd()
        {
            var pageModel = new TravelLocalAgencyPageModel();
            InitIndex(pageModel);
            pageModel.PageModel = new Repository.Models.TravelLocalAgency();
            pageModel.PageModel.TravelNo = String.Format("DJ_No{0}", DateTime.Now.ToString("yyyyMMddHHmmss"));
            pageModel.PageModel.Point = 60;
            return PartialView("_TravelLocalAgencyAdd", pageModel);
        }

        [HttpGet, LoginCheck]
        public PartialViewResult TravelLocalAgencyEdit(int id)
        {
            var pageModel = new TravelLocalAgencyPageModel();
            InitIndex(pageModel);
            pageModel.PageModel = tBiz.GetTravelLocalAgencyBySEQ(id);
            return PartialView("_TravelLocalAgencyAdd", pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult TravelLocalAgencyEdit(TravelLocalAgencyPageModel model)
        {
            var uploadFile = base.FileUpload(Request);
            foreach (var item in uploadFile)
            {
                model.PageModel.CooperationInfo = item.Key;
            }
            var result = tBiz.TravelLocalAgencyEdit(model);
            return RedirectToAction("TravelLocalAgencyList");
        }


        #endregion

        #region 地接社比较管理

        private void TravelLocalAgencyCompareInit(TravelAgencyPageModel pageModel)
        {
            ViewBag.TravelLocalAgencyNav = "active open";
            ViewBag.TravelLocalAgencyCompareListNav = "active";
            var tempSource = new Dictionary<string, string>();
            for (int i = 0; i < 5; i++)
            {
                var year = DateTime.Now.Year - 2 + i;
                tempSource.Add(year.ToString(), year.ToString());
            }
            pageModel.RecordDateSource = new SelectList(tempSource, "Key", "Value", DateTime.Now.Year);
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
        }

        [HttpGet, LoginCheck]
        public ActionResult TravelLocalAgencyCompareList()
        {
            var pageModel = new TravelAgencyPageModel();
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            TravelLocalAgencyCompareInit(pageModel);
            pageModel.ModelList = tBiz.GetTravelAgencyByAll(pageModel);
            var seqList = pageModel.ModelList.Select(a => a.SEQ).ToList();
            pageModel.PluginList = tBiz.GetTravelAreaPlugInByTravelAreaSEQ(seqList);
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult TravelLocalAgencyCompareList(TravelAgencyPageModel model)
        {
            TravelLocalAgencyCompareInit(model);
            model.ModelList = tBiz.GetTravelAgencyByCondition(model);
            var seqList = model.ModelList.Select(a => a.SEQ).ToList();
            model.PluginList = tBiz.GetTravelAreaPlugInByTravelAreaSEQ(seqList);
            return View(model);
        }

        [AjaxOnly, LoginCheck]
        public JsonResult TravelLocalAgencyCompareDel(int id)
        {
            var result = tBiz.TravelAgencyDel(id);
            return Json(new { result = result, message = result ? "删除成功！" : "删除失败！" });
        }

        [HttpGet, LoginCheck]
        public PartialViewResult TravelLocalAgencyCompareEdit(int? id)
        {
            var pageModel = new TravelAgencyPageModel();
            TicketManageLogic logic = new TicketManageLogic();
            if (id != null)
            {
                pageModel.PageModel = tBiz.GetTravelAgencyBySEQ(id ?? 0);
                pageModel.PluginList = tBiz.GetTravelAreaPlugInByTravelAreaSEQ(new List<int> { id ?? 0 });

                var tempRoomObj = pageModel.PluginList.Where(a => a.PlugInCategory == 1);
                if (tempRoomObj != null && tempRoomObj.Count() > 0)
                {
                    pageModel.RoomSummaryNum = tempRoomObj.Count();
                    pageModel.RoomSEQ = tempRoomObj.FirstOrDefault().SEQ;
                    pageModel.RoomSummaryText = tempRoomObj.FirstOrDefault().Summary;
                    pageModel.RoomPriceText = tempRoomObj.FirstOrDefault().PlugInPirce;
                }

                var tempFoodObj = pageModel.PluginList.Where(a => a.PlugInCategory == 2);
                if (tempFoodObj != null && tempFoodObj.Count() > 0)
                {
                    pageModel.FoodSummaryNum = tempFoodObj.Count();
                    pageModel.FoodSEQ = tempFoodObj.FirstOrDefault().SEQ;
                    pageModel.FoodSummaryText = tempFoodObj.FirstOrDefault().Summary;
                    pageModel.FoodPriceText = tempFoodObj.FirstOrDefault().PlugInPirce;
                }

                var tempCarObj = pageModel.PluginList.Where(a => a.PlugInCategory == 3);
                if (tempCarObj != null && tempCarObj.Count() > 0)
                {
                    pageModel.CarSummaryNum = tempCarObj.Count();
                    pageModel.CarSEQ = tempCarObj.FirstOrDefault().SEQ;
                    pageModel.CarSummaryText = tempCarObj.FirstOrDefault().Summary;
                    pageModel.CarPriceText = tempCarObj.FirstOrDefault().PlugInPirce;
                }


                pageModel.TravelAreaList = logic.CodeListToSelectList(tBiz.GetSystemCodeListByType("TravelArea"));
            }
            pageModel.TravelAreaList = logic.CodeListToSelectList(tBiz.GetSystemCodeListByType("TravelArea"));
            return PartialView("_TravelLocalAgencyCompareEdit", pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult TravelLocalAgencyCompareEdit(TravelAgencyPageModel model)
        {
            model.PluginList = FileUploadByTravelAgency(Request);
            var fileModel = model.PluginList.Where(a => a.PlugInCategory == 4 && a.Summary == "PageModelPlugin.TravelAgencyFile").FirstOrDefault();
            if (fileModel != null)
            {
                model.PageModel.TravelAgencyFile = fileModel.FileContent;
            }
            model.PluginList.Remove(fileModel);
            tBiz.TravelAgencyEdit(model);
            return RedirectToAction("TravelLocalAgencyCompareList");
        }

        [AjaxOnly, LoginCheck]
        public JsonResult TravelAgencyCompareDel(int id)
        {
            var result = tBiz.TravelAgencyPlugInsDel(id);
            return Json(new { result = result, message = result ? "操作成功！" : "操作失败！" });
        }

        /// <summary>
        /// 导出Excel文件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult ExcelExportTravel(TravelAgencyPageModel model)
        {
            Session["ExcelExportTravel"] = model;
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 生成下载文件
        /// </summary>
        /// <returns></returns>
        public FileContentResult ExcelDownLoadTravel()
        {
            var titleList = new List<string>() { "旅游段", "地接社名称", "日期", "地接成本", "联系方式", "门票", "房价格", "房描述", "餐价格", "餐描述", "车价格", "车描述", "自费项目", "购物店" };
            var searchCondition = Session["ExcelExportTravel"] as TravelAgencyPageModel;
            var dataResult = tBiz.ShowExportTravelAgency();
            var book = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = book.CreateSheet("ExcelExport");

            var tableBorderStyle = book.CreateCellStyle();
            tableBorderStyle.BorderBottom = BorderStyle.Thin;
            tableBorderStyle.BorderLeft = BorderStyle.Thin;
            tableBorderStyle.BorderRight = BorderStyle.Thin;
            tableBorderStyle.BorderTop = BorderStyle.Thin;
            var tableContentFont = book.CreateFont();
            tableContentFont.FontName = "微软雅黑";
            tableContentFont.FontHeightInPoints = 12;
            tableBorderStyle.SetFont(tableContentFont);

            var tableTitleStyle = book.CreateCellStyle();
            tableTitleStyle.BorderBottom = BorderStyle.Thin;
            tableTitleStyle.BorderLeft = BorderStyle.Thin;
            tableTitleStyle.BorderRight = BorderStyle.Thin;
            tableTitleStyle.BorderTop = BorderStyle.Thin;
            var tableTitleFont = book.CreateFont();
            tableTitleFont.FontName = "微软雅黑";
            tableTitleFont.FontHeightInPoints = 12;
            tableTitleFont.Boldweight = short.MaxValue;
            tableTitleStyle.SetFont(tableTitleFont);

            ICellStyle style = book.CreateCellStyle();

            //设置单元格的样式：水平对齐居中
            style.Alignment = HorizontalAlignment.Center;
            //新建一个字体样式对象
            IFont font = book.CreateFont();
            //设置字体加粗样式
            font.Boldweight = short.MaxValue;
            font.FontName = "微软雅黑";
            font.FontHeightInPoints = 16;
            //使用SetFont方法将字体样式添加到单元格样式中 
            style.SetFont(font);
            //将新的样式赋给单元格
            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, titleList.Count));

            var row0 = sheet.CreateRow(0);
            row0.CreateCell(0).SetCellValue("地接社比较列表");
            row0.GetCell(0).CellStyle = style;


            var indexVal = 0;

            var rowTitle = sheet.CreateRow(indexVal + 3);
            for (int i = 0; i < titleList.Count; i++)
            {
                rowTitle.CreateCell(i).SetCellValue(titleList[i]);
                rowTitle.GetCell(i).CellStyle = tableTitleStyle;
            }

            indexVal = 4;

            foreach (var item in dataResult)
            {
                var row = sheet.CreateRow(indexVal);
                row.CreateCell(0).SetCellValue(item.TravelAreaCodeName);
                row.CreateCell(1).SetCellValue(item.TravelAgencyName);
                if (item.RecordDate != null)
                {
                    row.CreateCell(2).SetCellValue(Convert.ToDateTime(item.RecordDate).ToString("yyyy-MM-dd"));
                }
                else
                {
                    row.CreateCell(2).SetCellValue(string.Empty);
                }
                row.CreateCell(3).SetCellValue(item.AgencyBaseMoney);
                row.CreateCell(4).SetCellValue(item.ContactInfo);
                row.CreateCell(5).SetCellValue(item.TicketGate);
                row.CreateCell(6).SetCellValue(item.RoomPirce);
                row.CreateCell(7).SetCellValue(item.RoomSummary);
                row.CreateCell(8).SetCellValue(item.FoodPirce);
                row.CreateCell(9).SetCellValue(item.FoodSummary);
                row.CreateCell(10).SetCellValue(item.CarPirce);
                row.CreateCell(11).SetCellValue(item.CarSummary);
                row.CreateCell(12).SetCellValue(item.BuyItem);
                row.CreateCell(13).SetCellValue(item.StoreName);
                for (int i = 0; i < titleList.Count; i++)
                {
                    row.GetCell(i).CellStyle = tableBorderStyle;
                }
                indexVal += 1;
            }

            for (int i = 0; i < titleList.Count; i++)
            {
                sheet.AutoSizeColumn(i);
            }

            // 写入到客户端  
            var ms = new System.IO.MemoryStream();
            book.Write(ms);
            var fileName = string.Format("{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"));
            return File(ms.ToArray(), "application/ms-excel", fileName);

        }

        #endregion

        #region 会议记录管理

        private void MeetingRecordListInit(MeetingInfoPageModel model)
        {
            ViewBag.TravelLocalAgencyNav = "active open";
            ViewBag.MeetingRecordList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
            model.OperatorAuthNum = CommonUserInfoModel.PageModel.AuthNum;
        }

        [HttpGet, LoginCheck]
        public ActionResult MeetingRecordList()
        {
            var model = new MeetingInfoPageModel() { PageSize = 10, CurrentPage = 1 };
            MeetingRecordListInit(model);
            model.ModelList = tBiz.GetMeetingInfoByCondition(model);
            return View(model);
        }

        [HttpPost, LoginCheck]
        public ActionResult MeetingRecordList(MeetingInfoPageModel model)
        {
            MeetingRecordListInit(model);
            model.ModelList = tBiz.GetMeetingInfoByCondition(model);
            return View(model);
        }

        [HttpGet, LoginCheck]
        public PartialViewResult MeetingRecordEdit(int? id)
        {
            var pageModel = new MeetingInfoPageModel();
            if (id != null)
            {
                pageModel.PageModel = tBiz.GetMeetingRecordBySEQ(id ?? 0);
            }
            return PartialView("_MeetingRecordEdit", pageModel);
        }


        [HttpPost, LoginCheck]
        public ActionResult MeetingRecordFormEdit(MeetingInfoPageModel model)
        {
            var resultList = FileUpload(Request);
            model.PageModel.FileInfo = resultList.FirstOrDefault().Key;
            var pageModel = tBiz.MeetingRecordEdit(model);
            return RedirectToAction("MeetingRecordList");
        }

        [AjaxOnly, LoginCheck]
        public JsonResult MeetingRecordEdit(MeetingInfoPageModel model)
        {
            var resultList = FileUpload(Request);
            model.PageModel.FileInfo = resultList.FirstOrDefault().Key;
            var pageModel = tBiz.MeetingRecordEdit(model);
            return Json(new { result = pageModel, message = pageModel ? "操作成功！" : "操作失败！" });
        }

        [AjaxOnly, LoginCheck]
        public JsonResult MeetingRecordDel(int id)
        {
            var result = tBiz.MeetingRecordDel(id);
            return Json(new { result = result, message = result ? "删除成功！" : "删除失败！" });
        }

        #endregion

        #region 景点管理

        private void TravelAreaListInit(TravelAreaListPageModel pageModel)
        {
            ViewBag.TravelLocalAgencyNav = "active open";
            ViewBag.TravelAreaList = "active";
            pageModel.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", pageModel.PageSize);
        }

        [HttpGet, LoginCheck]
        public ActionResult TravelAreaList()
        {
            var pageModel = new TravelAreaListPageModel();
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            TravelAreaListInit(pageModel);
            pageModel.ModelList = tBiz.GetTravelAreaByCondition(pageModel);
            return View(pageModel);
        }



        [HttpPost, LoginCheck]
        public ActionResult TravelAreaList(TravelAreaListPageModel model)
        {
            TravelAreaListInit(model);
            model.ModelList = tBiz.GetTravelAreaByCondition(model);
            return View(model);
        }

        [HttpGet, LoginCheck]
        public ActionResult TravelAreaPointList(int id)
        {
            var pageModel = new TravelAreaListPageModel();
            pageModel.TraveAreaSEQ = id;
            pageModel.PageSize = 10;
            pageModel.CurrentPage = 1;
            TravelAreaListInit(pageModel);
            pageModel.ModelList = tBiz.GetTravelAreaPointByCondition(pageModel);
            return View(pageModel);
        }
        [HttpPost, LoginCheck]
        public ActionResult TravelAreaPointList(TravelAreaListPageModel model)
        {
            TravelAreaListInit(model);
            model.ModelList = tBiz.GetTravelAreaPointByCondition(model);
            return View(model);
        }

        [HttpGet, LoginCheck]
        public PartialViewResult TravelAreaPointEdit(int? id, int travelSEQ)
        {
            var pageModel = new TravelAreaListPageModel();
            pageModel.TraveAreaSEQ = travelSEQ;
            if (id != null)
            {
                pageModel.PageModel = tBiz.GetTravelAreaBySEQ(id ?? 0);
            }
            return PartialView("_TravelAreaPointEdit", pageModel);
        }

        [AjaxOnly, LoginCheck]
        public JsonResult TravelAreaPointEdit(TravelAreaListPageModel model)
        {
            var result = tBiz.TravelAreaPointEdit(model);
            return Json(new { result = result, message = result ? "操作成功！" : "操作失败！" });
        }

        [HttpGet, LoginCheck]
        public PartialViewResult TravelAreaEdit(int? id)
        {
            var pageModel = new TravelAreaListPageModel();
            if (id != null)
            {
                pageModel.PageModel = tBiz.GetTravelAreaBySEQ(id ?? 0);
            }
            return PartialView("_TravelAreaEdit", pageModel);
        }



        [AjaxOnly, LoginCheck]
        public JsonResult TravelAreaEdit(TravelAreaListPageModel model)
        {
            var result = tBiz.TravelAreaEdit(model);
            return Json(new { result = result, message = result ? "操作成功！" : "操作失败！" });
        }

        [AjaxOnly, LoginCheck]
        public JsonResult TravelAreaDel(int id)
        {
            var result = tBiz.TravelAreaDel(id);
            return Json(new { result = result, message = result ? "删除成功！" : "删除失败！" });
        }

        #endregion

    }
}
