using System;
using System.Web.Mvc;
using System.Configuration;
using TicketManage.Entity.TicketManage;
using TieckManage.Biz.TicketManage;
using TicketManage.WebSite.Logic;
using TicketManage.WebSite.Filters;

namespace TicketManage.WebSite.Controllers
{
    /// <summary>
    /// Description of BaseController.
    /// </summary>
    [LoginCheck]
    public class TransportReportController : BaseController
    {
        public PartialViewResult TransportReportInfo(string yearStr, string monthStr)
        {
            TransportReportManageBiz biz = new TransportReportManageBiz();
            TransportReportPageModel model = new TransportReportPageModel();
            TransportReportPageModel input = new TransportReportPageModel();
            TicketManageLogic logic = new TicketManageLogic();
            ViewBag.BaseDataNav = "active open";
            ViewBag.TReport = "active";
            if (input.PageModel == null)
            {
                input.PageModel = new Repository.Models.TransportReport();
                input.PageModel.YearNum = DateTime.Now.Year.ToString();
                input.PageModel.MonthNum = DateTime.Now.Month.ToString();
            }
            else
            {
                input.PageModel.YearNum = yearStr;
                input.PageModel.MonthNum = monthStr;
            }
            model.PageModel = biz.GetTransportReportByQueryable(input);
            model.MonthList = new System.Collections.Generic.List<SelectListItem>() { 
            new SelectListItem(){Value="1",Text="1月"},
            new SelectListItem(){Value="2",Text="2月"},
            new SelectListItem(){Value="3",Text="3月"},
            new SelectListItem(){Value="4",Text="4月"},
            new SelectListItem(){Value="5",Text="5月"},
            new SelectListItem(){Value="6",Text="6月"},
            new SelectListItem(){Value="7",Text="7月"},
            new SelectListItem(){Value="8",Text="8月"},
            new SelectListItem(){Value="9",Text="9月"},
            new SelectListItem(){Value="10",Text="10月"},
            new SelectListItem(){Value="11",Text="11月"},
            new SelectListItem(){Value="12",Text="12月"},
            };
            foreach (var item in model.MonthList)
            {
                if (item.Value == DateTime.Now.Month.ToString())
                {
                    item.Selected = true;
                }
            }
            int year = DateTime.Now.Year;
            model.YearList = new System.Collections.Generic.List<SelectListItem>() { 
                new SelectListItem(){Value=(year-4).ToString(),Text=(year-4).ToString()},
                new SelectListItem(){Value=(year-3).ToString(),Text=(year-3).ToString()},
                new SelectListItem(){Value=(year-2).ToString(),Text=(year-2).ToString()},
                new SelectListItem(){Value=(year-1).ToString(),Text=(year-1).ToString()},
                new SelectListItem(){Value=year.ToString(),Text=year.ToString(),Selected=true},
            };
            return PartialView(model);
        }

        public ActionResult _PartialTransportReportList(TransportReportPageModel input)
        {
            return PartialView("~/Views/TransportReport/_PartialTransportReportList.cshtml", input);
        }

        public ActionResult TransportReportList(TransportReportPageModel input)
        {
            ViewBag.ReportNav = "active open";
            ViewBag.TransportReportList = "active";

            TransportReportManageBiz biz = new TransportReportManageBiz();
            TransportReportPageModel model = new TransportReportPageModel();
            model.MonthList = new System.Collections.Generic.List<SelectListItem>() { 
            new SelectListItem(){Value="1",Text="1月"},
            new SelectListItem(){Value="2",Text="2月"},
            new SelectListItem(){Value="3",Text="3月"},
            new SelectListItem(){Value="4",Text="4月"},
            new SelectListItem(){Value="5",Text="5月"},
            new SelectListItem(){Value="6",Text="6月"},
            new SelectListItem(){Value="7",Text="7月"},
            new SelectListItem(){Value="8",Text="8月"},
            new SelectListItem(){Value="9",Text="9月"},
            new SelectListItem(){Value="10",Text="10月"},
            new SelectListItem(){Value="11",Text="11月"},
            new SelectListItem(){Value="12",Text="12月"}
            };
            foreach (var item in model.MonthList)
            {
                if (item.Value == DateTime.Now.Month.ToString())
                {
                    item.Selected = true;
                }
            }
            int year = DateTime.Now.Year;
            model.YearList = new System.Collections.Generic.List<SelectListItem>() { 
                new SelectListItem(){Value=(year-4).ToString(),Text=(year-4).ToString()},
                new SelectListItem(){Value=(year-3).ToString(),Text=(year-3).ToString()},
                new SelectListItem(){Value=(year-2).ToString(),Text=(year-2).ToString()},
                new SelectListItem(){Value=(year-1).ToString(),Text=(year-1).ToString()},
                new SelectListItem(){Value=year.ToString(),Text=year.ToString(),Selected=true}
            };
            if (input.PageModel == null)
            {
                input.PageModel = new Repository.Models.TransportReport();
                input.PageModel.YearNum = year.ToString();
                input.PageModel.MonthNum = DateTime.Now.Month.ToString();
            }
            model.PageModel = biz.GetTransportReportPageInfo(input);
            return View(model);
        }


        [HttpPost]
        public JsonResult SaveTransportReport(TransportReportPageModel model)
        {
            JsonResult result = new JsonResult();
            bool res = false;
            TransportReportManageBiz biz = new TransportReportManageBiz();
            model.PageModel.CreateTime = DateTime.Now;
            model.PageModel.OperationSEQ = 1;
            res = biz.SaveTransportReport(model);
            if (res)
            {
                result.Data = "保存成功";
            }
            else
            {
                result.Data = "保存失败";
            }
            return result;
        }

        public ActionResult LoadTransportReportInfo(string yearStr, string monthStr)
        {
            TransportReportManageBiz biz = new TransportReportManageBiz();
            TransportReportPageModel model = new TransportReportPageModel();
            model.PageModel = new Repository.Models.TransportReport();
            model.PageModel.YearNum = yearStr;
            model.PageModel.MonthNum = monthStr;
            model.PageModel = biz.GetTransportReportByQueryable(model);
            return View(model);
        }
    }
}
