﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.TravelLocalAgencyManage;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.TravelLocalAgencyManage;

namespace TicketManage.WebSite.Controllers
{
    [LoginCheck]
    public class TravelAgencyPointController : BaseController
    {

        private TravelAgencyPointBiz TravelPointBiz = new TravelAgencyPointBiz();

        private void TravelAgencyPointListInit(TravelAgencyPointPageModel model)
        {
            ViewBag.TravelLocalAgencyNav = "active open";
            ViewBag.TravelLocalAgencyListNav = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
        }

        [HttpGet]
        public ActionResult TravelAgencyPointList(int id, string name)
        {
            var model = new TravelAgencyPointPageModel() { PageSize = 10, CurrentPage = 1 };
            TravelAgencyPointListInit(model);
            model.PageModel = new Repository.Models.TravelAgencyPoint() { AgencySEQ = id, TravelName = name };
            model.ModelList = TravelPointBiz.SearchTravelAgencyPointByCondition(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult TravelAgencyPointList(TravelAgencyPointPageModel model)
        {
            TravelAgencyPointListInit(model);
            model.ModelList = TravelPointBiz.SearchTravelAgencyPointByCondition(model);
            return View(model);
        }

        /// <summary>
        /// 显示增加评分详细页面
        /// </summary>
        /// <param name="id">地接社序号</param>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult ShowTravelAgencyPointAdd(int id, string name)
        {
            var model = new TravelAgencyPointPageModel();
            model.PageModel = new Repository.Models.TravelAgencyPoint() { AgencySEQ = id, TravelName = name };
            return PartialView("_TravelAgencyPointEdit", model);
        }

        /// <summary>
        /// 显示编辑评分详细页面
        /// </summary>
        /// <param name="id">评分详细序号</param>
        /// <returns></returns>
        [AjaxOnly]
        public PartialViewResult ShowTravelAgencyPointEdit(int id)
        {
            var model = new TravelAgencyPointPageModel();
            model.PageModel = TravelPointBiz.GetTravelAgencyPointBySEQ(id);
            return PartialView("_TravelAgencyPointEdit", model);
        }


        /// <summary>
        /// 添加地接社评分详细信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult TravelAgencyPointEdit(TravelAgencyPointPageModel model)
        {
            var uploadFile = base.FileUpload(Request);
            foreach (var item in uploadFile)
            {
                if (string.Compare(item.Value, "PointFile", true) == 0)
                {
                    model.PageModel.PointFile = item.Key;
                }
                if (string.Compare(item.Value, "CooperationInfo", true) == 0)
                {
                    model.PageModel.CooperationInfo = item.Key;
                }
            }
            var result = TravelPointBiz.EditTravelAgencyPoint(model);
            return RedirectToAction("TravelAgencyPointList", new { id = model.PageModel.AgencySEQ, name = model.PageModel.TravelName });
        }

        [AjaxOnly]
        public JsonResult TravelAgencyPointDel(int id)
        {
            var result = TravelPointBiz.DelTravelAgencyPoint(id);
            return Json(new { result = result, message = result ? "操作成功" : "操作失败" }, JsonRequestBehavior.AllowGet);
        }

    }
}
