﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.KPIDataManage;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.KPIDataManage;

namespace TicketManage.WebSite.Controllers
{

    /// <summary>
    /// 功效系统基础数据
    /// </summary>
    [LoginCheckKPI]
    public class KPIDataController : BaseController
    {

        #region 公司部门维护

        private KPIDeptInfoBiz DeptBiz = new KPIDeptInfoBiz();

        public void OrganizationListInit(GX_DeptInfoPageModel model)
        {
            ViewBag.KPIDataNav = "active open";
            ViewBag.OrganizationList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
            model.ParentList = new SelectList(DeptBiz.GetDeptInfoByParent(), "seq", "DeptName");
        }

        /// <summary>
        /// 公司部门列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OrganizationList()
        {
            var pageModel = new GX_DeptInfoPageModel() { PageSize = 10, CurrentPage = 1 };
            OrganizationListInit(pageModel);
            pageModel.ModelList = DeptBiz.SearchDeptInfoByCondition(pageModel);
            return View(pageModel);
        }

        /// <summary>
        /// 公司部门列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OrganizationList(GX_DeptInfoPageModel model)
        {
            OrganizationListInit(model);
            model.ModelList = DeptBiz.SearchDeptInfoByCondition(model);
            return View(model);
        }

        [AjaxOnly]
        public PartialViewResult ShowDeptInfoAdd()
        {
            var model = new GX_DeptInfoPageModel();
            model.ParentList = new SelectList(DeptBiz.GetDeptInfoByParent(), "seq", "DeptName");
            return PartialView("PartialOrganizationEdit", model);
        }

        [AjaxOnly]
        public PartialViewResult ShowDeptInfoEdit(int id)
        {
            var model = new GX_DeptInfoPageModel();
            model.PageModel = DeptBiz.GetDeptInfoBySEQ(id);
            model.ParentList = new SelectList(DeptBiz.GetDeptInfoByParent(), "seq", "DeptName");
            ViewBag.ParentList = new SelectList(DeptBiz.GetDeptInfoByParent(), "seq", "DeptName");
            return PartialView("PartialOrganizationEdit", model);
        }

        [AjaxOnly]
        public JsonResult DeptInfoEdit(GX_DeptInfoPageModel model)
        {
            var result = DeptBiz.EditDeptInfo(model);
            return Json(new { result = result, message = model.Message }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult DeptInfoDel(int id)
        {
            var result = DeptBiz.DelDeptInfo(id);
            return Json(new { result = result, message = result ? "操作成功" : "操作失败" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 级别维护

        private KPIRankInfoBiz RankBiz = new KPIRankInfoBiz();

        private void RankListInit(GX_RankInfoPageModel model)
        {
            ViewBag.KPIDataNav = "active open";
            ViewBag.RankList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
        }

        /// <summary>
        /// 级别信息列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RankList()
        {
            var model = new GX_RankInfoPageModel() { PageSize = 10, CurrentPage = 1 };
            RankListInit(model);
            model.ModelList = RankBiz.SearchRankInfoByCondition(model);
            return View(model);
        }

        /// <summary>
        /// 级别信息列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RankList(GX_RankInfoPageModel model)
        {
            RankListInit(model);
            model.ModelList = RankBiz.SearchRankInfoByCondition(model);
            return View(model);
        }

        [HttpGet]
        public PartialViewResult ShowRankAdd()
        {
            var model = new GX_RankInfoPageModel() { PageModel = new Repository.Models.GX_RankInfo { OrderNo = 0, CreateTime = DateTime.Now } };
            return PartialView("PartialRankEdit", model);
        }

        [HttpGet]
        public PartialViewResult ShowRankEdit(int id)
        {
            var model = new GX_RankInfoPageModel();
            model.PageModel = RankBiz.GetRankInfoBySEQ(id);
            return PartialView("PartialRankEdit", model);
        }

        [AjaxOnly]
        public JsonResult RankEdit(GX_RankInfoPageModel model)
        {
            var result = RankBiz.EditRankInfo(model);
            return Json(new { result = result, message = model.Message });
        }

        /// <summary>
        /// 删除级别信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult RankDel(int id)
        {
            var result = RankBiz.DelRankInfo(id);
            return Json(new { Result = true, message = "删除成功" }, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region 职务维护

        private KPIDutyInfoBiz DutyBiz = new KPIDutyInfoBiz();

        private void DutyListInit(GX_DutyInfoPageModel model)
        {
            ViewBag.KPIDataNav = "active open";
            ViewBag.DutyList = "open";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
        }

        /// <summary>
        /// 职务维护信息列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DutyList()
        {
            var model = new GX_DutyInfoPageModel() { PageSize = 10, CurrentPage = 1 };
            DutyListInit(model);
            model.ModelList = DutyBiz.SearchDeptInfoByCondition(model);
            return View(model);
        }

        /// <summary>
        /// 职务维护信息列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DutyList(GX_DutyInfoPageModel model)
        {
            DutyListInit(model);
            model.ModelList = DutyBiz.SearchDeptInfoByCondition(model);
            return View(model);
        }

        [HttpGet]
        public PartialViewResult ShowDutyAdd()
        {
            var pageModel = new GX_DutyInfoPageModel();
            return PartialView("PartialDutyEdit", pageModel);
        }

        [HttpGet]
        public PartialViewResult ShowDutyEdit(int id)
        {
            var pageModel = new GX_DutyInfoPageModel();
            pageModel.PageModel = DutyBiz.GetDutyInfoBySEQ(id);
            return PartialView("PartialDutyEdit", pageModel);
        }

        public JsonResult DutyDel(int id)
        {
            var result = DutyBiz.DelDutyInfo(id);
            return Json(new { data = result, message = result ? "操作成功！" : "操作失败！" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DutyEdit(GX_DutyInfoPageModel model)
        {
            var result = DutyBiz.EditDutyInfo(model);
            return Json(new { result = result, message = result ? "操作成功" : "操作失败" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 岗位维护

        private KPIPostionInfoBiz PostionBiz = new KPIPostionInfoBiz();

        private void PostionListInit(GX_PostionInfoPageModel model)
        {
            ViewBag.KPIDataNav = "active open";
            ViewBag.PostionList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
        }

        /// <summary>
        /// 岗位维护信息列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PostionList()
        {
            var model = new GX_PostionInfoPageModel() { PageSize = 10, CurrentPage = 1 };
            PostionListInit(model);
            model.ModelList = PostionBiz.SearchPostionInfoByCondition(model);
            return View(model);
        }

        /// <summary>
        /// 岗位维护信息列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PostionList(GX_PostionInfoPageModel model)
        {
            PostionListInit(model);
            model.ModelList = PostionBiz.SearchPostionInfoByCondition(model);
            return View(model);
        }

        [HttpGet]
        public PartialViewResult ShowPostionAdd()
        {
            var model = new GX_PostionInfoPageModel() { PageModel = new Repository.Models.GX_PostionInfo { OrderNo = 0, CreateTime = DateTime.Now } };
            return PartialView("PartialPostionEdit", model);
        }

        [HttpGet]
        public PartialViewResult ShowPostionEdit(int id)
        {
            var model = new GX_PostionInfoPageModel();
            model.PageModel = PostionBiz.GetPostionBySEQ(id);
            return PartialView("PartialPostionEdit", model);
        }

        [AjaxOnly]
        public JsonResult PostionEdit(GX_PostionInfoPageModel model)
        {
            var result = PostionBiz.EditPostionInfo(model);
            return Json(new { result = result, message = model.Message });
        }

        /// <summary>
        /// 删除级别信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult PostionDel(int id)
        {
            var result = PostionBiz.DelPostionInfo(id);
            return Json(new { Result = true, message = "删除成功" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 人员维护

        private KPIEmployeeInfoBiz EmployeeBiz = new KPIEmployeeInfoBiz();

        private void EmployeeListInit(GX_EmployeeInfoPageModel model)
        {
            ViewBag.KPIDataNav = "active open";
            ViewBag.EmployeeList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
            model.SexSource = new SelectList(SchSexSource, "Key", "Value");
            model.DeptSource = new SelectList(EmployeeBiz.GetDeptInfoByParent(), "seq", "DeptName");
            var tempList = EmployeeBiz.GetPostionByAll();
            model.PostionSource = new SelectList(tempList, "seq", "PostionName");
            model.DutySource = new SelectList(EmployeeBiz.GetDutyByAll(), "seq", "DutyName");
            model.EmployeeStateSource = new SelectList(EmployeeBiz.GetSystemCodeListByType("EmployeeState"), "CodeNo", "CodeName");

        }

        /// <summary>
        /// 人员台帐信息列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EmployeeList()
        {
            var model = new GX_EmployeeInfoPageModel() { PageSize = 10, CurrentPage = 1 };
            EmployeeListInit(model);
            model.ModelList = EmployeeBiz.SearchEmployeeInfoByCondition(model);
            return View(model);
        }

        /// <summary>
        /// 人员台帐信息列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EmployeeList(GX_EmployeeInfoPageModel model)
        {
            EmployeeListInit(model);
            model.ModelList = EmployeeBiz.SearchEmployeeInfoByCondition(model);
            return View(model);
        }

        [AjaxOnly]
        public PartialViewResult ShowEmployeeAdd()
        {
            var model = new GX_EmployeeInfoPageModel();
            EmployeeListInit(model);
            return PartialView("PartialEmployeeEdit", model);
        }

        [AjaxOnly]
        public PartialViewResult ShowEmployeeEdit(int id)
        {
            var model = new GX_EmployeeInfoPageModel();
            EmployeeListInit(model);
            model.PageModel = EmployeeBiz.GetEmployeeInfoBySEQ(id);
            return PartialView("PartialEmployeeEdit", model);
        }

        [AjaxOnly]
        public JsonResult EmployeeDel(int id)
        {
            var result = EmployeeBiz.DelEmployeeInfo(id);
            return Json(new { data = result, messasge = result ? "操作成功" : "操作失败" }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult EmployeeEdit(GX_EmployeeInfoPageModel model)
        {
            var result = false;
            if (model.PageModel.seq > 0)
            {
                result = EmployeeBiz.EditEmployeeInfo(model);
            }
            else if (!EmployeeBiz.CheckUserNo(model.PageModel.EmployeeNo))
            {
                result = EmployeeBiz.EditEmployeeInfo(model);
            }
            else
            {
                model.Message = "登录名已经存在";
            }
            return Json(new { result = result, message = model.Message }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult EmployeeIsJob(int id)
        {
            var result = EmployeeBiz.EmployeeIsJob(id);
            return Json(new { result = result, messasge = result ? "操作成功" : "操作失败" }, JsonRequestBehavior.AllowGet);
        }


        [AjaxOnly]
        public JsonResult ShowPostionDetail(int id)
        {
            var list = EmployeeBiz.GetPostionByAll();
            var result = list.Where(a => a.seq == id).FirstOrDefault();
            return Json(new { result = result, messasge = result != null ? "操作成功" : "操作失败" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}
