using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.TicketManage;
using TicketManage.WebSite.Filters;
using TicketManage.WebSite.Logic;
using TieckManage.Biz.TicketManage;

namespace TicketManage.WebSite.Controllers
{
    [LoginCheck]
    public class GroupController : BaseController
    {

        private GroupInfoManageBiz biz = new GroupInfoManageBiz();
        //
        // GET: /Group/
        public PartialViewResult GroupInfoManage(string id)
        {
            var model = new GroupPageModel();
            model.PageModel = biz.ShowGroupInfoById(Convert.ToInt32(id));
            return PartialView(model);
        }


        private void GroupListManageInit(GroupPageModel model)
        {
            ViewBag.BaseDataNav = "active open";
            ViewBag.GroupNav = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", 10);
            model.OperatorAuthNum = UserInfoModel.PageModel.AuthNum;
        }

        [HttpGet]
        public ActionResult GroupListManage()
        {
            var model = new GroupPageModel() { PageSize = 10, CurrentPage = 1 };
            GroupListManageInit(model);
            model.ListModel = biz.GetGroupListByQueryable(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult GroupListManage(GroupPageModel model)
        {
            GroupListManageInit(model);
            model.ListModel = biz.GetGroupListByQueryable(model);
            return View(model);
        }
        public ActionResult _PartialGroupList(GroupPageModel input)
        {
            return PartialView("~/Views/Group/_PartialGroupList.cshtml", input);
        }
        [HttpPost]
        public JsonResult SaveGroupInfo(GroupPageModel model)
        {
            var result = new JsonResult();
            bool res = false;
            TicketManageLogic logic = new TicketManageLogic();
            GroupInfoManageBiz biz = new GroupInfoManageBiz();
            model.PageModel.CreateTime = DateTime.Now;
            res = biz.SaveGroupInfo(model);
            if (res)
            {
                result.Data = "����ɹ�";
            }
            else
            {
                result.Data = "����ʧ��";
            }
            return result;
        }

        public JsonResult DeleteGroupInfo(string seq)
        {
            JsonResult result = new JsonResult();
            bool res = false;
            if (!string.IsNullOrEmpty(seq))
            {
                res = biz.DelGroupInfoById(Convert.ToInt32(seq));
            }
            if (res)
            {
                result.Data = "ɾ���ɹ���";
            }
            else
            {
                result.Data = "ɾ��ʧ�ܣ�";
            }

            return result;
        }
    }
}
