using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TicketManage.WebSite.Filters;
using TicketManage.WebSite.Logic;
using TieckManage.Biz.TicketManage;
namespace TicketManage.WebSite.Controllers
{
    /// <summary>
    /// Description of TicketController.
    /// </summary>
    [LoginCheck]
    public class TicketNoticeController : BaseController
    {


        public ActionResult TicketListManage()
        {
            ViewBag.TicketNoticeNav = "active open";
            ViewBag.TicketListManage = "active";
            Ticket_NoticoPageModel model = new Ticket_NoticoPageModel();
            TicketNoticeInfoManageBiz biz = new TicketNoticeInfoManageBiz();
            string topCount = ConfigurationManager.AppSettings["TopCount"];
            model.NocticeList = biz.GetNoticeTopList(topCount, "Notice001");
            model.RankingList = biz.GetNoticeTopList(topCount, "Notice002");
            model.ProblemList = biz.GetNoticeTopList(topCount, "Notice003");
            model.AdviceList = biz.GetNoticeTopList(topCount, "Notice004");
            return View(model);
        }
        [HttpGet]
        public ActionResult TicketDetaileList()
        {
            ViewBag.TicketNoticeNav = "active open";
            ViewBag.TicketDetaileList = "active";
            TicketNoticeInfoManageBiz biz = new TicketNoticeInfoManageBiz();
            Ticket_NoticoPageModel model = new Ticket_NoticoPageModel();
            string Category = Request.QueryString["Category"];
            TicketManageLogic logic = new TicketManageLogic();
            Ticket_NoticoPageModel input = new Ticket_NoticoPageModel();
            model.ListModel = biz.GetTicket_NoticeInfoListByCatgory(Category);
            string filepath = ConfigurationManager.AppSettings["FilePath"].ToString();
            string pageCount = ConfigurationManager.AppSettings["PageCount"].ToString();
            foreach (var item in model.ListModel)
            {
                item.Category = biz.GetCodeByCodeNo(item.Category).CodeName;
            }
            model.ListCount = model.ListModel.Count;
            if (input.PageNum == 0)
            {
                model.PageNum = 1;
            }
            else
            {
                model.PageNum = input.PageNum;
            }
            if (input.PageSize == 0)
            {
                model.PageSize = Convert.ToInt32(pageCount);
            }
            else
            {
                model.PageSize = Convert.ToInt32(input.PageSize);
            }
            if (model.ListModel.Count > model.PageSize)
            {
                model.PageTotal = model.ListModel.Count % model.PageSize == 0 ? model.ListModel.Count / model.PageSize : (model.ListModel.Count / model.PageSize) + 1;
            }
            model.ListModel = model.ListModel.Skip(model.PageSize * (model.PageNum - 1)).Take(model.PageSize).ToList();
            model.CategoryList = logic.CodeListToSelectList(biz.GetSystemCodeListByType("NoticeType"));
            return View(model);
        }


        [HttpPost]
        public ActionResult TicketDetaileList(Ticket_NoticoPageModel input, string startDate, string endDate)
        {
            ViewBag.TicketNoticeNav = "active open";
            ViewBag.TicketDetaileList = "active";
            TicketNoticeInfoManageBiz biz = new TicketNoticeInfoManageBiz();
            Ticket_NoticoPageModel model = new Ticket_NoticoPageModel();

            TicketManageLogic logic = new TicketManageLogic();
            model.ListModel = biz.GetTicketNoticeListByQueryable(input, startDate, endDate);
            string filepath = ConfigurationManager.AppSettings["FilePath"].ToString();
            string pageCount = ConfigurationManager.AppSettings["PageCount"].ToString();
            foreach (var item in model.ListModel)
            {
                item.Category = biz.GetCodeByCodeNo(item.Category).CodeName;
            }
            model.ListCount = model.ListModel.Count;
            if (input.PageNum == 0)
            {
                model.PageNum = 1;
            }
            else
            {
                model.PageNum = input.PageNum;
            }
            if (input.PageSize == 0)
            {
                model.PageSize = Convert.ToInt32(pageCount);
            }
            else
            {
                model.PageSize = Convert.ToInt32(input.PageSize);
            }
            if (model.ListModel.Count > model.PageSize)
            {
                model.PageTotal = model.ListModel.Count % model.PageSize == 0 ? model.ListModel.Count / model.PageSize : (model.ListModel.Count / model.PageSize) + 1;
            }
            model.ListModel = model.ListModel.Skip(model.PageSize * (model.PageNum - 1)).Take(model.PageSize).ToList();
            model.CategoryList = logic.CodeListToSelectList(biz.GetSystemCodeListByType("NoticeType"));
            return View(model);
        }

        public ActionResult _PartialTicketDetaileList(Ticket_NoticoPageModel input)
        {
            return PartialView("~/Views/TicketNotice/_PartialTicketDetaileList.cshtml", input);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult SaveTicketNoticeInfo(Ticket_NoticoPageModel model)
        {
            JsonResult result = new JsonResult();
            bool res = false;
            TicketManageLogic logic = new TicketManageLogic();
            TicketNoticeInfoManageBiz biz = new TicketNoticeInfoManageBiz();
            model.PageModel.CreateTime = DateTime.Now;
            model.PageModel.PublisherSEQ = UserInfoModel.PageModel.SEQ;
            res = biz.SaveTicket_NoticeInfo(model);
            if (res)
            {
                result.Data = "����ɹ�";
            }
            else
            {
                result.Data = "����ʧ��";
            }
            return result;
        }

        public JsonResult DeleteTicketNoticeInfo(string seq)
        {
            JsonResult result = new JsonResult();
            TicketNoticeInfoManageBiz biz = new TicketNoticeInfoManageBiz();
            bool res = false;
            if (!string.IsNullOrEmpty(seq))
            {
                res = biz.DeleteTicket_NoticeInfoByEntity(Convert.ToInt32(seq));
            }
            result.Data = res ? "ɾ���ɹ���" : "ɾ��ʧ�ܣ�";
            return result;
        }


        public PartialViewResult TicketInfoManage(string id)
        {
            ViewBag.TicketNoticeNav = "active open";
            ViewBag.TicketListManage = "active";
            TicketNoticeInfoManageBiz biz = new TicketNoticeInfoManageBiz();
            Ticket_NoticoPageModel model = new Ticket_NoticoPageModel();
            TicketManageLogic logic = new TicketManageLogic();
            model.PageModel = biz.GetTicketNoticeBySEQ(Convert.ToInt32(id));
            model.CategoryList = logic.CodeListToSelectList(biz.GetSystemCodeListByType("NoticeType"));
            string viewstate = Request.QueryString["viewstate"];
            ViewBag.FormState = !string.IsNullOrEmpty(viewstate) ? "false" : "true";
            return PartialView(model);
        }

        [HttpPost, AjaxOnly]
        public JsonResult NoticeClickCount(string id)
        {
            JsonResult result = new JsonResult();
            TicketNoticeInfoManageBiz biz = new TicketNoticeInfoManageBiz();
            bool res = biz.SaveNoticeInfoClickCount(Convert.ToInt32(id));
            result.Data = res;
            return result;
        }

        [HttpPost]
        public JsonResult SummernoteImage()
        {
            JsonResult result = new JsonResult();
            TicketManageLogic logic = new TicketManageLogic();
            logic.ConvertFileToEntity(Request, new Ticket_NoticoPageModel());
            return result;
        }
    }
}