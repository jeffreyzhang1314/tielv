using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.TicketGroupManage;
using TieckManage.Biz.TicketManage;

namespace TicketManage.WebSite.Controllers
{
    [LoginCheck]
    public class TicketGroupController : BaseController
    {
        private TicketGroupBiz biz = new TicketGroupBiz();

        /// <summary>
        /// 列表加载及查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public ActionResult TicketGroupList(TicketGroupListPageModel model)
        {
            ViewBag.TicketManageNav = "active open";
            ViewBag.TicketGroupList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
            model.OperatorAuthNum = UserInfoModel.PageModel.AuthNum;
            if ((model.OperatorAuthNum & 2) == 2)
            {
                model.OperatorUserNoList = new List<int> { UserInfoModel.PageModel.SEQ };
            }
            else if ((model.OperatorAuthNum & 4) == 4)
            {
                model.OperatorUserNoList = biz.GetUserNoByGroupSEQ(UserInfoModel.PageModel.GroupSEQ);
            }
            else if ((model.OperatorAuthNum & 8) == 8)
            {
                model.OperatorUserNoList = biz.GetUserNoByCompanySEQ(UserInfoModel.PageModel.CompanySEQ);
            }
            model.ModelList = biz.GetTicketGroupByCondition(model);
            string pageCount = ConfigurationManager.AppSettings["PageCount"].ToString();
            model.ListCount = model.ModelList.Count;
            if (model.PageNum == 0)
            {
                model.PageNum = 1;
            }
            else
            {
                model.PageNum = model.PageNum;
            }
            if (model.PageSize == 0)
            {
                model.PageSize = Convert.ToInt32(pageCount);
            }
            else
            {
                model.PageSize = Convert.ToInt32(model.PageSize);
            }
            if (model.ModelList.Count() > model.PageSize)
            {
                model.PageTotal = model.ModelList.Count() % model.PageSize == 0 ? model.ModelList.Count() / model.PageSize : (model.ModelList.Count() / model.PageSize) + 1;
            }
            model.ModelList = model.ModelList.Skip(model.PageSize * (model.PageNum - 1)).Take(model.PageSize).ToList();
            return View(model);
        }
        public PartialViewResult TicketGroupInfoManage(string id)
        {
            TicketGroupListPageModel pageModel = new TicketGroupListPageModel();
            pageModel.PageModel = biz.GetTickGroupInfo(Convert.ToInt32(id));

            var tempCompanyList = new List<KeyValueModel>();
            tempCompanyList.Add(new KeyValueModel { Key = 0, Value = "全部" });

            if (UserInfoModel.IsAdmin)
            {
                tempCompanyList.AddRange(biz.GetCompanyInfoByAll());
            }
            else
            {
                tempCompanyList.AddRange(biz.GetCompanyInfoById(UserInfoModel.PageModel.CompanySEQ ?? 0));
            }

            pageModel.CompanySource = new SelectList(tempCompanyList, "Key", "Value");


            pageModel.OperatorAuthNum = UserInfoModel.PageModel.AuthNum;
            return PartialView(pageModel);
        }
        [HttpPost]
        public JsonResult SaveTicketGroupInfo(TicketGroupListPageModel model)
        {
            JsonResult result = new JsonResult();
            bool res = false;

            CompanyInfoManageBiz combiz = new CompanyInfoManageBiz();

            CompanyInfo info = combiz.ShowCompanyInfoById(Convert.ToInt32(model.PageModel.CompanySEQ));
            if (info != null)
            {
                model.PageModel.CompanyName = info.CompanyName;
            }
            model.PageModel.OperatorSEQ = UserInfoModel.PageModel.SEQ;
            res = biz.SaveInfo(model);
            if (res)
            {
                result.Data = "保存成功";
            }
            else
            {
                result.Data = "保存失败";
            }
            return result;
        }
        public JsonResult DeleteTicketGroupInfo(string seq)
        {
            JsonResult result = new JsonResult();
            bool res = false;
            if (!string.IsNullOrEmpty(seq))
            {
                res = biz.DeleteInfo(Convert.ToInt32(seq));
            }
            if (res)
            {
                result.Data = "删除成功！";
            }
            else
            {
                result.Data = "删除失败！";
            }

            return result;
        }
    }
}
