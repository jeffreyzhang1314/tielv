﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TicketManage.WebSite.Models;
using TieckManage.Biz.TicketManage;

namespace TicketManage.WebSite.Controllers
{
    /// <summary>
    /// 提供登录接口
    /// </summary>
    public class InterfaceLoginController : BaseController
    {
        private BaseDataManageBiz biz = new BaseDataManageBiz();

        /// <summary>
        /// 密码加密
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private String EncryptCode(String message)
        {
            var clearBytes = new UnicodeEncoding().GetBytes(message);
            var hashedBytes = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(clearBytes);
            var tt = BitConverter.ToString(hashedBytes).Replace("-", "");
            return tt;
        }
        /// <summary>
        /// 修改用户登录密码
        /// </summary>
        /// <param name="loginName">登录名</param>
        /// <param name="oldPwd">旧密码</param>
        /// <param name="newPwd">新密码</param>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult ChangePassword()
        {
            var flag = false;
            var dbContext = new TicketManageContext();
            var loginName = Request.QueryString["loginName"];
            var newPwd = Request.QueryString["newPwd"];
            if (!string.IsNullOrEmpty(loginName)
                && !string.IsNullOrEmpty(newPwd))
            {
                var entity = dbContext.UserInfoes.Where(a => a.UserNo == loginName).FirstOrDefault();
                if (entity != null)
                {
                    var convertNewPassword = EncryptCode(newPwd);
                    entity.UserPassword = convertNewPassword;
                    dbContext.SaveChanges();
                    flag = true;
                }
                else
                {
                    ViewBag.ErrorMessage = "用户名密码不正确";
                }
            }
            else
            {
                ViewBag.ErrorMessage = "请传入正确的参数：loginName、newPwd";
            }
            return PartialView(flag);
        }

        [HttpGet]
        public JsonResult UserInfoPasswordChange()
        {
            var flag = false;
            var message = "请传入正确的参数：loginName、newPwd";
            var dbContext = new TicketManageContext();
            var loginName = Request.QueryString["loginName"];
            var newPwd = Request.QueryString["newPwd"];
            if (!string.IsNullOrEmpty(loginName) && !string.IsNullOrEmpty(newPwd))
            {
                var entity = dbContext.UserInfoes.Where(a => a.UserNo == loginName).FirstOrDefault();
                if (entity != null)
                {
                    var convertNewPassword = EncryptCode(newPwd);
                    entity.UserPassword = convertNewPassword;
                    //修改其它系统用户同名的密码
                    var otherEntity = dbContext.UserInfo_Other.Where(a => a.UserNo == loginName).FirstOrDefault();
                    if (otherEntity != null)
                    {
                        otherEntity.UserPassword = convertNewPassword;
                    }
                    dbContext.SaveChanges();
                    message = "修改密码成功！";
                    flag = true;
                }
                else
                {
                    message = "未找到用户名！";
                }
            }
            return Json(new InterfaceMessageModel { Result = flag, Message = message }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 票务系统登录
        /// </summary>
        /// <param name="userNo"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TicketManage()
        {
            var userNo = Request.QueryString["userNo"];
            var password = Request.QueryString["password"];
            var model = new UserPageModel() { PageModel = new UserInfo() { UserNo = userNo, UserPassword = password } };
            model.MessageState = true;
            model.Message = "用户名密码不正确";
            model.SysteCategory = SystemName.票务管理系统;
            if (!string.IsNullOrEmpty(userNo) && !string.IsNullOrEmpty(password))
            {
                var result = biz.CheckUserNo(userNo);
                if (result)
                {
                    biz.UserLogin(model);
                    var keyValue = ConfigurationManager.AppSettings["UserInfo"];
                    Session[keyValue] = model;
                    return RedirectToAction("Index", "Ticket");
                }
            }
            return RedirectToAction("Login", "Home", model);
        }

        /// <summary>
        /// 地接管理系统登录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TravelLocalAgency()
        {
            var userNo = Request.QueryString["userNo"];
            var password = Request.QueryString["password"];
            var model = new UserLoginPageModel() { PageModel = new UserInfo_Other { UserNo = userNo, UserPassword = password } };
            model.Message = "您没有权限访问地接管理系统！";
            model.MessageState = true;
            model.SysteCategory = SystemName.地接管理系统;
            model.SystemName = "地接管理系统";
            ViewBag.Title = "地接管理系统登录";
            if (!string.IsNullOrEmpty(userNo) && !string.IsNullOrEmpty(password))
            {
                var result = biz.CheckOtherUserNo(userNo);
                if (result)
                {
                    biz.CommonLogin(model);
                    if (CheckUserAuth(model.PageModel.AuthNum, model.SysteCategory))
                    {
                        Session.Clear();
                        Session[UserInfo] = model;
                        return RedirectToAction("TravelLocalAgencyList", "TravelLocalAgency");
                    }
                    else
                    {
                        model.Message = "您没有权限访问地接管理系统！";
                        model.MessageState = true;
                    }
                }
            }
            return RedirectToAction("Login", "TravelLocalAgency", model);
        }

        /// <summary>
        /// 导游管理系统登录
        /// </summary>
        /// <param name="userNo"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GuideInfo()
        {
            var userNo = Request.QueryString["userNo"];
            var password = Request.QueryString["password"];
            var model = new UserLoginPageModel() { PageModel = new UserInfo_Other { UserNo = userNo, UserPassword = password } };
            model.Message = "用户名密码错误！";
            model.MessageState = true;
            model.SysteCategory = SystemName.导游管理系统;
            model.SystemName = "导游管理系统";
            ViewBag.Title = "导游管理系统登录";
            if (!string.IsNullOrEmpty(userNo) && !string.IsNullOrEmpty(password))
            {
                var result = biz.CheckOtherUserNo(userNo);
                if (result)
                {
                    biz.CommonLogin(model);
                    if (CheckUserAuth(model.PageModel.AuthNum, model.SysteCategory))
                    {
                        Session.Clear();
                        Session[UserInfo] = model;
                        return RedirectToAction("GuideInfoList", "GuideInfo");
                    }
                    else
                    {
                        model.Message = "您没有权限访问导游管理系统！";
                        model.MessageState = true;
                    }
                }
            }
            return RedirectToAction("Login", "GuideInfo", model);
        }

        /// <summary>
        /// 宣传品管理系统登录
        /// </summary>
        /// <param name="userNo"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Publicity()
        {
            var userNo = Request.QueryString["userNo"];
            var password = Request.QueryString["password"];
            var model = new UserLoginPageModel() { PageModel = new UserInfo_Other { UserNo = userNo, UserPassword = password } };
            model.Message = "用户名密码错误！";
            model.MessageState = true;
            model.SysteCategory = SystemName.宣传品管理系统;
            model.SystemName = "宣传品管理系统";
            ViewBag.Title = "宣传品管理系统登录";
            if (!string.IsNullOrEmpty(userNo) && !string.IsNullOrEmpty(password))
            {
                var result = biz.CheckOtherUserNo(userNo);
                if (result)
                {
                    biz.CommonLogin(model);
                    if (CheckUserAuth(model.PageModel.AuthNum, model.SysteCategory))
                    {
                        Session.Clear();
                        Session[UserInfo] = model;
                        return RedirectToAction("PublicityInfoList", "Publicity");
                    }
                    else
                    {
                        model.Message = "您没有权限访问宣传品管理系统！";
                        model.MessageState = true;
                    }
                }
            }
            return RedirectToAction("Login", "Publicity", model);
        }

        /// <summary>
        /// 功效管理系统登录
        /// </summary>
        /// <param name="userNo"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult KPICommon()
        {
            var userNo = Request.QueryString["userNo"];
            var password = Request.QueryString["password"];
            var model = new UserLoginPageModel() { PageModel = new UserInfo_Other { UserNo = userNo, UserPassword = password } };
            model.Message = "用户名密码错误！";
            model.MessageState = true;
            model.SysteCategory = SystemName.工效管理系统;
            model.SystemName = "工效管理系统";
            ViewBag.Title = "工效管理系统登录";
            if (!string.IsNullOrEmpty(userNo) && !string.IsNullOrEmpty(password))
            {
                var result = biz.CheckOtherUserNo(userNo);
                if (result)
                {
                    biz.CommonLogin(model);
                    if (CheckUserAuth(model.PageModel.AuthNum, model.SysteCategory))
                    {
                        Session.Clear();
                        Session[UserInfo] = model;
                        return RedirectToAction("OrganizationList", "KPIData");
                    }
                    else
                    {
                        model.Message = "您没有权限访问工效管理系统！";
                        model.MessageState = true;
                    }
                }
            }
            return RedirectToAction("Login", "KPICommon", model);
        }

    }
}
