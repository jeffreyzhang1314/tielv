﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.KPIGrossProfit;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.KPIGrossProfitManage;

namespace TicketManage.WebSite.Controllers
{
    /// <summary>
    /// 个人计调功效
    /// </summary>
    [LoginCheckKPI]
    public class KPIGrossProfitController : BaseController
    {

        private GrossProfitInfoBiz GrossProfitBiz = new GrossProfitInfoBiz();

        private void KPIGrossProfitListInit(GX_GrossProfitInfoPageModel model)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.KPIGrossProfitList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
            model.YearSource = new SelectList(GrossProfitBiz.GetSystemCodeListByType("CommonYear"), "CodeNo", "CodeName");
            model.MonthSource = new SelectList(GrossProfitBiz.GetSystemCodeListByType("CommonMonth"), "CodeNo", "CodeName");
            model.DutySource = new SelectList(GrossProfitBiz.GetDutyInfo(), "seq", "DutyName");

        }

        [HttpGet]
        public ActionResult KPIGrossProfitList()
        {
            var model = new GX_GrossProfitInfoPageModel() { PageSize = 10, CurrentPage = 1 };

            KPIGrossProfitListInit(model);
            model.PageModel = new Repository.Models.GX_GrossProfitInfo()
            {
                OperatorYear = Convert.ToString(DateTime.Now.Year),
                OperatorMonth = Convert.ToString(DateTime.Now.Month),
                UserSEQ = CommonUserInfoModel.PageModel.SEQ,
                UserName = CommonUserInfoModel.PageModel.UserName
            };
            model.ModelList = GrossProfitBiz.SearchGrossProfitByCondition(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult KPIGrossProfitList(GX_GrossProfitInfoPageModel model)
        {
            KPIGrossProfitListInit(model);
            model.ModelList = GrossProfitBiz.SearchGrossProfitByCondition(model);
            return View(model);
        }

        /// <summary>
        /// 添加显示个人计调信息
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public PartialViewResult ShowKPIGrossProfitAdd()
        {
            var model = new GX_GrossProfitInfoPageModel();
            KPIGrossProfitListInit(model);
            model.PageModel = new Repository.Models.GX_GrossProfitInfo()
            {
                OperatorYear = Convert.ToString(DateTime.Now.Year),
                OperatorMonth = Convert.ToString(DateTime.Now.Month),
                UserSEQ = CommonUserInfoModel.PageModel.SEQ,
                UserName = CommonUserInfoModel.PageModel.UserName
            };

            return PartialView("PartialGrossProfitEdit", model);
        }

        [AjaxOnly]
        public PartialViewResult ShowGrossProfitEdit(int id)
        {
            var model = new GX_GrossProfitInfoPageModel();
            KPIGrossProfitListInit(model);
            model.PageModel = GrossProfitBiz.GetGrossProfitBySEQ(id);
            //model.ParentList = new SelectList(DeptBiz.GetDeptInfoByParent(), "seq", "DeptName");
            //ViewBag.ParentList = new SelectList(DeptBiz.GetDeptInfoByParent(), "seq", "DeptName");
            return PartialView("PartialGrossProfitEdit", model);
        }

        [AjaxOnly]
        public JsonResult GrossProfitEdit(GX_GrossProfitInfoPageModel model)
        {
            var result = GrossProfitBiz.EditGrossProfit(model);
            return Json(new { result = result, message = result ? "操作成功" : "操作失败" }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult GrossProfitDel(int id)
        {
            var result = GrossProfitBiz.DelGrossProfit(id);
            return Json(new { result = result, message = result ? "操作成功" : "操作失败" }, JsonRequestBehavior.AllowGet);
        }


    }
}
