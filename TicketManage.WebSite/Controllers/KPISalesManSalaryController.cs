﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.KPICalcEfficacyManage;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.KPICalcEfficacyManage;

namespace TicketManage.WebSite.Controllers
{
     [LoginCheck]
    public class KPISalesManSalaryController : BaseController
    {
        private SalesmanSalaryInfoBiz SalesManBiz = new SalesmanSalaryInfoBiz();

        private void SalesManSalaryListInit(SalesmanSalaryInfoPageModel model)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.SalesManSalaryList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
        }

        [HttpGet]
        public ActionResult SalesManSalaryList()
        {
            var model = new SalesmanSalaryInfoPageModel() { PageSize = 10, CurrentPage = 1 };
            SalesManSalaryListInit(model);
            model.ModelList = SalesManBiz.SearchSalesmanSalaryInfoByCondition(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult SalesManSalaryList(SalesmanSalaryInfoPageModel model)
        {
            SalesManSalaryListInit(model);
            model.ModelList = SalesManBiz.SearchSalesmanSalaryInfoByCondition(model);
            return View(model);
        }

        [HttpGet]
        public PartialViewResult ShowSalesManSalaryAdd()
        {
            var model = new SalesmanSalaryInfoPageModel();
            return PartialView("PartialSalesManSalaryEdit", model);
        }

        [HttpGet]
        public PartialViewResult ShowSalesManSalaryEdit(int id)
        {
            var model = new SalesmanSalaryInfoPageModel();
            model.PageModel = SalesManBiz.GetSalesmanSalaryInfoBySEQ(id);
            return PartialView("PartialSalesManSalaryEdit", model);
        }

        [AjaxOnly]
        public JsonResult SalesManSalaryEdit(SalesmanSalaryInfoPageModel model)
        {
            var result = SalesManBiz.EditSalesmanSalaryInfo(model);
            return Json(new { result = result, message = "操作成功！" }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult SalesManSalaryDel(int id)
        {
            var result = SalesManBiz.DelSalesmanSalaryInfo(id);
            return Json(new { result = result, message = "操作成功！" }, JsonRequestBehavior.AllowGet);
        }

    }
}
