﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.KPISalesManManage;
using TicketManage.WebSite.Filters;
using TieckManage.Biz.KPISalesManManage;

namespace TicketManage.WebSite.Controllers
{
    [LoginCheckKPI]
    public class KPISalesManController : BaseController
    {
        private GX_SalesManKPIBiz SalesMamBiz = new GX_SalesManKPIBiz();

        private void KPISalesManListInit(GX_SalesManKPIPageModel model)
        {
            ViewBag.KPICalcEfficacyNav = "active open";
            ViewBag.KPISalesManList = "active";
            model.PageSizeList = new SelectList(PageSizeSource, "Key", "Value", model.PageSize);
            model.YearSource = new SelectList(SalesMamBiz.GetSystemCodeListByType("CommonYear"), "CodeNo", "CodeName");
            model.MonthSource = new SelectList(SalesMamBiz.GetSystemCodeListByType("CommonMonth"), "CodeNo", "CodeName");
        }

        [HttpGet]
        public ActionResult KPISalesManList()
        {
            var model = new GX_SalesManKPIPageModel() { PageSize = 10, CurrentPage = 1 };
            KPISalesManListInit(model);
            model.ModelList = SalesMamBiz.SearchGX_SalesManKPIByCondition(model);
            model.PageModel = new Repository.Models.GX_SalesManKPI()
            {
                OperatorYear = Convert.ToString(DateTime.Now.Year),
                OperatorMonth = Convert.ToString(DateTime.Now.Month)
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult KPISalesManList(GX_SalesManKPIPageModel model)
        {
            KPISalesManListInit(model);
            model.ModelList = SalesMamBiz.SearchGX_SalesManKPIByCondition(model);
            return View(model);
        }

        /// <summary>
        /// 添加显示个人计调信息
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public PartialViewResult ShowKPISalesManAdd()
        {
            var model = new GX_SalesmanSalaryDetailInfoPageModel();
            return PartialView("PartialGrossProfitEdit", model);
        }

        [AjaxOnly]
        public PartialViewResult ShowGrossProfitEdit(int id)
        {
            var model = new GX_SalesManKPIPageModel();
            return PartialView("PartialGrossProfitEdit", model);
        }

        [AjaxOnly]
        public JsonResult GrossProfitEdit(GX_SalesManKPIPageModel model)
        {
            return Json(new { result = model, message = false ? "操作成功" : "操作失败" }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public JsonResult GrossProfitDel(int id)
        {
            return Json(new { result = id, message = false ? "操作成功" : "操作失败" }, JsonRequestBehavior.AllowGet);
        }

    }
}
