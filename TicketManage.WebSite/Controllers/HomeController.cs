/*
 * Created by SharpDevelop.
 * User: Admin
 * Date: 2017/5/29
 * Time: 13:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Web.Mvc;
using TicketManage.WebSite.Filters;
using TicketManage.Entity.TicketManage;
using TieckManage.Biz.CommonManage;
using System.Collections.Generic;
using TicketManage.Entity.CommonManage;
using TicketManage.WebSite.Logic;

namespace TicketManage.WebSite.Controllers
{
    public class HomeController : BaseController
    {
        private CommonBiz biz = new CommonBiz();

       

        [LoginCheck]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            Logger.Default.Trace("Hello World! Trace");
            var pageModel = new UserPageModel();
            pageModel.PageModel = new Repository.Models.UserInfo();
            return View(pageModel);
        }

        [HttpPost]
        public ActionResult Login(UserPageModel model)
        {
            var result = biz.UserLogin(model);
            if (result)
            {
                model.SysteCategory = SystemName.票务管理系统;
                Session[UserInfo] = model;
                return RedirectToAction("Index", "Ticket");
            }
            else
            {
                model.Message = "用户名密码错误~！";
                return View(model);
            }
        }

        [HttpGet, LoginCheck]
        public ActionResult Logout()
        {
            if (UserInfoModel != null && UserInfoModel.SysteCategory == SystemName.票务管理系统)
            {
                Session.Clear();
                return RedirectToAction("Login", "Home");
            }
            else
            {
                var authNum = CommonUserInfoModel.SysteCategory;
                var forwardName = "Home";
                if (authNum == SystemName.地接管理系统)
                {
                    forwardName = "TravelLocalAgency";
                }
                else if (authNum == SystemName.导游管理系统)
                {
                    forwardName = "GuideInfo";
                }
                else if (authNum == SystemName.宣传品管理系统)
                {
                    forwardName = "Publicity";
                }
                else if (authNum == SystemName.工效管理系统)
                {
                    forwardName = "KPICommon";
                }
                Session.Clear();
                return RedirectToAction("Login", forwardName);
            }
        }

        [HttpGet, LoginCheck]
        public ActionResult UserInfoView()
        {
            var pageModel = new UserPageModel();
            pageModel = UserInfoModel;
            return View(pageModel);
        }

        [HttpGet, LoginCheck]
        public ActionResult UserInfoEdit()
        {
            var pageModel = new UserPageModel();
            pageModel = UserInfoModel;
            var SourceListSex = new List<SelectListItem>();
            SourceListSex.Add(new SelectListItem { Text = "男", Value = "True" });
            SourceListSex.Add(new SelectListItem { Text = "女", Value = "False" });
            ViewBag.SourceListSex = SourceListSex;
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult UserInfoEdit(UserPageModel model)
        {
            var result = biz.UserInfoEdit(model);
            var SourceListSex = new List<SelectListItem>();
            SourceListSex.Add(new SelectListItem { Text = "男", Value = "True" });
            SourceListSex.Add(new SelectListItem { Text = "女", Value = "False" });
            ViewBag.SourceListSex = SourceListSex;
            if (result)
            {
                model.Message = "个人信息修改成功！";
            }
            else
            {
                model.Message = "个人信息修改为修改成功！";
            }
            return View(model);
        }

        [HttpGet, LoginCheck]
        public ActionResult ChangePassword()
        {
            var pageModel = UserInfoModel;
            return View(pageModel);
        }

        [HttpPost, LoginCheck]
        public ActionResult ChangePassword(UserPageModel model)
        {
            var result = biz.ChangePassWord(model);
            return View(model);
        }
    }
}
