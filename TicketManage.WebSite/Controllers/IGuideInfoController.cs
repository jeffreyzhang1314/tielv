﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TieckManage.Biz.TourGuideManage;

namespace TicketManage.WebSite.Controllers
{
    public class IGuideInfoController : BaseController
    {
        /// <summary>
        /// 返回所有导游信息
        /// </summary>
        /// <returns></returns>
        public JsonResult ShowGuideInfo(string guideName)
        {
            var biz = new GuideInfoBiz();
            var guideList = biz.ShowGuideInfo(guideName);
            return Json(guideList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 设置导游出团时间接口
        /// </summary>
        /// <param name="passportNo">身份证号</param>
        /// <param name="startDate">出团时间</param>
        /// <param name="endDate">回团时间</param>
        /// <returns>result：调用结果：True 成功，False 失败 message：错误信息</returns>
        public JsonResult EditGuideInfoLeadTime(string passportNo, DateTime? startDate, DateTime? endDate)
        {
            var message = string.Empty;
            var biz = new GuideInfoBiz();
            var result = biz.EditGuideLeadTime(passportNo, startDate, endDate, ref message);
            return Json(new { result = result, message = message }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 清空指定身份证号的导游出团回团时间
        /// </summary>
        /// <param name="passportNo">身份证号</param>
        /// <returns>result:true执行成功false执行失败 message:执行反馈信息</returns>
        public JsonResult GuideLeadTimeClear(string passportNo)
        {
            var message = string.Empty;
            var biz = new GuideInfoBiz();
            var result = biz.GuideLeadTimeClear(passportNo, ref message);
            return Json(new { result = result, message = message }, JsonRequestBehavior.AllowGet);
        }



    }
}
