/*
 * Created by SharpDevelop.
 * User: Admin
 * Date: 2017/5/29
 * Time: 7:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Web.Mvc;

namespace TicketManage.WebSite.Filters
{
	/// <summary>
	/// Description of AjaxOnly.
	/// </summary>
	public class AjaxOnlyAttribute : System.Web.Mvc.ActionFilterAttribute
	{
		public bool AllowGetJson { get; set; }
        
		public AjaxOnlyAttribute()
        {
            this.AllowGetJson = true;
        }

        public AjaxOnlyAttribute(bool allowJsonGet)
        {
            this.AllowGetJson = allowJsonGet;
        }

        public override void OnActionExecuting(System.Web.Mvc.ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
                filterContext.HttpContext.Response.Redirect("/Home/Login");
        }

        public override void OnActionExecuted(System.Web.Mvc.ActionExecutedContext filterContext)
        {
            //默认是否允许Get JSON类型数据
            if (filterContext.Result is JsonResult)
                ((JsonResult)filterContext.Result).JsonRequestBehavior =
                    this.AllowGetJson ? JsonRequestBehavior.AllowGet : JsonRequestBehavior.DenyGet;
        }
	}
}
