/* * Created by SharpDevelop. * User: Admin * Date: 2017/5/29 * Time: 7:06 *  * To change this template use Tools | Options | Coding | Edit Standard Headers. */
using System;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Entity.TicketManage;
using TicketManage.WebSite.Models.DataBaseManage;

namespace TicketManage.WebSite.Filters
{
    /// <summary>
    /// Description of LoginCheck.
    /// </summary>
    public class DataBaseManageAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Session["DataBaseManageInfo"] == null)
            {
                filterContext.Result = new RedirectResult(new UrlHelper(filterContext.HttpContext.Request.RequestContext).Action("Login", "DataBaseManage", new { Message = "用户名密码不正确~！" }));
            }
            else
            {
                var tempAdmin = filterContext.HttpContext.Session["DataBaseManageInfo"] as LoginModel;
                if (tempAdmin != null)
                {
                    filterContext.Controller.ViewBag.UserName = tempAdmin.UserNo;
                }
                base.OnActionExecuting(filterContext);
            }
        }
    }
}
