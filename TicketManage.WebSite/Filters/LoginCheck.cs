/* * Created by SharpDevelop. * User: Admin * Date: 2017/5/29 * Time: 7:06 *  * To change this template use Tools | Options | Coding | Edit Standard Headers. */
using System;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Entity.TicketManage;

namespace TicketManage.WebSite.Filters
{
    /// <summary>
    /// Description of LoginCheck.
    /// </summary>
    public class LoginCheckAttribute : ActionFilterAttribute
    {
        
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (string.Compare(filterContext.ActionDescriptor.ActionName, "ShowMobileTicketAdd", true) == 0
                || string.Compare(filterContext.ActionDescriptor.ActionName, "CheckUserInfo", true) == 0)
            {
                base.OnActionExecuting(filterContext);
            }
            else
            {
                if (filterContext.HttpContext.Session["UserInfo"] == null)
                {
                    //var tempPath = filterContext.RequestContext.HttpContext.Request.Url;
                    //var loginPath = tempPath.AbsoluteUri.Replace(tempPath.AbsolutePath, string.Empty);
                    filterContext.Result = new RedirectResult(new UrlHelper(filterContext.HttpContext.Request.RequestContext).Action("Login", "Home"));
                }
                else
                {
                    var tempAdmin = filterContext.HttpContext.Session["UserInfo"] as UserPageModel;
                    if (tempAdmin != null)
                    {
                        filterContext.Controller.ViewBag.AuthNum = tempAdmin.PageModel.AuthNum;
                        filterContext.Controller.ViewBag.UserName = tempAdmin.PageModel.UserName;
                        filterContext.Controller.ViewBag.IsAdmin = tempAdmin.PageModel.AuthNum == 1 ? "true" : "false";
                    }
                    var tempUserModel = filterContext.HttpContext.Session["UserInfo"] as UserLoginPageModel;
                    if (tempUserModel != null)
                    {
                        filterContext.Controller.ViewBag.AuthNum = tempUserModel.PageModel.AuthNum;
                        filterContext.Controller.ViewBag.SystemName = tempUserModel.SystemName;
                        filterContext.Controller.ViewBag.UserName = tempUserModel.PageModel.UserName;
                        filterContext.Controller.ViewBag.IsAdmin = tempUserModel.PageModel.AuthNum == 1 ? "true" : "false";
                    }
                    base.OnActionExecuting(filterContext);
                }
            }
        }
    }
}
