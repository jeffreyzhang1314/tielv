using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TicketManage.WebSite.Models
{
    public class SelectPageModel
    {
        /// <summary>
        /// 数据元
        /// </summary>
        public SelectList SelectSource { get; set; }


        /// <summary>
        /// 控件名称
        /// </summary>
        public string ControlName { get; set; }

        /// <summary>
        /// 控件Id
        /// </summary>
        public string ControlId { get; set; }
    }
}