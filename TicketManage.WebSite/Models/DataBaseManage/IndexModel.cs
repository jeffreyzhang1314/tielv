﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TicketManage.WebSite.Models.DataBaseManage
{
    public class IndexModel
    {
        /// <summary>
        /// 数据库中所有表信息
        /// </summary>
        public SelectList TableNameSource { get; set; }

        /// <summary>
        /// 表格数据
        /// </summary>
        public DataTable TableContent { get; set; }

        /// <summary>
        /// 页面选择表名称
        /// </summary>
        public string TableName { get; set; }
    }
}