﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketManage.WebSite.Models.DataBaseManage
{
    public class LoginModel : BaseModel
    {
        public string UserNo { get; set; }

        public string PassWord { get; set; }
    }
}