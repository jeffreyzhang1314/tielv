﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketManage.WebSite.Models
{
    /// <summary>
    /// 接口方法使用实体
    /// </summary>
    public class InterfaceMessageModel
    {
        /// <summary>
        /// True执行成功False执行失败
        /// </summary>
        public bool Result { get; set; }

        /// <summary>
        /// 执行结果的提示信息
        /// </summary>
        public string Message { get; set; }
    }
}