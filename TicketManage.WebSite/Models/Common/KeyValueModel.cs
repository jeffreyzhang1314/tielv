﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketManage.WebSite.Models.DataBaseManage
{
    public class KeyValueModel
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}