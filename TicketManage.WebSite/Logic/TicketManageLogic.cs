using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.TicketManage;
using TicketManage.Repository.Models;
using TieckManage.Biz.TicketManage;
namespace TicketManage.WebSite.Logic
{
    public class TicketManageLogic
    {
        //addby gaowenchao 20170619
        public List<SelectListItem> ToPublicitySelectList(List<PublicityInfo> companyList)
        {
            List<SelectListItem> result = new List<SelectListItem>();

            try
            {
                if (companyList != null && companyList.Count > 0)
                {
                    foreach (var item in companyList)
                    {
                        SelectListItem selectItem = new SelectListItem()
                        {
                            Value = item.SEQ.ToString(),
                            Text = item.PublicityName
                        };
                        result.Add(selectItem);
                    }

                }
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }
        public List<SelectListItem> ToBudgetSelectList(List<BudgetInfo> companyList)
        {
            List<SelectListItem> result = new List<SelectListItem>();

            try
            {
                if (companyList != null && companyList.Count > 0)
                {
                    foreach (var item in companyList)
                    {
                        SelectListItem selectItem = new SelectListItem()
                        {
                            Value = item.SEQ.ToString(),
                            Text = item.BudgetName
                        };
                        result.Add(selectItem);
                    }

                }
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        /// <summary>
        /// 把SystemCodeList 转换成下拉列表
        /// </summary>
        /// <param name="codeList"></param>
        /// <returns></returns>
        public List<SelectListItem> CodeListToSelectList(List<SystemCode> codeList)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem() { Value = "0", Text = "-请选择-" });
            try
            {
                if (codeList != null && codeList.Count > 0)
                {
                    foreach (var item in codeList)
                    {
                        SelectListItem selectItem = new SelectListItem()
                        {
                            Text = item.CodeName,
                            Value = item.CodeNo
                        };
                        result.Add(selectItem);
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// 设置代售点列表页的单位下拉列表
        /// </summary>
        /// <param name="companyList"></param>
        /// <returns></returns>
        public List<SelectListItem> ConvertUnitList(List<CompanyInfo> companyList)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem() { Value = "0", Text = "-请选择-" });
            try
            {
                if (companyList != null && companyList.Count > 0)
                {
                    foreach (var item in companyList)
                    {
                        SelectListItem selectItem = new SelectListItem()
                        {
                            Value = item.SEQ.ToString(),
                            Text = item.CompanyName
                        };
                        result.Add(selectItem);
                    }

                }
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public List<SelectListItem> ConvertLinkSiteList(List<SalePointInfo> pointList)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem() { Value = "0", Text = "-请选择-" });
            try
            {
                if (pointList != null && pointList.Count > 0)
                {
                    foreach (var item in pointList)
                    {
                        SelectListItem selectItem = new SelectListItem()
                        {
                            Value = item.SEQ.ToString(),
                            Text = item.PointName
                        };
                        result.Add(selectItem);
                    }

                }
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }
        /// <summary>
        /// 代售点上传文件转换
        /// </summary>
        public void ConvertFileToEntity(HttpRequestBase request, SalePointPageModel model)
        {
            string filepath = ConfigurationManager.AppSettings["FilePath"].ToString();
            if (request.Files != null)
            {
                var SBLicenceInfo = new StringBuilder();
                var SBContractInfo = new StringBuilder();
                var SBDepositInf = new StringBuilder();
                var SBAreaInfo = new StringBuilder();
                var SBSaleMachine = new StringBuilder();
                var SBApproveInfo = new StringBuilder();
                var SBLinkPointInfo = new StringBuilder();
                for (int i = 0; i < request.Files.Count; i++)
                {
                    if (request.Files[i].ContentLength > 0)
                    {
                        HttpPostedFileBase file = request.Files[i];
                        string CFileName = Guid.NewGuid().ToString() + "." + file.FileName.Split('.')[1];
                        var filename = Path.Combine(string.Format("{0}{1}", request.PhysicalApplicationPath, filepath), CFileName);
                        file.SaveAs(filename);
                        if (request.Files.Keys[i].ToString() == "LicenceInfo")
                        {
                            SBLicenceInfo.AppendFormat("{0};", CFileName);
                        }
                        else if (request.Files.Keys[i].ToString() == "ContractInfo")
                        {
                            SBContractInfo.AppendFormat("{0};", CFileName);
                        }
                        else if (request.Files.Keys[i].ToString() == "DepositInf")
                        {
                            SBDepositInf.AppendFormat("{0};", CFileName);
                        }
                        else if (request.Files.Keys[i].ToString() == "AreaInfo")
                        {
                            SBAreaInfo.AppendFormat("{0};", CFileName);
                        }
                        else if (request.Files.Keys[i].ToString() == "SaleMachine")
                        {
                            SBSaleMachine.AppendFormat("{0};", CFileName);
                        }
                        else if (request.Files.Keys[i].ToString() == "ApproveInfo")
                        {
                            SBApproveInfo.AppendFormat("{0};", CFileName);
                        }
                        else if (request.Files.Keys[i].ToString() == "LinkPointInfo")
                        {
                            SBLinkPointInfo.AppendFormat("{0};", CFileName);
                        }
                    }
                }
                if (SBLicenceInfo.Length > 0)
                {
                    model.PageModel.LicenceInfo = SBLicenceInfo.ToString();
                }
                if (SBContractInfo.Length > 0)
                {
                    model.PageModel.ContractInfo = SBContractInfo.ToString();
                }
                if (SBDepositInf.Length > 0)
                {
                    model.PageModel.DepositInf = SBDepositInf.ToString();
                }
                if (SBAreaInfo.Length > 0)
                {
                    model.PageModel.AreaInfo = SBAreaInfo.ToString();
                }
                if (SBSaleMachine.Length > 0)
                {
                    model.PageModel.SaleMachine = SBSaleMachine.ToString();
                }
                if (SBApproveInfo.Length > 0)
                {
                    model.PageModel.ApproveInfo = SBApproveInfo.ToString();
                }
                if (SBLinkPointInfo.Length > 0)
                {
                    model.PageModel.LinkPointInfo = SBLinkPointInfo.ToString();
                }
            }
        }

        public void ConvertFileToEntity(HttpRequestBase request, Ticket_NoticoPageModel model)
        {
            string filepath = ConfigurationManager.AppSettings["FilePath"].ToString();
            if (request.Files != null)
            {
                for (int i = 0; i < request.Files.Count; i++)
                {
                    if (request.Files[i].ContentLength > 0)
                    {
                        HttpPostedFileBase file = request.Files[i];
                        string CFileName = Guid.NewGuid().ToString() + "." + file.FileName.Split('.')[1];
                        var filename = Path.Combine(string.Format("{0}{1}", request.PhysicalApplicationPath, filepath), CFileName);
                        file.SaveAs(filename);
                        model.PageModel.Content = CFileName;
                    }
                }
            }
        }


        public List<SelectListItem> ConvertGroupList(List<GroupInfo> companyList)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem() { Value = "0", Text = "-请选择-" });
            try
            {
                if (companyList != null && companyList.Count > 0)
                {
                    foreach (var item in companyList)
                    {
                        SelectListItem selectItem = new SelectListItem()
                        {
                            Value = item.SEQ.ToString(),
                            Text = item.GroupName
                        };
                        result.Add(selectItem);
                    }

                }
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }
    }
}