﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TicketManage.Report.ReportView
{
    public partial class GroupReportByMonthPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindPageList();
                LoadReportInfo();
            }
        }


        public void BindPageList()
        {
       


            int year = DateTime.Now.Year;
            List<ListItem> yearItemList = new List<ListItem>() { 
                new ListItem(){Value="0",Text="选择年份"},
                new ListItem(){Value=(year-4).ToString(),Text=(year-4).ToString()},
                new ListItem(){Value=(year-3).ToString(),Text=(year-3).ToString()},
                new ListItem(){Value=(year-2).ToString(),Text=(year-2).ToString()},
                new ListItem(){Value=(year-1).ToString(),Text=(year-1).ToString()},
                new ListItem(){Value=year.ToString(),Text=year.ToString()},
            };
            string connStr = ConfigurationManager.ConnectionStrings["TicketManageConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            SqlCommand companyCommand = new SqlCommand("Select * from CompanyInfo", con);
            DataSet CompanyDS = new DataSet();
            SqlDataAdapter CompanyAdp = new SqlDataAdapter(companyCommand);
            CompanyAdp.Fill(CompanyDS);
            CompanyList.DataSource = CompanyDS;
            CompanyList.DataValueField = "CompanyName";
            CompanyList.DataTextField = "CompanyName";
            CompanyList.DataBind();
            CompanyList.Items.Insert(0, new ListItem("请选择", "0", true));



            SqlCommand PointCommand = new SqlCommand("Select * from SalePointInfo", con);
            DataSet PointDS = new DataSet();
            SqlDataAdapter PointAdp = new SqlDataAdapter(PointCommand);
            PointAdp.Fill(PointDS);
            SalePointList.DataSource = PointDS;
            SalePointList.DataValueField = "PointName";
            SalePointList.DataTextField = "PointName";
            SalePointList.DataBind();
            SalePointList.Items.Insert(0, new ListItem("请选择", "0", true));



            YearList.DataSource = yearItemList;
            //MonthList.DataSource = monthItemList;
            YearList.DataBind();
            //MonthList.DataBind();
            YearList.SelectedValue = DateTime.Now.Year.ToString();
            //MonthList.SelectedIndex = (DateTime.Now.Month);
        }

        public void LoadReportInfo()
        {

            string pathStr = "./Report/GroupReportByMonth.rdlc";

            ReportViewer1.LocalReport.ReportPath = pathStr;
            DataSet ds = new DataSet();
            string connStr = ConfigurationManager.ConnectionStrings["TicketManageConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            string SqlStr = "select * from dbo.fn_GroupReportByMonth() where  1 = 1     ";

            if (YearList.SelectedValue != "0")
            {

                SqlStr += " and 年 = '" + YearList.SelectedValue + "' ";

            }
          
            if (CompanyList.SelectedValue != "0")
            {
                SqlStr += " and CompanyName = '" + CompanyList.SelectedValue + "'";

            }

            if (SalePointList.SelectedValue != "0")
            {
                SqlStr += " and PointName = '" + SalePointList.SelectedValue + "'";

            }
            if (!string.IsNullOrEmpty(WindowsNo.Text))
            {

                SqlStr += " and WindowsNo = '" + WindowsNo.Text + "'";
            }
            if (InvoType.SelectedValue != "0")
            {
                SqlStr += " and Category = '" + InvoType.SelectedValue + "'";
            }

            SqlCommand cmd = new SqlCommand(SqlStr, con);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            ReportDataSource rds = new ReportDataSource("GroupByMonth", dt);
            ReportViewer1.LocalReport.DataSources.Clear();

            ReportViewer1.LocalReport.DataSources.Add(rds);

            ReportViewer1.LocalReport.Refresh();
            con.Close();
        }

        protected void refushBtn_Click(object sender, EventArgs e)
        {
            LoadReportInfo();
        }
    }
}