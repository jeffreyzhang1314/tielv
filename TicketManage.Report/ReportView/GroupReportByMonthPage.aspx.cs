﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TicketManage.Report.ReportView
{
    public partial class GroupReportByMonthPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindPageList();
                LoadReportInfo();
            }
        }


        public void BindPageList()
        {
           // List<ListItem> monthItemList = new List<ListItem>() { 
           //new ListItem(){Value="0",Text="请选择月份"},
           // new ListItem(){Value="1",Text="1月"},
           // new ListItem(){Value="2",Text="2月"},
           // new ListItem(){Value="3",Text="3月"},
           // new ListItem(){Value="4",Text="4月"},
           // new ListItem(){Value="5",Text="5月"},
           // new ListItem(){Value="6",Text="6月"},
           // new ListItem(){Value="7",Text="7月"},
           // new ListItem(){Value="8",Text="8月"},
           // new ListItem(){Value="9",Text="9月"},
           // new ListItem(){Value="10",Text="10月"},
           // new ListItem(){Value="11",Text="11月"},
           // new ListItem(){Value="12",Text="12月"},
           // };


            int year = DateTime.Now.Year;
            List<ListItem> yearItemList = new List<ListItem>() { 
                new ListItem(){Value="0",Text="请选择年份"},
                new ListItem(){Value=(year-4).ToString(),Text=(year-4).ToString()},
                new ListItem(){Value=(year-3).ToString(),Text=(year-3).ToString()},
                new ListItem(){Value=(year-2).ToString(),Text=(year-2).ToString()},
                new ListItem(){Value=(year-1).ToString(),Text=(year-1).ToString()},
                new ListItem(){Value=year.ToString(),Text=year.ToString()},
            };
            SqlConnection con = new SqlConnection(@"Data Source = USERMIC-1K4TJQM\MSSQLSERVER2012;Initial Catalog = TicketManage;User Id = sa;Password = sql2012!;");
            con.Open();
            SqlCommand companyCommand = new SqlCommand("Select * from CompanyInfo", con);
            DataSet CompanyDS = new DataSet();
            SqlDataAdapter CompanyAdp = new SqlDataAdapter(companyCommand);
            CompanyAdp.Fill(CompanyDS);
            CompanyList.DataSource = CompanyDS;
            CompanyList.DataValueField = "CompanyName";
            CompanyList.DataTextField = "CompanyName";
            CompanyList.DataBind();
            CompanyList.Items.Insert(0, new ListItem("请选择", "0", true));



            SqlCommand PointCommand = new SqlCommand("Select * from SalePointInfo", con);
            DataSet PointDS = new DataSet();
            SqlDataAdapter PointAdp = new SqlDataAdapter(PointCommand);
            PointAdp.Fill(PointDS);
            SalePointList.DataSource = PointDS;
            SalePointList.DataValueField = "PointName";
            SalePointList.DataTextField = "PointName";
            SalePointList.DataBind();
            SalePointList.Items.Insert(0, new ListItem("请选择", "0", true));



            YearList.DataSource = yearItemList;
            //MonthList.DataSource = monthItemList;
            YearList.DataBind();
            //MonthList.DataBind();
            YearList.SelectedValue = DateTime.Now.Year.ToString();
            //MonthList.SelectedIndex = (DateTime.Now.Month);
        }

        public void LoadReportInfo()
        {

            ReportViewer1.LocalReport.ReportPath = @"E:\票务网站\TicketManage20170602\TicketManage.Report\Report\GroupReportByMonth.rdlc";

            //如何传参数

            //ReportParameter myParameter = new ReportParameter("参数01", "参数的值");

            //ReportParameter[] myParameterList = { myParameter };

            //ReportViewer1.LocalReport.SetParameters(myParameterList);

            //这个地方的mydts_dts是你定义的dataset,dts是你定义的datatable

            DataSet ds = new DataSet();
            string connStr = ConfigurationManager.ConnectionStrings["TicketManageConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            string SqlStr = "select * from dbo.fn_GroupReportByMonth() where  1 = 1     ";

            if (YearList.SelectedValue != "0")
            {

                SqlStr += " and 年 = '" + YearList.SelectedValue + "' ";

            }
          
            if (CompanyList.SelectedValue != "0")
            {
                SqlStr += " CompanyName = '" + CompanyList.SelectedValue + "'";

            }

            if (SalePointList.SelectedValue != "0")
            {
                SqlStr += " PointName = '" + SalePointList.SelectedValue + "'";

            }
            if (!string.IsNullOrEmpty(WindowsNo.Text))
            {

                SqlStr += " WindowsNo = '" + WindowsNo.Text + "'";
            }
            if (InvoType.SelectedValue != "0")
            {
                SqlStr += " Category = '" + InvoType.SelectedValue + "'";
            }

            SqlCommand cmd = new SqlCommand(SqlStr, con);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            ReportDataSource rds = new ReportDataSource("GroupByMonth", dt);
            ReportViewer1.LocalReport.DataSources.Clear();

            ReportViewer1.LocalReport.DataSources.Add(rds);

            ReportViewer1.LocalReport.Refresh();
            con.Close();
        }

        protected void refushBtn_Click(object sender, EventArgs e)
        {
            LoadReportInfo();
        }
    }
}