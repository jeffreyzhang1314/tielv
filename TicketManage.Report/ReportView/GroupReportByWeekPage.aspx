﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GroupReportByWeekPage.aspx.cs" Inherits="TicketManage.Report.ReportView.GroupReportByWeekPage" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            年
            <asp:DropDownList ID="YearList" runat="server">
            </asp:DropDownList>
         <%--   月
            <asp:DropDownList ID="MonthList" runat="server"></asp:DropDownList>--%>
            单位
            <asp:DropDownList ID="CompanyList" runat="server"></asp:DropDownList>
            代售点
            <asp:DropDownList ID="SalePointList" runat="server"></asp:DropDownList>
            联网站
            <asp:DropDownList ID="LinkList" runat="server"></asp:DropDownList>
            经营性质
            <asp:DropDownList ID="InvoType" runat="server">
                <asp:ListItem Value="0">
                    请选择
                </asp:ListItem>
                <asp:ListItem Value="false">
                    自营
                </asp:ListItem>
                <asp:ListItem Value="true">
                    联营
                </asp:ListItem>
            </asp:DropDownList>
            窗口
            <asp:TextBox ID="WindowsNo" runat="server"></asp:TextBox>
            <asp:Button runat="server" OnClick="refushBtn_Click" Text="刷新" ID="refushBtn" />
            <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
            <rsweb:ReportViewer ID="ReportViewer1" SizeToReportContent="true" AsyncRendering="false" ZoomMode="FullPage"
                runat="server" Width="6000px">
            </rsweb:ReportViewer>
        </div>
    </form>
</body>
</html>
