 use [TicketManage]
 GO
 alter table GuideInfo add DirectoryCompany nvarchar(10)
 GO

--SET IDENTITY_INSERT [dbo].[SystemCode] ON 

INSERT [dbo].[SystemCode] ( [CategoryNo], [CategoryName], [CodeNo], [CodeName], [IsParent], [ParentCodeNo], [Memo]) VALUES ( N'DirectoryCompany', N'选派公司', N'凤上分公司', N'凤上分公司', 0, NULL, NULL)
INSERT [dbo].[SystemCode] ( [CategoryNo], [CategoryName], [CodeNo], [CodeName], [IsParent], [ParentCodeNo], [Memo]) VALUES ( N'DirectoryCompany', N'选派公司', N'大连分公司', N'大连分公司', 0, NULL, NULL)
INSERT [dbo].[SystemCode] ( [CategoryNo], [CategoryName], [CodeNo], [CodeName], [IsParent], [ParentCodeNo], [Memo]) VALUES ( N'DirectoryCompany', N'选派公司', N'延边分公司', N'延边分公司', 0, NULL, NULL)
INSERT [dbo].[SystemCode] ( [CategoryNo], [CategoryName], [CodeNo], [CodeName], [IsParent], [ParentCodeNo], [Memo]) VALUES ( N'DirectoryCompany', N'选派公司', N'通化分公司', N'通化分公司', 0, NULL, NULL)
INSERT [dbo].[SystemCode] ( [CategoryNo], [CategoryName], [CodeNo], [CodeName], [IsParent], [ParentCodeNo], [Memo]) VALUES ( N'DirectoryCompany', N'选派公司', N'通辽分公司', N'通辽分公司', 0, NULL, NULL)
INSERT [dbo].[SystemCode] ( [CategoryNo], [CategoryName], [CodeNo], [CodeName], [IsParent], [ParentCodeNo], [Memo]) VALUES ( N'DirectoryCompany', N'选派公司', N'葫芦岛分公司', N'葫芦岛分公司', 0, NULL, NULL)
INSERT [dbo].[SystemCode] ( [CategoryNo], [CategoryName], [CodeNo], [CodeName], [IsParent], [ParentCodeNo], [Memo]) VALUES ( N'DirectoryCompany', N'选派公司', N'吉林分公司', N'吉林分公司', 0, NULL, NULL)
INSERT [dbo].[SystemCode] ( [CategoryNo], [CategoryName], [CodeNo], [CodeName], [IsParent], [ParentCodeNo], [Memo]) VALUES ( N'DirectoryCompany', N'选派公司', N'葫芦岛分公司', N'锦州分公司', 0, NULL, NULL)
INSERT [dbo].[SystemCode] ( [CategoryNo], [CategoryName], [CodeNo], [CodeName], [IsParent], [ParentCodeNo], [Memo]) VALUES ( N'DirectoryCompany', N'选派公司', N'丹东分公司', N'丹东分公司', 0, NULL, NULL)
INSERT [dbo].[SystemCode] ( [CategoryNo], [CategoryName], [CodeNo], [CodeName], [IsParent], [ParentCodeNo], [Memo]) VALUES ( N'DirectoryCompany', N'选派公司', N'长春分公司', N'长春分公司', 0, NULL, NULL)
INSERT [dbo].[SystemCode] ( [CategoryNo], [CategoryName], [CodeNo], [CodeName], [IsParent], [ParentCodeNo], [Memo]) VALUES ( N'DirectoryCompany', N'选派公司', N'沈阳分公司', N'沈阳分公司', 0, NULL, NULL)
--SET IDENTITY_INSERT [dbo].[SystemCode] OFF
