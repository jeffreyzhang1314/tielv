use TicketManage
alter table GX_PlanPersonInfo add Approve1 int default 0;
alter table GX_PlanPersonInfo add Approve2 int default 0;
alter table GX_PlanPersonInfo add Approve3 int default 0;
alter table GX_PlanPersonInfo add Approve4 int default 0;

alter table TicketCustomer add StartTicketNo2 nvarchar(20);
alter table TicketCustomer add EndTicketNo2  nvarchar(20);

alter table TicketUnion add CustomerNumRSFL decimal(12,2);
alter table TicketUnion add SleepNumRSFL  decimal(12,2);
alter table TicketUnion add Summary  text;