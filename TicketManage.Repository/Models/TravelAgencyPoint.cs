using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class TravelAgencyPoint
    {
        public int SEQ { get; set; }
        public Nullable<int> AgencySEQ { get; set; }
        public string TravelName { get; set; }
        public Nullable<decimal> PointNum { get; set; }
        public string PointFile { get; set; }
        public string Memo { get; set; }
        public string CooperationInfo { get; set; }
        public string CreateTime { get; set; }
    }
}
