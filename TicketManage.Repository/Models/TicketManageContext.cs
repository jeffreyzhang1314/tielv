using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using TicketManage.Repository.Models.Mapping;

namespace TicketManage.Repository.Models
{
    public partial class TicketManageContext : DbContext
    {
        static TicketManageContext()
        {
            Database.SetInitializer<TicketManageContext>(null);
        }

        public TicketManageContext()
            : base("Name=TicketManageContext")
        {
        }

        public DbSet<ApplicationRecord> ApplicationRecords { get; set; }
        public DbSet<ApplyBudget> ApplyBudgets { get; set; }
        public DbSet<BudgetAppend> BudgetAppends { get; set; }
        public DbSet<BudgetExpenditure> BudgetExpenditures { get; set; }
        public DbSet<BudgetInfo> BudgetInfoes { get; set; }
        public DbSet<BudgetSplitInfo> BudgetSplitInfoes { get; set; }
        public DbSet<CompanyInfo> CompanyInfoes { get; set; }
        public DbSet<DeptInfo> DeptInfoes { get; set; }
        public DbSet<GroupInfo> GroupInfoes { get; set; }
        public DbSet<GuideInfo> GuideInfoes { get; set; }
        public DbSet<GuideInfoEvaluate> GuideInfoEvaluates { get; set; }
        public DbSet<GX_DeptInfo> GX_DeptInfo { get; set; }
        public DbSet<GX_DutyInfo> GX_DutyInfo { get; set; }
        public DbSet<GX_EmployeeInfo> GX_EmployeeInfo { get; set; }
        public DbSet<GX_EmployeeSalaryDetailInfo> GX_EmployeeSalaryDetailInfo { get; set; }
        public DbSet<GX_EmployeeSalaryInfo> GX_EmployeeSalaryInfo { get; set; }
        public DbSet<GX_GrossProfitCompany> GX_GrossProfitCompany { get; set; }
        public DbSet<GX_GrossProfitInfo> GX_GrossProfitInfo { get; set; }
        public DbSet<GX_PlanInfoYear> GX_PlanInfoYear { get; set; }
        public DbSet<GX_PlanPersonInfo> GX_PlanPersonInfo { get; set; }
        public DbSet<GX_PlanprofitInfoYear> GX_PlanprofitInfoYear { get; set; }
        public DbSet<GX_PostionInfo> GX_PostionInfo { get; set; }
        public DbSet<GX_RankInfo> GX_RankInfo { get; set; }
        public DbSet<GX_SalesManKPI> GX_SalesManKPI { get; set; }
        public DbSet<GX_SalesManKPIReport> GX_SalesManKPIReport { get; set; }
        public DbSet<GX_SalesmanSalaryDetailInfo> GX_SalesmanSalaryDetailInfo { get; set; }
        public DbSet<GX_SalesmanSalaryDetailInfoReport> GX_SalesmanSalaryDetailInfoReport { get; set; }
        public DbSet<GX_SalesmanSalaryInfo> GX_SalesmanSalaryInfo { get; set; }
        public DbSet<GX_TicketSellerSalaryDetailInfo> GX_TicketSellerSalaryDetailInfo { get; set; }
        public DbSet<GX_TicketSellerSalaryReport> GX_TicketSellerSalaryReport { get; set; }
        public DbSet<KidTicketInfo> KidTicketInfoes { get; set; }
        public DbSet<MeetingInfo> MeetingInfoes { get; set; }
        public DbSet<PublicityInfo> PublicityInfoes { get; set; }
        public DbSet<PublicityReceive> PublicityReceives { get; set; }
        public DbSet<PublicityStoctInfo> PublicityStoctInfoes { get; set; }
        public DbSet<ReviewGroup> ReviewGroups { get; set; }
        public DbSet<SalePointInfo> SalePointInfoes { get; set; }
        public DbSet<StockRecord> StockRecords { get; set; }
        public DbSet<SystemCode> SystemCodes { get; set; }
        public DbSet<Ticket_NoticeInfo> Ticket_NoticeInfo { get; set; }
        public DbSet<TicketCustomer> TicketCustomers { get; set; }
        public DbSet<TicketGroup> TicketGroups { get; set; }
        public DbSet<TicketLaSa> TicketLaSas { get; set; }
        public DbSet<TicketPlan> TicketPlans { get; set; }
        public DbSet<TicketUnion> TicketUnions { get; set; }
        public DbSet<TicketUnionBed> TicketUnionBeds { get; set; }
        public DbSet<TransportReport> TransportReports { get; set; }
        public DbSet<TravelAgency> TravelAgencies { get; set; }
        public DbSet<TravelAgencyPlugIn> TravelAgencyPlugIns { get; set; }
        public DbSet<TravelAgencyPoint> TravelAgencyPoints { get; set; }
        public DbSet<TravelLocalAgency> TravelLocalAgencies { get; set; }
        public DbSet<UserDeptInfo> UserDeptInfoes { get; set; }
        public DbSet<UserInfo> UserInfoes { get; set; }
        public DbSet<UserInfo_Other> UserInfo_Other { get; set; }
        public DbSet<V_GroupCompanySaleUserView> V_GroupCompanySaleUserView { get; set; }
        public DbSet<V_GroupCompanySaleView> V_GroupCompanySaleView { get; set; }
        public DbSet<V_GroupCompanyView> V_GroupCompanyView { get; set; }
        public DbSet<V_GuideInfoEvaluateView> V_GuideInfoEvaluateView { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ApplicationRecordMap());
            modelBuilder.Configurations.Add(new ApplyBudgetMap());
            modelBuilder.Configurations.Add(new BudgetAppendMap());
            modelBuilder.Configurations.Add(new BudgetExpenditureMap());
            modelBuilder.Configurations.Add(new BudgetInfoMap());
            modelBuilder.Configurations.Add(new BudgetSplitInfoMap());
            modelBuilder.Configurations.Add(new CompanyInfoMap());
            modelBuilder.Configurations.Add(new DeptInfoMap());
            modelBuilder.Configurations.Add(new GroupInfoMap());
            modelBuilder.Configurations.Add(new GuideInfoMap());
            modelBuilder.Configurations.Add(new GuideInfoEvaluateMap());
            modelBuilder.Configurations.Add(new GX_DeptInfoMap());
            modelBuilder.Configurations.Add(new GX_DutyInfoMap());
            modelBuilder.Configurations.Add(new GX_EmployeeInfoMap());
            modelBuilder.Configurations.Add(new GX_EmployeeSalaryDetailInfoMap());
            modelBuilder.Configurations.Add(new GX_EmployeeSalaryInfoMap());
            modelBuilder.Configurations.Add(new GX_GrossProfitCompanyMap());
            modelBuilder.Configurations.Add(new GX_GrossProfitInfoMap());
            modelBuilder.Configurations.Add(new GX_PlanInfoYearMap());
            modelBuilder.Configurations.Add(new GX_PlanPersonInfoMap());
            modelBuilder.Configurations.Add(new GX_PlanprofitInfoYearMap());
            modelBuilder.Configurations.Add(new GX_PostionInfoMap());
            modelBuilder.Configurations.Add(new GX_RankInfoMap());
            modelBuilder.Configurations.Add(new GX_SalesManKPIMap());
            modelBuilder.Configurations.Add(new GX_SalesManKPIReportMap());
            modelBuilder.Configurations.Add(new GX_SalesmanSalaryDetailInfoMap());
            modelBuilder.Configurations.Add(new GX_SalesmanSalaryDetailInfoReportMap());
            modelBuilder.Configurations.Add(new GX_SalesmanSalaryInfoMap());
            modelBuilder.Configurations.Add(new GX_TicketSellerSalaryDetailInfoMap());
            modelBuilder.Configurations.Add(new GX_TicketSellerSalaryReportMap());
            modelBuilder.Configurations.Add(new KidTicketInfoMap());
            modelBuilder.Configurations.Add(new MeetingInfoMap());
            modelBuilder.Configurations.Add(new PublicityInfoMap());
            modelBuilder.Configurations.Add(new PublicityReceiveMap());
            modelBuilder.Configurations.Add(new PublicityStoctInfoMap());
            modelBuilder.Configurations.Add(new ReviewGroupMap());
            modelBuilder.Configurations.Add(new SalePointInfoMap());
            modelBuilder.Configurations.Add(new StockRecordMap());
            modelBuilder.Configurations.Add(new SystemCodeMap());
            modelBuilder.Configurations.Add(new Ticket_NoticeInfoMap());
            modelBuilder.Configurations.Add(new TicketCustomerMap());
            modelBuilder.Configurations.Add(new TicketGroupMap());
            modelBuilder.Configurations.Add(new TicketLaSaMap());
            modelBuilder.Configurations.Add(new TicketPlanMap());
            modelBuilder.Configurations.Add(new TicketUnionMap());
            modelBuilder.Configurations.Add(new TicketUnionBedMap());
            modelBuilder.Configurations.Add(new TransportReportMap());
            modelBuilder.Configurations.Add(new TravelAgencyMap());
            modelBuilder.Configurations.Add(new TravelAgencyPlugInMap());
            modelBuilder.Configurations.Add(new TravelAgencyPointMap());
            modelBuilder.Configurations.Add(new TravelLocalAgencyMap());
            modelBuilder.Configurations.Add(new UserDeptInfoMap());
            modelBuilder.Configurations.Add(new UserInfoMap());
            modelBuilder.Configurations.Add(new UserInfo_OtherMap());
            modelBuilder.Configurations.Add(new V_GroupCompanySaleUserViewMap());
            modelBuilder.Configurations.Add(new V_GroupCompanySaleViewMap());
            modelBuilder.Configurations.Add(new V_GroupCompanyViewMap());
            modelBuilder.Configurations.Add(new V_GuideInfoEvaluateViewMap());
        }
    }
}
