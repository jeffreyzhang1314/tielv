using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class TravelLocalAgency
    {
        public int SEQ { get; set; }
        public string TravelNo { get; set; }
        public string TravelName { get; set; }
        public string CompanyName { get; set; }
        public string Qualifications { get; set; }
        public string TravelAddress { get; set; }
        public string ScenicArea { get; set; }
        public string Contacts { get; set; }
        public string ContactPhone { get; set; }
        public Nullable<decimal> Point { get; set; }
        public string Memo { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string CategoryArea { get; set; }
        public string CategoryBiz { get; set; }
        public string CooperationInfo { get; set; }
        public string HistoryContent { get; set; }
        public string CooperationCount { get; set; }
        public Nullable<System.DateTime> CooperationDate { get; set; }
    }
}
