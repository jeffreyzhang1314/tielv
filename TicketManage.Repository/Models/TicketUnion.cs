using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class TicketUnion
    {
        public int SEQ { get; set; }
        public Nullable<int> PingRangNum { get; set; }
        public Nullable<int> XinYiZhouNum { get; set; }
        public Nullable<int> KidNum { get; set; }
        public Nullable<int> BackNum { get; set; }
        public string StartNoTicket { get; set; }
        public Nullable<decimal> CustomerNum { get; set; }
        public Nullable<decimal> SleepNum { get; set; }
        public Nullable<decimal> MonthAcount { get; set; }
        public Nullable<int> OperationSEQ { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string EndTicketNo { get; set; }
        public Nullable<decimal> CustomerNumRSFL { get; set; }
        public Nullable<decimal> SleepNumRSFL { get; set; }
        public string Summary { get; set; }
        public string StartNoTicket2 { get; set; }
        public string EndTicketNo2 { get; set; }
    }
}
