using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class TransportReport
    {
        public int SEQ { get; set; }
        public string YearNum { get; set; }
        public string MonthNum { get; set; }
        public Nullable<decimal> z_Ticket { get; set; }
        public Nullable<decimal> w_Ticket { get; set; }
        public Nullable<decimal> z_TicketSleep { get; set; }
        public Nullable<decimal> w_TicketSleep { get; set; }
        public Nullable<decimal> z_TicketPlus { get; set; }
        public Nullable<decimal> w_TickePlust { get; set; }
        public Nullable<decimal> z_TicketSleepPlus { get; set; }
        public Nullable<decimal> w_TicketSleepPlus { get; set; }
        public Nullable<decimal> z_MealMoney { get; set; }
        public Nullable<decimal> w_MealMoney { get; set; }
        public Nullable<decimal> z_DeductionMoney { get; set; }
        public Nullable<decimal> w_DeductionMoney { get; set; }
        public Nullable<decimal> z_PackageMoney { get; set; }
        public Nullable<decimal> w_PackageMoney { get; set; }
        public Nullable<decimal> z_TransitMoney { get; set; }
        public Nullable<decimal> w_TransitMoney { get; set; }
        public Nullable<decimal> z_PatchMoney { get; set; }
        public Nullable<decimal> w_PatchMoney { get; set; }
        public Nullable<decimal> EndMoney { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public Nullable<int> OperationSEQ { get; set; }
    }
}
