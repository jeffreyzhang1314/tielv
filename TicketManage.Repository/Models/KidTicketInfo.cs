using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class KidTicketInfo
    {
        public int SEQ { get; set; }
        public Nullable<int> TicketCustomerSEQ { get; set; }
        public string TicketOutTime { get; set; }
        public string KidTicketContent { get; set; }
        public Nullable<System.DateTime> CreateTIme { get; set; }
    }
}
