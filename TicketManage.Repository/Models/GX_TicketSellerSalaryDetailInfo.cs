using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GX_TicketSellerSalaryDetailInfo
    {
        public int SEQ { get; set; }
        public Nullable<int> UserNo { get; set; }
        public string UserName { get; set; }
        public Nullable<int> DeptSEQ { get; set; }
        public string DeptName { get; set; }
        public Nullable<int> PostionSEQ { get; set; }
        public string PostionName { get; set; }
        public Nullable<int> OperatorYear { get; set; }
        public Nullable<int> OperatorMonth { get; set; }
        public Nullable<System.DateTime> OperatorDay { get; set; }
        public string Company { get; set; }
        public string StartEndTime { get; set; }
        public string TicketNoStart { get; set; }
        public string TicketNoEnd { get; set; }
        public Nullable<int> SaleNum { get; set; }
        public Nullable<int> TicketKidNum { get; set; }
        public Nullable<int> SaleDay { get; set; }
        public Nullable<decimal> PostionSalary { get; set; }
        public Nullable<decimal> PerformanceSalary { get; set; }
        public Nullable<int> SaleActualNum { get; set; }
        public Nullable<int> GroupTicketNum { get; set; }
        public Nullable<int> TotalTicketNum { get; set; }
        public Nullable<decimal> PerformanceReward { get; set; }
        public string InsuranceNoStart { get; set; }
        public string InsuranceNoEnd { get; set; }
        public Nullable<int> GroupTicketNet { get; set; }
        public Nullable<int> LaSaTicketNum { get; set; }
        public Nullable<int> GroupTotalNum { get; set; }
        public Nullable<decimal> PerformanceRewardGroup { get; set; }
        public Nullable<decimal> WagesNum { get; set; }
        public Nullable<int> UserSEQ { get; set; }
        public string OperatorName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
