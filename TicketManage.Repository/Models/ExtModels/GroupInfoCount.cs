﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    public class GroupInfoCount
    {
        /// <summary>
        /// 集团名称
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// 集团下所属分公司数量
        /// </summary>
        public int CompanyNum { get; set; }

        /// <summary>
        /// 分公司下所属代售点数量
        /// </summary>
        public int SalePointNum { get; set; }
    }
}
