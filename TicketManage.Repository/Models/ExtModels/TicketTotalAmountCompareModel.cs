﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    public class TicketTotalAmountCompareModel
    {
        public string Month { get; set; }

        /// <summary>
        /// 集团名称
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 总收入
        /// </summary>
        public Nullable<decimal> TotalAmount { get; set; }

        /// <summary>
        /// 客票收入
        /// </summary>
        public Nullable<decimal> profit { get; set; }
    }
}
