﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    public class TravelAgencyExtPageModel
    {
        public int SEQ { get; set; }
        public string TravelAreaCodeNo { get; set; }
        public string TravelAreaCodeName { get; set; }
        public string TravelAgencyName { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }

        public string AgencyBaseMoney { get; set; }
        public string ContactInfo { get; set; }
        public string TicketGate { get; set; }
        public string BuyItem { get; set; }
        public string StoreName { get; set; }
        public string TravelAgencyFile { get; set; }


        /// <summary>
        /// 房间价格
        /// </summary>
        public string RoomPirce { get; set; }

        /// <summary>
        /// 房间描述
        /// </summary>
        public string RoomSummary { get; set; }

        /// <summary>
        /// 餐价格
        /// </summary>
        public string FoodPirce { get; set; }

        /// <summary>
        /// 餐描述
        /// </summary>
        public string FoodSummary { get; set; }

        /// <summary>
        /// 车价格
        /// </summary>
        public string CarPirce { get; set; }

        /// <summary>
        /// 车描述
        /// </summary>
        public string CarSummary { get; set; }
    }
}
