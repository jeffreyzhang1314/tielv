﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    public class TravelLocalAgencyExtPageModel
    {
       

        /// <summary>
        /// 地接社编号
        /// </summary>
        public string AgencyNo { get; set; }
        /// <summary>
        /// 地接社名称
        /// </summary>
        public string AgencyName { get; set; }

        /// <summary>
        /// 地接社位置
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        public string Contract { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string Telephone { get; set; }

        /// <summary>
        /// 房间价格
        /// </summary>
        public string RoomPirce { get; set; }

        /// <summary>
        /// 房间描述
        /// </summary>
        public string RoomSummary { get; set; }

        /// <summary>
        /// 餐价格
        /// </summary>
        public string FoodPirce { get; set; }

        /// <summary>
        /// 餐描述
        /// </summary>
        public string FoodSummary { get; set; }

        /// <summary>
        /// 车价格
        /// </summary>
        public string CarPirce { get; set; }

        /// <summary>
        /// 车描述
        /// </summary>
        public string CarSummary { get; set; }
    }
}
