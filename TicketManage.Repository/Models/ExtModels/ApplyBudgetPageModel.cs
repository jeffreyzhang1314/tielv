﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    public class ApplyBudgetPageModel
    {
        public int SEQ { get; set; }

        public int BudgetYear { get; set; }

        public string CompanyName { get; set; }

        public string CategoryName { get; set; }

        public Nullable<decimal> budgetMoney { get; set; }
        public string BudgetSplitName { get; set; }
        public Nullable<int> BudgetSEQ { get; set; }
        public string BudgetName { get; set; }
        public Nullable<decimal> ApplyMoney { get; set; }

        /// <summary>
        /// 当前剩余
        /// </summary>
        public Nullable<decimal> CurrentBudgetBalance { get; set; }
        public string Voucher { get; set; }
        public Nullable<int> OperatorSEQ { get; set; }
        public string OperatorName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
