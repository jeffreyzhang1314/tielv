﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    /// <summary>
    /// 导游系统页面实体
    /// </summary>
    public class GuideInfoExtPageModel
    {
        
        /// <summary>
        /// 导游编号
        /// </summary>
        public string GuideNo { get; set; }

        /// <summary>
        /// 导游名称
        /// </summary>
        public string GuideName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Nullable<bool> Sex { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public Nullable<int> Age { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        public string PassportNo { get; set; }

        /// <summary>
        /// 导游证号
        /// </summary>
        public string GuideCardID { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string ContactNo { get; set; }

        /// <summary>
        /// 导游状态
        /// </summary>
        public Nullable<bool> GuideState { get; set; }

        /// <summary>
        /// 带团开始时间
        /// </summary>
        public Nullable<DateTime> StartDate { get; set; }

        /// <summary>
        /// 带团结束时间
        /// </summary>
        public Nullable<DateTime> EndDate { get; set; }

    }
}
