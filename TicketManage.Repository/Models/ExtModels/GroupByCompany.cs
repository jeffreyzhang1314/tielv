﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    public class GroupByCompany
    {
        public int GroupSEQ { get; set; }

        public string GroupName { get; set; }

        public string CompanyName { get; set; }
    }
}
