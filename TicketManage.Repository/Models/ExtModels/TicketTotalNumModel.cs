﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    public class TicketTotalNumModel
    {
        public string Month { get; set; }
        public string GroupName { get; set; }
        /// <summary>
        /// 公司 
        /// </summary>
        public string CompanyName { get; set; }
        public Nullable<int> MonthlyTicketNum { get; set; }
        public Nullable<int> MonthlyPreTicketNum { get; set; } //上月票务收入 
        public Nullable<int> YearCurrentTicketNum { get; set; } //当年票务收入 
        public Nullable<int> YearPreTicketNum { get; set; } //上年票务收入
    }
}
