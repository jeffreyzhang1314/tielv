using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    public partial class TicketCustomerPageModel
    {

        public string CompanyName { get; set; }

        public int? CompanySEQ { get; set; }

        public string SalePointName { get; set; }

        public int? SalePointSEQ { get; set; }

        public string UserName { get; set; }

        public int SEQ { get; set; }
        public Nullable<int> OperationSEQ { get; set; }

        public string CreateTimeShow { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string StartTicketNo { get; set; }
        public string EndTicketNo { get; set; }

        public string StartTicketNo2 { get; set; }
        public string EndTicketNo2 { get; set; }
        public Nullable<int> TicketNum { get; set; }
        public Nullable<int> KidTicketNum { get; set; }
        public Nullable<decimal> Brokerage { get; set; }
        public Nullable<decimal> InsuranceMoney { get; set; }
        public Nullable<int> GroupTicketNum { get; set; }
        public Nullable<int> LaSaTicketNum { get; set; }

        public Nullable<decimal> DivideInto { get; set; }

        /// <summary>
        /// 客票收入（票面金额）
        /// </summary>
        public Nullable<decimal> TicketMoney { get; set; }

    }
}
