﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    /// <summary>
    /// 接口16实体
    /// </summary>
    public class TicketTotalNumModel16
    {
        public string GroupName { get; set; }
        public string Month { get; set; }
        /// <summary>
        /// 公司 
        /// </summary>
        public string CompanyName { get; set; }
        public Nullable<int> RunNum { get; set; }
        public Nullable<int> PreDateRunNum { get; set; } // 
        public Nullable<int> TicketNum { get; set; } // 
        public Nullable<int> AvgTicketNum { get; set; } //

        public Nullable<int> PreDateTicketNum { get; set; }

        public Nullable<int> PreDateAvgTicketNum { get; set; }

        public Nullable<int> TotalTicketNum { get; set; }

        public Nullable<int> TotalPreDateTicketNum { get; set; }

        public Nullable<decimal> MonthAmount { get; set; }

        public Nullable<decimal> MonthPreAmount { get; set; }

        public Nullable<decimal> TotalAmount { get; set; }

        public Nullable<decimal> TotalPreAmount { get; set; }

    }
}
