﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    public class TicketTotalAmountModel
    {
        /// <summary>
        /// 集团
        /// </summary>
        public string GroupName { get; set; }
        /// <summary>
        /// 公司 
        /// </summary>
        public string CompanyName { get; set; }
        public Nullable<decimal> MonthlyTotalAmount { get; set; }
        public Nullable<decimal> MonthlyPreTotalAmount { get; set; } //上月票务收入 
        public Nullable<decimal> YearTotalAmount { get; set; } //当年票务收入 
        public Nullable<decimal> YearPreTotalAmount { get; set; } //上年票务收入

    }
}
