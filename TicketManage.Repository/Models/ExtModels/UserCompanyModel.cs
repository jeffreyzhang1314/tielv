﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    public class UserCompanyModel
    {
        public string UserNo_Other { get; set; }
        public string UserName_Other { get; set; }
        public string UserNo { get; set; }
        public string UserName { get; set; }

        public string CompanyName { get; set; }
    }
}
