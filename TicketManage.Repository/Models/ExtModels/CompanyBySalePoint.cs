﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    /// <summary>
    /// 代售点信息
    /// </summary>
    public class CompanyBySalePoint
    {
        /// <summary>
        /// 集团名称
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// 分公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 代售点名称
        /// </summary>
        public string SalePointName { get; set; }
    }
}
