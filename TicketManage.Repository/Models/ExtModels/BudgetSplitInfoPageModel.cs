﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models.ExtModels
{
    public class BudgetSplitInfoPageModel
    {
        public int SEQ { get; set; }
        public string CompanyName { get; set; }
        public string CategoryName { get; set; }
        public Nullable<int> BudgetYear { get; set; }
        public Nullable<decimal> BudgetMoney { get; set; }
        /// <summary>
        /// 支出费用
        /// </summary>
        public Nullable<decimal> BudgetApplyMoney { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }

        /// <summary>
        /// 追加预算钱数
        /// </summary>
        public Nullable<Decimal> BudgetAppendMoney { get; set; }
    }
}
