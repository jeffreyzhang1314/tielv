using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class TicketCustomer
    {
        public int SEQ { get; set; }
        public Nullable<int> OperationSEQ { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string StartTicketNo { get; set; }
        public string EndTicketNo { get; set; }
        public Nullable<int> TicketNum { get; set; }
        public Nullable<int> KidTicketNum { get; set; }
        public Nullable<decimal> Brokerage { get; set; }
        public Nullable<decimal> InsuranceMoney { get; set; }
        public Nullable<decimal> TicketPrice { get; set; }
        public Nullable<decimal> KidTicketPrice { get; set; }
        public Nullable<int> GroupTicketNum { get; set; }
        public Nullable<int> LaSaTicketNum { get; set; }
        public string StartTicketNo2 { get; set; }
        public string EndTicketNo2 { get; set; }
        public Nullable<decimal> DivideInto { get; set; }
        public Nullable<decimal> TicketMoney { get; set; }
    }
}
