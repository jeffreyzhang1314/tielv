using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class SystemCode
    {
        public int SEQ { get; set; }
        public string CategoryNo { get; set; }
        public string CategoryName { get; set; }
        public string CodeNo { get; set; }
        public string CodeName { get; set; }
        public Nullable<bool> IsParent { get; set; }
        public string ParentCodeNo { get; set; }
        public string Memo { get; set; }
    }
}
