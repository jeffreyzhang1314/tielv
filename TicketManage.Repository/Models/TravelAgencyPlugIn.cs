using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class TravelAgencyPlugIn
    {
        public int SEQ { get; set; }
        public Nullable<int> TravelAgencySEQ { get; set; }
        public Nullable<int> PlugInCategory { get; set; }
        public string Summary { get; set; }
        public string FileContent { get; set; }
        public string PlugInPirce { get; set; }
    }
}
