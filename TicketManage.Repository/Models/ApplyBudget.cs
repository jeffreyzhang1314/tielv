using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class ApplyBudget
    {
        public int SEQ { get; set; }
        public Nullable<int> BudgetSEQ { get; set; }
        public string BudgetName { get; set; }
        public Nullable<decimal> ApplyMoney { get; set; }
        public string Voucher { get; set; }
        public Nullable<int> OperatorSEQ { get; set; }
        public string OperatorName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public Nullable<decimal> CurrentBudgetBalance { get; set; }
    }
}
