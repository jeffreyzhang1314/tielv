using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class TicketLaSa
    {
        public int SEQ { get; set; }
        public Nullable<int> CompanySEQ { get; set; }
        public string CompanyName { get; set; }
        public string TicketWindow { get; set; }
        public Nullable<System.DateTime> StartDriveDate { get; set; }
        public string TrainNo { get; set; }
        public string StartStation { get; set; }
        public string EndSation { get; set; }
        public string TicketCategoryCode { get; set; }
        public string TicketCategoryCodeName { get; set; }
        public Nullable<int> TicketNum { get; set; }
        public Nullable<int> KidNum { get; set; }
        public string StartTicketNo { get; set; }
        public Nullable<decimal> TicketMoney { get; set; }
        public Nullable<decimal> Brokerage { get; set; }
        public Nullable<decimal> InsuranceMoney { get; set; }
        public Nullable<int> OperatorSEQ { get; set; }
        public string OperatorName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public Nullable<decimal> CostMoney { get; set; }
        public Nullable<decimal> TotalMoney { get; set; }
        public string GroupNo { get; set; }
    }
}
