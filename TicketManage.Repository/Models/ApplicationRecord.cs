using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class ApplicationRecord
    {
        public int SEQ { get; set; }
        public Nullable<int> PublicitySEQ { get; set; }
        public Nullable<int> OperationState { get; set; }
        public string Reason { get; set; }
        public Nullable<int> Num { get; set; }
        public string Memo { get; set; }
        public Nullable<int> ApprovalSEQ { get; set; }
        public string ApprovalName { get; set; }
        public Nullable<System.DateTime> ApprovalTime { get; set; }
        public Nullable<int> OperatorSEQ { get; set; }
        public string OperatorName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string ApprovalFile { get; set; }
        public Nullable<int> OperatorCompanySEQ { get; set; }
        public string OperatorCompanyName { get; set; }
        public Nullable<System.DateTime> ConfirmStockDate { get; set; }
    }
}
