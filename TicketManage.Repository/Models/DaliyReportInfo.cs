using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class DaliyReportInfo
    {
        public int SEQ { get; set; }
        public Nullable<int> UserSEQ { get; set; }
        public string ReportMonth { get; set; }
        public string ReportContent { get; set; }
        public Nullable<int> State { get; set; }
    }
}
