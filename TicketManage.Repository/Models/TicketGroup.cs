using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class TicketGroup
    {
        public int SEQ { get; set; }
        public Nullable<int> CompanySEQ { get; set; }
        public string CompanyName { get; set; }
        public string TicketWindow { get; set; }
        public string TicketGetCompany { get; set; }
        public string TicketGetContacts { get; set; }
        public string TicketGetPassportNo { get; set; }
        public string TicketGetContactsNo { get; set; }
        public string OrderNo { get; set; }
        public Nullable<System.DateTime> StartDriveDate { get; set; }
        public string TrainNo { get; set; }
        public string StartStation { get; set; }
        public string EndSation { get; set; }
        public string TicketCategoryCode { get; set; }
        public string TicketCategoryCodeName { get; set; }
        public Nullable<int> TicketNum { get; set; }
        public Nullable<int> KidNum { get; set; }
        public string StartTicketNo1 { get; set; }
        public Nullable<decimal> TicketMoney { get; set; }
        public Nullable<decimal> Brokerage { get; set; }
        public Nullable<decimal> InsuranceMoney { get; set; }
        public Nullable<int> OperatorSEQ { get; set; }
        public string OperatorName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string StartTicketNo2 { get; set; }
        public string StartTicketNo3 { get; set; }
        public string EndTicketNo1 { get; set; }
        public string EndTicketNo2 { get; set; }
        public string EndTicketNo3 { get; set; }
        public string StartInsuranceNo1 { get; set; }
        public string StartInsuranceNo2 { get; set; }
        public string StartInsuranceNo3 { get; set; }
        public string EndInsuranceNo1 { get; set; }
        public string EndInsuranceNo2 { get; set; }
        public string EndInsuranceNo3 { get; set; }
        public string GrossProfitMemo { get; set; }
        public string ServiceMemo { get; set; }
        public string InsuranceMemo { get; set; }
        public string StartTicketNo { get; set; }
    }
}
