using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class DailyReportAssessment
    {
        public int SEQ { get; set; }
        public string AssessmentMonth { get; set; }
        public string DeptNo { get; set; }
        public string DeptName { get; set; }
        public Nullable<decimal> Pointer { get; set; }
        public Nullable<int> LevelNum { get; set; }
        public string AssessmentContent { get; set; }
        public Nullable<int> OperatorSEQ { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
