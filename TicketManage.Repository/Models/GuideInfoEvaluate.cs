using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GuideInfoEvaluate
    {
        public int SEQ { get; set; }
        public Nullable<int> GuideSEQ { get; set; }
        public Nullable<System.DateTime> LeadDate { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Summary { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
        public Nullable<int> StarNum { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
    }
}
