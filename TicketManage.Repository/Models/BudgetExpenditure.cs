using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class BudgetExpenditure
    {
        public int SEQ { get; set; }
        public Nullable<int> BudgetSplitSEQ { get; set; }
        public Nullable<System.DateTime> VoucherDate { get; set; }
        public string Voucher { get; set; }
        public Nullable<int> OperatorSEQ { get; set; }
        public string OperatorName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string Summary { get; set; }
    }
}
