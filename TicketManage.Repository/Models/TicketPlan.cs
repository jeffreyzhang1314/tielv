using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class TicketPlan
    {
        public int SEQ { get; set; }
        public Nullable<int> CompanySEQ { get; set; }
        public string YearStr { get; set; }
        public Nullable<decimal> OneMonthPlan { get; set; }
        public Nullable<decimal> TwoMonthPlan { get; set; }
        public Nullable<decimal> ThreeMonthPlan { get; set; }
        public Nullable<decimal> FourMonthPlan { get; set; }
        public Nullable<decimal> FiveMonthPlan { get; set; }
        public Nullable<decimal> SixMonthPlan { get; set; }
        public Nullable<decimal> SevenMonthPlan { get; set; }
        public Nullable<decimal> EightMonthPlan { get; set; }
        public Nullable<decimal> NineMonthPlan { get; set; }
        public Nullable<decimal> TenMonthPlan { get; set; }
        public Nullable<decimal> EleMonthPlan { get; set; }
        public Nullable<decimal> TwolMonthPlan { get; set; }
        public Nullable<decimal> YearPlan { get; set; }
        public Nullable<decimal> YearPushPlan { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
