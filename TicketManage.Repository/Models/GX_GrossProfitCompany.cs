using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GX_GrossProfitCompany
    {
        public int SEQ { get; set; }
        public string OperatorYear { get; set; }
        public string OperatorMonth { get; set; }
        public string CompanyName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string CompleteBook { get; set; }
        public string GroupMoney { get; set; }
        public string BridgeIncome { get; set; }
        public string UnSettlement { get; set; }
        public string LaSaMoney { get; set; }
        public string XiZheLiMuMoney { get; set; }
        public string PayManageMoney { get; set; }
        public string ActualMoney { get; set; }
        public string ProcessPlan { get; set; }
        public string ProcessPlan2 { get; set; }
        public string RatioPlan { get; set; }
        public string LastYearSame { get; set; }
        public string AnYuan { get; set; }
        public string AnRatio { get; set; }
        public string LastMonthSame { get; set; }
        public string RankNo { get; set; }
        public string SalaryRatio { get; set; }
    }
}
