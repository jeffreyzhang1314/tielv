using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class UserInfo
    {
        public int SEQ { get; set; }
        public Nullable<int> CompanySEQ { get; set; }
        public Nullable<int> SalePointSEQ { get; set; }
        public string UserNo { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public Nullable<bool> Sex { get; set; }
        public string PassportNo { get; set; }
        public Nullable<int> Age { get; set; }
        public string ContactNo { get; set; }
        public Nullable<int> AuthNum { get; set; }
        public Nullable<bool> UserState { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public Nullable<int> GroupSEQ { get; set; }
    }
}
