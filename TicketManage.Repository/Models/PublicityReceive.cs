using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class PublicityReceive
    {
        public int SEQ { get; set; }
        public Nullable<int> PublicitySEQ { get; set; }
        public string publicityName { get; set; }
        public Nullable<int> AppNum { get; set; }
        public Nullable<int> TravelProductSEQ { get; set; }
        public string TravelProdcutName { get; set; }
        public Nullable<int> CustomerNum { get; set; }
        public string Memo { get; set; }
        public Nullable<int> OperatorSEQ { get; set; }
        public string OperatorName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string AppCategory { get; set; }
    }
}
