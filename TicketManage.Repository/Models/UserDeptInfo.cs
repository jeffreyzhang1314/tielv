using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class UserDeptInfo
    {
        public System.Guid UserDeptGUID { get; set; }
        public Nullable<System.Guid> UserGUID { get; set; }
        public Nullable<System.Guid> DeptGUID { get; set; }
        public string CreatedUserNo { get; set; }
        public string CreatedUserName { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> DeleteTime { get; set; }
        public Nullable<System.DateTime> CreatedTime { get; set; }
    }
}
