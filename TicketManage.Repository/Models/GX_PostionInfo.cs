using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GX_PostionInfo
    {
        public int seq { get; set; }
        public string PostionName { get; set; }
        public Nullable<decimal> PostionSalary { get; set; }
        public Nullable<decimal> PerformanceNum { get; set; }
        public Nullable<int> OrderNo { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
