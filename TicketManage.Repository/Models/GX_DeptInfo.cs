using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GX_DeptInfo
    {
        public int seq { get; set; }
        public string DeptNo { get; set; }
        public string DeptName { get; set; }
        public Nullable<int> OrderNo { get; set; }
        public Nullable<int> ParentSEQ { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
