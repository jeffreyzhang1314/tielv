using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GroupInfo
    {
        public int SEQ { get; set; }
        public string GroupNo { get; set; }
        public string GroupName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string Memo { get; set; }
    }
}
