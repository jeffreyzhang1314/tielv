using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class StockRecord
    {
        public int SEQ { get; set; }
        public Nullable<int> PublicitySEQ { get; set; }
        public Nullable<bool> OperationState { get; set; }
        public Nullable<int> Num { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
