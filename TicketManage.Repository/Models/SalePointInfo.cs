using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class SalePointInfo
    {
        public int SEQ { get; set; }
        public Nullable<int> GroupSEQ { get; set; }
        public Nullable<int> CompanySEQ { get; set; }
        public string PointNo { get; set; }
        public string PointName { get; set; }
        public Nullable<bool> Category { get; set; }
        public Nullable<int> LinkPointSEQ { get; set; }
        public string WindowNo { get; set; }
        public string PointAddress { get; set; }
        public string ContactNo { get; set; }
        public string LicenceInfo { get; set; }
        public string ContractInfo { get; set; }
        public string DepositInf { get; set; }
        public string AreaInfo { get; set; }
        public string SaleMachine { get; set; }
        public string ApproveInfo { get; set; }
        public Nullable<bool> SalePointState { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string DivideInto { get; set; }
        public string LinkPointBlock { get; set; }
        public string LinkPointInfo { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string Memo { get; set; }
        public string LinkPointName { get; set; }
    }
}
