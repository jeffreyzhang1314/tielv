using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class RbacUserDeptRelation
    {
        public int SEQ { get; set; }
        public Nullable<int> DeptSEQ { get; set; }
        public Nullable<int> UserSEQ { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
