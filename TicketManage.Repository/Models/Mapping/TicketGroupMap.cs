using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class TicketGroupMap : EntityTypeConfiguration<TicketGroup>
    {
        public TicketGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.CompanyName)
                .HasMaxLength(100);

            this.Property(t => t.TicketWindow)
                .HasMaxLength(100);

            this.Property(t => t.TicketGetCompany)
                .HasMaxLength(100);

            this.Property(t => t.TicketGetContacts)
                .HasMaxLength(100);

            this.Property(t => t.TicketGetPassportNo)
                .HasMaxLength(20);

            this.Property(t => t.TicketGetContactsNo)
                .HasMaxLength(20);

            this.Property(t => t.OrderNo)
                .HasMaxLength(40);

            this.Property(t => t.TrainNo)
                .HasMaxLength(20);

            this.Property(t => t.StartStation)
                .HasMaxLength(40);

            this.Property(t => t.EndSation)
                .HasMaxLength(40);

            this.Property(t => t.TicketCategoryCode)
                .HasMaxLength(20);

            this.Property(t => t.TicketCategoryCodeName)
                .HasMaxLength(40);

            this.Property(t => t.StartTicketNo1)
                .HasMaxLength(50);

            this.Property(t => t.OperatorName)
                .HasMaxLength(40);

            this.Property(t => t.StartTicketNo2)
                .HasMaxLength(50);

            this.Property(t => t.StartTicketNo3)
                .HasMaxLength(50);

            this.Property(t => t.EndTicketNo1)
                .HasMaxLength(50);

            this.Property(t => t.EndTicketNo2)
                .HasMaxLength(50);

            this.Property(t => t.EndTicketNo3)
                .HasMaxLength(50);

            this.Property(t => t.StartInsuranceNo1)
                .HasMaxLength(50);

            this.Property(t => t.StartInsuranceNo2)
                .HasMaxLength(50);

            this.Property(t => t.StartInsuranceNo3)
                .HasMaxLength(50);

            this.Property(t => t.EndInsuranceNo1)
                .HasMaxLength(50);

            this.Property(t => t.EndInsuranceNo2)
                .HasMaxLength(50);

            this.Property(t => t.EndInsuranceNo3)
                .HasMaxLength(50);

            this.Property(t => t.GrossProfitMemo)
                .HasMaxLength(400);

            this.Property(t => t.ServiceMemo)
                .HasMaxLength(400);

            this.Property(t => t.InsuranceMemo)
                .HasMaxLength(400);

            this.Property(t => t.StartTicketNo)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TicketGroup");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.CompanySEQ).HasColumnName("CompanySEQ");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.TicketWindow).HasColumnName("TicketWindow");
            this.Property(t => t.TicketGetCompany).HasColumnName("TicketGetCompany");
            this.Property(t => t.TicketGetContacts).HasColumnName("TicketGetContacts");
            this.Property(t => t.TicketGetPassportNo).HasColumnName("TicketGetPassportNo");
            this.Property(t => t.TicketGetContactsNo).HasColumnName("TicketGetContactsNo");
            this.Property(t => t.OrderNo).HasColumnName("OrderNo");
            this.Property(t => t.StartDriveDate).HasColumnName("StartDriveDate");
            this.Property(t => t.TrainNo).HasColumnName("TrainNo");
            this.Property(t => t.StartStation).HasColumnName("StartStation");
            this.Property(t => t.EndSation).HasColumnName("EndSation");
            this.Property(t => t.TicketCategoryCode).HasColumnName("TicketCategoryCode");
            this.Property(t => t.TicketCategoryCodeName).HasColumnName("TicketCategoryCodeName");
            this.Property(t => t.TicketNum).HasColumnName("TicketNum");
            this.Property(t => t.KidNum).HasColumnName("KidNum");
            this.Property(t => t.StartTicketNo1).HasColumnName("StartTicketNo1");
            this.Property(t => t.TicketMoney).HasColumnName("TicketMoney");
            this.Property(t => t.Brokerage).HasColumnName("Brokerage");
            this.Property(t => t.InsuranceMoney).HasColumnName("InsuranceMoney");
            this.Property(t => t.OperatorSEQ).HasColumnName("OperatorSEQ");
            this.Property(t => t.OperatorName).HasColumnName("OperatorName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.StartTicketNo2).HasColumnName("StartTicketNo2");
            this.Property(t => t.StartTicketNo3).HasColumnName("StartTicketNo3");
            this.Property(t => t.EndTicketNo1).HasColumnName("EndTicketNo1");
            this.Property(t => t.EndTicketNo2).HasColumnName("EndTicketNo2");
            this.Property(t => t.EndTicketNo3).HasColumnName("EndTicketNo3");
            this.Property(t => t.StartInsuranceNo1).HasColumnName("StartInsuranceNo1");
            this.Property(t => t.StartInsuranceNo2).HasColumnName("StartInsuranceNo2");
            this.Property(t => t.StartInsuranceNo3).HasColumnName("StartInsuranceNo3");
            this.Property(t => t.EndInsuranceNo1).HasColumnName("EndInsuranceNo1");
            this.Property(t => t.EndInsuranceNo2).HasColumnName("EndInsuranceNo2");
            this.Property(t => t.EndInsuranceNo3).HasColumnName("EndInsuranceNo3");
            this.Property(t => t.GrossProfitMemo).HasColumnName("GrossProfitMemo");
            this.Property(t => t.ServiceMemo).HasColumnName("ServiceMemo");
            this.Property(t => t.InsuranceMemo).HasColumnName("InsuranceMemo");
            this.Property(t => t.StartTicketNo).HasColumnName("StartTicketNo");
        }
    }
}
