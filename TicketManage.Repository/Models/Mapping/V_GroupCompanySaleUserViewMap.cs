using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class V_GroupCompanySaleUserViewMap : EntityTypeConfiguration<V_GroupCompanySaleUserView>
    {
        public V_GroupCompanySaleUserViewMap()
        {
            // Primary Key
            this.HasKey(t => t.GSEQ);

            // Properties
            this.Property(t => t.GroupNo)
                .HasMaxLength(100);

            this.Property(t => t.GroupName)
                .HasMaxLength(100);

            this.Property(t => t.CompanyNo)
                .HasMaxLength(100);

            this.Property(t => t.CompanyName)
                .HasMaxLength(100);

            this.Property(t => t.PointNo)
                .HasMaxLength(50);

            this.Property(t => t.PointName)
                .HasMaxLength(100);

            this.Property(t => t.WindowNo)
                .HasMaxLength(20);

            this.Property(t => t.PointAddress)
                .HasMaxLength(200);

            this.Property(t => t.ContactNo)
                .HasMaxLength(20);

            this.Property(t => t.ContractInfo)
                .HasMaxLength(200);

            this.Property(t => t.LicenceInfo)
                .HasMaxLength(200);

            this.Property(t => t.DepositInf)
                .HasMaxLength(200);

            this.Property(t => t.SaleMachine)
                .HasMaxLength(200);

            this.Property(t => t.AreaInfo)
                .HasMaxLength(200);

            this.Property(t => t.ApproveInfo)
                .HasMaxLength(200);

            this.Property(t => t.StartTime)
                .HasMaxLength(20);

            this.Property(t => t.EndTime)
                .HasMaxLength(20);

            this.Property(t => t.DivideInto)
                .HasMaxLength(20);

            this.Property(t => t.LinkPointBlock)
                .HasMaxLength(20);

            this.Property(t => t.LinkPointInfo)
                .HasMaxLength(200);

            this.Property(t => t.UserNo)
                .HasMaxLength(20);

            this.Property(t => t.UserName)
                .HasMaxLength(100);

            this.Property(t => t.PassportNo)
                .HasMaxLength(30);

            this.Property(t => t.UContactNo)
                .HasMaxLength(20);

            this.Property(t => t.GSEQ)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("V_GroupCompanySaleUserView");
            this.Property(t => t.GroupNo).HasColumnName("GroupNo");
            this.Property(t => t.GroupName).HasColumnName("GroupName");
            this.Property(t => t.CompanyNo).HasColumnName("CompanyNo");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.PointNo).HasColumnName("PointNo");
            this.Property(t => t.PointName).HasColumnName("PointName");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.LinkPointSEQ).HasColumnName("LinkPointSEQ");
            this.Property(t => t.WindowNo).HasColumnName("WindowNo");
            this.Property(t => t.PointAddress).HasColumnName("PointAddress");
            this.Property(t => t.ContactNo).HasColumnName("ContactNo");
            this.Property(t => t.ContractInfo).HasColumnName("ContractInfo");
            this.Property(t => t.LicenceInfo).HasColumnName("LicenceInfo");
            this.Property(t => t.DepositInf).HasColumnName("DepositInf");
            this.Property(t => t.SaleMachine).HasColumnName("SaleMachine");
            this.Property(t => t.AreaInfo).HasColumnName("AreaInfo");
            this.Property(t => t.ApproveInfo).HasColumnName("ApproveInfo");
            this.Property(t => t.SalePointState).HasColumnName("SalePointState");
            this.Property(t => t.StartTime).HasColumnName("StartTime");
            this.Property(t => t.EndTime).HasColumnName("EndTime");
            this.Property(t => t.DivideInto).HasColumnName("DivideInto");
            this.Property(t => t.LinkPointBlock).HasColumnName("LinkPointBlock");
            this.Property(t => t.LinkPointInfo).HasColumnName("LinkPointInfo");
            this.Property(t => t.UserNo).HasColumnName("UserNo");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.Sex).HasColumnName("Sex");
            this.Property(t => t.PassportNo).HasColumnName("PassportNo");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.UContactNo).HasColumnName("UContactNo");
            this.Property(t => t.UserState).HasColumnName("UserState");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.GSEQ).HasColumnName("GSEQ");
            this.Property(t => t.CSEQ).HasColumnName("CSEQ");
            this.Property(t => t.SSEQ).HasColumnName("SSEQ");
            this.Property(t => t.USEQ).HasColumnName("USEQ");
        }
    }
}
