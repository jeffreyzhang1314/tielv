using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class TicketCustomerMap : EntityTypeConfiguration<TicketCustomer>
    {
        public TicketCustomerMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.StartTicketNo)
                .HasMaxLength(20);

            this.Property(t => t.EndTicketNo)
                .HasMaxLength(20);

            this.Property(t => t.StartTicketNo2)
                .HasMaxLength(20);

            this.Property(t => t.EndTicketNo2)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("TicketCustomer");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.OperationSEQ).HasColumnName("OperationSEQ");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.StartTicketNo).HasColumnName("StartTicketNo");
            this.Property(t => t.EndTicketNo).HasColumnName("EndTicketNo");
            this.Property(t => t.TicketNum).HasColumnName("TicketNum");
            this.Property(t => t.KidTicketNum).HasColumnName("KidTicketNum");
            this.Property(t => t.Brokerage).HasColumnName("Brokerage");
            this.Property(t => t.InsuranceMoney).HasColumnName("InsuranceMoney");
            this.Property(t => t.TicketPrice).HasColumnName("TicketPrice");
            this.Property(t => t.KidTicketPrice).HasColumnName("KidTicketPrice");
            this.Property(t => t.GroupTicketNum).HasColumnName("GroupTicketNum");
            this.Property(t => t.LaSaTicketNum).HasColumnName("LaSaTicketNum");
            this.Property(t => t.StartTicketNo2).HasColumnName("StartTicketNo2");
            this.Property(t => t.EndTicketNo2).HasColumnName("EndTicketNo2");
            this.Property(t => t.DivideInto).HasColumnName("DivideInto");
            this.Property(t => t.TicketMoney).HasColumnName("TicketMoney");
        }
    }
}
