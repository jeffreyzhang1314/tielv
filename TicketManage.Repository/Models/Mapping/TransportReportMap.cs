using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class TransportReportMap : EntityTypeConfiguration<TransportReport>
    {
        public TransportReportMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.YearNum)
                .HasMaxLength(10);

            this.Property(t => t.MonthNum)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("TransportReport");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.YearNum).HasColumnName("YearNum");
            this.Property(t => t.MonthNum).HasColumnName("MonthNum");
            this.Property(t => t.z_Ticket).HasColumnName("z_Ticket");
            this.Property(t => t.w_Ticket).HasColumnName("w_Ticket");
            this.Property(t => t.z_TicketSleep).HasColumnName("z_TicketSleep");
            this.Property(t => t.w_TicketSleep).HasColumnName("w_TicketSleep");
            this.Property(t => t.z_TicketPlus).HasColumnName("z_TicketPlus");
            this.Property(t => t.w_TickePlust).HasColumnName("w_TickePlust");
            this.Property(t => t.z_TicketSleepPlus).HasColumnName("z_TicketSleepPlus");
            this.Property(t => t.w_TicketSleepPlus).HasColumnName("w_TicketSleepPlus");
            this.Property(t => t.z_MealMoney).HasColumnName("z_MealMoney");
            this.Property(t => t.w_MealMoney).HasColumnName("w_MealMoney");
            this.Property(t => t.z_DeductionMoney).HasColumnName("z_DeductionMoney");
            this.Property(t => t.w_DeductionMoney).HasColumnName("w_DeductionMoney");
            this.Property(t => t.z_PackageMoney).HasColumnName("z_PackageMoney");
            this.Property(t => t.w_PackageMoney).HasColumnName("w_PackageMoney");
            this.Property(t => t.z_TransitMoney).HasColumnName("z_TransitMoney");
            this.Property(t => t.w_TransitMoney).HasColumnName("w_TransitMoney");
            this.Property(t => t.z_PatchMoney).HasColumnName("z_PatchMoney");
            this.Property(t => t.w_PatchMoney).HasColumnName("w_PatchMoney");
            this.Property(t => t.EndMoney).HasColumnName("EndMoney");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.OperationSEQ).HasColumnName("OperationSEQ");
        }
    }
}
