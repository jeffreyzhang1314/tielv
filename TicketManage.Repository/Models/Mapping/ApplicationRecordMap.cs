using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class ApplicationRecordMap : EntityTypeConfiguration<ApplicationRecord>
    {
        public ApplicationRecordMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.Reason)
                .HasMaxLength(200);

            this.Property(t => t.Memo)
                .HasMaxLength(200);

            this.Property(t => t.ApprovalName)
                .HasMaxLength(40);

            this.Property(t => t.OperatorName)
                .HasMaxLength(40);

            this.Property(t => t.ApprovalFile)
                .HasMaxLength(200);

            this.Property(t => t.OperatorCompanyName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("ApplicationRecord");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.PublicitySEQ).HasColumnName("PublicitySEQ");
            this.Property(t => t.OperationState).HasColumnName("OperationState");
            this.Property(t => t.Reason).HasColumnName("Reason");
            this.Property(t => t.Num).HasColumnName("Num");
            this.Property(t => t.Memo).HasColumnName("Memo");
            this.Property(t => t.ApprovalSEQ).HasColumnName("ApprovalSEQ");
            this.Property(t => t.ApprovalName).HasColumnName("ApprovalName");
            this.Property(t => t.ApprovalTime).HasColumnName("ApprovalTime");
            this.Property(t => t.OperatorSEQ).HasColumnName("OperatorSEQ");
            this.Property(t => t.OperatorName).HasColumnName("OperatorName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.ApprovalFile).HasColumnName("ApprovalFile");
            this.Property(t => t.OperatorCompanySEQ).HasColumnName("OperatorCompanySEQ");
            this.Property(t => t.OperatorCompanyName).HasColumnName("OperatorCompanyName");
            this.Property(t => t.ConfirmStockDate).HasColumnName("ConfirmStockDate");
        }
    }
}
