using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class TravelLocalAgencyMap : EntityTypeConfiguration<TravelLocalAgency>
    {
        public TravelLocalAgencyMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.TravelNo)
                .HasMaxLength(50);

            this.Property(t => t.TravelName)
                .HasMaxLength(50);

            this.Property(t => t.CompanyName)
                .HasMaxLength(50);

            this.Property(t => t.Qualifications)
                .HasMaxLength(50);

            this.Property(t => t.TravelAddress)
                .HasMaxLength(100);

            this.Property(t => t.ScenicArea)
                .HasMaxLength(100);

            this.Property(t => t.Contacts)
                .HasMaxLength(50);

            this.Property(t => t.ContactPhone)
                .HasMaxLength(50);

            this.Property(t => t.Memo)
                .HasMaxLength(200);

            this.Property(t => t.CategoryArea)
                .HasMaxLength(200);

            this.Property(t => t.CategoryBiz)
                .HasMaxLength(200);

            this.Property(t => t.CooperationInfo)
                .HasMaxLength(200);

            this.Property(t => t.CooperationCount)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("TravelLocalAgency");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.TravelNo).HasColumnName("TravelNo");
            this.Property(t => t.TravelName).HasColumnName("TravelName");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.Qualifications).HasColumnName("Qualifications");
            this.Property(t => t.TravelAddress).HasColumnName("TravelAddress");
            this.Property(t => t.ScenicArea).HasColumnName("ScenicArea");
            this.Property(t => t.Contacts).HasColumnName("Contacts");
            this.Property(t => t.ContactPhone).HasColumnName("ContactPhone");
            this.Property(t => t.Point).HasColumnName("Point");
            this.Property(t => t.Memo).HasColumnName("Memo");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.CategoryArea).HasColumnName("CategoryArea");
            this.Property(t => t.CategoryBiz).HasColumnName("CategoryBiz");
            this.Property(t => t.CooperationInfo).HasColumnName("CooperationInfo");
            this.Property(t => t.HistoryContent).HasColumnName("HistoryContent");
            this.Property(t => t.CooperationCount).HasColumnName("CooperationCount");
            this.Property(t => t.CooperationDate).HasColumnName("CooperationDate");
        }
    }
}
