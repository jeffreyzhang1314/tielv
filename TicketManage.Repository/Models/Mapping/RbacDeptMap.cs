using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class RbacDeptMap : EntityTypeConfiguration<RbacDept>
    {
        public RbacDeptMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.DeptNo)
                .HasMaxLength(100);

            this.Property(t => t.DeptName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("RbacDept");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.DeptNo).HasColumnName("DeptNo");
            this.Property(t => t.DeptName).HasColumnName("DeptName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
