using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_SalesManKPIReportMap : EntityTypeConfiguration<GX_SalesManKPIReport>
    {
        public GX_SalesManKPIReportMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.OperatorYear)
                .HasMaxLength(10);

            this.Property(t => t.OperatorMonth)
                .HasMaxLength(10);

            this.Property(t => t.SubmitUserName)
                .HasMaxLength(50);

            this.Property(t => t.NodeOneUserName)
                .HasMaxLength(50);

            this.Property(t => t.NodeOneSubmitResult)
                .HasMaxLength(50);

            this.Property(t => t.NodeOneSubmitReason)
                .HasMaxLength(100);

            this.Property(t => t.NodeTwoUserName)
                .HasMaxLength(50);

            this.Property(t => t.NodeTwoSubmitResult)
                .HasMaxLength(50);

            this.Property(t => t.NodeTwoSubmitReason)
                .HasMaxLength(100);

            this.Property(t => t.NodeThreeUserName)
                .HasMaxLength(50);

            this.Property(t => t.NodeThreeSubmitResult)
                .HasMaxLength(50);

            this.Property(t => t.NodeThreeSubmitReason)
                .HasMaxLength(100);

            this.Property(t => t.NodeFourUserName)
                .HasMaxLength(50);

            this.Property(t => t.NodeFourSubmitResult)
                .HasMaxLength(50);

            this.Property(t => t.NodeFourSubmitReason)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GX_SalesManKPIReport");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.OperatorYear).HasColumnName("OperatorYear");
            this.Property(t => t.OperatorMonth).HasColumnName("OperatorMonth");
            this.Property(t => t.SubmitUserName).HasColumnName("SubmitUserName");
            this.Property(t => t.SubmitTimeUser).HasColumnName("SubmitTimeUser");
            this.Property(t => t.SubmitState).HasColumnName("SubmitState");
            this.Property(t => t.NodeOneUserName).HasColumnName("NodeOneUserName");
            this.Property(t => t.NodeOneSubmitTime).HasColumnName("NodeOneSubmitTime");
            this.Property(t => t.NodeOneSubmitResult).HasColumnName("NodeOneSubmitResult");
            this.Property(t => t.NodeOneSubmitReason).HasColumnName("NodeOneSubmitReason");
            this.Property(t => t.NodeTwoUserName).HasColumnName("NodeTwoUserName");
            this.Property(t => t.NodeTwoSubmitTime).HasColumnName("NodeTwoSubmitTime");
            this.Property(t => t.NodeTwoSubmitResult).HasColumnName("NodeTwoSubmitResult");
            this.Property(t => t.NodeTwoSubmitReason).HasColumnName("NodeTwoSubmitReason");
            this.Property(t => t.NodeThreeUserName).HasColumnName("NodeThreeUserName");
            this.Property(t => t.NodeThreeSubmitTime).HasColumnName("NodeThreeSubmitTime");
            this.Property(t => t.NodeThreeSubmitResult).HasColumnName("NodeThreeSubmitResult");
            this.Property(t => t.NodeThreeSubmitReason).HasColumnName("NodeThreeSubmitReason");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.NodeFourUserName).HasColumnName("NodeFourUserName");
            this.Property(t => t.NodeFourSubmitTime).HasColumnName("NodeFourSubmitTime");
            this.Property(t => t.NodeFourSubmitResult).HasColumnName("NodeFourSubmitResult");
            this.Property(t => t.NodeFourSubmitReason).HasColumnName("NodeFourSubmitReason");
        }
    }
}
