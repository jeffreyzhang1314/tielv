using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class SystemCodeMap : EntityTypeConfiguration<SystemCode>
    {
        public SystemCodeMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.CategoryNo)
                .HasMaxLength(20);

            this.Property(t => t.CategoryName)
                .HasMaxLength(40);

            this.Property(t => t.CodeNo)
                .HasMaxLength(10);

            this.Property(t => t.CodeName)
                .HasMaxLength(40);

            this.Property(t => t.ParentCodeNo)
                .HasMaxLength(10);

            this.Property(t => t.Memo)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("SystemCode");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.CategoryNo).HasColumnName("CategoryNo");
            this.Property(t => t.CategoryName).HasColumnName("CategoryName");
            this.Property(t => t.CodeNo).HasColumnName("CodeNo");
            this.Property(t => t.CodeName).HasColumnName("CodeName");
            this.Property(t => t.IsParent).HasColumnName("IsParent");
            this.Property(t => t.ParentCodeNo).HasColumnName("ParentCodeNo");
            this.Property(t => t.Memo).HasColumnName("Memo");
        }
    }
}
