using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class TravelAgencyPointMap : EntityTypeConfiguration<TravelAgencyPoint>
    {
        public TravelAgencyPointMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.TravelName)
                .HasMaxLength(50);

            this.Property(t => t.PointFile)
                .HasMaxLength(50);

            this.Property(t => t.Memo)
                .HasMaxLength(200);

            this.Property(t => t.CooperationInfo)
                .HasMaxLength(50);

            this.Property(t => t.CreateTime)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("TravelAgencyPoint");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.AgencySEQ).HasColumnName("AgencySEQ");
            this.Property(t => t.TravelName).HasColumnName("TravelName");
            this.Property(t => t.PointNum).HasColumnName("PointNum");
            this.Property(t => t.PointFile).HasColumnName("PointFile");
            this.Property(t => t.Memo).HasColumnName("Memo");
            this.Property(t => t.CooperationInfo).HasColumnName("CooperationInfo");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
