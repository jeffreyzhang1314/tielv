using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_DutyInfoMap : EntityTypeConfiguration<GX_DutyInfo>
    {
        public GX_DutyInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.seq);

            // Properties
            this.Property(t => t.DutyName)
                .HasMaxLength(50);

            this.Property(t => t.RankName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("GX_DutyInfo");
            this.Property(t => t.seq).HasColumnName("seq");
            this.Property(t => t.DutyName).HasColumnName("DutyName");
            this.Property(t => t.RankSEQ).HasColumnName("RankSEQ");
            this.Property(t => t.RankName).HasColumnName("RankName");
            this.Property(t => t.PerformanceNum).HasColumnName("PerformanceNum");
            this.Property(t => t.OrderNo).HasColumnName("OrderNo");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
