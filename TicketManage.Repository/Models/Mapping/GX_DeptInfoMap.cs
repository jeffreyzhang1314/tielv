using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_DeptInfoMap : EntityTypeConfiguration<GX_DeptInfo>
    {
        public GX_DeptInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.seq);

            // Properties
            this.Property(t => t.DeptNo)
                .HasMaxLength(50);

            this.Property(t => t.DeptName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("GX_DeptInfo");
            this.Property(t => t.seq).HasColumnName("seq");
            this.Property(t => t.DeptNo).HasColumnName("DeptNo");
            this.Property(t => t.DeptName).HasColumnName("DeptName");
            this.Property(t => t.OrderNo).HasColumnName("OrderNo");
            this.Property(t => t.ParentSEQ).HasColumnName("ParentSEQ");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
