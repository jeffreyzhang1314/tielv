using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_PlanPersonInfoMap : EntityTypeConfiguration<GX_PlanPersonInfo>
    {
        public GX_PlanPersonInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.CompanyName)
                .HasMaxLength(100);

            this.Property(t => t.UserName)
                .HasMaxLength(100);

            this.Property(t => t.OperatorYear)
                .HasMaxLength(10);

            this.Property(t => t.DutySEQ)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GX_PlanPersonInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.CompanySEQ).HasColumnName("CompanySEQ");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.UserSEQ).HasColumnName("UserSEQ");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.MonthNum1).HasColumnName("MonthNum1");
            this.Property(t => t.MonthNum2).HasColumnName("MonthNum2");
            this.Property(t => t.MonthNum3).HasColumnName("MonthNum3");
            this.Property(t => t.MonthNum4).HasColumnName("MonthNum4");
            this.Property(t => t.MonthNum5).HasColumnName("MonthNum5");
            this.Property(t => t.MonthNum6).HasColumnName("MonthNum6");
            this.Property(t => t.MonthNum7).HasColumnName("MonthNum7");
            this.Property(t => t.MonthNum8).HasColumnName("MonthNum8");
            this.Property(t => t.MonthNum9).HasColumnName("MonthNum9");
            this.Property(t => t.MonthNum10).HasColumnName("MonthNum10");
            this.Property(t => t.MonthNum11).HasColumnName("MonthNum11");
            this.Property(t => t.MonthNum12).HasColumnName("MonthNum12");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.OperatorYear).HasColumnName("OperatorYear");
            this.Property(t => t.DutySEQ).HasColumnName("DutySEQ");
            this.Property(t => t.Approve1).HasColumnName("Approve1");
            this.Property(t => t.Approve2).HasColumnName("Approve2");
            this.Property(t => t.Approve3).HasColumnName("Approve3");
            this.Property(t => t.Approve4).HasColumnName("Approve4");
        }
    }
}
