using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class CompanyInfoMap : EntityTypeConfiguration<CompanyInfo>
    {
        public CompanyInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.CompanyNo)
                .HasMaxLength(100);

            this.Property(t => t.CompanyName)
                .HasMaxLength(100);

            this.Property(t => t.Memo)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("CompanyInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.GroupSEQ).HasColumnName("GroupSEQ");
            this.Property(t => t.CompanyNo).HasColumnName("CompanyNo");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.Memo).HasColumnName("Memo");
        }
    }
}
