using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class TicketLaSaMap : EntityTypeConfiguration<TicketLaSa>
    {
        public TicketLaSaMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.CompanyName)
                .HasMaxLength(100);

            this.Property(t => t.TicketWindow)
                .HasMaxLength(100);

            this.Property(t => t.TrainNo)
                .HasMaxLength(20);

            this.Property(t => t.StartStation)
                .HasMaxLength(40);

            this.Property(t => t.EndSation)
                .HasMaxLength(40);

            this.Property(t => t.TicketCategoryCode)
                .HasMaxLength(20);

            this.Property(t => t.TicketCategoryCodeName)
                .HasMaxLength(40);

            this.Property(t => t.StartTicketNo)
                .HasMaxLength(50);

            this.Property(t => t.OperatorName)
                .HasMaxLength(40);

            this.Property(t => t.GroupNo)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TicketLaSa");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.CompanySEQ).HasColumnName("CompanySEQ");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.TicketWindow).HasColumnName("TicketWindow");
            this.Property(t => t.StartDriveDate).HasColumnName("StartDriveDate");
            this.Property(t => t.TrainNo).HasColumnName("TrainNo");
            this.Property(t => t.StartStation).HasColumnName("StartStation");
            this.Property(t => t.EndSation).HasColumnName("EndSation");
            this.Property(t => t.TicketCategoryCode).HasColumnName("TicketCategoryCode");
            this.Property(t => t.TicketCategoryCodeName).HasColumnName("TicketCategoryCodeName");
            this.Property(t => t.TicketNum).HasColumnName("TicketNum");
            this.Property(t => t.KidNum).HasColumnName("KidNum");
            this.Property(t => t.StartTicketNo).HasColumnName("StartTicketNo");
            this.Property(t => t.TicketMoney).HasColumnName("TicketMoney");
            this.Property(t => t.Brokerage).HasColumnName("Brokerage");
            this.Property(t => t.InsuranceMoney).HasColumnName("InsuranceMoney");
            this.Property(t => t.OperatorSEQ).HasColumnName("OperatorSEQ");
            this.Property(t => t.OperatorName).HasColumnName("OperatorName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.CostMoney).HasColumnName("CostMoney");
            this.Property(t => t.TotalMoney).HasColumnName("TotalMoney");
            this.Property(t => t.GroupNo).HasColumnName("GroupNo");
        }
    }
}
