using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class ApplyBudgetMap : EntityTypeConfiguration<ApplyBudget>
    {
        public ApplyBudgetMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.BudgetName)
                .HasMaxLength(100);

            this.Property(t => t.Voucher)
                .HasMaxLength(200);

            this.Property(t => t.OperatorName)
                .HasMaxLength(40);

            // Table & Column Mappings
            this.ToTable("ApplyBudget");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.BudgetSEQ).HasColumnName("BudgetSEQ");
            this.Property(t => t.BudgetName).HasColumnName("BudgetName");
            this.Property(t => t.ApplyMoney).HasColumnName("ApplyMoney");
            this.Property(t => t.Voucher).HasColumnName("Voucher");
            this.Property(t => t.OperatorSEQ).HasColumnName("OperatorSEQ");
            this.Property(t => t.OperatorName).HasColumnName("OperatorName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.CurrentBudgetBalance).HasColumnName("CurrentBudgetBalance");
        }
    }
}
