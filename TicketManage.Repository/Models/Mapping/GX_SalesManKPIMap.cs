using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_SalesManKPIMap : EntityTypeConfiguration<GX_SalesManKPI>
    {
        public GX_SalesManKPIMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.OperatorYear)
                .HasMaxLength(10);

            this.Property(t => t.OperatorMonth)
                .HasMaxLength(10);

            this.Property(t => t.DeptName)
                .HasMaxLength(100);

            this.Property(t => t.UserName)
                .HasMaxLength(100);

            this.Property(t => t.DutyName)
                .HasMaxLength(100);

            this.Property(t => t.PostionSalary)
                .HasMaxLength(200);

            this.Property(t => t.PerformanceNum)
                .HasMaxLength(200);

            this.Property(t => t.GroupPlanNum)
                .HasMaxLength(200);

            this.Property(t => t.PersonPlanNum)
                .HasMaxLength(200);

            this.Property(t => t.ActualRatio)
                .HasMaxLength(200);

            this.Property(t => t.KPINumMonth)
                .HasMaxLength(200);

            this.Property(t => t.KPINumYear)
                .HasMaxLength(200);

            this.Property(t => t.ShouldWages)
                .HasMaxLength(200);

            this.Property(t => t.Memo)
                .HasMaxLength(400);

            this.Property(t => t.KPINumMonthTotal)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("GX_SalesManKPI");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.OperatorYear).HasColumnName("OperatorYear");
            this.Property(t => t.OperatorMonth).HasColumnName("OperatorMonth");
            this.Property(t => t.DeptName).HasColumnName("DeptName");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.DutyName).HasColumnName("DutyName");
            this.Property(t => t.PostionSalary).HasColumnName("PostionSalary");
            this.Property(t => t.PerformanceNum).HasColumnName("PerformanceNum");
            this.Property(t => t.GroupPlanNum).HasColumnName("GroupPlanNum");
            this.Property(t => t.PersonPlanNum).HasColumnName("PersonPlanNum");
            this.Property(t => t.ActualRatio).HasColumnName("ActualRatio");
            this.Property(t => t.KPINumMonth).HasColumnName("KPINumMonth");
            this.Property(t => t.KPINumYear).HasColumnName("KPINumYear");
            this.Property(t => t.ShouldWages).HasColumnName("ShouldWages");
            this.Property(t => t.Memo).HasColumnName("Memo");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.KPINumMonthTotal).HasColumnName("KPINumMonthTotal");
        }
    }
}
