using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class TravelAgencyPlugInMap : EntityTypeConfiguration<TravelAgencyPlugIn>
    {
        public TravelAgencyPlugInMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.Summary)
                .HasMaxLength(200);

            this.Property(t => t.FileContent)
                .HasMaxLength(200);

            this.Property(t => t.PlugInPirce)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("TravelAgencyPlugIn");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.TravelAgencySEQ).HasColumnName("TravelAgencySEQ");
            this.Property(t => t.PlugInCategory).HasColumnName("PlugInCategory");
            this.Property(t => t.Summary).HasColumnName("Summary");
            this.Property(t => t.FileContent).HasColumnName("FileContent");
            this.Property(t => t.PlugInPirce).HasColumnName("PlugInPirce");
        }
    }
}
