using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_PlanprofitInfoYearMap : EntityTypeConfiguration<GX_PlanprofitInfoYear>
    {
        public GX_PlanprofitInfoYearMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.CompanyName)
                .HasMaxLength(100);

            this.Property(t => t.OperatorYear)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("GX_PlanprofitInfoYear");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.CompanySEQ).HasColumnName("CompanySEQ");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.MonthNum1).HasColumnName("MonthNum1");
            this.Property(t => t.MonthNum2).HasColumnName("MonthNum2");
            this.Property(t => t.MonthNum3).HasColumnName("MonthNum3");
            this.Property(t => t.MonthNum4).HasColumnName("MonthNum4");
            this.Property(t => t.MonthNum5).HasColumnName("MonthNum5");
            this.Property(t => t.MonthNum6).HasColumnName("MonthNum6");
            this.Property(t => t.MonthNum7).HasColumnName("MonthNum7");
            this.Property(t => t.MonthNum8).HasColumnName("MonthNum8");
            this.Property(t => t.MonthNum9).HasColumnName("MonthNum9");
            this.Property(t => t.MonthNum10).HasColumnName("MonthNum10");
            this.Property(t => t.MonthNum11).HasColumnName("MonthNum11");
            this.Property(t => t.MonthNum12).HasColumnName("MonthNum12");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.OperatorYear).HasColumnName("OperatorYear");
        }
    }
}
