using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class SalaryAssessmentMap : EntityTypeConfiguration<SalaryAssessment>
    {
        public SalaryAssessmentMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.SalaryAssessmentMonth)
                .HasMaxLength(20);

            this.Property(t => t.UserNo)
                .HasMaxLength(20);

            this.Property(t => t.UserName)
                .HasMaxLength(50);

            this.Property(t => t.UserType)
                .HasMaxLength(50);

            this.Property(t => t.UserPassport)
                .HasMaxLength(50);

            this.Property(t => t.DeptName)
                .HasMaxLength(100);

            this.Property(t => t.UserPostion)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("SalaryAssessment");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.SalaryAssessmentMonth).HasColumnName("SalaryAssessmentMonth");
            this.Property(t => t.UserNo).HasColumnName("UserNo");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.UserPassport).HasColumnName("UserPassport");
            this.Property(t => t.DeptName).HasColumnName("DeptName");
            this.Property(t => t.UserPostion).HasColumnName("UserPostion");
            this.Property(t => t.WorkTime).HasColumnName("WorkTime");
            this.Property(t => t.Evaluate).HasColumnName("Evaluate");
            this.Property(t => t.Achievement).HasColumnName("Achievement");
            this.Property(t => t.OperatorSEQ).HasColumnName("OperatorSEQ");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
