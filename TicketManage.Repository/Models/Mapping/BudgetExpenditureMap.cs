using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class BudgetExpenditureMap : EntityTypeConfiguration<BudgetExpenditure>
    {
        public BudgetExpenditureMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.Voucher)
                .HasMaxLength(200);

            this.Property(t => t.OperatorName)
                .HasMaxLength(40);

            this.Property(t => t.Summary)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("BudgetExpenditure");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.BudgetSplitSEQ).HasColumnName("BudgetSplitSEQ");
            this.Property(t => t.VoucherDate).HasColumnName("VoucherDate");
            this.Property(t => t.Voucher).HasColumnName("Voucher");
            this.Property(t => t.OperatorSEQ).HasColumnName("OperatorSEQ");
            this.Property(t => t.OperatorName).HasColumnName("OperatorName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.Summary).HasColumnName("Summary");
        }
    }
}
