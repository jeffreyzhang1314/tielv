using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class BudgetAppendMap : EntityTypeConfiguration<BudgetAppend>
    {
        public BudgetAppendMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.Voucher)
                .HasMaxLength(200);

            this.Property(t => t.OperatorName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("BudgetAppend");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.BudgetSplitSEQ).HasColumnName("BudgetSplitSEQ");
            this.Property(t => t.BudgetMoney).HasColumnName("BudgetMoney");
            this.Property(t => t.Voucher).HasColumnName("Voucher");
            this.Property(t => t.OperatorSEQ).HasColumnName("OperatorSEQ");
            this.Property(t => t.OperatorName).HasColumnName("OperatorName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
