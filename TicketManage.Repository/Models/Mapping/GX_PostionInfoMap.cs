using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_PostionInfoMap : EntityTypeConfiguration<GX_PostionInfo>
    {
        public GX_PostionInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.seq);

            // Properties
            this.Property(t => t.PostionName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("GX_PostionInfo");
            this.Property(t => t.seq).HasColumnName("seq");
            this.Property(t => t.PostionName).HasColumnName("PostionName");
            this.Property(t => t.PostionSalary).HasColumnName("PostionSalary");
            this.Property(t => t.PerformanceNum).HasColumnName("PerformanceNum");
            this.Property(t => t.OrderNo).HasColumnName("OrderNo");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
