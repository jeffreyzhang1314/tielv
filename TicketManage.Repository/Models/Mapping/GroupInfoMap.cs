using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GroupInfoMap : EntityTypeConfiguration<GroupInfo>
    {
        public GroupInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.GroupNo)
                .HasMaxLength(100);

            this.Property(t => t.GroupName)
                .HasMaxLength(100);

            this.Property(t => t.Memo)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("GroupInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.GroupNo).HasColumnName("GroupNo");
            this.Property(t => t.GroupName).HasColumnName("GroupName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.Memo).HasColumnName("Memo");
        }
    }
}
