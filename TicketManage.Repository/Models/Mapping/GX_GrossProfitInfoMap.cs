using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_GrossProfitInfoMap : EntityTypeConfiguration<GX_GrossProfitInfo>
    {
        public GX_GrossProfitInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.UserName)
                .HasMaxLength(100);

            this.Property(t => t.OperatorYear)
                .HasMaxLength(10);

            this.Property(t => t.OperatorMonth)
                .HasMaxLength(10);

            this.Property(t => t.DutyName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("GX_GrossProfitInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.UserSEQ).HasColumnName("UserSEQ");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.OperatorYear).HasColumnName("OperatorYear");
            this.Property(t => t.OperatorMonth).HasColumnName("OperatorMonth");
            this.Property(t => t.DutySEQ).HasColumnName("DutySEQ");
            this.Property(t => t.DutyName).HasColumnName("DutyName");
            this.Property(t => t.PerformanceNum).HasColumnName("PerformanceNum");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
