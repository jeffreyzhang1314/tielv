using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class UserInfo_OtherMap : EntityTypeConfiguration<UserInfo_Other>
    {
        public UserInfo_OtherMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.UserNo)
                .HasMaxLength(20);

            this.Property(t => t.UserName)
                .HasMaxLength(100);

            this.Property(t => t.UserPassword)
                .HasMaxLength(50);

            this.Property(t => t.PassportNo)
                .HasMaxLength(30);

            this.Property(t => t.ContactNo)
                .HasMaxLength(20);

            this.Property(t => t.CompanyName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("UserInfo_Other");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.UserNo).HasColumnName("UserNo");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.UserPassword).HasColumnName("UserPassword");
            this.Property(t => t.Sex).HasColumnName("Sex");
            this.Property(t => t.PassportNo).HasColumnName("PassportNo");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.ContactNo).HasColumnName("ContactNo");
            this.Property(t => t.AuthNum).HasColumnName("AuthNum");
            this.Property(t => t.UserState).HasColumnName("UserState");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.CompanySEQ).HasColumnName("CompanySEQ");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
        }
    }
}
