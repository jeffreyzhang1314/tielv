using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class PublicityStoctInfoMap : EntityTypeConfiguration<PublicityStoctInfo>
    {
        public PublicityStoctInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.CompanyName)
                .HasMaxLength(100);

            this.Property(t => t.PublicityName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("PublicityStoctInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.CompanySEQ).HasColumnName("CompanySEQ");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.StockNum).HasColumnName("StockNum");
            this.Property(t => t.PublicitySEQ).HasColumnName("PublicitySEQ");
            this.Property(t => t.PublicityName).HasColumnName("PublicityName");
        }
    }
}
