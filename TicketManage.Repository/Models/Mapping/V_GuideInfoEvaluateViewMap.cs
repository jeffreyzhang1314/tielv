using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class V_GuideInfoEvaluateViewMap : EntityTypeConfiguration<V_GuideInfoEvaluateView>
    {
        public V_GuideInfoEvaluateViewMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.SEQ)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ProductCode)
                .HasMaxLength(10);

            this.Property(t => t.ProductName)
                .HasMaxLength(100);

            this.Property(t => t.Summary)
                .HasMaxLength(200);

            this.Property(t => t.LevelCode)
                .HasMaxLength(10);

            this.Property(t => t.LevelName)
                .HasMaxLength(20);

            this.Property(t => t.GuideName)
                .HasMaxLength(20);

            this.Property(t => t.PassportNo)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("V_GuideInfoEvaluateView");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.GuideSEQ).HasColumnName("GuideSEQ");
            this.Property(t => t.LeadDate).HasColumnName("LeadDate");
            this.Property(t => t.ProductCode).HasColumnName("ProductCode");
            this.Property(t => t.ProductName).HasColumnName("ProductName");
            this.Property(t => t.Summary).HasColumnName("Summary");
            this.Property(t => t.LevelCode).HasColumnName("LevelCode");
            this.Property(t => t.LevelName).HasColumnName("LevelName");
            this.Property(t => t.StarNum).HasColumnName("StarNum");
            this.Property(t => t.GuideName).HasColumnName("GuideName");
            this.Property(t => t.Sex).HasColumnName("Sex");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.PassportNo).HasColumnName("PassportNo");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
        }
    }
}
