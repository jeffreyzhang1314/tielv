using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class SalePointInfoMap : EntityTypeConfiguration<SalePointInfo>
    {
        public SalePointInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.PointNo)
                .HasMaxLength(50);

            this.Property(t => t.PointName)
                .HasMaxLength(100);

            this.Property(t => t.WindowNo)
                .HasMaxLength(20);

            this.Property(t => t.PointAddress)
                .HasMaxLength(200);

            this.Property(t => t.ContactNo)
                .HasMaxLength(20);

            this.Property(t => t.StartTime)
                .HasMaxLength(20);

            this.Property(t => t.EndTime)
                .HasMaxLength(20);

            this.Property(t => t.DivideInto)
                .HasMaxLength(20);

            this.Property(t => t.LinkPointBlock)
                .HasMaxLength(20);

            this.Property(t => t.Memo)
                .HasMaxLength(200);

            this.Property(t => t.LinkPointName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("SalePointInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.GroupSEQ).HasColumnName("GroupSEQ");
            this.Property(t => t.CompanySEQ).HasColumnName("CompanySEQ");
            this.Property(t => t.PointNo).HasColumnName("PointNo");
            this.Property(t => t.PointName).HasColumnName("PointName");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.LinkPointSEQ).HasColumnName("LinkPointSEQ");
            this.Property(t => t.WindowNo).HasColumnName("WindowNo");
            this.Property(t => t.PointAddress).HasColumnName("PointAddress");
            this.Property(t => t.ContactNo).HasColumnName("ContactNo");
            this.Property(t => t.LicenceInfo).HasColumnName("LicenceInfo");
            this.Property(t => t.ContractInfo).HasColumnName("ContractInfo");
            this.Property(t => t.DepositInf).HasColumnName("DepositInf");
            this.Property(t => t.AreaInfo).HasColumnName("AreaInfo");
            this.Property(t => t.SaleMachine).HasColumnName("SaleMachine");
            this.Property(t => t.ApproveInfo).HasColumnName("ApproveInfo");
            this.Property(t => t.SalePointState).HasColumnName("SalePointState");
            this.Property(t => t.StartTime).HasColumnName("StartTime");
            this.Property(t => t.EndTime).HasColumnName("EndTime");
            this.Property(t => t.DivideInto).HasColumnName("DivideInto");
            this.Property(t => t.LinkPointBlock).HasColumnName("LinkPointBlock");
            this.Property(t => t.LinkPointInfo).HasColumnName("LinkPointInfo");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.Memo).HasColumnName("Memo");
            this.Property(t => t.LinkPointName).HasColumnName("LinkPointName");
        }
    }
}
