using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class PublicityReceiveMap : EntityTypeConfiguration<PublicityReceive>
    {
        public PublicityReceiveMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.publicityName)
                .HasMaxLength(20);

            this.Property(t => t.TravelProdcutName)
                .HasMaxLength(40);

            this.Property(t => t.Memo)
                .HasMaxLength(200);

            this.Property(t => t.OperatorName)
                .HasMaxLength(40);

            this.Property(t => t.AppCategory)
                .HasMaxLength(40);

            // Table & Column Mappings
            this.ToTable("PublicityReceive");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.PublicitySEQ).HasColumnName("PublicitySEQ");
            this.Property(t => t.publicityName).HasColumnName("publicityName");
            this.Property(t => t.AppNum).HasColumnName("AppNum");
            this.Property(t => t.TravelProductSEQ).HasColumnName("TravelProductSEQ");
            this.Property(t => t.TravelProdcutName).HasColumnName("TravelProdcutName");
            this.Property(t => t.CustomerNum).HasColumnName("CustomerNum");
            this.Property(t => t.Memo).HasColumnName("Memo");
            this.Property(t => t.OperatorSEQ).HasColumnName("OperatorSEQ");
            this.Property(t => t.OperatorName).HasColumnName("OperatorName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.AppCategory).HasColumnName("AppCategory");
        }
    }
}
