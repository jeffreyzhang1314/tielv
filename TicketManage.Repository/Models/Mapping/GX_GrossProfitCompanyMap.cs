using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_GrossProfitCompanyMap : EntityTypeConfiguration<GX_GrossProfitCompany>
    {
        public GX_GrossProfitCompanyMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.OperatorYear)
                .HasMaxLength(10);

            this.Property(t => t.OperatorMonth)
                .HasMaxLength(10);

            this.Property(t => t.CompanyName)
                .HasMaxLength(200);

            this.Property(t => t.CompleteBook)
                .HasMaxLength(200);

            this.Property(t => t.GroupMoney)
                .HasMaxLength(200);

            this.Property(t => t.BridgeIncome)
                .HasMaxLength(200);

            this.Property(t => t.UnSettlement)
                .HasMaxLength(200);

            this.Property(t => t.LaSaMoney)
                .HasMaxLength(200);

            this.Property(t => t.XiZheLiMuMoney)
                .HasMaxLength(200);

            this.Property(t => t.PayManageMoney)
                .HasMaxLength(200);

            this.Property(t => t.ActualMoney)
                .HasMaxLength(200);

            this.Property(t => t.ProcessPlan)
                .HasMaxLength(200);

            this.Property(t => t.ProcessPlan2)
                .HasMaxLength(200);

            this.Property(t => t.RatioPlan)
                .HasMaxLength(200);

            this.Property(t => t.LastYearSame)
                .HasMaxLength(200);

            this.Property(t => t.AnYuan)
                .HasMaxLength(200);

            this.Property(t => t.AnRatio)
                .HasMaxLength(200);

            this.Property(t => t.LastMonthSame)
                .HasMaxLength(200);

            this.Property(t => t.RankNo)
                .HasMaxLength(200);

            this.Property(t => t.SalaryRatio)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("GX_GrossProfitCompany");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.OperatorYear).HasColumnName("OperatorYear");
            this.Property(t => t.OperatorMonth).HasColumnName("OperatorMonth");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.CompleteBook).HasColumnName("CompleteBook");
            this.Property(t => t.GroupMoney).HasColumnName("GroupMoney");
            this.Property(t => t.BridgeIncome).HasColumnName("BridgeIncome");
            this.Property(t => t.UnSettlement).HasColumnName("UnSettlement");
            this.Property(t => t.LaSaMoney).HasColumnName("LaSaMoney");
            this.Property(t => t.XiZheLiMuMoney).HasColumnName("XiZheLiMuMoney");
            this.Property(t => t.PayManageMoney).HasColumnName("PayManageMoney");
            this.Property(t => t.ActualMoney).HasColumnName("ActualMoney");
            this.Property(t => t.ProcessPlan).HasColumnName("ProcessPlan");
            this.Property(t => t.ProcessPlan2).HasColumnName("ProcessPlan2");
            this.Property(t => t.RatioPlan).HasColumnName("RatioPlan");
            this.Property(t => t.LastYearSame).HasColumnName("LastYearSame");
            this.Property(t => t.AnYuan).HasColumnName("AnYuan");
            this.Property(t => t.AnRatio).HasColumnName("AnRatio");
            this.Property(t => t.LastMonthSame).HasColumnName("LastMonthSame");
            this.Property(t => t.RankNo).HasColumnName("RankNo");
            this.Property(t => t.SalaryRatio).HasColumnName("SalaryRatio");
        }
    }
}
