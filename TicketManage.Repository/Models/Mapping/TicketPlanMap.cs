using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class TicketPlanMap : EntityTypeConfiguration<TicketPlan>
    {
        public TicketPlanMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.YearStr)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("TicketPlan");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.CompanySEQ).HasColumnName("CompanySEQ");
            this.Property(t => t.YearStr).HasColumnName("YearStr");
            this.Property(t => t.OneMonthPlan).HasColumnName("OneMonthPlan");
            this.Property(t => t.TwoMonthPlan).HasColumnName("TwoMonthPlan");
            this.Property(t => t.ThreeMonthPlan).HasColumnName("ThreeMonthPlan");
            this.Property(t => t.FourMonthPlan).HasColumnName("FourMonthPlan");
            this.Property(t => t.FiveMonthPlan).HasColumnName("FiveMonthPlan");
            this.Property(t => t.SixMonthPlan).HasColumnName("SixMonthPlan");
            this.Property(t => t.SevenMonthPlan).HasColumnName("SevenMonthPlan");
            this.Property(t => t.EightMonthPlan).HasColumnName("EightMonthPlan");
            this.Property(t => t.NineMonthPlan).HasColumnName("NineMonthPlan");
            this.Property(t => t.TenMonthPlan).HasColumnName("TenMonthPlan");
            this.Property(t => t.EleMonthPlan).HasColumnName("EleMonthPlan");
            this.Property(t => t.TwolMonthPlan).HasColumnName("TwolMonthPlan");
            this.Property(t => t.YearPlan).HasColumnName("YearPlan");
            this.Property(t => t.YearPushPlan).HasColumnName("YearPushPlan");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
