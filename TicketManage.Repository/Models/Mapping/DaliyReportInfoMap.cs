using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class DaliyReportInfoMap : EntityTypeConfiguration<DaliyReportInfo>
    {
        public DaliyReportInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.ReportMonth)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("DaliyReportInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.UserSEQ).HasColumnName("UserSEQ");
            this.Property(t => t.ReportMonth).HasColumnName("ReportMonth");
            this.Property(t => t.ReportContent).HasColumnName("ReportContent");
            this.Property(t => t.State).HasColumnName("State");
        }
    }
}
