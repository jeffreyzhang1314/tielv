using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class RbacUserDeptRelationMap : EntityTypeConfiguration<RbacUserDeptRelation>
    {
        public RbacUserDeptRelationMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            // Table & Column Mappings
            this.ToTable("RbacUserDeptRelation");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.DeptSEQ).HasColumnName("DeptSEQ");
            this.Property(t => t.UserSEQ).HasColumnName("UserSEQ");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
