using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class ReviewGroupMap : EntityTypeConfiguration<ReviewGroup>
    {
        public ReviewGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.GroupName)
                .HasMaxLength(50);

            this.Property(t => t.GroupPerson)
                .HasMaxLength(100);

            this.Property(t => t.ReviewItem_Room)
                .HasMaxLength(200);

            this.Property(t => t.ReviewItem_Food)
                .HasMaxLength(200);

            this.Property(t => t.ReviewItem_Car)
                .HasMaxLength(200);

            this.Property(t => t.OfferPrice)
                .HasMaxLength(20);

            this.Property(t => t.Summary)
                .HasMaxLength(200);

            this.Property(t => t.Memo)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ReviewGroup");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.GroupName).HasColumnName("GroupName");
            this.Property(t => t.GroupPerson).HasColumnName("GroupPerson");
            this.Property(t => t.TravelAgencySEQ).HasColumnName("TravelAgencySEQ");
            this.Property(t => t.ReviewItem_Room).HasColumnName("ReviewItem_Room");
            this.Property(t => t.ReviewItem_Food).HasColumnName("ReviewItem_Food");
            this.Property(t => t.ReviewItem_Car).HasColumnName("ReviewItem_Car");
            this.Property(t => t.OfferPrice).HasColumnName("OfferPrice");
            this.Property(t => t.Summary).HasColumnName("Summary");
            this.Property(t => t.Memo).HasColumnName("Memo");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
