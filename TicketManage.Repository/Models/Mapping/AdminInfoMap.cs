using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class AdminInfoMap : EntityTypeConfiguration<AdminInfo>
    {
        public AdminInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.SEQ)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.UserNo)
                .HasMaxLength(20);

            this.Property(t => t.UserName)
                .HasMaxLength(40);

            // Table & Column Mappings
            this.ToTable("AdminInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.UserNo).HasColumnName("UserNo");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.AuthNum).HasColumnName("AuthNum");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
