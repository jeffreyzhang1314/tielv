using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GuideInfoMap : EntityTypeConfiguration<GuideInfo>
    {
        public GuideInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.GuideNo)
                .HasMaxLength(50);

            this.Property(t => t.GuideName)
                .HasMaxLength(20);

            this.Property(t => t.PassportNo)
                .HasMaxLength(20);

            this.Property(t => t.GuideCardID)
                .HasMaxLength(200);

            this.Property(t => t.ContactNo)
                .HasMaxLength(20);

            this.Property(t => t.PlaceOrigin)
                .HasMaxLength(100);

            this.Property(t => t.Education)
                .HasMaxLength(10);

            this.Property(t => t.Reason)
                .HasMaxLength(200);

            this.Property(t => t.GuidNo)
                .HasMaxLength(50);

            this.Property(t => t.PassportNoFile)
                .HasMaxLength(200);

            this.Property(t => t.GuideCardIDFile)
                .HasMaxLength(200);

            this.Property(t => t.GuidNoFile)
                .HasMaxLength(200);

            this.Property(t => t.DirectoryCompanyNo)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("GuideInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.GuideNo).HasColumnName("GuideNo");
            this.Property(t => t.GuideName).HasColumnName("GuideName");
            this.Property(t => t.Sex).HasColumnName("Sex");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.PassportNo).HasColumnName("PassportNo");
            this.Property(t => t.GuideCardID).HasColumnName("GuideCardID");
            this.Property(t => t.ContactNo).HasColumnName("ContactNo");
            this.Property(t => t.PlaceOrigin).HasColumnName("PlaceOrigin");
            this.Property(t => t.Education).HasColumnName("Education");
            this.Property(t => t.GuideState).HasColumnName("GuideState");
            this.Property(t => t.Reason).HasColumnName("Reason");
            this.Property(t => t.ChangeStateTime).HasColumnName("ChangeStateTime");
            this.Property(t => t.IsGuideCard).HasColumnName("IsGuideCard");
            this.Property(t => t.IsLeaderCard).HasColumnName("IsLeaderCard");
            this.Property(t => t.IsPassportCard).HasColumnName("IsPassportCard");
            this.Property(t => t.IsInterGroup).HasColumnName("IsInterGroup");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.GuideStarNum).HasColumnName("GuideStarNum");
            this.Property(t => t.GuidNo).HasColumnName("GuidNo");
            this.Property(t => t.PassportNoFile).HasColumnName("PassportNoFile");
            this.Property(t => t.GuideCardIDFile).HasColumnName("GuideCardIDFile");
            this.Property(t => t.GuidNoFile).HasColumnName("GuidNoFile");
            this.Property(t => t.DirectoryCompanyNo).HasColumnName("DirectoryCompanyNo");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.IsLeadState).HasColumnName("IsLeadState");
        }
    }
}
