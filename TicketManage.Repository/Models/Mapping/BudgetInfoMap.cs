using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class BudgetInfoMap : EntityTypeConfiguration<BudgetInfo>
    {
        public BudgetInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.BudgetName)
                .HasMaxLength(100);

            this.Property(t => t.OperatorName)
                .HasMaxLength(40);

            // Table & Column Mappings
            this.ToTable("BudgetInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.BudgetName).HasColumnName("BudgetName");
            this.Property(t => t.BudgetMoney).HasColumnName("BudgetMoney");
            this.Property(t => t.ApplyAmount).HasColumnName("ApplyAmount");
            this.Property(t => t.OperatorSEQ).HasColumnName("OperatorSEQ");
            this.Property(t => t.OperatorName).HasColumnName("OperatorName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
