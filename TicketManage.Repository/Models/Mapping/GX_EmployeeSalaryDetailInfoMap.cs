using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_EmployeeSalaryDetailInfoMap : EntityTypeConfiguration<GX_EmployeeSalaryDetailInfo>
    {
        public GX_EmployeeSalaryDetailInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.UserName)
                .HasMaxLength(100);

            this.Property(t => t.DeptName)
                .HasMaxLength(100);

            this.Property(t => t.PostionName)
                .HasMaxLength(100);

            this.Property(t => t.SalaryCardNo)
                .HasMaxLength(50);

            this.Property(t => t.ContractRatio)
                .HasMaxLength(50);

            this.Property(t => t.GrossProfit)
                .HasMaxLength(50);

            this.Property(t => t.ContractRatio2)
                .HasMaxLength(50);

            this.Property(t => t.ComprehensiveRatio)
                .HasMaxLength(50);

            this.Property(t => t.PostionSalary)
                .HasMaxLength(50);

            this.Property(t => t.RankingNum)
                .HasMaxLength(50);

            this.Property(t => t.MoneyInCome)
                .HasMaxLength(50);

            this.Property(t => t.PerformanceSalary)
                .HasMaxLength(50);

            this.Property(t => t.PublicityNum)
                .HasMaxLength(50);

            this.Property(t => t.SeasonSalary)
                .HasMaxLength(50);

            this.Property(t => t.ShouldWages)
                .HasMaxLength(50);

            this.Property(t => t.OperatorName)
                .HasMaxLength(100);

            this.Property(t => t.CompanyName)
                .HasMaxLength(50);

            this.Property(t => t.SalaryStanderAverage)
                .HasMaxLength(50);

            this.Property(t => t.SalaryOther)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("GX_EmployeeSalaryDetailInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.UserNo).HasColumnName("UserNo");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.DeptSEQ).HasColumnName("DeptSEQ");
            this.Property(t => t.DeptName).HasColumnName("DeptName");
            this.Property(t => t.PostionSEQ).HasColumnName("PostionSEQ");
            this.Property(t => t.PostionName).HasColumnName("PostionName");
            this.Property(t => t.OperatorYear).HasColumnName("OperatorYear");
            this.Property(t => t.OperatorMonth).HasColumnName("OperatorMonth");
            this.Property(t => t.OperatorDay).HasColumnName("OperatorDay");
            this.Property(t => t.SalaryCardNo).HasColumnName("SalaryCardNo");
            this.Property(t => t.ContractSignNum).HasColumnName("ContractSignNum");
            this.Property(t => t.ContractRatio).HasColumnName("ContractRatio");
            this.Property(t => t.GrossProfit).HasColumnName("GrossProfit");
            this.Property(t => t.ContractRatio2).HasColumnName("ContractRatio2");
            this.Property(t => t.ComprehensiveRatio).HasColumnName("ComprehensiveRatio");
            this.Property(t => t.PostionSalary).HasColumnName("PostionSalary");
            this.Property(t => t.RankingNum).HasColumnName("RankingNum");
            this.Property(t => t.MoneyInCome).HasColumnName("MoneyInCome");
            this.Property(t => t.PerformanceSalary).HasColumnName("PerformanceSalary");
            this.Property(t => t.PublicityNum).HasColumnName("PublicityNum");
            this.Property(t => t.SeasonSalary).HasColumnName("SeasonSalary");
            this.Property(t => t.ShouldWages).HasColumnName("ShouldWages");
            this.Property(t => t.UserSEQ).HasColumnName("UserSEQ");
            this.Property(t => t.OperatorName).HasColumnName("OperatorName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.CompanySEQ).HasColumnName("CompanySEQ");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.SalaryStanderAverage).HasColumnName("SalaryStanderAverage");
            this.Property(t => t.SalaryOther).HasColumnName("SalaryOther");
        }
    }
}
