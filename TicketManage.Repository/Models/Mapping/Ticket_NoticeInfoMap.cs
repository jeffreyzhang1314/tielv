using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class Ticket_NoticeInfoMap : EntityTypeConfiguration<Ticket_NoticeInfo>
    {
        public Ticket_NoticeInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.Title)
                .HasMaxLength(200);

            this.Property(t => t.Category)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("Ticket_NoticeInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.Content).HasColumnName("Content");
            this.Property(t => t.IsTop).HasColumnName("IsTop");
            this.Property(t => t.PointNum).HasColumnName("PointNum");
            this.Property(t => t.PublisherSEQ).HasColumnName("PublisherSEQ");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
