using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class PublicityInfoMap : EntityTypeConfiguration<PublicityInfo>
    {
        public PublicityInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.PublicityName)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("PublicityInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.PublicityName).HasColumnName("PublicityName");
            this.Property(t => t.StockNum).HasColumnName("StockNum");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
