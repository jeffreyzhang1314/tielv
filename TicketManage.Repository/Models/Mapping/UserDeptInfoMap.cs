using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class UserDeptInfoMap : EntityTypeConfiguration<UserDeptInfo>
    {
        public UserDeptInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.UserDeptGUID);

            // Properties
            this.Property(t => t.CreatedUserNo)
                .HasMaxLength(100);

            this.Property(t => t.CreatedUserName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("UserDeptInfo");
            this.Property(t => t.UserDeptGUID).HasColumnName("UserDeptGUID");
            this.Property(t => t.UserGUID).HasColumnName("UserGUID");
            this.Property(t => t.DeptGUID).HasColumnName("DeptGUID");
            this.Property(t => t.CreatedUserNo).HasColumnName("CreatedUserNo");
            this.Property(t => t.CreatedUserName).HasColumnName("CreatedUserName");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.DeleteTime).HasColumnName("DeleteTime");
            this.Property(t => t.CreatedTime).HasColumnName("CreatedTime");
        }
    }
}
