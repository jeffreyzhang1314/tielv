using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class DeptInfoMap : EntityTypeConfiguration<DeptInfo>
    {
        public DeptInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.DeptGUID);

            // Properties
            this.Property(t => t.DeptName)
                .HasMaxLength(100);

            this.Property(t => t.DeptNo)
                .HasMaxLength(100);

            this.Property(t => t.CreatedUserNo)
                .HasMaxLength(100);

            this.Property(t => t.CreatedUserName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("DeptInfo");
            this.Property(t => t.DeptGUID).HasColumnName("DeptGUID");
            this.Property(t => t.DeptName).HasColumnName("DeptName");
            this.Property(t => t.DeptNo).HasColumnName("DeptNo");
            this.Property(t => t.ParentGUID).HasColumnName("ParentGUID");
            this.Property(t => t.Summary).HasColumnName("Summary");
            this.Property(t => t.CreatedUserNo).HasColumnName("CreatedUserNo");
            this.Property(t => t.CreatedUserName).HasColumnName("CreatedUserName");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.DeleteTime).HasColumnName("DeleteTime");
            this.Property(t => t.CreatedTime).HasColumnName("CreatedTime");
        }
    }
}
