using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class MeetingInfoMap : EntityTypeConfiguration<MeetingInfo>
    {
        public MeetingInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.MeetingTitle)
                .HasMaxLength(100);

            this.Property(t => t.PersonDetial)
                .HasMaxLength(400);

            this.Property(t => t.MeetingContent)
                .HasMaxLength(400);

            this.Property(t => t.FileInfo)
                .HasMaxLength(200);

            this.Property(t => t.OperatorName)
                .HasMaxLength(40);

            // Table & Column Mappings
            this.ToTable("MeetingInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.MeetingTitle).HasColumnName("MeetingTitle");
            this.Property(t => t.MeetingDate).HasColumnName("MeetingDate");
            this.Property(t => t.PersonDetial).HasColumnName("PersonDetial");
            this.Property(t => t.MeetingContent).HasColumnName("MeetingContent");
            this.Property(t => t.FileInfo).HasColumnName("FileInfo");
            this.Property(t => t.OperatorSEQ).HasColumnName("OperatorSEQ");
            this.Property(t => t.OperatorName).HasColumnName("OperatorName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
