using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_SalesmanSalaryDetailInfoMap : EntityTypeConfiguration<GX_SalesmanSalaryDetailInfo>
    {
        public GX_SalesmanSalaryDetailInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.UserName2)
                .HasMaxLength(100);

            this.Property(t => t.DeptName)
                .HasMaxLength(100);

            this.Property(t => t.PostionName)
                .HasMaxLength(100);

            this.Property(t => t.RankName)
                .HasMaxLength(100);

            this.Property(t => t.UserName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GX_SalesmanSalaryDetailInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.UserNo).HasColumnName("UserNo");
            this.Property(t => t.UserName2).HasColumnName("UserName2");
            this.Property(t => t.DeptSEQ).HasColumnName("DeptSEQ");
            this.Property(t => t.DeptName).HasColumnName("DeptName");
            this.Property(t => t.PostionSEQ).HasColumnName("PostionSEQ");
            this.Property(t => t.PostionName).HasColumnName("PostionName");
            this.Property(t => t.RankSEQ).HasColumnName("RankSEQ");
            this.Property(t => t.RankName).HasColumnName("RankName");
            this.Property(t => t.OperatorYear).HasColumnName("OperatorYear");
            this.Property(t => t.OperatorMonth).HasColumnName("OperatorMonth");
            this.Property(t => t.OperatorDay).HasColumnName("OperatorDay");
            this.Property(t => t.PostionSalary).HasColumnName("PostionSalary");
            this.Property(t => t.PerformanceSalary).HasColumnName("PerformanceSalary");
            this.Property(t => t.TargetMonthSalary).HasColumnName("TargetMonthSalary");
            this.Property(t => t.TargetMonthSalaryGroup).HasColumnName("TargetMonthSalaryGroup");
            this.Property(t => t.PerformanceProportion).HasColumnName("PerformanceProportion");
            this.Property(t => t.SeasonSalary).HasColumnName("SeasonSalary");
            this.Property(t => t.CommunicationSalary).HasColumnName("CommunicationSalary");
            this.Property(t => t.UserSEQ).HasColumnName("UserSEQ");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
