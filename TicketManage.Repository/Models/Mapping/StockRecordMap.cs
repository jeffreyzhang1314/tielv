using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class StockRecordMap : EntityTypeConfiguration<StockRecord>
    {
        public StockRecordMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            // Table & Column Mappings
            this.ToTable("StockRecord");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.PublicitySEQ).HasColumnName("PublicitySEQ");
            this.Property(t => t.OperationState).HasColumnName("OperationState");
            this.Property(t => t.Num).HasColumnName("Num");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
