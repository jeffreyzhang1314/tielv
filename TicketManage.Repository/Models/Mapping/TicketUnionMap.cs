using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class TicketUnionMap : EntityTypeConfiguration<TicketUnion>
    {
        public TicketUnionMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.StartNoTicket)
                .HasMaxLength(100);

            this.Property(t => t.EndTicketNo)
                .HasMaxLength(100);

            this.Property(t => t.StartNoTicket2)
                .HasMaxLength(100);

            this.Property(t => t.EndTicketNo2)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TicketUnion");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.PingRangNum).HasColumnName("PingRangNum");
            this.Property(t => t.XinYiZhouNum).HasColumnName("XinYiZhouNum");
            this.Property(t => t.KidNum).HasColumnName("KidNum");
            this.Property(t => t.BackNum).HasColumnName("BackNum");
            this.Property(t => t.StartNoTicket).HasColumnName("StartNoTicket");
            this.Property(t => t.CustomerNum).HasColumnName("CustomerNum");
            this.Property(t => t.SleepNum).HasColumnName("SleepNum");
            this.Property(t => t.MonthAcount).HasColumnName("MonthAcount");
            this.Property(t => t.OperationSEQ).HasColumnName("OperationSEQ");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.EndTicketNo).HasColumnName("EndTicketNo");
            this.Property(t => t.CustomerNumRSFL).HasColumnName("CustomerNumRSFL");
            this.Property(t => t.SleepNumRSFL).HasColumnName("SleepNumRSFL");
            this.Property(t => t.Summary).HasColumnName("Summary");
            this.Property(t => t.StartNoTicket2).HasColumnName("StartNoTicket2");
            this.Property(t => t.EndTicketNo2).HasColumnName("EndTicketNo2");
        }
    }
}
