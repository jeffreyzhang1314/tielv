using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class DailyReportAssessmentMap : EntityTypeConfiguration<DailyReportAssessment>
    {
        public DailyReportAssessmentMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.AssessmentMonth)
                .HasMaxLength(20);

            this.Property(t => t.DeptNo)
                .HasMaxLength(100);

            this.Property(t => t.DeptName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("DailyReportAssessment");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.AssessmentMonth).HasColumnName("AssessmentMonth");
            this.Property(t => t.DeptNo).HasColumnName("DeptNo");
            this.Property(t => t.DeptName).HasColumnName("DeptName");
            this.Property(t => t.Pointer).HasColumnName("Pointer");
            this.Property(t => t.LevelNum).HasColumnName("LevelNum");
            this.Property(t => t.AssessmentContent).HasColumnName("AssessmentContent");
            this.Property(t => t.OperatorSEQ).HasColumnName("OperatorSEQ");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
