using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class BudgetSplitInfoMap : EntityTypeConfiguration<BudgetSplitInfo>
    {
        public BudgetSplitInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.CompanyName)
                .HasMaxLength(100);

            this.Property(t => t.CategoryName)
                .HasMaxLength(50);

            this.Property(t => t.OperatorName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("BudgetSplitInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.CompanySEQ).HasColumnName("CompanySEQ");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.CategorySEQ).HasColumnName("CategorySEQ");
            this.Property(t => t.CategoryName).HasColumnName("CategoryName");
            this.Property(t => t.BudgetYear).HasColumnName("BudgetYear");
            this.Property(t => t.BudgetMoney).HasColumnName("BudgetMoney");
            this.Property(t => t.OperatorSEQ).HasColumnName("OperatorSEQ");
            this.Property(t => t.OperatorName).HasColumnName("OperatorName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
