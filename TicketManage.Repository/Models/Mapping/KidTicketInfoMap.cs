using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class KidTicketInfoMap : EntityTypeConfiguration<KidTicketInfo>
    {
        public KidTicketInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.TicketOutTime)
                .HasMaxLength(20);

            this.Property(t => t.KidTicketContent)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("KidTicketInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.TicketCustomerSEQ).HasColumnName("TicketCustomerSEQ");
            this.Property(t => t.TicketOutTime).HasColumnName("TicketOutTime");
            this.Property(t => t.KidTicketContent).HasColumnName("KidTicketContent");
            this.Property(t => t.CreateTIme).HasColumnName("CreateTIme");
        }
    }
}
