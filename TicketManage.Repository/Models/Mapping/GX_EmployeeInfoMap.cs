using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_EmployeeInfoMap : EntityTypeConfiguration<GX_EmployeeInfo>
    {
        public GX_EmployeeInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.seq);

            // Properties
            this.Property(t => t.EmployeeNo)
                .HasMaxLength(30);

            this.Property(t => t.EmployeeName)
                .HasMaxLength(50);

            this.Property(t => t.LoginPassword)
                .HasMaxLength(50);

            this.Property(t => t.Nation)
                .HasMaxLength(50);

            this.Property(t => t.PassportNo)
                .HasMaxLength(25);

            this.Property(t => t.Birthday)
                .HasMaxLength(20);

            this.Property(t => t.DeptName)
                .HasMaxLength(50);

            this.Property(t => t.DeptMember)
                .HasMaxLength(50);

            this.Property(t => t.PostionName)
                .HasMaxLength(50);

            this.Property(t => t.EmployeeState)
                .HasMaxLength(50);

            this.Property(t => t.HomeAddress)
                .HasMaxLength(50);

            this.Property(t => t.JoinPartyDate)
                .HasMaxLength(50);

            this.Property(t => t.JoinDeptDate)
                .HasMaxLength(50);

            this.Property(t => t.DutyContent)
                .HasMaxLength(200);

            this.Property(t => t.SalaryOther)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GX_EmployeeInfo");
            this.Property(t => t.seq).HasColumnName("seq");
            this.Property(t => t.EmployeeNo).HasColumnName("EmployeeNo");
            this.Property(t => t.EmployeeName).HasColumnName("EmployeeName");
            this.Property(t => t.LoginPassword).HasColumnName("LoginPassword");
            this.Property(t => t.Sex).HasColumnName("Sex");
            this.Property(t => t.Nation).HasColumnName("Nation");
            this.Property(t => t.PassportNo).HasColumnName("PassportNo");
            this.Property(t => t.Birthday).HasColumnName("Birthday");
            this.Property(t => t.IsParty).HasColumnName("IsParty");
            this.Property(t => t.DeptSEQ).HasColumnName("DeptSEQ");
            this.Property(t => t.DeptName).HasColumnName("DeptName");
            this.Property(t => t.DeptMember).HasColumnName("DeptMember");
            this.Property(t => t.PostionSEQ).HasColumnName("PostionSEQ");
            this.Property(t => t.PostionName).HasColumnName("PostionName");
            this.Property(t => t.EmployeeState).HasColumnName("EmployeeState");
            this.Property(t => t.PostionSalary).HasColumnName("PostionSalary");
            this.Property(t => t.PerformanceNum).HasColumnName("PerformanceNum");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.HomeAddress).HasColumnName("HomeAddress");
            this.Property(t => t.JoinPartyDate).HasColumnName("JoinPartyDate");
            this.Property(t => t.JoinDeptDate).HasColumnName("JoinDeptDate");
            this.Property(t => t.IsJob).HasColumnName("IsJob");
            this.Property(t => t.DutyContent).HasColumnName("DutyContent");
            this.Property(t => t.SalaryOther).HasColumnName("SalaryOther");
        }
    }
}
