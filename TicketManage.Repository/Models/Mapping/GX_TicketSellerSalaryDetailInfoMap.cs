using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class GX_TicketSellerSalaryDetailInfoMap : EntityTypeConfiguration<GX_TicketSellerSalaryDetailInfo>
    {
        public GX_TicketSellerSalaryDetailInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.UserName)
                .HasMaxLength(100);

            this.Property(t => t.DeptName)
                .HasMaxLength(100);

            this.Property(t => t.PostionName)
                .HasMaxLength(100);

            this.Property(t => t.Company)
                .HasMaxLength(200);

            this.Property(t => t.StartEndTime)
                .HasMaxLength(200);

            this.Property(t => t.TicketNoStart)
                .HasMaxLength(200);

            this.Property(t => t.TicketNoEnd)
                .HasMaxLength(200);

            this.Property(t => t.InsuranceNoStart)
                .HasMaxLength(200);

            this.Property(t => t.InsuranceNoEnd)
                .HasMaxLength(200);

            this.Property(t => t.OperatorName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GX_TicketSellerSalaryDetailInfo");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.UserNo).HasColumnName("UserNo");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.DeptSEQ).HasColumnName("DeptSEQ");
            this.Property(t => t.DeptName).HasColumnName("DeptName");
            this.Property(t => t.PostionSEQ).HasColumnName("PostionSEQ");
            this.Property(t => t.PostionName).HasColumnName("PostionName");
            this.Property(t => t.OperatorYear).HasColumnName("OperatorYear");
            this.Property(t => t.OperatorMonth).HasColumnName("OperatorMonth");
            this.Property(t => t.OperatorDay).HasColumnName("OperatorDay");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.StartEndTime).HasColumnName("StartEndTime");
            this.Property(t => t.TicketNoStart).HasColumnName("TicketNoStart");
            this.Property(t => t.TicketNoEnd).HasColumnName("TicketNoEnd");
            this.Property(t => t.SaleNum).HasColumnName("SaleNum");
            this.Property(t => t.TicketKidNum).HasColumnName("TicketKidNum");
            this.Property(t => t.SaleDay).HasColumnName("SaleDay");
            this.Property(t => t.PostionSalary).HasColumnName("PostionSalary");
            this.Property(t => t.PerformanceSalary).HasColumnName("PerformanceSalary");
            this.Property(t => t.SaleActualNum).HasColumnName("SaleActualNum");
            this.Property(t => t.GroupTicketNum).HasColumnName("GroupTicketNum");
            this.Property(t => t.TotalTicketNum).HasColumnName("TotalTicketNum");
            this.Property(t => t.PerformanceReward).HasColumnName("PerformanceReward");
            this.Property(t => t.InsuranceNoStart).HasColumnName("InsuranceNoStart");
            this.Property(t => t.InsuranceNoEnd).HasColumnName("InsuranceNoEnd");
            this.Property(t => t.GroupTicketNet).HasColumnName("GroupTicketNet");
            this.Property(t => t.LaSaTicketNum).HasColumnName("LaSaTicketNum");
            this.Property(t => t.GroupTotalNum).HasColumnName("GroupTotalNum");
            this.Property(t => t.PerformanceRewardGroup).HasColumnName("PerformanceRewardGroup");
            this.Property(t => t.WagesNum).HasColumnName("WagesNum");
            this.Property(t => t.UserSEQ).HasColumnName("UserSEQ");
            this.Property(t => t.OperatorName).HasColumnName("OperatorName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
        }
    }
}
