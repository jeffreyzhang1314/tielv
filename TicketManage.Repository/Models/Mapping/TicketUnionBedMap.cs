using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class TicketUnionBedMap : EntityTypeConfiguration<TicketUnionBed>
    {
        public TicketUnionBedMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.StartNoTicket)
                .HasMaxLength(100);

            this.Property(t => t.OperationName)
                .HasMaxLength(50);

            this.Property(t => t.EndTicketNo)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TicketUnionBed");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.TicketNumAll).HasColumnName("TicketNumAll");
            this.Property(t => t.TicketNumBad).HasColumnName("TicketNumBad");
            this.Property(t => t.StartNoTicket).HasColumnName("StartNoTicket");
            this.Property(t => t.MonthAcount).HasColumnName("MonthAcount");
            this.Property(t => t.OperationSEQ).HasColumnName("OperationSEQ");
            this.Property(t => t.OperationName).HasColumnName("OperationName");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.EndTicketNo).HasColumnName("EndTicketNo");
        }
    }
}
