using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TicketManage.Repository.Models.Mapping
{
    public class TravelAgencyMap : EntityTypeConfiguration<TravelAgency>
    {
        public TravelAgencyMap()
        {
            // Primary Key
            this.HasKey(t => t.SEQ);

            // Properties
            this.Property(t => t.TravelAreaCodeNo)
                .HasMaxLength(20);

            this.Property(t => t.TravelAreaCodeName)
                .HasMaxLength(100);

            this.Property(t => t.TravelAgencyName)
                .HasMaxLength(100);

            this.Property(t => t.ContactInfo)
                .HasMaxLength(100);

            this.Property(t => t.TicketGate)
                .HasMaxLength(200);

            this.Property(t => t.BuyItem)
                .HasMaxLength(200);

            this.Property(t => t.StoreName)
                .HasMaxLength(200);

            this.Property(t => t.TravelAgencyFile)
                .HasMaxLength(200);

            this.Property(t => t.AgencyBaseMoney)
                .HasMaxLength(400);

            // Table & Column Mappings
            this.ToTable("TravelAgency");
            this.Property(t => t.SEQ).HasColumnName("SEQ");
            this.Property(t => t.TravelAreaCodeNo).HasColumnName("TravelAreaCodeNo");
            this.Property(t => t.TravelAreaCodeName).HasColumnName("TravelAreaCodeName");
            this.Property(t => t.TravelAgencyName).HasColumnName("TravelAgencyName");
            this.Property(t => t.RecordDate).HasColumnName("RecordDate");
            this.Property(t => t.ContactInfo).HasColumnName("ContactInfo");
            this.Property(t => t.TicketGate).HasColumnName("TicketGate");
            this.Property(t => t.BuyItem).HasColumnName("BuyItem");
            this.Property(t => t.StoreName).HasColumnName("StoreName");
            this.Property(t => t.TravelAgencyFile).HasColumnName("TravelAgencyFile");
            this.Property(t => t.AgencyBaseMoney).HasColumnName("AgencyBaseMoney");
            this.Property(t => t.TrainStartDate).HasColumnName("TrainStartDate");
        }
    }
}
