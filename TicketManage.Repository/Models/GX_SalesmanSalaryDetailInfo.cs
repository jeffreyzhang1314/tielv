using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GX_SalesmanSalaryDetailInfo
    {
        public int SEQ { get; set; }
        public Nullable<int> UserNo { get; set; }
        public string UserName2 { get; set; }
        public Nullable<int> DeptSEQ { get; set; }
        public string DeptName { get; set; }
        public Nullable<int> PostionSEQ { get; set; }
        public string PostionName { get; set; }
        public Nullable<int> RankSEQ { get; set; }
        public string RankName { get; set; }
        public Nullable<int> OperatorYear { get; set; }
        public Nullable<int> OperatorMonth { get; set; }
        public Nullable<System.DateTime> OperatorDay { get; set; }
        public Nullable<decimal> PostionSalary { get; set; }
        public Nullable<decimal> PerformanceSalary { get; set; }
        public Nullable<decimal> TargetMonthSalary { get; set; }
        public Nullable<decimal> TargetMonthSalaryGroup { get; set; }
        public Nullable<decimal> PerformanceProportion { get; set; }
        public Nullable<decimal> SeasonSalary { get; set; }
        public Nullable<decimal> CommunicationSalary { get; set; }
        public Nullable<int> UserSEQ { get; set; }
        public string UserName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
