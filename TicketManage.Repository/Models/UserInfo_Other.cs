using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class UserInfo_Other
    {
        public int SEQ { get; set; }
        public string UserNo { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public Nullable<bool> Sex { get; set; }
        public string PassportNo { get; set; }
        public Nullable<int> Age { get; set; }
        public string ContactNo { get; set; }
        public Nullable<int> AuthNum { get; set; }
        public Nullable<bool> UserState { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public Nullable<int> CompanySEQ { get; set; }
        public string CompanyName { get; set; }
    }
}
