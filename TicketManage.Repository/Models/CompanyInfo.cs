using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class CompanyInfo
    {
        public int SEQ { get; set; }
        public Nullable<int> GroupSEQ { get; set; }
        public string CompanyNo { get; set; }
        public string CompanyName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string Memo { get; set; }
    }
}
