using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class RbacDept
    {
        public int SEQ { get; set; }
        public string DeptNo { get; set; }
        public string DeptName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
