using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class MeetingInfo
    {
        public int SEQ { get; set; }
        public string MeetingTitle { get; set; }
        public Nullable<System.DateTime> MeetingDate { get; set; }
        public string PersonDetial { get; set; }
        public string MeetingContent { get; set; }
        public string FileInfo { get; set; }
        public Nullable<int> OperatorSEQ { get; set; }
        public string OperatorName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
