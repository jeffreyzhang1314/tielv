using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class BudgetInfo
    {
        public int SEQ { get; set; }
        public string BudgetName { get; set; }
        public Nullable<decimal> BudgetMoney { get; set; }
        public Nullable<decimal> ApplyAmount { get; set; }
        public Nullable<int> OperatorSEQ { get; set; }
        public string OperatorName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
