using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class AdminInfo
    {
        public int SEQ { get; set; }
        public string UserNo { get; set; }
        public string UserName { get; set; }
        public Nullable<int> AuthNum { get; set; }
        public Nullable<bool> State { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
