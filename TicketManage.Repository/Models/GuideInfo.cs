using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GuideInfo
    {
        public int SEQ { get; set; }
        public string GuideNo { get; set; }
        public string GuideName { get; set; }
        public Nullable<bool> Sex { get; set; }
        public Nullable<int> Age { get; set; }
        public string PassportNo { get; set; }
        public string GuideCardID { get; set; }
        public string ContactNo { get; set; }
        public string PlaceOrigin { get; set; }
        public string Education { get; set; }
        public Nullable<bool> GuideState { get; set; }
        public string Reason { get; set; }
        public Nullable<System.DateTime> ChangeStateTime { get; set; }
        public Nullable<bool> IsGuideCard { get; set; }
        public Nullable<bool> IsLeaderCard { get; set; }
        public Nullable<bool> IsPassportCard { get; set; }
        public Nullable<bool> IsInterGroup { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public Nullable<int> GuideStarNum { get; set; }
        public string GuidNo { get; set; }
        public string PassportNoFile { get; set; }
        public string GuideCardIDFile { get; set; }
        public string GuidNoFile { get; set; }
        public string DirectoryCompanyNo { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<bool> IsLeadState { get; set; }
    }
}
