using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GX_EmployeeSalaryDetailInfo
    {
        public int SEQ { get; set; }
        public Nullable<int> UserNo { get; set; }
        public string UserName { get; set; }
        public Nullable<int> DeptSEQ { get; set; }
        public string DeptName { get; set; }
        public Nullable<int> PostionSEQ { get; set; }
        public string PostionName { get; set; }
        public Nullable<int> OperatorYear { get; set; }
        public Nullable<int> OperatorMonth { get; set; }
        public Nullable<System.DateTime> OperatorDay { get; set; }
        public string SalaryCardNo { get; set; }
        public Nullable<int> ContractSignNum { get; set; }
        public string ContractRatio { get; set; }
        public string GrossProfit { get; set; }
        public string ContractRatio2 { get; set; }
        public string ComprehensiveRatio { get; set; }
        public string PostionSalary { get; set; }
        public string RankingNum { get; set; }
        public string MoneyInCome { get; set; }
        public string PerformanceSalary { get; set; }
        public string PublicityNum { get; set; }
        public string SeasonSalary { get; set; }
        public string ShouldWages { get; set; }
        public Nullable<int> UserSEQ { get; set; }
        public string OperatorName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public Nullable<int> CompanySEQ { get; set; }
        public string CompanyName { get; set; }
        public string SalaryStanderAverage { get; set; }
        public string SalaryOther { get; set; }
    }
}
