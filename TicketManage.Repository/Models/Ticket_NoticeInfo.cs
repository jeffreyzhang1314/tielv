using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class Ticket_NoticeInfo
    {
        public int SEQ { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public string Content { get; set; }
        public Nullable<bool> IsTop { get; set; }
        public Nullable<int> PointNum { get; set; }
        public Nullable<int> PublisherSEQ { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
