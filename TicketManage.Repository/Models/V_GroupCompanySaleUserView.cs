using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class V_GroupCompanySaleUserView
    {
        public string GroupNo { get; set; }
        public string GroupName { get; set; }
        public string CompanyNo { get; set; }
        public string CompanyName { get; set; }
        public string PointNo { get; set; }
        public string PointName { get; set; }
        public Nullable<bool> Category { get; set; }
        public Nullable<int> LinkPointSEQ { get; set; }
        public string WindowNo { get; set; }
        public string PointAddress { get; set; }
        public string ContactNo { get; set; }
        public string ContractInfo { get; set; }
        public string LicenceInfo { get; set; }
        public string DepositInf { get; set; }
        public string SaleMachine { get; set; }
        public string AreaInfo { get; set; }
        public string ApproveInfo { get; set; }
        public Nullable<bool> SalePointState { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string DivideInto { get; set; }
        public string LinkPointBlock { get; set; }
        public string LinkPointInfo { get; set; }
        public string UserNo { get; set; }
        public string UserName { get; set; }
        public Nullable<bool> Sex { get; set; }
        public string PassportNo { get; set; }
        public Nullable<int> Age { get; set; }
        public string UContactNo { get; set; }
        public Nullable<bool> UserState { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public int GSEQ { get; set; }
        public Nullable<int> CSEQ { get; set; }
        public Nullable<int> SSEQ { get; set; }
        public Nullable<int> USEQ { get; set; }
    }
}
