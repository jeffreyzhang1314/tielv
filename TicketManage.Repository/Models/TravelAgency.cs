using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class TravelAgency
    {
        public int SEQ { get; set; }
        public string TravelAreaCodeNo { get; set; }
        public string TravelAreaCodeName { get; set; }
        public string TravelAgencyName { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }
        public string ContactInfo { get; set; }
        public string TicketGate { get; set; }
        public string BuyItem { get; set; }
        public string StoreName { get; set; }
        public string TravelAgencyFile { get; set; }
        public string AgencyBaseMoney { get; set; }
        public Nullable<System.DateTime> TrainStartDate { get; set; }
    }
}
