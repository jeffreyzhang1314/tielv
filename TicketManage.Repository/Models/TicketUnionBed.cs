using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class TicketUnionBed
    {
        public int SEQ { get; set; }
        public Nullable<int> TicketNumAll { get; set; }
        public Nullable<int> TicketNumBad { get; set; }
        public string StartNoTicket { get; set; }
        public Nullable<decimal> MonthAcount { get; set; }
        public Nullable<int> OperationSEQ { get; set; }
        public string OperationName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string EndTicketNo { get; set; }
    }
}
