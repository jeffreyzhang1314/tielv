using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GX_SalesManKPIReport
    {
        public int SEQ { get; set; }
        public string OperatorYear { get; set; }
        public string OperatorMonth { get; set; }
        public string SubmitUserName { get; set; }
        public Nullable<System.DateTime> SubmitTimeUser { get; set; }
        public Nullable<bool> SubmitState { get; set; }
        public string NodeOneUserName { get; set; }
        public Nullable<System.DateTime> NodeOneSubmitTime { get; set; }
        public string NodeOneSubmitResult { get; set; }
        public string NodeOneSubmitReason { get; set; }
        public string NodeTwoUserName { get; set; }
        public Nullable<System.DateTime> NodeTwoSubmitTime { get; set; }
        public string NodeTwoSubmitResult { get; set; }
        public string NodeTwoSubmitReason { get; set; }
        public string NodeThreeUserName { get; set; }
        public Nullable<System.DateTime> NodeThreeSubmitTime { get; set; }
        public string NodeThreeSubmitResult { get; set; }
        public string NodeThreeSubmitReason { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string NodeFourUserName { get; set; }
        public Nullable<System.DateTime> NodeFourSubmitTime { get; set; }
        public string NodeFourSubmitResult { get; set; }
        public string NodeFourSubmitReason { get; set; }
    }
}
