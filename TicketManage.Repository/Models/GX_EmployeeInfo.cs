using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GX_EmployeeInfo
    {
        public int seq { get; set; }
        public string EmployeeNo { get; set; }
        public string EmployeeName { get; set; }
        public string LoginPassword { get; set; }
        public Nullable<bool> Sex { get; set; }
        public string Nation { get; set; }
        public string PassportNo { get; set; }
        public string Birthday { get; set; }
        public Nullable<bool> IsParty { get; set; }
        public Nullable<int> DeptSEQ { get; set; }
        public string DeptName { get; set; }
        public string DeptMember { get; set; }
        public Nullable<int> PostionSEQ { get; set; }
        public string PostionName { get; set; }
        public string EmployeeState { get; set; }
        public Nullable<decimal> PostionSalary { get; set; }
        public Nullable<decimal> PerformanceNum { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string HomeAddress { get; set; }
        public string JoinPartyDate { get; set; }
        public string JoinDeptDate { get; set; }
        public Nullable<bool> IsJob { get; set; }
        public string DutyContent { get; set; }
        public string SalaryOther { get; set; }
    }
}
