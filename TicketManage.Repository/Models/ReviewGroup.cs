using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class ReviewGroup
    {
        public int SEQ { get; set; }
        public string GroupName { get; set; }
        public string GroupPerson { get; set; }
        public Nullable<int> TravelAgencySEQ { get; set; }
        public string ReviewItem_Room { get; set; }
        public string ReviewItem_Food { get; set; }
        public string ReviewItem_Car { get; set; }
        public string OfferPrice { get; set; }
        public string Summary { get; set; }
        public string Memo { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
