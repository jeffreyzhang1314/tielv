using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GX_SalesManKPI
    {
        public int SEQ { get; set; }
        public string OperatorYear { get; set; }
        public string OperatorMonth { get; set; }
        public string DeptName { get; set; }
        public string UserName { get; set; }
        public string DutyName { get; set; }
        public string PostionSalary { get; set; }
        public string PerformanceNum { get; set; }
        public string GroupPlanNum { get; set; }
        public string PersonPlanNum { get; set; }
        public string ActualRatio { get; set; }
        public string KPINumMonth { get; set; }
        public string KPINumYear { get; set; }
        public string ShouldWages { get; set; }
        public string Memo { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string KPINumMonthTotal { get; set; }
    }
}
