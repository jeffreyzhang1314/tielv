using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class PublicityInfo
    {
        public int SEQ { get; set; }
        public string PublicityName { get; set; }
        public Nullable<int> StockNum { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
