using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GX_GrossProfitInfo
    {
        public int SEQ { get; set; }
        public Nullable<int> UserSEQ { get; set; }
        public string UserName { get; set; }
        public string OperatorYear { get; set; }
        public string OperatorMonth { get; set; }
        public Nullable<int> DutySEQ { get; set; }
        public string DutyName { get; set; }
        public Nullable<decimal> PerformanceNum { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
