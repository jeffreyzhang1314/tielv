using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class SalaryAssessment
    {
        public int SEQ { get; set; }
        public string SalaryAssessmentMonth { get; set; }
        public string UserNo { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string UserPassport { get; set; }
        public string DeptName { get; set; }
        public string UserPostion { get; set; }
        public Nullable<decimal> WorkTime { get; set; }
        public Nullable<decimal> Evaluate { get; set; }
        public Nullable<decimal> Achievement { get; set; }
        public Nullable<int> OperatorSEQ { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
