using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class PublicityStoctInfo
    {
        public int SEQ { get; set; }
        public Nullable<int> CompanySEQ { get; set; }
        public string CompanyName { get; set; }
        public Nullable<int> StockNum { get; set; }
        public Nullable<int> PublicitySEQ { get; set; }
        public string PublicityName { get; set; }
    }
}
