﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Repository.Models
{
    /// <summary>
    /// 通用数据源
    /// </summary>
    public class KeyValueModel
    {
        /// <summary>
        /// Key唯一值
        /// </summary>
        public int Key { get; set; }

        /// <summary>
        /// Value数据值
        /// </summary>
        public string Value { get; set; }
    }
}
