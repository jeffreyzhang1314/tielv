using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class GX_PlanInfoYear
    {
        public int SEQ { get; set; }
        public Nullable<int> CompanySEQ { get; set; }
        public string CompanyName { get; set; }
        public Nullable<decimal> MonthNum1 { get; set; }
        public Nullable<decimal> MonthNum2 { get; set; }
        public Nullable<decimal> MonthNum3 { get; set; }
        public Nullable<decimal> MonthNum4 { get; set; }
        public Nullable<decimal> MonthNum5 { get; set; }
        public Nullable<decimal> MonthNum6 { get; set; }
        public Nullable<decimal> MonthNum7 { get; set; }
        public Nullable<decimal> MonthNum8 { get; set; }
        public Nullable<decimal> MonthNum9 { get; set; }
        public Nullable<decimal> MonthNum10 { get; set; }
        public Nullable<decimal> MonthNum11 { get; set; }
        public Nullable<decimal> MonthNum12 { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string OperatorYear { get; set; }
    }
}
