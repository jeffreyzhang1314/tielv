using System;
using System.Collections.Generic;

namespace TicketManage.Repository.Models
{
    public partial class BudgetSplitInfo
    {
        public int SEQ { get; set; }
        public Nullable<int> CompanySEQ { get; set; }
        public string CompanyName { get; set; }
        public Nullable<int> CategorySEQ { get; set; }
        public string CategoryName { get; set; }
        public Nullable<int> BudgetYear { get; set; }
        public Nullable<decimal> BudgetMoney { get; set; }
        public Nullable<int> OperatorSEQ { get; set; }
        public string OperatorName { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
