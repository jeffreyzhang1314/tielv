using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.TourGuideInfoEvaluate
{
    /// <summary>
    ///   导游评价页面
    /// </summary>
    public class GuideInfoEvaluatePageModel : BasePageModel
    {

        /// <summary>
        /// 评价序号
        /// </summary>
        public Nullable<int> GuideSEQ { get; set; }
        /// <summary>
        /// 评价星级号
        /// </summary>
        public Nullable<int> StarNumNew { get; set; }
        /// <summary>
        /// 查询结果需要实体
        /// </summary>
        public List<V_GuideInfoEvaluateView> ListModel { get; set; }
        /// <summary>
        /// 搜索需要实体
        /// </summary>
        public V_GuideInfoEvaluateView PageModel { get; set; }


        /// <summary>
        /// 性别数据源
        /// </summary>
        public SelectList SexSource { get; set; }
        public List<SelectListItem> ProductNameSource { get; set; }

    }
}
