﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketManage.Entity.InterfaceTicketDataManage
{
    public class TicketTotalModel
    {
        /// <summary>
        /// 月份信息
        /// </summary>
        public string Month { get; set; }

        /// <summary>
        /// 每月总收入
        /// </summary>
        public decimal TicketAmount { get; set; }

        /// <summary>
        /// 毛利润
        /// </summary>
        public Decimal Profit { get; set; }
    }
}
