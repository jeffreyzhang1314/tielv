﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIGrossProfit
{
    /// <summary>
    /// 毛利润实体
    /// </summary>
    public class GX_GrossProfitInfoPageModel : BasePageModel
    {
        /// <summary>
        /// 查询实体
        /// </summary>
        public GX_GrossProfitInfo PageModel { get; set; }

        /// <summary>
        /// 列表信息
        /// </summary>
        public List<GX_GrossProfitInfo> ModelList { get; set; }

        /// <summary>
        /// 年份选择数据源
        /// </summary>
        public SelectList YearSource { get; set; }

        /// <summary>
        /// 月份选择数据源
        /// </summary>
        public SelectList MonthSource { get; set; }

        /// <summary>
        /// 职务数据源
        /// </summary>
        public SelectList DutySource { get; set; }

        /// <summary>
        /// 公司数据源
        /// </summary>
        public SelectList CompanySource { get; set; }


        /// <summary>
        /// 转换上级编号为文字
        /// </summary>
        /// <param name="parentSEQ"></param>
        /// <returns></returns>
        public String ConvertDutySEQ(int? parentSEQ)
        {
            var strValue = string.Empty;
            if (parentSEQ != null && DutySource != null)
            {
                var seq = Convert.ToString(parentSEQ);
                var tempModel = DutySource.Where(a => a.Value == seq).FirstOrDefault();
                if (tempModel != null)
                {
                    strValue = tempModel.Text;
                }
            }
            return strValue;
        }

    }
}
