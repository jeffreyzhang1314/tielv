using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.PublicityInfoManage
{
    public class PublicityReceivePageModel : BasePageModel
    {

        public int PublicitySEQ { get; set; }
        public string LeadDate { get; set; }
        public string PassportNo { get; set; }
        public PublicityReceive PageModel { get; set; }
        public List<PublicityReceive> ModelList { get; set; }
        public PublicityReceiveInfo RecPageModel { get; set; }
        public List<PublicityReceiveInfo> RecModelList { get; set; }
        public BudgetInfo BudgetPageModel { get; set; }
        public List<BudgetInfo> BudgetModelList { get; set; }
        public ApplyBudget ApplyBudgetPageModel { get; set; }
        public List<ApplyBudget> ApplyBudgetModelList { get; set; }
        public List<SelectListItem> PublicityBudgetSource { get; set; }
        public List<SelectListItem> TravelProdcutNameSource { get; set; }

        /// <summary>
        /// 分公司数据源
        /// </summary>
        public SelectList CompanySource { get; set; }

        /// <summary>
        /// 分公司名称
        /// </summary>
        public string ComanyName { get; set; }
    }
}
