using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.PublicityInfoManage
{
    public class ApplicationRecordPageModel : BasePageModel
    {
        public PublicityInfo PIPageModel { get; set; }
        public ApplicationRecord PageModel { get; set; }
        public List<ApplicationRecord> ModelList { get; set; }
        public List<ApplicationPageModel> AppModelList { get; set; }
        public ApplicationPageModel AppPageModel { get; set; }
        public List<SelectListItem> PublicityNameSource { get; set; }

        /// <summary>
        /// 当前操作人所属分公司序号
        /// </summary>
        public int CompanySEQ2 { get; set; }
        /// <summary>
        /// 当前操作人所属分公司
        /// </summary>
        public string CompanyName { get; set; }

    }
}
