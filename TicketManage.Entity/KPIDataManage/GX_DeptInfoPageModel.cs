﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIDataManage
{
    /// <summary>
    /// 公司部门信息实体
    /// </summary>
    public class GX_DeptInfoPageModel : BasePageModel
    {
        /// <summary>
        /// 页面操作实体
        /// </summary>
        public GX_DeptInfo PageModel { get; set; }

        /// <summary>
        /// 查询结果集合
        /// </summary>
        public List<GX_DeptInfo> ModelList { get; set; }

        /// <summary>
        /// 附件内容数据源
        /// </summary>
        public SelectList ParentList { get; set; }

        /// <summary>
        /// 转换上级编号为文字
        /// </summary>
        /// <param name="parentSEQ"></param>
        /// <returns></returns>
        public String ConvertParentSEQ(int? parentSEQ)
        {
            var strValue = string.Empty;
            if (parentSEQ != null && ParentList != null)
            {
                var seq = Convert.ToString(parentSEQ);
                var tempModel = ParentList.Where(a => a.Value == seq).FirstOrDefault();
                if (tempModel != null)
                {
                    strValue = tempModel.Text;
                }
            }
            return strValue;
        }

    }
}
