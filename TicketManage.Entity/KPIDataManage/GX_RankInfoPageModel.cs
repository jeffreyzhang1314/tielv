﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIDataManage
{
    /// <summary>
    /// 级别信息实体
    /// </summary>
    public class GX_RankInfoPageModel : BasePageModel
    {
        /// <summary>
        /// 页面操作实体
        /// </summary>
        public GX_RankInfo PageModel { get; set; }

        /// <summary>
        /// 查询结果集合
        /// </summary>
        public List<GX_RankInfo> ModelList { get; set; }
    }
}
