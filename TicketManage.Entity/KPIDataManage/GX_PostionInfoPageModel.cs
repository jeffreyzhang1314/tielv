﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIDataManage
{
    /// <summary>
    /// 岗位信息实体
    /// </summary>
    public class GX_PostionInfoPageModel : BasePageModel
    {
        /// <summary>
        /// 页面操作实体
        /// </summary>
        public GX_PostionInfo PageModel { get; set; }

        /// <summary>
        /// 查询结果集合
        /// </summary>
        public List<GX_PostionInfo> ModelList { get; set; }
    }
}
