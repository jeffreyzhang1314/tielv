﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIDataManage
{
    /// <summary>
    /// 人员信息实体 
    /// </summary>
    public class GX_EmployeeInfoPageModel : BasePageModel
    {
        /// <summary>
        /// 页面操作实体
        /// </summary>
        public GX_EmployeeInfo PageModel { get; set; }

        /// <summary>
        /// 查询结果集合
        /// </summary>
        public List<GX_EmployeeInfo> ModelList { get; set; }

        /// <summary>
        /// 性别数据源
        /// </summary>
        public SelectList SexSource { get; set; }

        /// <summary>
        /// 岗位数据源
        /// </summary>
        public SelectList PostionSource { get; set; }

        /// <summary>
        /// 部门数据源
        /// </summary>
        public SelectList DeptSource { get; set; }

        /// <summary>
        /// 职务数据源
        /// </summary>
        public SelectList DutySource { get; set; }

        /// <summary>
        /// 员工性质数据源
        /// </summary>
        public SelectList EmployeeStateSource { get; set; }

        /// <summary>
        /// 岗位基础数据源
        /// </summary>
        public List<GX_PostionInfo> BasePostionSource { get; set; }

        public String ConvertSexSEQ(bool? sex)
        {
            var strVal = string.Empty;
            if (sex != null)
            {
                var tempStrVal = sex ?? true;
                strVal = tempStrVal ? "男" : "女";
            }
            return strVal;
        }
        public String ConvertIsJob(bool? flag)
        {
            var strVal = string.Empty;
            if (flag != null)
            {
                var tempStrVal = flag ?? true;
                strVal = tempStrVal ? "在职" : "离职";
            }
            return strVal;
        }

        public String ConvertIsJobText(bool? flag)
        {
            var strVal = "操作";
            if (flag != null)
            {
                var tempStrVal = flag ?? true;
                strVal = tempStrVal ? "离职" : "在职";
            }
            return strVal;
        }

    }
}
