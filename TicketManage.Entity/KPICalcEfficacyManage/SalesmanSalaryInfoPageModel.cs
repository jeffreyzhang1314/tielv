﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPICalcEfficacyManage
{
    /// <summary>
    /// 任务人员页面实体
    /// </summary>
    public class SalesmanSalaryInfoPageModel : BasePageModel
    {
        /// <summary>
        /// 页面操作实体
        /// </summary>
        public GX_SalesmanSalaryInfo PageModel { get; set; }

        /// <summary>
        /// 查询结果集合
        /// </summary>
        public List<GX_SalesmanSalaryInfo> ModelList { get; set; }

    }
}
