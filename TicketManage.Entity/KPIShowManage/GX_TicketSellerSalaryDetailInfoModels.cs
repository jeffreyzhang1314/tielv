﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIShowManage
{
    public class GX_TicketSellerSalaryDetailInfoModels : BasePageModel
    {
        /// <summary>
        /// 页面查询需要实体
        /// </summary>
        public GX_TicketSellerSalaryDetailInfo SearchModel { get; set; }

        /// <summary>
        /// 页面集合需要实体
        /// </summary>
        public List<GX_TicketSellerSalaryDetailInfo> ModelList { get; set; }

        /// <summary>
        /// 当前页面处理的报表信息
        /// </summary>
        public GX_TicketSellerSalaryReport ReportModel { get; set; }

        /// <summary>
        /// 当前操作人名字
        /// </summary>
        public string CurrentOperatorName { get; set; }

        public int ReportDetailSEQ { get; set; }
        public Nullable<int> AuthNum { get; set; }

    }
}
