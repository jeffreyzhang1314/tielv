﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIShowManage
{
    public class GX_GrossProfitCompanyModels : BasePageModel
    {
        /// <summary>
        /// 页面查询需要实体
        /// </summary>
        public GX_GrossProfitCompany SearchModel { get; set; }

        /// <summary>
        /// 页面集合需要实体
        /// </summary>
        public List<GX_GrossProfitCompany> ModelList { get; set; }
    }
}
