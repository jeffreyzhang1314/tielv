﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIShowManage
{
    public class GX_EmployeeSalaryDetailInfoModels : BasePageModel
    {
        /// <summary>
        /// 页面查询需要实体
        /// </summary>
        public GX_EmployeeSalaryDetailInfo SearchModel { get; set; }

        /// <summary>
        /// 页面集合需要实体
        /// </summary>
        public List<GX_EmployeeSalaryDetailInfo> ModelList { get; set; }

        public string CurrentOperatorName { get; set; }

        /// <summary>
        /// 当前操作人所属分公司序号
        /// </summary>
        public Nullable<int> CompanySEQ { get; set; }

        /// <summary>
        /// 当前操作人所属分公司
        /// </summary>
        public string CompanyName { get; set; }

        public Nullable<int> AuthNum { get; set; }

        public int ReportDetailSEQ { get; set; }

        /// <summary>
        /// 分公司数据源
        /// </summary>
        public SelectList CompanySource { get; set; }
    }
}
