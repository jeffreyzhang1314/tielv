﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIShowManage
{
    public class GX_SalesManKPIReportModels : BasePageModel
    {
        /// <summary>
        /// 页面查询需要实体
        /// </summary>
        public GX_SalesManKPIReport SearchModel { get; set; }

        /// <summary>
        /// 页面集合需要实体
        /// </summary>
        public List<GX_SalesManKPIReport> ModelList { get; set; }
    }

    public class GX_EmployeeSalaryDetailInfoReportModels : BasePageModel
    {
        /// <summary>
        /// 页面查询需要实体
        /// </summary>
        public GX_SalesmanSalaryDetailInfoReport SearchModel { get; set; }

        /// <summary>
        /// 页面集合需要实体
        /// </summary>
        public List<GX_SalesmanSalaryDetailInfoReport> ModelList { get; set; }

        /// <summary>
        /// 分公司数据源
        /// </summary>
        public SelectList CompanySource { get; set; }

        /// <summary>
        /// 当前操作人所属分公司序号
        /// </summary>
        public Nullable<int> CompanySEQ { get; set; }

        /// <summary>
        /// 当前操作人所属分公司
        /// </summary>
        public string CompanyName { get; set; }

    }

    /// <summary>
    /// 售票员工资报表
    /// </summary>
    public class GX_TicketSellerSalaryReportModels : BasePageModel
    {
        /// <summary>
        /// 页面查询需要实体
        /// </summary>
        public GX_TicketSellerSalaryReport SearchModel { get; set; }

        /// <summary>
        /// 页面集合需要实体
        /// </summary>
        public List<GX_TicketSellerSalaryReport> ModelList { get; set; }
    }
}
