﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIShowManage
{
    public class GX_SalesManKPIModels : BasePageModel
    {
        /// <summary>
        /// 页面查询需要实体
        /// </summary>
        public GX_SalesManKPI SearchModel { get; set; }

        /// <summary>
        /// 页面集合需要实体
        /// </summary>
        public List<GX_SalesManKPI> ModelList { get; set; }

        /// <summary>
        /// 当前操作人名字
        /// </summary>
        public string CurrentOperatorName { get; set; }

        /// <summary>
        /// 当前操作用户权限
        /// </summary>
        public Nullable<int> AuthNum { get; set; }

        /// <summary>
        /// 待评价报表序号
        /// </summary>
        public int ReportDetailSEQ { get; set; }
    }
}
