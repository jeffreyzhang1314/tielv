﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPISalesManManage
{
    public class GX_SalesmanSalaryDetailInfoPageModel
    {
        public GX_SalesmanSalaryDetailInfo SearchModel { get; set; }

        public List<GX_SalesmanSalaryDetailInfo> ModelList { get; set; }

        /// <summary>
        /// 年份选择数据源
        /// </summary>
        public SelectList YearSource { get; set; }

        /// <summary>
        /// 月份选择数据源
        /// </summary>
        public SelectList MonthSource { get; set; }

        public SelectList DeptSource { get; set; }
    }
}
