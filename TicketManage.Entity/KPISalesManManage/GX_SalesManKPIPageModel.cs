﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPISalesManManage
{
    /// <summary>
    /// 计调人员功效实体类
    /// </summary>
    public class GX_SalesManKPIPageModel : BasePageModel
    {
        /// <summary>
        /// 页面查询实体
        /// </summary>
        public GX_SalesManKPI PageModel { get; set; }

        /// <summary>
        /// 页面列表集合
        /// </summary>
        public List<GX_SalesManKPI> ModelList { get; set; }

        /// <summary>
        /// 年份选择数据源
        /// </summary>
        public SelectList YearSource { get; set; }

        /// <summary>
        /// 月份选择数据源
        /// </summary>
        public SelectList MonthSource { get; set; }

        /// <summary>
        /// 部门数据源
        /// </summary>
        public SelectList DeptSource { get; set; }

        /// <summary>
        /// 完成比率
        /// </summary>
        public Nullable<decimal> BiLv { get; set; }

        /// <summary>
        /// 绩效工资
        /// </summary>
        public Nullable<decimal> JiXiaoGongZi { get; set; }

        /// <summary>
        /// 月度清算工资
        /// </summary>
        public Nullable<decimal> MonthQingSuanGongZi { get; set; }

        /// <summary>
        /// 清算工资
        /// </summary>
        public Nullable<decimal> QingSuanGongZi { get; set; }

        /// <summary>
        /// 应付工资
        /// </summary>
        public Nullable<decimal> YingFuGongZi { get; set; }

    }
}
