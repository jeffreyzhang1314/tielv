﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.AdvertisementManage
{
    public class BudgetManageShowModel
    {
        /// <summary>
        /// 页面需要实体
        /// </summary>
        public ApplyBudget PageModel { get; set; }

        /// <summary>
        /// 消息内容
        /// </summary>
        public string Message { get; set; }

    }
}
