﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.AdvertisementManage
{
    public class BudgetSplitModel : BasePageModel
    {
        public BudgetSplitInfo PageModel { get; set; }

        public List<BudgetSplitInfo> ListModel { get; set; }

        /// <summary>
        /// 分公司数据源
        /// </summary>
        public SelectList CompanySource { get; set; }

        /// <summary>
        /// 预算类别 广告费、业务宣传费
        /// </summary>
        public SelectList CategorySource { get; set; }
    }
}
