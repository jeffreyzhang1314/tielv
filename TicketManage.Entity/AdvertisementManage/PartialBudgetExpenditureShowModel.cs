﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.AdvertisementManage
{
    public class PartialBudgetExpenditureShowModel
    {
        public BudgetExpenditure PageModel { get; set; }

        /// <summary>
        /// 预算索引
        /// </summary>
        public int SEQ { get; set; }
    }
}
