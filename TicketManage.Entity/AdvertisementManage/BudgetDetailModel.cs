﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;
using TicketManage.Repository.Models.ExtModels;

namespace TicketManage.Entity.AdvertisementManage
{
    public class BudgetDetailModel : BasePageModel
    {
        public ApplyBudget PageModel { get; set; }

        public List<ApplyBudgetPageModel> ListModel { get; set; }
    }
}
