﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.AdvertisementManage
{
    public class BudgetAppendModel
    {
        /// <summary>
        /// 页面需要实体
        /// </summary>
        public BudgetAppend PageModel { get; set; }
    }
}
