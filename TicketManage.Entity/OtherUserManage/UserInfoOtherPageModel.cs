﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.OtherUserManage
{
    /// <summary>
    /// 其他用户实体
    /// </summary>
    public class UserInfoOtherPageModel : BasePageModel
    {

        /// <summary>
        /// 分公司数据源
        /// </summary>
        public SelectList CompanySource { get; set; }

        /// <summary>
        /// 列表用户列表集合
        /// </summary>
        public List<UserInfo_Other> ModelList { get; set; }

        /// <summary>
        /// 页面查询实体
        /// </summary>
        public UserInfo_Other PageModel { get; set; }

        /// <summary>
        /// 性别数据源
        /// </summary>
        public SelectList SexSource { get; set; }

        /// <summary>
        /// 用户权限数据源
        /// </summary>
        public SelectList AuthListSource { get; set; }

        /// <summary>
        /// 添加用户是判断当前系统所需的全部权限集合
        /// </summary>
        public Dictionary<string, string> AuthList { get; set; }

        /// <summary>
        /// 系统名称
        /// </summary>
        public SystemName SystemName { get; set; }

        public string OldPassword { get; set; }

        public string ConfirmPassword { get; set; }

        /// <summary>
        /// 系统分类
        /// </summary>
        public SystemName SystemCategory { get; set; }

        /// <summary>
        /// 选择分公司序号获取票务分公司信息
        /// </summary>
        public int CompanySEQ2 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CompanyName { get; set; }

    }
}
