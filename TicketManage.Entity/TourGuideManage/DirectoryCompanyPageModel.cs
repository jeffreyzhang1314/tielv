﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.TourGuideManage
{
    public class DirectoryCompanyPageModel : BasePageModel
    {
        /// <summary>
        /// 页面展示集合
        /// </summary>
        public List<SystemCode> ListModel { get; set; }

        /// <summary>
        /// 页面搜索需要实体
        /// </summary>
        public SystemCode SearchModel { get; set; }

    }
}
