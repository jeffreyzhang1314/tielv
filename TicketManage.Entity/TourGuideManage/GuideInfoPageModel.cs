using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.TourGuideManage
{
    /// <summary>
    ///   导游管理页面需要实体，添加和修改
    /// </summary>
    public class GuideInfoPageModel : BasePageModel
    {
        /// <summary>
        /// 查询结果需要实体
        /// </summary>
        public List<GuideInfo> ListModel { get; set; }

        /// <summary>
        /// 评价具体个数
        /// </summary>
        public List<GuideInfoEvaluate> EvaluateInfo { get; set; }

        /// <summary>
        /// 搜索需要实体
        /// </summary>
        public GuideInfo PageModel { get; set; }

        /// <summary>
        /// 是否黑导游状态
        /// </summary>
        public string IsBlackGuide { get; set; }

        /// <summary>
        /// 性别数据源
        /// </summary>
        public SelectList SexSource { get; set; }
        /// <summary>
        /// 学历列表
        /// </summary>
        public List<SelectListItem> EducationList { get; set; }


        /// <summary>
        /// 选派公司数据源
        /// </summary>
        public SelectList DirectoryCompanySource { get; set; }

        /// <summary>
        /// 根据导游序号查询导游评价次数
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        public int EvaluateCount(int seq, List<GuideInfoEvaluate> source)
        {
            var count = source.Where(a => a.GuideSEQ == seq).Count();
            return count;
        }
    }
}
