using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.TravelLocalAgencyManage
{
    public class TravelAgencyPageModel : BasePageModel
    {

        /// <summary>
        /// 页面查询年份数据源 
        /// </summary>
        public SelectList RecordDateSource { get; set; }

        /// <summary>
        /// 页面查询需要
        /// </summary>
        public String RecordDateSearch { get; set; }

        /// <summary>
        /// 页面查询实体
        /// </summary>
        public TravelAgency PageModel { get; set; }

        /// <summary>
        /// 房间描述数量
        /// </summary>
        public int RoomSummaryNum { get; set; }

        public int RoomSEQ { get; set; }

        /// <summary>
        /// 房间描述
        /// </summary>
        public string RoomSummaryText { get; set; }

        /// <summary>
        /// 房间价格
        /// </summary>
        public string RoomPriceText { get; set; }

        public int FoodSEQ { get; set; }

        /// <summary>
        /// 餐间描述数量
        /// </summary>
        public int FoodSummaryNum { get; set; }

        /// <summary>
        /// 餐描述
        /// </summary>
        public string FoodSummaryText { get; set; }

        /// <summary>
        /// 餐价格
        /// </summary>
        public string FoodPriceText { get; set; }

        public int CarSEQ { get; set; }

        /// <summary>
        /// 车间描述数量
        /// </summary>
        public int CarSummaryNum { get; set; }

        /// <summary>
        /// 车描述
        /// </summary>
        public string CarSummaryText { get; set; }

        /// <summary>
        /// 车价格
        /// </summary>
        public string CarPriceText { get; set; }

        /// <summary>
        /// 地接社配套信息
        /// </summary>
        public TravelAgencyPlugIn PageModelPlugin { get; set; }

        /// <summary>
        /// 配套设施集合
        /// </summary>
        public List<TravelAgencyPlugIn> PluginList { get; set; }

        /// <summary>
        /// 数据结果集合
        /// </summary>
        public List<TravelAgency> ModelList { get; set; }
        /// <summary>
        /// 旅游段
        /// </summary>
        public List<SelectListItem> TravelAreaList { get; set; }
    }
}
