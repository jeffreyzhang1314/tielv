﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.TravelLocalAgencyManage
{
    /// <summary>
    /// 地接社评分记录
    /// </summary>
    public class TravelAgencyPointPageModel : BasePageModel
    {
        public TravelAgencyPoint PageModel { get; set; }

        public List<TravelAgencyPoint> ModelList { get; set; }
    }
}
