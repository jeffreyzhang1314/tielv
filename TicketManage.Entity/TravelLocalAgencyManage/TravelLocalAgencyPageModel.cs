using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.TravelLocalAgencyManage
{
    public class TravelLocalAgencyPageModel : BasePageModel
    {

        /// <summary>
        /// 搜索需要实体
        /// </summary>
        public TravelLocalAgency PageModel { get; set; }

        /// <summary>
        /// 页面需要实体集合
        /// </summary>
        public List<TravelLocalAgency> ModelList { get; set; }

        /// <summary>
        /// 所属省份数据源
        /// </summary>
        public List<SelectListItem> CategoryAreaSource { get; set; }

        /// <summary>
        /// 业务范围数据源
        /// </summary>
        public List<SelectListItem> CategoryBizSource { get; set; }
    }
}
