﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIManage
{
    /// <summary>
    /// 考核利润录入实体
    /// </summary>
    public class PlanprofitInfoYearPageModel : BasePageModel
    {
        /// <summary>
        /// 查询实体
        /// </summary>
        public GX_PlanprofitInfoYear PageModel { get; set; }

        /// <summary>
        /// 列表信息
        /// </summary>
        public List<GX_PlanprofitInfoYear> ModelList { get; set; }
    }
}
