﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIManage
{
    /// <summary>
    /// 个人计划录入实体
    /// </summary>
    public class PlanPersonInfoPageModel : BasePageModel
    {
        /// <summary>
        /// 查询实体
        /// </summary>
        public GX_PlanPersonInfo PageModel { get; set; }

        /// <summary>
        /// 列表信息
        /// </summary>
        public List<GX_PlanPersonInfo> ModelList { get; set; }

        /// <summary>
        /// 年份选择数据源
        /// </summary>
        public SelectList YearSource { get; set; }

        
        /// <summary>
        /// 职位数据集合
        /// </summary>
        public SelectList DutySource { get; set; }
    }
}
