﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIManage
{
    public class PlanInfoYearListPageModel : BasePageModel
    {
        /// <summary>
        /// 查询实体
        /// </summary>
        public GX_PlanInfoYear PageModel { get; set; }

        /// <summary>
        /// 列表信息
        /// </summary>
        public List<GX_PlanInfoYear> ModelList { get; set; }
    }
}
