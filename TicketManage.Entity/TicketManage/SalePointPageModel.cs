using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.TicketManage
{
    public class SalePointPageModel : BasePageModel
    {

        public List<int> CompanySEQList { get; set; }

        public List<V_GroupCompanySaleView> ListModel { get; set; }

        public SalePointInfo PageModel { get; set; }

        /// <summary>
        /// 单位下拉选项
        /// </summary>
        public List<SelectListItem> UnitList { get; set; }

        /// <summary>
        /// 单位数据源
        /// </summary>
        public SelectList CompanySource { get; set; }

        /// <summary>
        /// 性质下拉选项
        /// </summary>
        public SelectList NatureSource { get; set; }

        /// <summary>
        /// 性质下拉选项
        /// </summary>
        public List<SelectListItem> NatureList { get; set; }

        /// <summary>
        /// 联营站点
        /// </summary>
        public List<SelectListItem> LinkSiteList { get; set; }

        /// <summary>
        /// 窗口
        /// </summary>
        public List<SelectListItem> WindowList { get; set; }

        /// <summary>
        /// 集团数据源
        /// </summary>
        public SelectList GroupSource { get; set; }

        /// <summary>
        /// 经营状态
        /// </summary>
        public Nullable<bool> SalePointState { get; set; }
        /// <summary>
        /// 预售开始时间
        /// </summary>
        public string StartTime { get; set; }
        /// <summary>
        /// 暂停时间
        /// </summary>
        public string EndTime { get; set; }
        /// <summary>
        /// 分成
        /// </summary>
        public string DivideInto { get; set; }
        /// <summary>
        /// 联网站段
        /// </summary>
        public string LinkPointBlock { get; set; }
        /// <summary>
        /// 联营方资质
        /// </summary>
        public string LinkPointInfo { get; set; }
        /// <summary>
        /// 营业状态
        /// </summary>
        public List<SelectListItem> SalePointStateList { get; set; }

    }
}
