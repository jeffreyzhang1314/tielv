using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.TicketManage
{
    public class GroupPageModel : BasePageModel
    {
        public List<GroupInfo> ListModel { get; set; }

        public GroupInfo PageModel { get; set; }
    }
}
