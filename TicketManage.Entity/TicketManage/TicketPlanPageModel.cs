using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.TicketManage
{
    public class TicketPlanPageModel : BasePageModel
    {
        public TicketPlan PageModel { get; set; }

        public List<SelectListItem> YearList { get; set; }


        public List<SelectListItem> CompanyList { get; set; }

    }
}
