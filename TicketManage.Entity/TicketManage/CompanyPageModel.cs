using System;using System.Collections.Generic;using System.Linq;using System.Text;using System.Web.Mvc;using TicketManage.Entity.CommonManage;using TicketManage.Repository.Models;namespace TicketManage.Entity.TicketManage{    public class CompanyPageModel : BasePageModel    {        public List<V_GroupCompanyView> ListModel { get; set; }        public CompanyInfo PageModel { get; set; }        public List<SelectListItem> GroupList { get; set; }

        /// <summary>
        /// 集团数据源
        /// </summary>
        public SelectList GroupSource { get; set; }

        /// <summary>
        /// 单位数据源
        /// </summary>
        public SelectList CompanySource { get; set; }

        /// <summary>
        /// 代售点数据源
        /// </summary>
        public SelectList SalePointSource { get; set; }    }}