/*
 * Created by SharpDevelop.
 * User: Admin
 * Date: 2017/5/29
 * Time: 8:02
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using TicketManage.Repository.Models;
using TicketManage.Entity.CommonManage;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace TicketManage.Entity.TicketManage
{
    /// <summary>
    /// 页面需要实体包含数据实体
    /// </summary>
    public class UserPageModel : BasePageModel
    {



        /// <summary>
        /// 判断当前用户是否是管理员
        /// </summary>
        public Boolean IsAdmin
        {
            get
            {
                return (this.PageModel.AuthNum & 1) == 1 ? true : false;
            }
        }

        /// <summary>
        /// 集团数据源
        /// </summary>
        public SelectList GroupSource { get; set; }

        /// <summary>
        /// 单位数据源
        /// </summary>
        public SelectList CompanySource { get; set; }

        /// <summary>
        /// 代售点数据源
        /// </summary>
        public SelectList SalePointSource { get; set; }

        /// <summary>
        /// 性别数据源
        /// </summary>
        public SelectList SexSource { get; set; }
        /// <summary>
        /// 用户权限数据源
        /// </summary>
        public SelectList AuthListSource { get; set; }

        /// <summary>
        /// 旧密码
        /// </summary>
        public String OldPassword { get; set; }

        /// <summary>
        /// 确认密码
        /// </summary>
        public String ConfirmPassword { get; set; }

        /// <summary>
        /// True:显示代销点和单位信息
        /// False:不显示代销点和单位信息
        /// </summary>
        public Boolean UserCategory { get; set; }

        /// <summary>
        /// 用户所属单位信息
        /// </summary>
        public String Company { get; set; }

        /// <summary>
        /// 用户所属销售点信息
        /// </summary>
        public String SalePoint { get; set; }

        /// <summary>
        /// 页面中需要实体，添加、修改、查询时获取页面参数使用
        /// </summary>
        public UserInfo PageModel { get; set; }

        /// <summary>
        /// 实体集合展现列表使用
        /// </summary>
        public List<UserInfoPageModel> ListModel { get; set; }

        /// <summary>
        /// 当前操作人登录名
        /// </summary>
        public string CurrentUserNo { get; set; }

        /// <summary>
        /// 性别码转换
        /// </summary>
        /// <param name="sex"></param>
        /// <returns></returns>
        public String ConvertSex(Boolean? sex)
        {
            var tempValue = string.Empty;
            if (sex != null)
            {
                var tempSex = sex ?? false;
                tempValue = tempSex ? "男" : "女";
            }
            return tempValue;
        }
        /// <summary>
        /// 转换权限值内容
        /// </summary>
        /// <param name="authNum">用户拥有的权限信息</param>
        /// <returns></returns>
        public string ConvertAuthNum(int? authNum)
        {
            authNum = authNum ?? 0;
            var AuthNameStr = new StringBuilder();
            if ((authNum & 1) == 1)
            {
                AuthNameStr = AuthNameStr.AppendFormat(" 管理员 ");
            }
            if ((authNum & 2) == 2)
            {
                AuthNameStr = AuthNameStr.AppendFormat(" 票务系统 ");
            }
            if ((authNum & 4) == 4)
            {
                AuthNameStr = AuthNameStr.AppendFormat(" 地接管理 ");
            }
            if ((authNum & 8) == 8)
            {
                AuthNameStr = AuthNameStr.AppendFormat(" 导游管理 ");
            }
            if ((authNum & 16) == 16)
            {
                AuthNameStr = AuthNameStr.AppendFormat(" 宣传品管理 ");
            }
            return AuthNameStr.ToString();
        }
    }
}
