using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;
using TicketManage.Repository.Models.ExtModels;

namespace TicketManage.Entity.TicketManage
{
    public class TicketIndexPageModel : BasePageModel
    {

        /// <summary>
        /// 票圈1
        /// </summary>
        public int TicketNum1 { get; set; }

        /// <summary>
        /// 票圈2
        /// </summary>
        public int TicketNum2 { get; set; }

        public string OperatorUserName { get; set; }

        public string OperatorUserNo { get; set; }

        /// <summary>
        /// 当前登录用户所属代售点分层
        /// </summary>
        public Decimal DivideInto { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>
        public string UserPassword { get; set; }

        public string OperaotrDate { get; set; }

        /// <summary>
        /// 单位名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 代售点名称
        /// </summary>
        public string SalePointName { get; set; }

        /// <summary>
        /// 代售点序号
        /// </summary>
        public int SalePointSEQ { get; set; }

        /// <summary>
        /// 代售点数据源
        /// </summary>
        public SelectList SalePointSource { get; set; }

        /// <summary>
        /// 单位数据源
        /// </summary>
        public SelectList CompanySource { get; set; }

        /// <summary>
        /// 日期类型数据源
        /// </summary>
        public SelectList TimeCategorySource { get; set; }

        /// <summary>
        /// 日期类型
        /// </summary>
        public int TimeCategory { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>

        public string StartDate { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndDate { get; set; }


        /// <summary>
        /// 页面查询需要实体
        /// </summary>
        public TicketCustomer PageModel { get; set; }

        /// <summary>
        /// 客票集合
        /// </summary>
        public List<TicketCustomerPageModel> ModelList { get; set; }

        /// <summary>
        /// 总张数
        /// </summary>
        public Nullable<int> TicketNumTotal { get; set; }

        /// <summary>
        /// 总收入
        /// </summary>
        public Nullable<decimal> TicketInComeTotal { get; set; }

        /// <summary>
        /// 总客票收入
        /// </summary>
        public Nullable<decimal> TicketMoneyTotal { get; set; }



    }
}
