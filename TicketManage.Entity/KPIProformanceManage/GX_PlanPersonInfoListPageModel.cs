﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIProformanceManage
{
    public class GX_PlanPersonInfoListPageModel:BasePageModel
    {
        /// <summary>
        /// 查询实体
        /// </summary>
        public GX_PlanPersonInfo PageModel { get; set; }
        /// <summary>
        /// 查询实体列表
        /// </summary>
        public List<GX_PlanPersonInfo> ModelList { get; set; }
    }
}
