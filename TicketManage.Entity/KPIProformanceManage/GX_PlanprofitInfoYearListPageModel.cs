﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Entity.CommonManage;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.KPIProformanceManage
{
    public class GX_PlanprofitInfoYearListPageModel : BasePageModel
    {
        /// <summary>
        /// 查询实体
        /// </summary>
        public GX_PlanprofitInfoYear PageModel { get; set; }

        /// <summary>
        /// 列表信息
        /// </summary>
        public List<GX_PlanprofitInfoYear> ModelList { get; set; }
    }
}
