using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicketManage.Repository.Models;

namespace TicketManage.Entity.CommonManage
{
    /// <summary>
    /// 通用登录实体
    /// </summary>
    public class UserLoginPageModel : BasePageModel
    {
        /// <summary>
        /// 系统分类
        /// </summary>
        public SystemName SystemCategory { get; set; }

        public String SystemName { get; set; }

        /// <summary>
        /// 旧密码
        /// </summary>
        public String OldPassword { get; set; }

        /// <summary>
        /// 确认密码
        /// </summary>
        public String ConfirmPassword { get; set; }

        /// <summary>
        /// 页面中需要实体，添加、修改、查询时获取页面参数使用
        /// </summary>
        public UserInfo_Other PageModel { get; set; }
    }
}
