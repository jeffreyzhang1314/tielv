/*
 * Created by SharpDevelop.
 * User: Admin
 * Date: 2017/5/29
 * Time: 7:44
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TicketManage.Entity.CommonManage
{

    /// <summary>
    /// 系统分类说明
    /// </summary>
    public enum SystemName
    {
        票务管理系统 = 2,
        地接管理系统 = 4,
        导游管理系统 = 8,
        宣传品管理系统 = 16,
        工效管理系统 = 32,
    }

    /// <summary>
    /// 识别区分审批节点
    /// </summary>
    public enum ApproveFlowNode
    {
        个人计划一级审批 = 1,
        个人计划二级审批 = 2,
        个人计划三级审批 = 4,
        个人计划四级审批 = 8,
    }

    /// <summary>
    /// Description of BasePageModel.
    /// </summary>
    public class BasePageModel
    {

        /// <summary>
        /// 转换审批状态
        /// </summary>
        /// <param name="num">审批值</param>
        /// <returns>0待审批1通过2驳回</returns>
        public string ConvertKPIApproveVal(int? num)
        {
            var flag = "待审批";
            if (num != null)
            {
                if (num == 1)
                {
                    flag = "通过";
                }
                else if (num == 2)
                {
                    flag = "驳回";
                }
            }
            return flag;
        }

        public string ConvertSelectVal(SelectList list, int? seq)
        {
            var strValue = string.Empty;
            if (seq != null && list != null)
            {
                var tempVal = Convert.ToString(seq);
                var tempModel = list.Where(a => a.Value == tempVal).FirstOrDefault();
                if (tempModel != null)
                {
                    strValue = tempModel.Text;
                }
            }
            return strValue;
        }

        /// <summary>
        /// 获取传入时间的月份内容
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public String ConvertDateTimeToMonth(DateTime? datetime)
        {
            var tempValue = string.Empty;
            if (datetime != null)
            {
                tempValue = datetime.Value.ToString("MM");
            }
            return tempValue;
        }

        /// <summary>
        /// 日期格式转换
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public String ConvertDateTime(DateTime? datetime)
        {
            var tempValue = string.Empty;
            if (datetime != null)
            {
                tempValue = datetime.Value.ToString("yyyy-MM-dd");
            }
            return tempValue;
        }

        /// <summary>
        /// 转换千位符
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public String ConvertDecimal(Decimal? num)
        {
            var tempValue = num ?? 0;
            var tempString = Convert.ToDecimal(tempValue).ToString("N2");
            return num != null ? tempString : string.Empty;
        }

        /// <summary>
        /// 当前登陆用户权限信息
        /// </summary>
        public Nullable<int> OperatorAuthNum { get; set; }

        /// <summary>
        /// 当前操作人所属集团序号
        /// </summary>
        public Nullable<int> GroupSEQ { get; set; }

        /// <summary>
        /// 当前操作人所属单位序号
        /// </summary>
        public Nullable<int> CompanySEQ { get; set; }

        /// <summary>
        /// 系统分类
        /// </summary>
        public SystemName SysteCategory { get; set; }

        /// <summary>
        /// 全部分页大小
        /// </summary>
        public SelectList PageSizeList { get; set; }

        /// <summary>
        /// 消息状态 True成功False失败
        /// </summary>
        public Boolean MessageState { get; set; }
        /// <summary>
        /// 页面中需要提示的信息
        /// </summary>
        public String Message { get; set; }

        /// <summary>
        /// 总页数
        /// </summary>
        public int PageTotal { get; set; }

        /// <summary>
        /// 总数量
        /// </summary>
        public int PageNum { get; set; }

        /// <summary>
        /// 当前页数
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// 一页数量
        /// </summary>
        public int PageSize { get; set; }

        public int ListCount { get; set; }


        /// <summary>
        /// 判断字符是否为Numeric类型
        /// </summary>
        /// <param name="value">判断字符</param>
        /// <returns>True是Numeric类型False不是Numeric类型</returns>
        public bool IsNumeric(string value)
        {
            return Regex.IsMatch(value, @"^[-]?\d+[.]?\d*$");
        }

        /// <summary>
        /// 判断字符是否为Int类型
        /// </summary>
        /// <param name="value">判断字符</param>
        /// <returns>True是Int类型False不是Int类型</returns>
        public bool IsInt(string value)
        {
            return Regex.IsMatch(value, @"^\d+$");
        }

    }
}
